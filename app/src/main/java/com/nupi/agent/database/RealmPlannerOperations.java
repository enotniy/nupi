package com.nupi.agent.database;

import android.app.Application;

import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;
import com.nupi.agent.database.schemas.PlannerModule;
import com.nupi.agent.helpers.KeyStoreHelper;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 20.02.15
 */

public class RealmPlannerOperations extends BaseRealmHelper {

    private static final String PLANNER_ALIAS = "PLANNER";

    public RealmPlannerOperations(Application app) {
        super(app);
    }

    @Override
    public RealmConfiguration getRealmConfig(KeyStoreHelper keyStoreHelper) {
        return new RealmConfiguration.Builder(app)
                .name("planner.realm")
                .encryptionKey(keyStoreHelper.getPrivateKey(PLANNER_ALIAS))
                .schemaVersion(1)
                .setModules(new PlannerModule())
                .build();
    }


    public RealmResults<PlannerRoute> getPlannerRoutes(Realm realm) {
        return realm
                .where(PlannerRoute.class)
                .findAllSorted("modificationDate");
    }

    public PlannerRoute getRouteByName(Realm realm, String name) {
        return realm
                .where(PlannerRoute.class)
                .equalTo("routeName", name)
                .findFirst();
    }

    public PlannerRoute getRouteByGuid(Realm realm, String guid) {
        return realm
                .where(PlannerRoute.class)
                .equalTo("guid", guid)
                .findFirst();
    }

    public RealmResults<ClientInfo> getClientInfoList(Realm realm, String query) {
        return realm
                .where(ClientInfo.class)
                .contains("clientName", query)
                .findAllSorted("clientName");
    }


    public ClientInfo getClientInfoById(Realm realm, String guid) {
        return realm
                .where(ClientInfo.class)
                .equalTo("clientId", guid)
                .findFirst();
    }

    public void addClients(Realm realm, RealmResults<CounterAgent> clients) {
        RealmResults<ClientInfo> clientInfos = getClientInfoList(realm, "");
        realm.beginTransaction();
        try {
            for (CounterAgent client : clients) {
                if (!client.isFolder() && clientInfos.where().equalTo("clientId", client.getGuid()).findAll().size() == 0) {
                    realm.copyToRealm(new ClientInfo(client.getGuid(), client.getName()));
                }
            }
        } finally {
            realm.commitTransaction();
        }

    }
}
