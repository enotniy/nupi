package com.nupi.agent.database.schemas;

import com.nupi.agent.database.models.registry.PriceEntry;
import com.nupi.agent.database.models.registry.RegistryEntry;

import io.realm.annotations.RealmModule;

/**
 * Created by Pasenchuk Victor on 28.10.15
 */

@RealmModule(classes = {RegistryEntry.class, PriceEntry.class})
public class RegistryModule {
}
