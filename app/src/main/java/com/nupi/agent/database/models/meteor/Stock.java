package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 11.01.16
 */

@RealmClass
public class Stock extends RealmObject {

    @PrimaryKey
    private transient String guid;

    @SerializedName("warehouse")
    @Expose
    private String warehouse;
    @SerializedName("nomenclature")
    @Expose
    private String nomenclature;
    @SerializedName("count")
    @Expose
    private float count;
    @SerializedName("revision")
    @Expose
    private long revision;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public String getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(String nomenclature) {
        this.nomenclature = nomenclature;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }
}
