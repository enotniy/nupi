package com.nupi.agent.database.models.registry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 19.02.15
 */
@RealmClass
public class RegistryEntry extends RealmObject {

    @Ignore
    public static final int
            REALISATION = 0,
            PAYMENT = 1,
            ORDER = 2;

    @Ignore
    public static final int
            NOT_SENT = 0,
            SENT = 1,
            RECEIVED = 2,
            REJECTED = 3,
            ACCEPTED = 4,
            SKIPPED = 5;

    @Ignore
    public static final int[] ALL_STATUSES = new int[]{SKIPPED, NOT_SENT, SENT, RECEIVED, ACCEPTED, REJECTED};

    @Ignore
    public static final int[] TODAY_STATUSES = new int[]{NOT_SENT, SENT, RECEIVED, ACCEPTED};

    @Ignore
    public static final int[] ONLY_PROCESSED_STATUSES = new int[]{ACCEPTED, REJECTED};

    @Ignore
    public static final int[] ONLY_PROCESSING_STATUSES = new int[]{SKIPPED, NOT_SENT, SENT, RECEIVED};

    @Ignore
    public static final int[] IN_THE_POCKET_STATUSES = new int[]{NOT_SENT, SENT, RECEIVED};

    @Ignore
    public static final int[] ONLY_NULLABLE_STATUSES = new int[]{SENT, RECEIVED};

    @Ignore
    public static final int[] ONLY_ACCEPTED_STATUSES = new int[]{ACCEPTED};


    @SerializedName("guid")
    @Expose
    @PrimaryKey
    private String guid;

    @SerializedName("date")
    @Expose
    private Date date;

    @SerializedName("comment")
    @Expose
    private String comment = "";

    @SerializedName("counterAgent")
    @Expose
    private String counterAgent;

    @SerializedName("contract")
    @Expose
    private String contract = "";

    @SerializedName("organization")
    @Expose
    private String organization;

    @Ignore
    @SerializedName("orderItems")
    @Expose
    private List<PriceEntry> orderItems = new ArrayList<>();

    @SerializedName("revision")
    @Expose
    private long revision;

    @SerializedName("sum")
    @Expose
    private double sum;

    @SerializedName("docType")
    @Expose
    private String docType = "";

    @SerializedName("docInfo")
    @Expose
    private String docInfo = "";

    @SerializedName("deal")
    @Expose
    private String deal = "";


    @SerializedName("conduct")
    @Expose
    private boolean conduct;


    @SerializedName("paid")
    @Expose
    private boolean paid;


    @SerializedName("deleted")
    @Expose
    private boolean deleted;


    @SerializedName("markRemoval")
    @Expose
    private boolean markRemoval;


    private transient int entryStatus = NOT_SENT;

    private transient int entryType;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getEntryType() {
        return entryType;
    }

    public void setEntryType(int entryType) {
        this.entryType = entryType;
    }

    public String getCounterAgent() {
        return counterAgent;
    }

    public void setCounterAgent(String counterAgent) {
        this.counterAgent = counterAgent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public int getEntryStatus() {
        return entryStatus;
    }

    public void setEntryStatus(int entryStatus) {
        this.entryStatus = entryStatus;
    }

    public String getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(String docInfo) {
        this.docInfo = docInfo;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    //ignored fields

    public List<PriceEntry> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<PriceEntry> orderItems) {
        this.orderItems = orderItems;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDeal() {
        return deal;
    }

    public void setDeal(String deal) {
        this.deal = deal;
    }

    public boolean isConduct() {
        return conduct;
    }

    public void setConduct(boolean conduct) {
        this.conduct = conduct;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isMarkRemoval() {
        return markRemoval;
    }

    public void setMarkRemoval(boolean markRemoval) {
        this.markRemoval = markRemoval;
    }
}
