package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 10.01.16
 */
@RealmClass
public class ExpandedPayment extends RealmObject {

    public static final String
            TYPE_RECEIPT = "Receipt",
            TYPE_EXPENSE = "Expense";

    @PrimaryKey
    private transient String guid;

    @SerializedName("contract")
    @Expose
    private String contract;
    @SerializedName("counterAgent")
    @Expose
    private String counterAgent;
    @SerializedName("registrar")
    @Expose
    private String registrar;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("sum")
    @Expose
    private float sum;
    @SerializedName("period")
    @Expose
    private Date period;
    @SerializedName("registrarInfo")
    @Expose
    private String registrarInfo;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("revision")
    @Expose
    private long revision;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getCounterAgent() {
        return counterAgent;
    }

    public void setCounterAgent(String counterAgent) {
        this.counterAgent = counterAgent;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public Date getPeriod() {
        return period;
    }

    public void setPeriod(Date period) {
        this.period = period;
    }

    public String getRegistrarInfo() {
        return registrarInfo;
    }

    public void setRegistrarInfo(String registrarInfo) {
        this.registrarInfo = registrarInfo;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
