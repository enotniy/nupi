package com.nupi.agent.database.models.chat;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by User on 20.10.2015
 */
@RealmClass
public class ChatMessage extends RealmObject {

    public static final String
            TYPE_TEXT = "TEXT",
            TYPE_IMAGE = "IMAGE",
            TYPE_GEOLOCATION = "GEOLOCATION";


    @PrimaryKey
    @SerializedName("id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("topic")
    private String topic;
    @SerializedName("message")
    private String message;
    @SerializedName("date")
    private Date date;
    @SerializedName("latitude")
    private Double latitude;
    @SerializedName("longitude")
    private Double longitude;
    @SerializedName("altitude")
    private Double altitude;
    @SerializedName("attachment")
    private String attachment;
    @SerializedName("wasRead")
    private boolean wasRead;
    @SerializedName("sent")
    private boolean sent;
    @SerializedName("revision")
    private int revision;
    private transient boolean wasSynchronized;
    private transient ChatUser userFrom;
    private transient ChatGroup chatGroup;
    private boolean wasLocallyRead = false;


    // Non model fields.
    @Ignore
    @SerializedName("sender")
    private String sender;
    @Ignore
    @SerializedName("group")
    private String group;
    @Ignore
    @SerializedName("recipient")
    private String recipient;

    public ChatGroup getChatGroup() {
        return chatGroup;
    }

    public void setChatGroup(ChatGroup chatGroup) {
        this.chatGroup = chatGroup;
    }

    public ChatUser getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(ChatUser userFrom) {
        this.userFrom = userFrom;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isWasRead() {
        return wasRead;
    }

    public void setWasRead(boolean wasRead) {
        this.wasRead = wasRead;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isWasSynchronized() {
        return wasSynchronized;
    }

    public void setWasSynchronized(boolean wasSynchronized) {
        this.wasSynchronized = wasSynchronized;
    }

    public boolean isWasLocallyRead() {
        return wasLocallyRead;
    }

    public void setWasLocallyRead(boolean wasLocallyRead) {
        this.wasLocallyRead = wasLocallyRead;
    }

    // Non model fields.
    public String getSender() {
        return sender;
    }

    public String getGroup() {
        return group;
    }

    public String getRecipient() {
        return recipient;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }
}
