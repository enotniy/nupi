package com.nupi.agent.database;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.nupi.agent.application.models.ContractGroup;
import com.nupi.agent.application.models.ContractsGroup;
import com.nupi.agent.application.models.CounterAgentGroup;
import com.nupi.agent.application.models.NomenclatureSaleGroup;
import com.nupi.agent.application.models.Tree;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.ExpandedPayment;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.database.models.meteor.NomenclaturePrice;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.database.models.meteor.Sale;
import com.nupi.agent.database.models.meteor.Stock;
import com.nupi.agent.database.models.meteor.Unit;
import com.nupi.agent.database.schemas.MeteorModule;
import com.nupi.agent.enums.SortType;
import com.nupi.agent.enums.TreeElementType;
import com.nupi.agent.helpers.KeyStoreHelper;
import com.nupi.agent.utils.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Pasenchuk Victor on 20.02.15
 */

public class RealmExchangeOperations extends BaseExchangeRealmHelper {

    private static final String EXCHANGE_ALIAS = "EXCHANGE";


    public RealmExchangeOperations(Application app) {
        super(app);
    }

    @Override
    public RealmConfiguration getRealmConfig(KeyStoreHelper keyStoreHelper) {
        return new RealmConfiguration.Builder(app)
                .name("exchange.realm")
                .encryptionKey(keyStoreHelper.getPrivateKey(EXCHANGE_ALIAS))
                .schemaVersion(1)
                .setModules(new MeteorModule())
                .build();
    }

    public RealmResults<CounterAgent> getCounterAgents(Realm realm) {
        return getRealmObjects(realm, CounterAgent.class)
                .where()
                .findAllSorted("name");
    }

    public RealmResults<CounterAgent> getNonFolderCounterAgents(Realm realm) {
        return realm
                .where(CounterAgent.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("isFolder", false)
                .findAllSorted("name");
    }

    public RealmResults<CounterAgent> getNonFolderCounterAgentsByOrder(Realm realm, Sort sort, String field) {
        return realm
                .where(CounterAgent.class)
                .equalTo("markRemoval", false)
                .equalTo("isFolder", false)
                .findAllSorted(field, sort);
    }

    public RealmResults<CounterAgent> getNonFolderCounterAgents(Realm realm, String query) {
        return realm
                .where(CounterAgent.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("isFolder", false)
                .contains("lowerCaseName", query)
                .findAllSorted("name");
    }

    public RealmResults<CounterAgent> getNonFolderCounterAgentsWithOrders(Realm realm, String query, SortType sortType, Sort sort) {
        RealmQuery<CounterAgent> counterAgentRealmQuery = realm
                .where(CounterAgent.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("isFolder", false);

        if (!TextUtils.isEmpty(query))
            counterAgentRealmQuery = counterAgentRealmQuery.contains("lowerCaseName", query);

        switch (sortType) {
            case DEBT_ORDER:
                return counterAgentRealmQuery
                        .findAllSorted(new String[]{"hasOrders", "debt", "name"}, new Sort[]{Sort.DESCENDING, sort, sort});
            default:
                return counterAgentRealmQuery
                        .findAllSorted(new String[]{"hasOrders", "name"}, new Sort[]{Sort.DESCENDING, sort});
        }
    }

    public RealmResults<CounterAgent> getNonFolderCounterAgents(Realm realm, String query, SortType sortType, Sort sort) {
        RealmQuery<CounterAgent> counterAgentRealmQuery = realm
                .where(CounterAgent.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("deleted", false)
                .equalTo("isFolder", false);

        if (!TextUtils.isEmpty(query))
            counterAgentRealmQuery = counterAgentRealmQuery.contains("lowerCaseName", query);

        switch (sortType) {
            case DEBT_ORDER:
                return counterAgentRealmQuery
                        .findAllSorted(new String[]{"debt", "name"}, new Sort[]{sort, sort});
            default:
                return counterAgentRealmQuery
                        .findAllSorted("name", sort);
        }
    }

    public Nomenclature getNomenclatureById(Realm realm, String guid) {
        return getRealmObjectByGuid(realm, guid, Nomenclature.class);
    }

    public RealmResults<Nomenclature> getNonFolderNomenclature(Realm realm) {
        return realm
                .where(Nomenclature.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("isFolder", false)
                .findAll();
    }

    public RealmResults<Nomenclature> getNomenclature(Realm realm) {
        return realm
                .where(Nomenclature.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .findAll();
    }

    public RealmResults<Nomenclature> getNomenclatureFiltered(Realm realm, String query, TreeElementType treeElementType) {
        return realm
                .where(Nomenclature.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("isFolder", treeElementType.getValue())
                .contains("lowerCaseName", query)
                .findAll();
    }

    public RealmResults<Nomenclature> getNomenclatureChildren(Realm realm, String parentGuid) {
        return realm
                .where(Nomenclature.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("folderId", parentGuid)
                .findAllSorted("name");
    }


    public RealmResults<CounterAgent> getCounterAgentChildren(Realm realm, String parentGuid) {
        return realm
                .where(CounterAgent.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("folderId", parentGuid)
                .findAllSorted("name");
    }

    public RealmResults<Nomenclature> getNomenclatureNonFolderChildren(Realm realm, String parentGuid) {
        return realm
                .where(Nomenclature.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("isFolder", false)
                .equalTo("folderId", parentGuid)
                .findAllSorted("name");
    }

    public RealmResults<Nomenclature> getNomenclatureNonFolderChildren(Realm realm, String parentGuid, String searchString) {
        return realm
                .where(Nomenclature.class)
                .equalTo("isFolder", false)
                .equalTo("folderId", parentGuid)
                .contains("lowerCaseName", searchString)
                .findAllSorted("name");
    }

    public Double getPrice(Realm realm, String unitId) {
        NomenclaturePrice nomenclaturePrice = getAndCompressNomenclaturePriceForUnit(realm, unitId);
        return nomenclaturePrice != null ? nomenclaturePrice.getPrice() : null;
    }

    public NomenclaturePrice getAndCompressNomenclaturePriceForUnit(Realm realm, String unitId) {
        RealmResults<NomenclaturePrice> allSorted = getPastNomenclaturePrices(realm, unitId);
        if (allSorted.size() > 0) {
            NomenclaturePrice nomenclaturePrice = allSorted.get(0);
            if (allSorted.size() > 1) {
                RealmResults<NomenclaturePrice> period = allSorted
                        .where()
                        .lessThan("period", nomenclaturePrice.getPeriod())
                        .findAll();
                if (period.size() > 0) {
                    realm.beginTransaction();
                    period.clear();
                    realm.commitTransaction();
                }
            }
            return nomenclaturePrice;
        }
        return null;
    }

    private RealmResults<NomenclaturePrice> getPastNomenclaturePrices(Realm realm, String unitId) {
        return realm.
                where(NomenclaturePrice.class)
                .equalTo("unit", unitId)
                .lessThanOrEqualTo("period", new Date())
                .findAllSorted("period", Sort.DESCENDING);
    }


    public RealmResults<Unit> getUnitsForGood(Realm realm, String goodId) {
        return realm
                .where(Unit.class)
                .equalTo("nomenclature", goodId)
                .findAllSorted("coefficient");
    }

    public RealmResults<PaymentByOrder> getZeroPaymentsByOrders(Realm realm) {
        return realm
                .where(PaymentByOrder.class)
                .equalTo("deleted", false)
                .equalTo("sum", 0d)
                .findAll();
    }

    public RealmResults<PaymentByOrder> getPaymentsByOrdersByContract(Realm realm, Contract contract) {
        return realm
                .where(PaymentByOrder.class)
                .equalTo("contract", contract.getGuid())
                .beginGroup()
                .greaterThan("sum", StringUtils.EPSILON)
                .or()
                .lessThan("sum", -StringUtils.EPSILON)
                .endGroup()
                .equalTo("deleted", false)
                .findAllSorted("date");
    }

    public RealmResults<Payment> getPaymentsByContract(Realm realm, Contract contract) {
        return realm
                .where(Payment.class)
                .equalTo("contract", contract.getGuid())
                .findAll();
    }


    public RealmResults<ExpandedPayment> getExpandedPaymentByCounterAgent(Realm realm, String counterAgentId, Date from, Date to) {
        return realm
                .where(ExpandedPayment.class)
                .greaterThanOrEqualTo("period", from)
                .equalTo("deleted", false)
                .lessThan("period", to)
                .equalTo("counterAgent", counterAgentId)
                .findAllSorted("period");
    }

    public double getExpandedPaymentByPeriod(Realm realm, Date from, Date to, String type) {
        final Number value = realm
                .where(ExpandedPayment.class)
                .greaterThanOrEqualTo("period", from)
                .lessThan("period", to)
                .equalTo("deleted", false)
                .equalTo("type", type)
                .sum("sum");
        return value != null ? value.doubleValue() : 0;
    }

    public RealmResults<ExpandedPayment> getExpandedPayment(Realm realm) {
        return realm
                .where(ExpandedPayment.class)
                .equalTo("deleted", false)
                .findAllSorted("period");
    }

    public Payment getPaymentByContract(Realm realm, Contract contract) {
        return realm
                .where(Payment.class)
                .equalTo("deleted", false)
                .equalTo("contract", contract.getGuid())
                .findFirst();
    }

    public RealmResults<Payment> getZeroPayments(Realm realm) {
        return realm
                .where(Payment.class)
                .equalTo("deleted", false)
                .between("sum", -StringUtils.EPSILON, StringUtils.EPSILON)
                .findAll();
    }

    public RealmResults<PaymentByOrder> getDebtsForCounterAgent(Realm realm, String counterAgentGuid) {
        return realm
                .where(PaymentByOrder.class)
                .equalTo("deleted", false)
                .equalTo("counterAgent", counterAgentGuid)
                .findAll();
    }

    public RealmResults<Contract> getContractsForCounterAgent(Realm realm, String counterAgentGuid) {
        return realm
                .where(Contract.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .equalTo("owner", counterAgentGuid)
                .findAll();
    }

    public RealmResults<Sale> getUniqueSales(Realm realm) {
        return realm
                .distinct(Sale.class, "nomenclature");
    }

    public List<NomenclatureSaleGroup> getNomenclatureSaleGroup(Realm realm, List<Nomenclature> nomenclatures, List<CounterAgent> counterAgents, Date from, Date to) {
        final HashSet<String> guidsSet = new HashSet<>();
        for (Sale sale : getUniqueSales(realm))
            guidsSet.add(sale.getNomenclature());

        Log.d("ANAL", "Search only used  nomenclatures");

        final RealmQuery<Sale> saleRealmQuery = realm
                .where(Sale.class)
                .greaterThanOrEqualTo("period", from)
                .lessThan("period", to)
                .beginGroup();

        for (int i = 0; i < counterAgents.size(); i++) {
            saleRealmQuery
                    .equalTo("counterAgent", counterAgents.get(i).getGuid());
            if (i < counterAgents.size() - 1)
                saleRealmQuery.or();
        }
        saleRealmQuery
                .endGroup()
                .beginGroup();

        for (int i = 0; i < nomenclatures.size(); i++) {
            final String guid = nomenclatures.get(i).getGuid();
            if (guidsSet.contains(guid)) {
                if (i > 0)
                    saleRealmQuery.or();
                saleRealmQuery
                        .equalTo("nomenclature", guid);
            }
        }
        RealmResults<Sale> saleRealmResults = saleRealmQuery
                .endGroup()
                .findAll();


        Log.d("ANAL", "Finish build query");

        Number sum;
        final List<NomenclatureSaleGroup> nomenclatureSaleGroups = new ArrayList<>(nomenclatures.size());
        for (Nomenclature nomenclature : nomenclatures) {
            final String guid = nomenclature.getGuid();
            if (guidsSet.contains(guid)) {
                final RealmQuery<Sale> salesByGuid = saleRealmResults
                        .where()
                        .equalTo("nomenclature", guid);

                sum = salesByGuid
                        .sum("sum");

                final double resultSum = sum != null ? sum.doubleValue() : 0;

                sum = salesByGuid
                        .sum("count");

                final double resultStock = sum != null ? sum.doubleValue() : 0;

                nomenclatureSaleGroups.add(new NomenclatureSaleGroup(nomenclature, resultStock, resultSum));
            } else
                nomenclatureSaleGroups.add(new NomenclatureSaleGroup(nomenclature, 0, 0));
        }
        return nomenclatureSaleGroups;
    }

    public double getDebtByCounterAgentSumFromDateToNow(Realm realm, Date from, CounterAgent counterAgent) {
        final Number expenseNumber = realm
                .where(ExpandedPayment.class)
                .equalTo("counterAgent", counterAgent.getGuid())
                .equalTo("type", ExpandedPayment.TYPE_EXPENSE)
                .equalTo("deleted", false)
                .greaterThanOrEqualTo("period", from)
                .sum("sum");

        final Number receiptNumber = realm
                .where(ExpandedPayment.class)
                .equalTo("counterAgent", counterAgent.getGuid())
                .equalTo("type", ExpandedPayment.TYPE_RECEIPT)
                .equalTo("deleted", false)
                .greaterThanOrEqualTo("period", from)
                .sum("sum");

        final double expense = expenseNumber != null ? expenseNumber.doubleValue() : 0;
        final double receipt = receiptNumber != null ? receiptNumber.doubleValue() : 0;
        return receipt - expense;
    }

    private double getPaymentSum(RealmQuery<ExpandedPayment> expandedPaymentRealmQuery, String type) {
        final Number value = expandedPaymentRealmQuery
                .equalTo("type", type)
                .equalTo("deleted", false)
                .sum("sum");
        return value != null ? value.doubleValue() : 0;
    }

    public double getDebtSumForContract(Realm realm, String counterAgentGuid) {
        final Number sum = realm
                .where(Payment.class)
                .equalTo("contract", counterAgentGuid)
                .sum("sum");
        if (sum != null)
            return sum.doubleValue();
        else return 0;
    }

    public double getDebtSumForCounterAgent(Realm realm, String counterAgentGuid) {
        final Number sum = realm
                .where(Payment.class)
                .equalTo("counterAgent", counterAgentGuid)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .sum("sum");
        if (sum != null)
            return sum.doubleValue();
        else return 0;
    }

    public double getDebtSumForAllCounterAgent(Realm realm) {
        Number debt = getNonFolderCounterAgents(realm).sum("debt");
        if (debt != null)
            return debt.doubleValue();
        return 0;
    }

    public double getStockSum(Realm realm, String guid) {
        final Number sum = realm
                .where(Stock.class)
                .equalTo("nomenclature", guid)
                .sum("count");
        if (sum != null)
            return sum.doubleValue();
        else return 0;
    }

    public Tree<String> getTreeIdNomenclature(Realm realm) {
        Tree<String> tree = new Tree<>(Nomenclature.ROOT_GUID);
        addLevelInTreeNomenclature(realm, tree);
        return tree;
    }


    public void addLevelInTreeNomenclature(Realm realm, Tree<String> tree) {
        RealmResults<Nomenclature> nomenclatures = getNomenclatureChildren(realm, tree.getHead());
        for (Nomenclature nomenclature : nomenclatures) {
            tree.addLeaf(nomenclature.getGuid());
            if (nomenclature.isFolder())
                addLevelInTreeNomenclature(realm, tree.getTree(nomenclature.getGuid()));
        }
    }

    public Tree<String> getTreeIdCounterAgent(Realm realm) {
        Tree<String> tree = new Tree<>(CounterAgent.ROOT_GUID);
        addLevelInTreeCounterAgent(realm, tree);
        return tree;
    }

    public void addLevelInTreeCounterAgent(Realm realm, Tree<String> tree) {
        RealmResults<CounterAgent> counterAgents = getCounterAgents(realm);
        for (CounterAgent agent : counterAgents) {
            tree.addLeaf(agent.getGuid());
        }
    }

    public ContractsGroup getContractsGroup(Realm realm, String counterAgentGuid) {
        RealmResults<Contract> contracts = getContractsForCounterAgent(realm, counterAgentGuid);
        LinkedList<ContractGroup> contractGroups = new LinkedList<>();
        for (Contract contract : contracts) {
            Payment payment = getPaymentByContract(realm, contract);
            contractGroups.add(new ContractGroup(contract, payment, getPaymentsByOrdersByContract(realm, contract)));
        }
        return new ContractsGroup(contractGroups);
    }

    public ArrayList<CounterAgentGroup> getCounterAgentGroupsByCounterAgent(Realm realm, String counterAgentGuid) {
        ArrayList<CounterAgentGroup> counterAgentGroups = new ArrayList<>();
        RealmResults<Contract> contracts = getContractsForCounterAgent(realm, counterAgentGuid);
        for (Contract contract : contracts) {
            CounterAgentGroup counterAgentGroup = new CounterAgentGroup(
                    getPaymentByContract(realm, contract),
                    getPaymentsByOrdersByContract(realm, contract),
                    contract);
            counterAgentGroups.add(counterAgentGroup);
        }
        return counterAgentGroups;
    }
}
