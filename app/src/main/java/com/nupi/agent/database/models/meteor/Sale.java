package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Sale extends RealmObject {

    @PrimaryKey
    private transient String guid;

    @SerializedName("counterAgent")
    @Expose
    @Index
    private String counterAgent;
    @SerializedName("registrar")
    @Expose
    private String registrar;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("deal")
    @Expose
    private String deal;
    @SerializedName("nomenclature")
    @Expose
    @Index
    private String nomenclature;
    @SerializedName("nomenclatureCharacteristic")
    @Expose
    private String nomenclatureCharacteristic;
    @SerializedName("realization")
    @Expose
    private String realization;
    @SerializedName("sum")
    @Expose
    private float sum;
    @SerializedName("vat")
    @Expose
    private float vat;
    @SerializedName("noVatSum")
    @Expose
    private float noVatSum;
    @SerializedName("count")
    @Expose
    private float count;
    @SerializedName("period")
    @Expose
    private Date period;
    @SerializedName("registrarInfo")
    @Expose
    private String registrarInfo;
    @SerializedName("revision")
    @Expose
    private long revision;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCounterAgent() {
        return counterAgent;
    }

    public void setCounterAgent(String counterAgent) {
        this.counterAgent = counterAgent;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public Date getPeriod() {
        return period;
    }

    public void setPeriod(Date period) {
        this.period = period;
    }

    public String getRegistrarInfo() {
        return registrarInfo;
    }

    public void setRegistrarInfo(String registrarInfo) {
        this.registrarInfo = registrarInfo;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public float getVat() {
        return vat;
    }

    public void setVat(float vat) {
        this.vat = vat;
    }

    public String getDeal() {
        return deal;
    }

    public void setDeal(String deal) {
        this.deal = deal;
    }

    public String getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(String nomenclature) {
        this.nomenclature = nomenclature;
    }

    public String getNomenclatureCharacteristic() {
        return nomenclatureCharacteristic;
    }

    public void setNomenclatureCharacteristic(String nomenclatureCharacteristic) {
        this.nomenclatureCharacteristic = nomenclatureCharacteristic;
    }

    public String getRealization() {
        return realization;
    }

    public void setRealization(String realization) {
        this.realization = realization;
    }

    public float getNoVatSum() {
        return noVatSum;
    }

    public void setNoVatSum(float noVatSum) {
        this.noVatSum = noVatSum;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }
}
