package com.nupi.agent.database;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.BuildConfig;
import com.nupi.agent.helpers.KeyStoreHelper;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.exceptions.RealmIOException;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by Pasenchuk Victor on 26.11.15
 */
public abstract class BaseRealmHelper {

    protected final Application app;

    public BaseRealmHelper(Application app) {
        this.app = app;
    }


    // TODO: 24.11.15 ADD MIGRATIONS
    public Realm getRealm() {
        final RealmConfiguration realmConfig = getRealmConfig(new KeyStoreHelper(app));
        try {
            return Realm.getInstance(realmConfig);
        } catch (RealmMigrationNeededException e) {
            e.printStackTrace();
            deleteDatabase();
            return Realm.getInstance(realmConfig);
        } catch (IllegalArgumentException e) {
            Crashlytics.logException(e);
            return recreateRealm(realmConfig, e);
        } catch (RealmIOException e) {
            // TODO: 06.12.15 Investigate what is wrong.
            try {
                Thread.sleep(100);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            return Realm.getInstance(realmConfig);
        }
    }

    private Realm recreateRealm(RealmConfiguration realmConfig, IllegalArgumentException e) {
        e.printStackTrace();
        deleteDatabase();
        return Realm.getInstance(realmConfig);
    }

    public void deleteDatabase() {
        try {
            Realm.deleteRealm(getRealmConfig(new KeyStoreHelper(app)));
        } catch (Exception e) {
            logException(e);
        }
    }

    private void logException(Exception e) {
        Crashlytics.logException(e);
        if (BuildConfig.DEBUG)
            e.printStackTrace();
    }


    public abstract RealmConfiguration getRealmConfig(KeyStoreHelper keyStoreHelper);

    public void updateBulkList(List<? extends RealmObject> realmObjects) {
        if (realmObjects != null && realmObjects.size() > 0) {
            Realm realm = getRealm();

            updateBulkList(realmObjects, realm);

            realm.close();
        }
    }

    public void updateBulkList(List<? extends RealmObject> realmObjects, Realm realm) {
        if (realmObjects != null && realmObjects.size() > 0) {
            realm.beginTransaction();

            try {
                realm.copyToRealmOrUpdate(realmObjects);
            } finally {
                realm.commitTransaction();
            }
        }
    }

    public void addBulkList(List<? extends RealmObject> realmObjects, Realm realm) {
        if (realmObjects != null && realmObjects.size() > 0) {
            realm.beginTransaction();

            try {
                realm.copyToRealm(realmObjects);
            } finally {
                realm.commitTransaction();
            }
        }
    }
}
