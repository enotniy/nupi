package com.nupi.agent.database;

import android.app.Application;
import android.support.annotation.NonNull;

import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatRoute;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.database.schemas.ChatModule;
import com.nupi.agent.helpers.KeyStoreHelper;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Pasenchuk Victor on 20.02.15
 */

public class RealmChatOperations extends BaseRealmHelper {

    private static final String CHAT_ALIAS = "CHAT";

    public RealmChatOperations(Application app) {
        super(app);
    }

    @Override
    public RealmConfiguration getRealmConfig(KeyStoreHelper keyStoreHelper) {
        return new RealmConfiguration.Builder(app)
                .name("chat.realm")
                .encryptionKey(keyStoreHelper.getPrivateKey(CHAT_ALIAS))
                .schemaVersion(1)
                .setModules(new ChatModule())
                .build();
    }

    public List<ChatRoute> copyToRealmOrUpdateChatRoutes(Realm realm, List<ChatRoute> items) {
        realm.beginTransaction();
        List<ChatRoute> realmItems;
        try {
            realmItems = realm.copyToRealmOrUpdate(items);
        } finally {
            realm.commitTransaction();
        }
        return realmItems;
    }

    public ChatMessage getMessageByGuid(Realm realm, String guid) {
        return realm
                .where(ChatMessage.class)
                .equalTo("id", guid)
                .findFirst();
    }


    public boolean getWasReadMessage(Realm realm, String guid) {
        final ChatMessage chatMessage = realm
                .where(ChatMessage.class)
                .equalTo("id", guid)
                .beginGroup()
                .equalTo("wasRead", true)
                .or()
                .equalTo("wasLocallyRead", true)
                .endGroup()
                .findFirst();
        return chatMessage != null;
    }

    public void setMessageReadStatus(Realm realm, String chatMessageGiud, boolean status) {
        ChatMessage chatMessage = getMessageByGuid(realm, chatMessageGiud);
        if (chatMessage != null && chatMessage.isWasLocallyRead() != status) {
            realm.beginTransaction();
            chatMessage.setWasLocallyRead(status);
            realm.commitTransaction();
        }
    }


    public void copyToRealmOrUpdateChatUsers(Realm realm, List<ChatUser> items) {
        for (ChatUser item : items) {
            String id = item.getRoute();
            if (id != null) {
                ChatRoute chatRoute = getChatRouteById(realm, id);
                if (chatRoute != null)
                    item.setChatRoute(chatRoute);
                else {
                    chatRoute = new ChatRoute();
                    chatRoute.setId(id);
                    item.setChatRoute(chatRoute);
                }
            }
        }

        realm.beginTransaction();
        try {
            realm.copyToRealmOrUpdate(items);
        } finally {
            realm.commitTransaction();
        }
    }

    public void copyToRealmOrUpdateChatGroups(Realm realm, List<ChatGroup> items) {
        for (ChatGroup item : items) {
            postProcessChatGroup(realm, item);
        }

        realm.beginTransaction();
        try {
            realm.copyToRealmOrUpdate(items);
        } finally {
            realm.commitTransaction();
        }
    }

    public void postProcessChatGroup(Realm realm, ChatGroup item) {
        List<String> ids = item.getUsers();
        RealmList<ChatUser> groupUsers = new RealmList<>();
        for (String id : ids) {
            if (id != null) {
                ChatUser chatUser = getChatUserById(realm, id);
                if (chatUser != null)
                    groupUsers.add(chatUser);
                else {
                    chatUser = new ChatUser();
                    chatUser.setUser(id);
                    groupUsers.add(chatUser);
                }
            }
        }
        item.setGroupUsers(groupUsers);
    }

    public void copyToRealmOrUpdateChatMessages(Realm realm, List<ChatMessage> items) {
        for (ChatMessage item : items) {
            setupChatMessage(realm, item);
        }

        realm.beginTransaction();
        try {
            realm.copyToRealmOrUpdate(items);
        } finally {
            realm.commitTransaction();
        }
    }

    public void copyToRealmOrUpdateChatMessage(Realm realm, ChatMessage item) {
        setupChatMessage(realm, item);

        realm.beginTransaction();
        try {
            realm.copyToRealmOrUpdate(item);
        } finally {
            realm.commitTransaction();
        }
    }

    public void setupChatMessage(Realm realm, ChatMessage item) {
        item.setSent(true);
        setupSender(realm, item);
        setupGroup(realm, item);

        if (item.isWasRead())
            item.setWasLocallyRead(true);
    }

    private void setupSender(Realm realm, ChatMessage item) {
        String id = item.getSender();
        if (id != null) {
            ChatUser userFrom = getChatUserById(realm, id);
            if (userFrom == null) {
                userFrom = new ChatUser();
                userFrom.setUser(id);
                if (item.getChatGroup() == null)
                    userFrom.setLastMessageDate(item.getDate());
            } else if (item.getChatGroup() == null) {
                if (userFrom.getLastMessageDate() != null &&
                        item.getDate() != null &&
                        userFrom.getLastMessageDate().getTime() < item.getDate().getTime()) {
                    realm.beginTransaction();
                    try {
                        userFrom.setLastMessageDate(item.getDate());
                    } finally {
                        realm.commitTransaction();
                    }
                }
            }
            item.setUserFrom(userFrom);
        }
    }


    private void setupGroup(Realm realm, ChatMessage item) {
        String id;
        id = item.getGroup();
        if (id != null) {
            ChatGroup chatGroup = getChatGroupById(realm, id);
            if (chatGroup == null) {
                chatGroup = new ChatGroup();
                chatGroup.setId(id);
                chatGroup.setUnknown(true);
                chatGroup.setLastMessageDate(item.getDate());
            } else if (
                    (chatGroup.getLastMessageDate() != null &&
                            item.getDate() != null &&
                            chatGroup.getLastMessageDate().getTime() < item.getDate().getTime()) ||
                    (chatGroup.getLastMessageDate() == null
                            && item.getDate() != null)) {
                realm.beginTransaction();
                try {
                    chatGroup.setLastMessageDate(item.getDate());
                    if (chatGroup.getType().equals(ChatGroup.TYPE_DIALOG)) {
                        for (ChatUser chatUser : chatGroup.getGroupUsers())
                            chatUser.setLastMessageDate(item.getDate());
                    }
                } finally {
                    realm.commitTransaction();
                }
            }
            item.setChatGroup(chatGroup);
        }
    }

    public ChatRoute getChatRouteById(Realm realm, @NonNull String id) {
        return realm.where(ChatRoute.class).equalTo("id", id).findFirst();
    }

    public RealmResults<ChatRoute> getChatRoutes(Realm realm) {
        return realm.where(ChatRoute.class).findAll();
    }

    public ChatUser getChatUserById(Realm realm, @NonNull String id) {
        RealmQuery<ChatUser> userRealmQuery = realm.where(ChatUser.class).equalTo("user", id);
        if (userRealmQuery.count() > 0)
            return userRealmQuery.findFirst();
        else return null;
    }

    public ChatGroup getChatGroupById(Realm realm, @NonNull String id) {
        return realm.where(ChatGroup.class).equalTo("id", id).findFirst();
    }

    public boolean isMessageExists(Realm realm, String id) {
        return realm.where(ChatMessage.class).equalTo("id", id).count() > 0;
    }

    public long getUnreadMessageCount(Realm realm, @NonNull String userId) {
        final RealmQuery<ChatMessage> realmQuery = realm
                .where(ChatMessage.class);
        return getUnreadMessages(userId, realmQuery);
    }


    public long getUnreadPrivateChatMessagesCount(Realm realm, String userId, String myId) {
        final RealmQuery<ChatMessage> realmQuery = getPrivateChatMessageRealmQuery(realm, userId);

        return getUnreadMessages(myId, realmQuery);
    }

    public long getUnreadGroupChatMessagesCount(Realm realm, String groupId, String myId) {
        final RealmQuery<ChatMessage> realmQuery = getGroupChatMessageRealmQuery(realm, groupId);
        return getUnreadMessages(myId, realmQuery);
    }

    private long getUnreadMessages(@NonNull String userId, RealmQuery<ChatMessage> realmQuery) {
        return realmQuery
                .notEqualTo("userFrom.user", userId)
                .equalTo("wasLocallyRead", false)
                .equalTo("wasRead", false)
                .count();
    }

    public RealmResults<ChatMessage> getPrivateChatMessages(Realm realm, String userId) {

        RealmQuery<ChatMessage> realmQuery = getPrivateChatMessageRealmQuery(realm, userId);

        return realmQuery.findAllSorted("date", Sort.ASCENDING);
    }

    public RealmResults<ChatMessage> getChatMessages(Realm realm) {
        return realm.where(ChatMessage.class).findAllSorted("date", Sort.ASCENDING);
    }

    public long getPrivateChatMessagesCount(Realm realm, String userId) {
        RealmQuery<ChatMessage> realmQuery = getPrivateChatMessageRealmQuery(realm, userId);

        return realmQuery.count();
    }

    private RealmQuery<ChatMessage> getPrivateChatMessageRealmQuery(Realm realm, String userId) {
        return realm
                .where(ChatMessage.class)
                .isNotNull("chatGroup")
                .equalTo("chatGroup.type", ChatGroup.TYPE_DIALOG)
                .equalTo("chatGroup.groupUsers.user", userId);
    }

    public ChatGroup getDialogByUserId(Realm realm, String userId) {
        return realm
                .where(ChatGroup.class)
                .equalTo("type", ChatGroup.TYPE_DIALOG)
                .equalTo("groupUsers.user", userId)
                .findFirst();
    }

    public RealmResults<ChatMessage> getGroupChatMessages(Realm realm, String groupId) {
        RealmQuery<ChatMessage> realmQuery = getGroupChatMessageRealmQuery(realm, groupId);
        return realmQuery
                .findAllSorted("date", Sort.ASCENDING);
    }

    public long getGroupChatMessagesCount(Realm realm, String groupId) {
        RealmQuery<ChatMessage> realmQuery = getGroupChatMessageRealmQuery(realm, groupId);
        return realmQuery.count();
    }

    private RealmQuery<ChatMessage> getGroupChatMessageRealmQuery(Realm realm, String groupId) {
        return realm
                .where(ChatMessage.class)
                .isNotNull("chatGroup")
                .equalTo("chatGroup.id", groupId);
    }

    public RealmResults<ChatGroup> getChatGroups(Realm realm) {
        final RealmQuery<ChatGroup> realmQuery = realm
                .where(ChatGroup.class)
                .notEqualTo("type", ChatGroup.TYPE_DIALOG)
                .notEqualTo("unknown", true);
        return realmQuery.findAllSorted("lastMessageDate", Sort.DESCENDING);
    }

    public Date getLastMessageDate(Realm realm, String userGuid) {
        final ChatMessage chatMessage = realm
                .where(ChatMessage.class)
                .isNotNull("chatGroup")
                .equalTo("chatGroup.type", ChatGroup.TYPE_DIALOG)
                .equalTo("chatGroup.groupUsers.user", userGuid)
                .findAllSorted("date", Sort.DESCENDING)
                .first();
        return chatMessage == null ? null : chatMessage.getDate();
    }

    public RealmResults<ChatUser> getChatUsers(Realm realm) {
        return realm
                .where(ChatUser.class)
                .findAllSorted("lastMessageDate", Sort.DESCENDING);
    }

    public RealmResults<ChatUser> getChatUsers(Realm realm, String myId) {
        return realm
                .where(ChatUser.class)
                .notEqualTo("user", myId)
                .findAllSorted("lastMessageDate", Sort.DESCENDING);
    }

    public Date getMaxSynchronisedDate(Realm realm) {
        return realm
                .where(ChatMessage.class)
                .equalTo("wasSynchronized", true)
                .maximumDate("date");
    }

}
