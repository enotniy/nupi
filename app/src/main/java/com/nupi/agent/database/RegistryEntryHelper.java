package com.nupi.agent.database;

import android.content.Context;

import com.nupi.agent.R;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.utils.TimeUtils;

import java.util.Date;

/**
 * Created by Pasenchuk Victor on 20.02.15
 */
public class RegistryEntryHelper {

    public static RegistryEntry initRegistryEntry(Context context, RegistryEntry registryEntry, String docId, int entryType, String clientId, Date date, double sum, String firmId, String comment) {
        registryEntry.setGuid(docId);
        registryEntry.setEntryType(entryType);
        if (clientId != null)
            registryEntry.setCounterAgent(clientId);
        registryEntry.setEntryStatus(RegistryEntry.NOT_SENT);
        registryEntry.setDate(date);
        registryEntry.setSum(sum);
        registryEntry.setOrganization(firmId);
        String docInfo = "";
        if (entryType == RegistryEntry.REALISATION)
            docInfo = String.format(context.getString(R.string.order_from), TimeUtils.timeAppFormat(date));
        else if (entryType == RegistryEntry.PAYMENT)
            docInfo = String.format(context.getString(R.string.payment_from), TimeUtils.timeAppFormat(date));

        registryEntry.setDocInfo(docInfo);
        if (comment != null)
            registryEntry.setComment(comment);
        return registryEntry;
    }

}
