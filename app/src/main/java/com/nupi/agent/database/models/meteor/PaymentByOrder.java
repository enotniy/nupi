package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 10.01.16
 */
@RealmClass
public class PaymentByOrder extends RealmObject {

    @PrimaryKey
    private transient String guid;

    @SerializedName("contract")
    @Expose
    private String contract;

    @SerializedName("counterAgent")
    @Expose
    private String counterAgent;

    @SerializedName("doc")
    @Expose
    private String doc;

    @SerializedName("organization")
    @Expose
    private String organization;

    @SerializedName("docInfo")
    @Expose
    private String docInfo;

    @SerializedName("docType")
    @Expose
    private String docType;

    @SerializedName("sum")
    @Expose
    private double sum;

    @SerializedName("revision")
    @Expose
    private long revision;

    @SerializedName("markRemoval")
    @Expose
    private boolean markRemoval;

    @SerializedName("deleted")
    @Expose
    private boolean deleted;

    @SerializedName("date")
    @Expose
    private Date date;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getCounterAgent() {
        return counterAgent;
    }

    public void setCounterAgent(String counterAgent) {
        this.counterAgent = counterAgent;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(String docInfo) {
        this.docInfo = docInfo;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isMarkRemoval() {
        return markRemoval;
    }

    public void setMarkRemoval(boolean markRemoval) {
        this.markRemoval = markRemoval;
    }
}
