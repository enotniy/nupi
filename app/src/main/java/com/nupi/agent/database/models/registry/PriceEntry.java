package com.nupi.agent.database.models.registry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nupi.agent.network.meteor.OrderItem;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 07.07.15
 */
@RealmClass
public class PriceEntry extends RealmObject {

//
//    "nomenclatureCharacteristic": "00000000-0000-0000-0000-000000000000",
//            "nomenclature": "c3af3e14-faeb-4859-b9d9-dc8c715d64e5",
//            "nomName": "кар Рошен эвкалипт-ментол 2кг*4шт (\"Рошен\")",
//            "unit": "5ae333b7-b1de-11e5-afc7-e8de2700ea37",
//            "quantity": 1,
//            "coefficient": 2,
//            "vatRate": "БезНДС",
//            "sum": 305.24,
//            "vatSum": 0,
//            "price": 320.5

    @SerializedName("nomenclature")
    @Expose
    private String nomenclature;

    @SerializedName("nomenclatureCharacteristic")
    @Expose
    private String nomenclatureCharacteristic;

    @SerializedName("nomName")
    @Expose
    private String nomName;

    @SerializedName("unitName")
    @Expose
    private String unitName;

    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("quantity")
    @Expose
    private double quantity;

    @SerializedName("coefficient")
    @Expose
    private double coefficient;

    @SerializedName("vatRate")
    @Expose
    private String vatRate;

    @SerializedName("sum")
    @Expose
    private double sum;

    @SerializedName("vatSum")
    @Expose
    private double vatSum;

    @SerializedName("price")
    @Expose
    private double price;

    private transient String ownerDocument;

    public PriceEntry() {
    }

    public PriceEntry(OrderItem item, String ownerDocument) {
        quantity = item.getQuantity();
        nomenclature = item.getNomenclature();
        sum = item.getSum();
        this.ownerDocument = ownerDocument;
        unit = item.getUnit();
    }

    public String getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(String nomenclature) {
        this.nomenclature = nomenclature;
    }

    public String getNomName() {
        return nomName;
    }

    public void setNomName(String nomName) {
        this.nomName = nomName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOwnerDocument() {
        return ownerDocument;
    }

    public void setOwnerDocument(String ownerDocument) {
        this.ownerDocument = ownerDocument;
    }

    public String getNomenclatureCharacteristic() {
        return nomenclatureCharacteristic;
    }

    public void setNomenclatureCharacteristic(String nomenclatureCharacteristic) {
        this.nomenclatureCharacteristic = nomenclatureCharacteristic;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public String getVatRate() {
        return vatRate;
    }

    public void setVatRate(String vatRate) {
        this.vatRate = vatRate;
    }

    public double getVatSum() {
        return vatSum;
    }

    public void setVatSum(double vatSum) {
        this.vatSum = vatSum;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
