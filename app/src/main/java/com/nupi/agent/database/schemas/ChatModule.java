package com.nupi.agent.database.schemas;

import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatRoute;
import com.nupi.agent.database.models.chat.ChatUser;

import io.realm.annotations.RealmModule;

/**
 * Created by Pasenchuk Victor on 28.10.15
 */

@RealmModule(classes = {ChatGroup.class, ChatMessage.class, ChatUser.class, ChatRoute.class})
public class ChatModule {
}
