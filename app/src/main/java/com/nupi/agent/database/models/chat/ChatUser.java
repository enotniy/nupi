package com.nupi.agent.database.models.chat;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 19.10.15
 */

@RealmClass
public class ChatUser extends RealmObject {

    @PrimaryKey
    @SerializedName("user")
    private String user;
    @SerializedName("nickname")
    private String nickname;
    @SerializedName("organisationAdminStatus")
    private Boolean organisationAdminStatus;
    @SerializedName("exchangeOperationsEnabled")
    private Boolean exchangeOperationsEnabled;
    @SerializedName("supervisorStatus")
    private Boolean supervisorStatus;
    @SerializedName("messageCount")
    private long messageCount;

    private transient Date lastMessageDate;
    private transient ChatRoute chatRoute;

    @Ignore
    @SerializedName("route")
    private String route;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNickname() {
        return nickname;
    }


    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Boolean getOrganisationAdminStatus() {
        return organisationAdminStatus;
    }

    public void setOrganisationAdminStatus(Boolean organisationAdminStatus) {
        this.organisationAdminStatus = organisationAdminStatus;
    }

    public Boolean getExchangeOperationsEnabled() {
        return exchangeOperationsEnabled;
    }

    public void setExchangeOperationsEnabled(Boolean exchangeOperationsEnabled) {
        this.exchangeOperationsEnabled = exchangeOperationsEnabled;
    }

    public Boolean getSupervisorStatus() {
        return supervisorStatus;
    }

    public void setSupervisorStatus(Boolean supervisorStatus) {
        this.supervisorStatus = supervisorStatus;
    }

    public long getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(long messageCount) {
        this.messageCount = messageCount;
    }

    public ChatRoute getChatRoute() {
        return chatRoute;
    }

    public void setChatRoute(ChatRoute chatRoute) {
        this.chatRoute = chatRoute;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    // Non model fields.
    public String getRoute() {
        return route;
    }
}

