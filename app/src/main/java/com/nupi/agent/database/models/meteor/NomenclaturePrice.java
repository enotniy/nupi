package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 26.12.15
 */
@RealmClass
public class NomenclaturePrice extends RealmObject {

    @Index
    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("nomenclature")
    @Expose
    private String nomenclature;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("price")
    @Expose
    private double price;

    @SerializedName("period")
    @Expose
    private Date period;

    @SerializedName("revision")
    @Expose
    private long revision;

    public String getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(String nomenclature) {
        this.nomenclature = nomenclature;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getPeriod() {
        return period;
    }

    public void setPeriod(Date period) {
        this.period = period;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }
}
