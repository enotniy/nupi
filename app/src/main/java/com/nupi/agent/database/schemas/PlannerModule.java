package com.nupi.agent.database.schemas;

import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;

import io.realm.annotations.RealmModule;

/**
 * Created by Pasenchuk Victor on 28.10.15
 */

@RealmModule(classes = {ClientInfo.class, PlannerRoute.class})
public class PlannerModule {
}
