package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 29.12.15
 */
@RealmClass
public class Organization extends RealmObject {
    @PrimaryKey
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("markRemoval")
    @Expose
    private boolean markRemoval;
    @SerializedName("revision")
    @Expose
    private long revision;
    @SerializedName("changedBy")
    @Expose
    private String changedBy;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMarkRemoval() {
        return markRemoval;
    }

    public void setMarkRemoval(boolean markRemoval) {
        this.markRemoval = markRemoval;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }
}
