package com.nupi.agent.database.models.planner;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 26.11.15
 */
@RealmClass
public class PlannerRoute extends RealmObject {
    @PrimaryKey
    private String guid;

    private String routeName;

    private RealmList<ClientInfo> clientsList;

    private Date creationDate;

    private Date modificationDate;

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public RealmList<ClientInfo> getClientsList() {
        return clientsList;
    }

    public void setClientsList(RealmList<ClientInfo> clientsList) {
        this.clientsList = clientsList;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getGuid() {
        return this.guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}
