package com.nupi.agent.database.models.chat;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 19.10.15
 */
@RealmClass
public class ChatGroup extends RealmObject {

    public static final String TYPE_DIALOG = "dialog";

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @SerializedName("headline")
    private String headline;

    @SerializedName("organisationBroadcast")
    private boolean organisationBroadcast;

    @SerializedName("messageCount")
    private long messageCount;

    @SerializedName("lastMessageDate")
    private Date lastMessageDate;

    @SerializedName("archived")
    private boolean archived;

    @SerializedName("type")
    private String type;

    private transient RealmList<ChatUser> groupUsers;

    private transient boolean unknown;

    // Non model fields.
    @Ignore
    @SerializedName("users")
    private List<String> users;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public boolean isOrganisationBroadcast() {
        return organisationBroadcast;
    }

    public void setOrganisationBroadcast(boolean organisationBroadcast) {
        this.organisationBroadcast = organisationBroadcast;
    }

    public long getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(long messageCount) {
        this.messageCount = messageCount;
    }

    public RealmList<ChatUser> getGroupUsers() {
        return groupUsers;
    }

    public void setGroupUsers(RealmList<ChatUser> groupUsers) {
        this.groupUsers = groupUsers;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isUnknown() {
        return unknown;
    }

    public void setUnknown(boolean unknown) {
        this.unknown = unknown;
    }

    // Non model fields.
    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }
}
