package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 18.12.15
 */
@RealmClass
public class Nomenclature extends RealmObject {

    public static final String ROOT_GUID = "00000000-0000-0000-0000-000000000000";

    @PrimaryKey
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("folderId")
    @Expose
    private String folderId;
    @SerializedName("isFolder")
    @Expose
    private boolean isFolder;
    @SerializedName("article")
    @Expose
    private String article;
    @SerializedName("baseUnit")
    @Expose
    private String baseUnit;
    @SerializedName("storeUnit")
    @Expose
    private String storeUnit;
    @SerializedName("reportUnit")
    @Expose
    private String reportUnit;
    @SerializedName("nGroup")
    @Expose
    private String nGroup;
    @SerializedName("useCharacteristics")
    @Expose
    private boolean useCharacteristics;
    @SerializedName("vat")
    @Expose
    private String vat;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("markRemoval")
    @Expose
    private boolean markRemoval;
    @SerializedName("revision")
    @Expose
    private long revision;
    @SerializedName("changedBy")
    @Expose
    private String changedBy;
    private transient String lowerCaseName;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getLowerCaseName() {
        return lowerCaseName;
    }

    public void setLowerCaseName(String lowerCaseName) {
        this.lowerCaseName = lowerCaseName;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public boolean isFolder() {
        return isFolder;
    }

    public void setIsFolder(boolean isFolder) {
        this.isFolder = isFolder;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(String baseUnit) {
        this.baseUnit = baseUnit;
    }

    public String getStoreUnit() {
        return storeUnit;
    }

    public void setStoreUnit(String storeUnit) {
        this.storeUnit = storeUnit;
    }

    public String getReportUnit() {
        return reportUnit;
    }

    public void setReportUnit(String reportUnit) {
        this.reportUnit = reportUnit;
    }

    public String getnGroup() {
        return nGroup;
    }

    public void setnGroup(String nGroup) {
        this.nGroup = nGroup;
    }

    public boolean isUseCharacteristics() {
        return useCharacteristics;
    }

    public void setUseCharacteristics(boolean useCharacteristics) {
        this.useCharacteristics = useCharacteristics;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public boolean isMarkRemoval() {
        return markRemoval;
    }

    public void setMarkRemoval(boolean markRemoval) {
        this.markRemoval = markRemoval;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

}
