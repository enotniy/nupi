package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 23.12.15
 */
@RealmClass
public class Unit extends RealmObject {

    @PrimaryKey
    @SerializedName("guid")
    @Expose
    private String guid;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("nomenclature")
    @Expose
    private String nomenclature;

    @SerializedName("coefficient")
    @Expose
    private double coefficient;

    @SerializedName("revision")
    @Expose
    private long revision;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(String nomenclature) {
        this.nomenclature = nomenclature;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }
}
