package com.nupi.agent.database.schemas;

import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.ExpandedPayment;
import com.nupi.agent.database.models.meteor.ExpandedPaymentByOrder;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.database.models.meteor.NomenclaturePrice;
import com.nupi.agent.database.models.meteor.Organization;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.database.models.meteor.Sale;
import com.nupi.agent.database.models.meteor.Stock;
import com.nupi.agent.database.models.meteor.Unit;

import io.realm.annotations.RealmModule;

/**
 * Created by Pasenchuk Victor on 28.10.15
 */

@RealmModule(classes = {CounterAgent.class, Organization.class, Nomenclature.class, NomenclaturePrice.class, Unit.class, PaymentByOrder.class, Payment.class, ExpandedPaymentByOrder.class, ExpandedPayment.class, Stock.class, Contract.class, Sale.class})
public class MeteorModule {
}
