package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 10.01.16
 */
@RealmClass
public class Payment extends RealmObject {

    @PrimaryKey
    private transient String guid;

    @SerializedName("contract")
    @Expose
    private String contract;

    @SerializedName("counterAgent")
    @Expose
    private String counterAgent;

    @SerializedName("organization")
    @Expose
    private String organization;

    @SerializedName("sum")
    @Expose
    private double sum;

    @SerializedName("revision")
    @Expose
    private long revision;

    @SerializedName("markRemoval")
    @Expose
    private boolean markRemoval;

    @SerializedName("deleted")
    @Expose
    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getCounterAgent() {
        return counterAgent;
    }

    public void setCounterAgent(String counterAgent) {
        this.counterAgent = counterAgent;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public boolean isMarkRemoval() {
        return markRemoval;
    }

    public void setMarkRemoval(boolean markRemoval) {
        this.markRemoval = markRemoval;
    }
}
