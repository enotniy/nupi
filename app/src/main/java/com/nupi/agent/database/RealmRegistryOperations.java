package com.nupi.agent.database;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.database.models.registry.PriceEntry;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.database.schemas.RegistryModule;
import com.nupi.agent.helpers.KeyStoreHelper;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.utils.TimeUtils;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Pasenchuk Victor on 20.02.15
 */

public class RealmRegistryOperations extends BaseExchangeRealmHelper {

    public static final int SCHEMA_VERSION = 1;
    private static final long ONE_DAY = 60 * 60 * 24 * 1000;
    private static final long IN_THE_POCKET_DELAY_THRESHOLD = 30;

    private static final String REGISTRY_ALIAS = "REGISTRY";

    public RealmRegistryOperations(Application app) {
        super(app);
    }


    @Override
    public RealmConfiguration getRealmConfig(KeyStoreHelper keyStoreHelper) {
        return new RealmConfiguration.Builder(app)
                .name("registry.realm")
                .encryptionKey(keyStoreHelper.getPrivateKey(REGISTRY_ALIAS))
                .schemaVersion(SCHEMA_VERSION)
                .setModules(new RegistryModule())
                .build();
    }


    public long getMaxRevision(Realm realm, int entryType) {
        Number revision = realm
                .where(RegistryEntry.class)
                .equalTo("entryType", entryType)
                .max("revision");
        if (revision != null)
            return revision
                    .longValue();
        else
            return 0;
    }


    public RealmResults<RegistryEntry> getRegistryEntries(@NonNull Realm realm, @Nullable Date fromDate, @Nullable Date toDate, @Nullable Integer type, @NonNull int[] entryStatuses, boolean onlyPositive, @Nullable CounterAgent counterAgent) {
        RealmQuery<RegistryEntry> realmQuery = realm
                .where(RegistryEntry.class)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false);

        if (fromDate != null) {
            realmQuery = realmQuery.greaterThan("date", fromDate);
        }
        if (toDate != null) {
            final Date upDateLimit = new Date(toDate.getTime() + ONE_DAY);
            realmQuery = realmQuery.lessThan("date", upDateLimit);
        }
        if (type != null) {
            realmQuery = realmQuery.equalTo("entryType", type);
        }
        if (counterAgent != null) {
            realmQuery = realmQuery.equalTo("counterAgent", counterAgent.getGuid());
        }
        realmQuery = realmQuery.beginGroup();
        for (int i = 0; i < entryStatuses.length; i++) {
            realmQuery.equalTo("entryStatus", entryStatuses[i]);
            if (i != entryStatuses.length - 1)
                realmQuery.or();

        }
        realmQuery.endGroup();
        if (onlyPositive) {
            realmQuery = realmQuery.greaterThan("sum", 0d);
        }
        return realmQuery.findAllSorted("date", Sort.DESCENDING);
    }


    public RealmResults<RegistryEntry> getInThePocketEntries(Realm realm) {
        final Date lowDateLimit = new Date(TimeUtils.currentTime() - IN_THE_POCKET_DELAY_THRESHOLD * ONE_DAY);
        return getRegistryEntries(realm, lowDateLimit, null, RegistryEntry.PAYMENT, RegistryEntry.IN_THE_POCKET_STATUSES, true, null);
    }

    public float getInThePocketValue(Realm realm) {
        return getInThePocketEntries(realm)
                .sum("sum")
                .floatValue();
    }

    public void addReportEntry(Realm realm, String docId, String clientId, double sum, int entryType, String firmId, List<OrderItem> orderItems, String comment) {
        realm.beginTransaction();
        RegistryEntry registryEntry = realm.createObject(RegistryEntry.class);
        RegistryEntryHelper.initRegistryEntry(application, registryEntry,
                docId,
                entryType,
                clientId,
                new Date(),
                sum,
                firmId,
                comment
        );
        for (OrderItem orderItem : orderItems) {
            realm.copyToRealm(new PriceEntry(orderItem, registryEntry.getGuid()));
        }
        realm.commitTransaction();
    }

    private RegistryEntry getOrCreateRealisationReportEntry(Realm realm, PaymentByOrder paymentByOrder) {
        RegistryEntry registryEntry = getRegistryEntryById(realm, paymentByOrder.getDoc());
        if (registryEntry == null) {
            registryEntry = initRealisationRegistryEntry(realm, paymentByOrder.getDoc(), paymentByOrder.getCounterAgent(), paymentByOrder.getOrganization());
            registryEntry.setDocInfo(paymentByOrder.getDocInfo());
            registryEntry.setSum(paymentByOrder.getSum());
            registryEntry.setDate(paymentByOrder.getDate());
        }
        return registryEntry;
    }

    public void updateReportEntryWasSent(Realm realm, String docId) {
        updateRegistryEntryStatus(realm, docId, RegistryEntry.SENT);
    }

    public void updateReportEntryWasRejected(Realm realm, String docId) {
        updateRegistryEntryStatus(realm, docId, RegistryEntry.REJECTED);
    }

    private void updateRegistryEntryStatus(Realm realm, String docId, int entryStatus) {
        realm.beginTransaction();
        RegistryEntry registryEntry = getRegistryEntryById(realm, docId);
        if (registryEntry != null) {
            registryEntry.setEntryStatus(entryStatus);
        }
        realm.commitTransaction();
    }

    private void updateReportEntry(Realm realm, String docId, String docNumber, int status) {
        RegistryEntry registryEntry = getRegistryEntryById(realm, docId);
        if (registryEntry == null) {
            registryEntry = initPaymentRegistryEntry(realm, docId, null);
        }
        registryEntry.setDocInfo(docNumber);
    }


    public RealmResults<PriceEntry> getPriceEntriesOfRealisation(Realm realm, RegistryEntry registryEntry) {
        return realm.where(PriceEntry.class).equalTo("ownerDocument", registryEntry.getGuid()).findAll();
    }


    private RegistryEntry initPaymentRegistryEntry(Realm realm, String docId, String firmId) {
        RegistryEntry registryEntry = realm.createObject(RegistryEntry.class);
        RegistryEntryHelper.initRegistryEntry(application, registryEntry,
                docId,
                RegistryEntry.PAYMENT,
                null,
                new Date(),
                0,
                firmId,
                ""
        );
        return registryEntry;
    }


    private RegistryEntry initRealisationRegistryEntry(Realm realm, String docId, String clientID, String firmId) {
        RegistryEntry registryEntry = realm.createObject(RegistryEntry.class);
        RegistryEntryHelper.initRegistryEntry(application, registryEntry,
                docId,
                RegistryEntry.REALISATION,
                clientID,
                new Date(),
                0,
                firmId,
                ""
        );
        return registryEntry;
    }

    public RegistryEntry getRegistryEntryById(Realm realm, String docId) {
        return realm.where(RegistryEntry.class).contains("guid", docId).findFirst();
    }
}
