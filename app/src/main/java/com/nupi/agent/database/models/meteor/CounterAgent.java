package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 16.12.15
 */
@RealmClass
public class CounterAgent extends RealmObject {

    public static final String ROOT_GUID = "00000000-0000-0000-0000-000000000000";
    public static final String NEW_POINT = "00000000-caca-caca-caca-cacacacacaca";

    @PrimaryKey
    @SerializedName("guid")
    @Expose
    private String guid;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("folderId")
    @Expose
    private String folderId;

    @SerializedName("isFolder")
    @Expose
    private boolean isFolder;

    @SerializedName("buyer")
    @Expose
    private boolean buyer;

    @SerializedName("provider")
    @Expose
    private boolean provider;

    @SerializedName("contract")
    @Expose
    private String contract;

    @SerializedName("manager")
    @Expose
    private String manager;

    @SerializedName("markRemoval")
    @Expose
    private boolean markRemoval;

    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("llong")
    @Expose
    private float longitude;
    @SerializedName("llat")
    @Expose
    private float latitude;
    @SerializedName("revision")
    @Expose
    private long revision;
    @SerializedName("changedBy")
    @Expose
    private String changedBy;
    private boolean hasOrders;
    private double debt;
    private transient String lowerCaseName;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getLowerCaseName() {
        return lowerCaseName;
    }

    public void setLowerCaseName(String lowerCaseName) {
        this.lowerCaseName = lowerCaseName;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public boolean isFolder() {
        return isFolder;
    }

    public void setIsFolder(boolean isFolder) {
        this.isFolder = isFolder;
    }

    public boolean isBuyer() {
        return buyer;
    }

    public void setBuyer(boolean buyer) {
        this.buyer = buyer;
    }

    public boolean isProvider() {
        return provider;
    }

    public void setProvider(boolean provider) {
        this.provider = provider;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public boolean isMarkRemoval() {
        return markRemoval;
    }

    public void setMarkRemoval(boolean markRemoval) {
        this.markRemoval = markRemoval;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    public boolean isHasOrders() {
        return hasOrders;
    }

    public void setHasOrders(boolean hasOrders) {
        this.hasOrders = hasOrders;
    }

    public double getDebt() {
        return debt;
    }

    public void setDebt(double debt) {
        this.debt = debt;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
