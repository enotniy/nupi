package com.nupi.agent.database;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 26.11.15
 */
public abstract class BaseExchangeRealmHelper extends BaseRealmHelper {

    protected final Application application;

    public BaseExchangeRealmHelper(Application app) {
        super(app);
        application = app;
    }

    public <T extends RealmObject> T getRealmObjectByGuid(Realm realm, String guid, Class<T> type) {
        return realm
                .where(type)
                .equalTo("guid", guid)
                .findFirst();
    }

    public <T extends RealmObject> RealmResults<T> getRealmObjects(Realm realm, Class<T> type) {
        return realm
                .where(type)
                .equalTo("markRemoval", false)
                .equalTo("deleted", false)
                .findAll();
    }

    public long getMaxRevision(Realm realm, Class<? extends RealmObject> clazz) {
        Number revision = realm
                .where(clazz)
                .max("revision");
        if (revision != null)
            return revision
                    .longValue();
        else
            return 0;
    }

}
