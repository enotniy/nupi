package com.nupi.agent.database.models.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 16.12.15
 */
@RealmClass
public class Contract extends RealmObject {

    @PrimaryKey
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("debtDaysControl")
    @Expose
    private boolean debtDaysControl;
    @SerializedName("debtDaysMax")
    @Expose
    private long debtDaysMax;
    @SerializedName("markRemoval")
    @Expose
    private boolean markRemoval;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("revision")
    @Expose
    private long revision;
    @SerializedName("useCaDocuments")
    @Expose
    private boolean useCaDocuments;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isDebtDaysControl() {
        return debtDaysControl;
    }

    public void setDebtDaysControl(boolean debtDaysControl) {
        this.debtDaysControl = debtDaysControl;
    }

    public long getDebtDaysMax() {
        return debtDaysMax;
    }

    public void setDebtDaysMax(long debtDaysMax) {
        this.debtDaysMax = debtDaysMax;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }

    public boolean isMarkRemoval() {
        return markRemoval;
    }

    public void setMarkRemoval(boolean markRemoval) {
        this.markRemoval = markRemoval;
    }

    public boolean isUseCaDocuments() {
        return useCaDocuments;
    }

    public void setUseCaDocuments(boolean useCaDocuments) {
        this.useCaDocuments = useCaDocuments;
    }
}
