package com.nupi.agent.database.models.planner;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Pasenchuk Victor on 26.11.15
 */
@RealmClass
public class ClientInfo extends RealmObject {

    @PrimaryKey
    private String clientId;

    private String clientName;

    private Date workingBeginTime;

    private Date workingEndTime;

    private Date lunchBeginTime;

    private Date lunchEndTime;

    private String responsiblePerson;

    private String visitFrequency;

    private String contacts;

    private String additionalPhone;

    private String email;

    public ClientInfo() {
    }

    public ClientInfo(String clientId, String clientName) {
        this.clientId = clientId;
        this.clientName = clientName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Date getWorkingBeginTime() {
        return workingBeginTime;
    }

    public void setWorkingBeginTime(Date workingBeginTime) {
        this.workingBeginTime = workingBeginTime;
    }

    public Date getWorkingEndTime() {
        return workingEndTime;
    }

    public void setWorkingEndTime(Date workingEndTime) {
        this.workingEndTime = workingEndTime;
    }

    public Date getLunchBeginTime() {
        return lunchBeginTime;
    }

    public void setLunchBeginTime(Date lunchBeginTime) {
        this.lunchBeginTime = lunchBeginTime;
    }

    public Date getLunchEndTime() {
        return lunchEndTime;
    }

    public void setLunchEndTime(Date lunchEndTime) {
        this.lunchEndTime = lunchEndTime;
    }

    public String getResponsiblePerson() {
        return responsiblePerson;
    }

    public void setResponsiblePerson(String responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    public String getVisitFrequency() {
        return visitFrequency;
    }

    public void setVisitFrequency(String visitFrequency) {
        this.visitFrequency = visitFrequency;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getAdditionalPhone() {
        return additionalPhone;
    }

    public void setAdditionalPhone(String additional_phone) {
        this.additionalPhone = additional_phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
