package com.nupi.agent.network;

import android.content.Intent;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.BuildConfig;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.RealmRegistryOperations;
import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatRoute;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.ExpandedPayment;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.database.models.meteor.NomenclaturePrice;
import com.nupi.agent.database.models.meteor.Organization;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.database.models.meteor.Sale;
import com.nupi.agent.database.models.meteor.Stock;
import com.nupi.agent.database.models.meteor.Unit;
import com.nupi.agent.database.models.registry.PriceEntry;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.enums.UpdateStatus;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.network.meteor.EndpointInfo;
import com.nupi.agent.network.meteor.Endpoints;
import com.nupi.agent.network.meteor.NupiMeteorApi;
import com.nupi.agent.network.meteor.StatusResponse;
import com.nupi.agent.network.nupi.NupiServerApi;
import com.nupi.agent.network.upload.UploadService;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.main.activities.MainActivity;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.utils.NetworkUtils;
import com.nupi.agent.utils.TimeUtils;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.exceptions.RealmIOException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

/**
 * Created by Pasenchuk Victor on 29.12.15
 */
public class MeteorDataUpdater {
    private final NupiFragment nupiFragment;
    @Inject
    RealmExchangeOperations realmExchangeOperations;

    @Inject
    RealmChatOperations realmChatOperations;

    @Inject
    RealmRegistryOperations realmRegistryOperations;

    @Inject
    NupiPreferences nupiPreferences;

    @Inject
    UploadService uploadService;

    @Inject
    NupiMeteorApi nupiMeteorApi;

    @Inject
    NupiServerApi nupiServerApi;

    @Inject
    OrderApi orderApi;

    @Inject
    Bus bus;

    public MeteorDataUpdater(NupiFragment nupiFragment) {
        this.nupiFragment = nupiFragment;
        nupiFragment.getNupiApp().getAppComponent().inject(this);
    }

    public void updateFromMeteor() {

        final EndpointsInfoHolder holder = new EndpointsInfoHolder();

        nupiMeteorApi
                .getStatus(0)
                .flatMap(new Func1<StatusResponse, Observable<List<PaymentByOrder>>>() {
                    @Override
                    public Observable<List<PaymentByOrder>> call(StatusResponse statusResponse) {

                        final StatusResponse savedStatusResponse = nupiPreferences.getStatusResponse();
                        holder.actualInfo = statusResponse.getEndpoints();

                        try (Realm meteorRealm = realmExchangeOperations.getRealm(); Realm chatRealm = realmChatOperations.getRealm(); Realm registryRealm = realmRegistryOperations.getRealm()) {


                            checkFilterRevisions(meteorRealm, registryRealm, savedStatusResponse, holder.actualInfo);

                            nupiPreferences.setStatusResponse(statusResponse);

                            getMaxRevisions(meteorRealm, chatRealm, registryRealm);

                        }

                        return getNormalOrJustObservable(
                                holder.maxRevisionPaymentByOrder,
                                holder.actualInfo
                                        .getPaymentsByOrders()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getPaymentsByOrders(holder.maxRevisionPaymentByOrder)
                        );
                    }

                    private void checkFilterRevisions(Realm meteorRealm, Realm registryRealm, StatusResponse savedStatusResponse, Endpoints actualInfo) {
                        if (savedStatusResponse != null) {
                            final Endpoints savedInfo = savedStatusResponse.getEndpoints();

                            meteorRealm.beginTransaction();

                            checkFiltersForTable(
                                    savedInfo.getPaymentsByOrders(),
                                    actualInfo.getPaymentsByOrders(),
                                    PaymentByOrder.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getPayments(),
                                    actualInfo.getPayments(),
                                    Payment.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getPaymentsExpanded(),
                                    actualInfo.getPaymentsExpanded(),
                                    ExpandedPayment.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getStocks(),
                                    actualInfo.getStocks(),
                                    Stock.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getSale(),
                                    actualInfo.getSale(),
                                    Sale.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getContracts(),
                                    actualInfo.getContracts(),
                                    Contract.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getCounterAgents(),
                                    actualInfo.getCounterAgents(),
                                    CounterAgent.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getOrganizations(),
                                    actualInfo.getOrganizations(),
                                    Organization.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getNomenclature(),
                                    actualInfo.getNomenclature(),
                                    Nomenclature.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getNomenclaturePrices(),
                                    actualInfo.getNomenclaturePrices(),
                                    NomenclaturePrice.class,
                                    meteorRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getUnits(),
                                    actualInfo.getUnits(),
                                    Unit.class,
                                    meteorRealm
                            );

                            meteorRealm.commitTransaction();
                            registryRealm.beginTransaction();

                            checkFiltersForTable(
                                    savedInfo.getRealization(),
                                    actualInfo.getRealization(),
                                    RegistryEntry.class,
                                    registryRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getOrders(),
                                    actualInfo.getOrders(),
                                    RegistryEntry.class,
                                    registryRealm
                            );

                            checkFiltersForTable(
                                    savedInfo.getCashInOrders(),
                                    actualInfo.getCashInOrders(),
                                    RegistryEntry.class,
                                    registryRealm
                            );

                            registryRealm.commitTransaction();
                        }

                    }

                    private void checkFiltersForTable(EndpointInfo savedStatus, EndpointInfo actualStatus, Class<? extends RealmObject> aClass, Realm realm) {
                        if (savedStatus.getFilterRevision() != actualStatus.getFilterRevision())
                            realm.clear(aClass);
                    }

                    private void getMaxRevisions(Realm meteorRealm, Realm chatRealm, Realm registryRealm) {
                        holder.maxRevisionPaymentByOrder = realmExchangeOperations.getMaxRevision(meteorRealm, PaymentByOrder.class);
                        holder.maxRevisionPayment = realmExchangeOperations.getMaxRevision(meteorRealm, Payment.class);
                        holder.maxRevisionRealization = realmRegistryOperations.getMaxRevision(registryRealm, RegistryEntry.REALISATION);
                        holder.maxRevisionOrders = realmRegistryOperations.getMaxRevision(registryRealm, RegistryEntry.ORDER);
                        holder.maxRevisionCashInOrders = realmRegistryOperations.getMaxRevision(registryRealm, RegistryEntry.PAYMENT);
                        holder.maxRevisionExpandedPayment = realmExchangeOperations.getMaxRevision(meteorRealm, ExpandedPayment.class);
                        holder.maxRevisionStock = realmExchangeOperations.getMaxRevision(meteorRealm, Stock.class);
                        holder.maxRevisionContract = realmExchangeOperations.getMaxRevision(meteorRealm, Contract.class);
                        holder.maxRevisionSale = realmExchangeOperations.getMaxRevision(meteorRealm, Sale.class);
                        holder.maxRevisionCounterAgent = realmExchangeOperations.getMaxRevision(meteorRealm, CounterAgent.class);
                        holder.maxRevisionOrganization = realmExchangeOperations.getMaxRevision(meteorRealm, Organization.class);
                        holder.maxRevisionNomenclature = realmExchangeOperations.getMaxRevision(meteorRealm, Nomenclature.class);
                        holder.maxRevisionNomenclaturePrice = realmExchangeOperations.getMaxRevision(meteorRealm, NomenclaturePrice.class);
                        holder.maxRevisionUnits = realmExchangeOperations.getMaxRevision(meteorRealm, Unit.class);
                        holder.maxSynchronizedDate = realmChatOperations.getMaxSynchronisedDate(chatRealm);
                    }
                })
                .flatMap(new Func1<List<PaymentByOrder>, Observable<List<Payment>>>() {
                    @Override
                    public Observable<List<Payment>> call(List<PaymentByOrder> paymentsByOrders) {
                        for (PaymentByOrder paymentByOrder : paymentsByOrders) {
                            paymentByOrder.setGuid(String.format("%s%s%s%s",
                                    paymentByOrder.getDoc(),
                                    paymentByOrder.getContract(),
                                    paymentByOrder.getCounterAgent(),
                                    paymentByOrder.getOrganization()
                            ));
                            paymentByOrder.setDate(TimeUtils.calculateDate(paymentByOrder));
                        }
                        realmExchangeOperations.updateBulkList(paymentsByOrders);

                        Realm realm1 = realmExchangeOperations.getRealm();

                        try {
                            realm1.beginTransaction();
                            realmExchangeOperations.getZeroPaymentsByOrders(realm1).clear();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            realm1.commitTransaction();
                            realm1.close();
                        }

                        return getNormalOrJustObservable(
                                holder.maxRevisionPayment,
                                holder.actualInfo
                                        .getPayments()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getPayments(holder.maxRevisionPayment)
                        );
                    }
                })
                .flatMap(new Func1<List<Payment>, Observable<List<ExpandedPayment>>>() {
                    @Override
                    public Observable<List<ExpandedPayment>> call(List<Payment> payments) {
                        for (Payment paymentByOrder : payments) {
                            paymentByOrder.setGuid(String.format("%s%s%s",
                                    paymentByOrder.getContract(),
                                    paymentByOrder.getCounterAgent(),
                                    paymentByOrder.getOrganization()
                            ));
                        }
                        Realm realm = realmExchangeOperations.getRealm();


                        try {
                            realmExchangeOperations.updateBulkList(payments, realm);
                            realm.beginTransaction();
                            realmExchangeOperations.getZeroPayments(realm).clear();
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            realm.commitTransaction();
                            realm.close();
                        }

                        return getNormalOrJustObservable(
                                holder.maxRevisionExpandedPayment,
                                holder.actualInfo
                                        .getPaymentsExpanded()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getExpandedPayments(holder.maxRevisionExpandedPayment)
                        );
                    }
                })
                .flatMap(new Func1<List<ExpandedPayment>, Observable<List<Sale>>>() {
                    @Override
                    public Observable<List<Sale>> call(List<ExpandedPayment> expandedPayments) {
                        for (ExpandedPayment expandedPayment : expandedPayments) {
                            expandedPayment.setGuid(String.format("%s%s%s%s",
                                    expandedPayment.getRegistrar(),
                                    expandedPayment.getContract(),
                                    expandedPayment.getCounterAgent(),
                                    expandedPayment.getOrganization()
                            ));
                        }

                        realmExchangeOperations.updateBulkList(expandedPayments);


                        return getNormalOrJustObservable(
                                holder.maxRevisionSale,
                                holder.actualInfo
                                        .getSale()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getSales(holder.maxRevisionSale)
                        );

                    }
                })
                .flatMap(new Func1<List<Sale>, Observable<List<Stock>>>() {
                    @Override
                    public Observable<List<Stock>> call(List<Sale> sales) {
                        for (Sale sale : sales) {
                            sale.setGuid(String.format("%s%s%s%s%s%s%s%s",
                                    sale.getPeriod(),
                                    sale.getRegistrar(),
                                    sale.getCounterAgent(),
                                    sale.getOrganization(),
                                    sale.getDeal(),
                                    sale.getRealization(),
                                    sale.getNomenclatureCharacteristic(),
                                    sale.getNomenclature()
                            ));
                        }

                        realmExchangeOperations.updateBulkList(sales);

                        return getNormalOrJustObservable(
                                holder.maxRevisionStock,
                                holder.actualInfo
                                        .getStocks()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getStocks(holder.maxRevisionStock)
                        );
                    }
                })
                .flatMap(new Func1<List<Stock>, Observable<List<Contract>>>() {
                    @Override
                    public Observable<List<Contract>> call(List<Stock> stocks) {
                        for (Stock stock : stocks)
                            stock.setGuid(String.format("%s%s",
                                    stock.getNomenclature(),
                                    stock.getWarehouse()
                            ));
                        realmExchangeOperations.updateBulkList(stocks);

                        return getNormalOrJustObservable(
                                holder.maxRevisionContract,
                                holder.actualInfo
                                        .getContracts()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getContracts(holder.maxRevisionContract)
                        );
                    }
                })
                .flatMap(new Func1<List<Contract>, Observable<List<CounterAgent>>>() {
                    @Override
                    public Observable<List<CounterAgent>> call(List<Contract> contracts) {
                        realmExchangeOperations.updateBulkList(contracts);

                        return getNormalOrJustObservable(
                                holder.maxRevisionCounterAgent,
                                holder.actualInfo
                                        .getCounterAgents()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getCounterAgents(holder.maxRevisionCounterAgent)
                        );
                    }
                })
                .flatMap(new Func1<List<CounterAgent>, Observable<List<NomenclaturePrice>>>() {
                    @Override
                    public Observable<List<NomenclaturePrice>> call(List<CounterAgent> counterAgents) {
                        final Realm realm = realmExchangeOperations.getRealm();
                        try {
                            realmExchangeOperations.updateBulkList(counterAgents, realm);
                            RealmResults<CounterAgent> realmObjects = realmExchangeOperations.getRealmObjects(realm, CounterAgent.class);
                            realm.beginTransaction();
                            for (int i = 0; i < realmObjects.size(); i++) {
                                CounterAgent counterAgent = realmObjects.get(i);
                                counterAgent.setLowerCaseName(counterAgent.getName().toLowerCase());
                                counterAgent.setDebt(realmExchangeOperations.getDebtSumForCounterAgent(realm, counterAgent.getGuid()));
                                counterAgent.setHasOrders(orderApi.hasOrders(counterAgent.getGuid()));
                            }
                        } finally {
                            realm.commitTransaction();
                            realm.close();
                        }

                        return getNormalOrJustObservable(
                                holder.maxRevisionNomenclaturePrice,
                                holder.actualInfo
                                        .getNomenclaturePrices()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getNomenclaturePrices(holder.maxRevisionNomenclaturePrice)

                        );
                    }
                })
                .flatMap(new Func1<List<NomenclaturePrice>, Observable<List<Nomenclature>>>() {
                    @Override
                    public Observable<List<Nomenclature>> call(List<NomenclaturePrice> nomenclaturePrices) {
                        try (Realm realm = realmExchangeOperations.getRealm()) {
                            realmExchangeOperations.addBulkList(nomenclaturePrices, realm);
                            RealmResults<NomenclaturePrice> uniqueUnits = realm.distinct(NomenclaturePrice.class, "unit");
                            ArrayList<String> unitGuids = new ArrayList<>(uniqueUnits.size());
                            for (NomenclaturePrice nomenclaturePrice : uniqueUnits)
                                unitGuids.add(nomenclaturePrice.getUnit());
                            for (String guid : unitGuids)
                                realmExchangeOperations.getAndCompressNomenclaturePriceForUnit(realm, guid);
                        }


                        return getNormalOrJustObservable(
                                holder.maxRevisionNomenclature,
                                holder.actualInfo
                                        .getNomenclature()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getNomenclature(holder.maxRevisionNomenclature)
                        );

                    }
                })
                .flatMap(new Func1<List<Nomenclature>, Observable<List<Unit>>>() {
                    @Override
                    public Observable<List<Unit>> call(List<Nomenclature> nomenclatures) {
                        try (Realm realm = realmExchangeOperations.getRealm()) {
                            for (Nomenclature nomenclature : nomenclatures)
                                nomenclature.setLowerCaseName(nomenclature.getName().toLowerCase());
                            realmExchangeOperations.updateBulkList(nomenclatures, realm);
                        }

                        return getNormalOrJustObservable(
                                holder.maxRevisionUnits,
                                holder.actualInfo
                                        .getUnits()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getUnits(holder.maxRevisionUnits)
                        );
                    }
                })
                .flatMap(new Func1<List<Unit>, Observable<List<Organization>>>() {
                    @Override
                    public Observable<List<Organization>> call(List<Unit> units) {
                        realmExchangeOperations.updateBulkList(units);

                        return getNormalOrJustObservable(
                                holder.maxRevisionOrganization,
                                holder.actualInfo
                                        .getOrganizations()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getOrganisations(holder.maxRevisionOrganization)
                        );
                    }
                })
                .flatMap(new Func1<List<Organization>, Observable<List<RegistryEntry>>>() {
                    @Override
                    public Observable<List<RegistryEntry>> call(List<Organization> organizations) {
                        realmExchangeOperations.updateBulkList(organizations);

                        return getNormalOrJustObservable(
                                holder.maxRevisionRealization,
                                holder.actualInfo
                                        .getRealization()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getRealization(holder.maxRevisionRealization)
                        );
                    }
                })
                .flatMap(new Func1<List<RegistryEntry>, Observable<List<RegistryEntry>>>() {
                    @Override
                    public Observable<List<RegistryEntry>> call(final List<RegistryEntry> registryEntries) {
                        addRegistryEntries(registryEntries, RegistryEntry.REALISATION);
                        return getNormalOrJustObservable(
                                holder.maxRevisionOrders,
                                holder.actualInfo
                                        .getOrders()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getOrders(holder.maxRevisionOrders)
                        );
                    }
                })
                .flatMap(new Func1<List<RegistryEntry>, Observable<List<RegistryEntry>>>() {
                    @Override
                    public Observable<List<RegistryEntry>> call(final List<RegistryEntry> registryEntries) {
                        addRegistryEntries(registryEntries, RegistryEntry.ORDER);
                        return getNormalOrJustObservable(
                                holder.maxRevisionCashInOrders,
                                holder.actualInfo
                                        .getCashInOrders()
                                        .getLastRevision(),
                                nupiMeteorApi
                                        .getCashInOrders(holder.maxRevisionCashInOrders)
                        );
                    }
                })
                .flatMap(new Func1<List<RegistryEntry>, Observable<List<ChatRoute>>>() {
                    @Override
                    public Observable<List<ChatRoute>> call(final List<RegistryEntry> registryEntries) {
                        addRegistryEntries(registryEntries, RegistryEntry.PAYMENT);

                        return nupiServerApi.getChatRoutes();
                    }
                })

                .flatMap(new Func1<List<ChatRoute>, Observable<List<ChatUser>>>() {
                    @Override
                    public Observable<List<ChatUser>> call(List<ChatRoute> chatRoutes) {

                        try (Realm realm = realmChatOperations.getRealm()) {
                            realmChatOperations.copyToRealmOrUpdateChatRoutes(realm, chatRoutes);
                        }

                        return nupiServerApi.getChatUsers();
                    }
                })
                .flatMap(new Func1<List<ChatUser>, Observable<List<ChatGroup>>>() {
                    @Override
                    public Observable<List<ChatGroup>> call(List<ChatUser> chatUsers) {
                        try (Realm realm = realmChatOperations.getRealm()) {
                            realmChatOperations.copyToRealmOrUpdateChatUsers(realm, chatUsers);
                        }

                        return nupiServerApi.getChatGroups();
                    }
                })
                .flatMap(new Func1<List<ChatGroup>, Observable<List<ChatMessage>>>() {
                    @Override
                    public Observable<List<ChatMessage>> call(List<ChatGroup> chatGroups) {
                        try (Realm realm = realmChatOperations.getRealm()) {
                            realmChatOperations.copyToRealmOrUpdateChatGroups(realm, chatGroups);
                        }

                        return nupiServerApi.getAllMessages(holder.maxSynchronizedDate, null);
                    }
                })
                .flatMap(new Func1<List<ChatMessage>, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(final List<ChatMessage> chatMessages) {
                        try (Realm realm = realmChatOperations.getRealm()) {
                            for (ChatMessage chatMessage : chatMessages)
                                chatMessage.setWasSynchronized(true);
                            realmChatOperations.copyToRealmOrUpdateChatMessages(realm, chatMessages);
                        }

                        return Observable.just(null);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                        uploadService.sendDataToServer();
                        nupiPreferences.setLastSyncTime(TimeUtils.currentTime());
                        bus.post(new UpdateWorkflowEvent(UpdateStatus.UPDATE_FINISHED));
                        Log.d("Meteor", "OK");
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (BuildConfig.DEBUG)
                            e.printStackTrace();
                        bus.post(new UpdateWorkflowEvent(UpdateStatus.UPDATE_FAILED));
                        nupiPreferences.setLastSyncTime(NupiPreferences.NO_SYNC);
                        if (e instanceof RealmIOException) {
                            Crashlytics.logException(e);
                            return;
                        }
                        switch (NetworkUtils.getNetworkError(e)) {
                            case NetworkUtils.NETWORK_ERROR:
                                if (nupiFragment.isVisible())
                                    MessageBox.show(nupiFragment.getString(R.string.no_server_connection), nupiFragment.getActivity());
                                break;
                            case 403:
                                if (nupiFragment.isVisible())
                                    MessageBox.show(nupiFragment.getString(R.string.forbidden), nupiFragment.getActivity());
                                break;
                            case 401:
                                if (nupiFragment.isVisible()) {
                                    nupiPreferences.deleteAccount();
                                    Intent intent = new Intent(nupiFragment.getNupiApp(), MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    nupiFragment.getActivity().startActivity(intent);
                                    nupiFragment.getActivity().finish();
                                }
                                break;
                            case 500:
                            case 501:
                            case 502:
                            case 503:
                                if (nupiFragment.isVisible())
                                    MessageBox.show(nupiFragment.getString(R.string.server_error), nupiFragment.getActivity());
                                break;
                            default:
                                Crashlytics.logException(e);
                                if (nupiFragment.isVisible())
                                    MessageBox.show(nupiFragment.getString(R.string.go_to_admins), nupiFragment.getActivity());
                                break;
                        }
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }
                });
    }


    private void addRegistryEntries(List<RegistryEntry> registryEntries, int type) {
        final Realm realm = realmRegistryOperations.getRealm();
        try {
            for (RegistryEntry registryEntry : registryEntries) {
                final String guid = registryEntry.getGuid();
                registryEntry.setEntryType(type);
                if (registryEntry.isDeleted() || registryEntry.isMarkRemoval())
                    registryEntry.setEntryStatus(RegistryEntry.REJECTED);
                else if (registryEntry.isConduct())
                    registryEntry.setEntryStatus(RegistryEntry.ACCEPTED);
                else
                    registryEntry.setEntryStatus(RegistryEntry.RECEIVED);

                for (PriceEntry priceEntry : registryEntry.getOrderItems())
                    priceEntry.setOwnerDocument(guid);
                realm.beginTransaction();
                realmRegistryOperations.getPriceEntriesOfRealisation(realm, registryEntry).clear();
                realm.copyToRealm(registryEntry.getOrderItems());
                realm.commitTransaction();
            }
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(registryEntries);
        } finally {
            realm.commitTransaction();
            realm.close();
        }
    }

    private <T> Observable<List<T>> getNormalOrJustObservable(
            long savedRevision,
            long actualRevision,
            Observable<List<T>> observable
    ) {
        if (savedRevision != actualRevision)
            return observable;
        final List<T> value = new ArrayList<>();
        return Observable.just(value);
    }

    private static class EndpointsInfoHolder {
        private Endpoints actualInfo;


        private long maxRevisionPaymentByOrder;
        private long maxRevisionPayment;
        private long maxRevisionRealization;
        private long maxRevisionOrders;
        private long maxRevisionCashInOrders;
        private long maxRevisionExpandedPayment;
        private long maxRevisionStock;
        private long maxRevisionSale;
        private long maxRevisionContract;
        private long maxRevisionCounterAgent;
        private long maxRevisionOrganization;
        private long maxRevisionNomenclature;
        private long maxRevisionNomenclaturePrice;
        private long maxRevisionUnits;
        private Date maxSynchronizedDate;
    }

}
