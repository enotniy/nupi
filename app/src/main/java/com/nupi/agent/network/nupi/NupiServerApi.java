package com.nupi.agent.network.nupi;

import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatRoute;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.network.nupi.models.SendingChatMessage;

import java.util.Date;
import java.util.List;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import rx.Observable;

/**
 * Created by Pasenchuk Victor on 17.07.15
 */
public interface NupiServerApi {

    @Multipart
    @POST("/backups/")
    Observable<Void> sendBackup(@Part("backup") TypedFile file);

    @GET("/last-backup/")
    Observable<Response> getLastBackup();

    @GET("/chat/users")
    Observable<List<ChatUser>> getChatUsers();

    @GET("/chat/routes")
    Observable<List<ChatRoute>> getChatRoutes();

    @POST("/chat/updateReadStatus")
    Observable<Void> updateReadingStatus(@Body List<String> messageIds);

    @GET("/chat/users/{id}")
    Observable<List<ChatMessage>> getChatUserMessages(@Path("id") String id);

    @GET("/chat/groups/{id}")
    Observable<List<ChatMessage>> getChatGroupMessages(@Path("id") String id);

    @GET("/chat/users/{id}")
    List<ChatMessage> getChatUserMessagesSync(@Path("id") String id);

    @GET("/chat/groups/{id}")
    List<ChatMessage> getChatGroupMessagesSync(@Path("id") String id);

    @GET("/chat/allMessages")
    Observable<List<ChatMessage>> getAllMessages(@Query("from") Date fromDate, @Query("to") Date toDate);

    @Multipart
    @PUT("/chat/users/{id}/")
    Observable<ChatMessage> sendChatUserMessage(@Path("id") String id, @Part("body") SendingChatMessage sendingChatMessage, @Part("attachment") TypedFile file);

    @Multipart
    @PUT("/chat/groups/{id}/")
    Observable<ChatMessage> sendChatGroupMessage(@Path("id") String id, @Part("body") SendingChatMessage sendingChatMessage, @Part("attachment") TypedFile file);

    @GET("/chat/groups")
    Observable<List<ChatGroup>> getChatGroups();

    @FormUrlEncoded
    @POST("/device/gcm/")
    Observable<Response> gcmRegister(@Field("name") String name, @Field("device_id") String device_id, @Field("registration_id") String registration_id);
}
