package com.nupi.agent.network.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Pasenchuk Victor on 09.01.16
 */
public class BaseResponse {

    @SerializedName("acceptedGuids")
    @Expose
    private List<String> acceptedGuids = new ArrayList<>();
    @SerializedName("errorGuids")
    @Expose
    private Map<String, Object> errorGuids;
    @SerializedName("ok")
    @Expose
    private boolean ok;

    public List<String> getAcceptedGuids() {
        return acceptedGuids;
    }

    public Map<String, Object> getErrorGuids() {
        return errorGuids;
    }

    public boolean isOk() {
        return ok;
    }
}
