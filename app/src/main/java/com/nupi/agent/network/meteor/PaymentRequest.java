package com.nupi.agent.network.meteor;

import com.google.gson.annotations.SerializedName;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pasenchuk Victor on 29.12.15
 */
public class PaymentRequest extends BaseRequest {

    @SerializedName("useCaDocuments")
    private final boolean useCaDocuments;

    @SerializedName("transcriptOfPayment")
    private final List<TranscriptOfPayment> transcriptOfPayment = new LinkedList<>();

    public PaymentRequest(PaymentByOrder paymentByOrder, double sum) {
        super();
        setCounterAgent(paymentByOrder.getCounterAgent());
        setOrganization(paymentByOrder.getOrganization());
        setContract(paymentByOrder.getContract());
        useCaDocuments = true;
        transcriptOfPayment.add(new TranscriptOfPayment(paymentByOrder.getDoc(), sum));
    }

    public PaymentRequest(Payment payment, double sum) {
        super();
        setCounterAgent(payment.getCounterAgent());
        setOrganization(payment.getOrganization());
        setContract(payment.getContract());
        useCaDocuments = false;
        transcriptOfPayment.add(new TranscriptOfPayment(null, sum));
    }

    public List<TranscriptOfPayment> getTranscriptOfPayment() {
        return transcriptOfPayment;
    }

}
