package com.nupi.agent.network.meteor;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pasenchuk Victor on 29.12.15
 */
public class OrderRequest extends BaseRequest {

    @SerializedName("orderItems")
    private List<OrderItem> orderItems = new LinkedList<>();

    public OrderRequest() {
        super();
    }

    public OrderRequest(String counterAgent) {
        super(counterAgent);
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public void addOrUpdateGood(String nomenclatureGuid, String unitGuid, double price, double quantity) {
        for (OrderItem orderItem : orderItems)
            if (orderItem.getNomenclature().equals(nomenclatureGuid)) {
                orderItem.setQuantity(quantity);
                orderItem.setPrice(price);
                orderItem.setUnit(unitGuid);
                return;
            }
        orderItems.add(new OrderItem(nomenclatureGuid, unitGuid, price, quantity));
    }

    public void removeGood(String nomenclatureGuid) {
        for (OrderItem orderItem : orderItems)
            if (orderItem.getNomenclature().equals(nomenclatureGuid)) {
                orderItems.remove(orderItem);
                break;
            }
    }

    public OrderItem getGood(String nomenclatureGuid) {
        for (OrderItem orderItem : orderItems)
            if (orderItem.getNomenclature().equals(nomenclatureGuid))
                return orderItem;
        return null;
    }

    public int getRecordsCount() {
        return orderItems.size();
    }

    public boolean hasOrders() {
        return getRecordsCount() > 0;
    }
}
