package com.nupi.agent.network.meteor;

import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.ExpandedPayment;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.database.models.meteor.NomenclaturePrice;
import com.nupi.agent.database.models.meteor.Organization;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.database.models.meteor.Sale;
import com.nupi.agent.database.models.meteor.Stock;
import com.nupi.agent.database.models.meteor.Unit;
import com.nupi.agent.database.models.registry.RegistryEntry;

import java.util.List;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by Pasenchuk Victor on 17.07.15
 */
public interface NupiMeteorApi {

    @GET("/exchange/mobile/organizations")
    Observable<List<Organization>> getOrganisations(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/counterAgents")
    Observable<List<CounterAgent>> getCounterAgents(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/nomenclature")
    Observable<List<Nomenclature>> getNomenclature(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/nomenclaturePrices")
    Observable<List<NomenclaturePrice>> getNomenclaturePrices(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/units")
    Observable<List<Unit>> getUnits(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/paymentsByOrders")
    Observable<List<PaymentByOrder>> getPaymentsByOrders(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/payments/expanded")
    Observable<List<ExpandedPayment>> getExpandedPayments(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/sale")
    Observable<List<Sale>> getSales(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/payments")
    Observable<List<Payment>> getPayments(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/stocks")
    Observable<List<Stock>> getStocks(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/contracts")
    Observable<List<Contract>> getContracts(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/realization")
    Observable<List<RegistryEntry>> getRealization(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/orders")
    Observable<List<RegistryEntry>> getOrders(@Header("Exchange-Revision") long exchangeRevision);

    @GET("/exchange/mobile/cashInOrders")
    Observable<List<RegistryEntry>> getCashInOrders(@Header("Exchange-Revision") long exchangeRevision);

    @POST("/exchange/mobile/orders")
    Observable<BaseResponse> sendOrders(@Body List<OrderRequest> orderRequests);

    @POST("/exchange/mobile/cashInOrders")
    Observable<BaseResponse> sendPayments(@Body List<PaymentRequest> orderRequests);

    @GET("/exchange/mobile/status")
    Observable<StatusResponse> getStatus(@Header("Exchange-Revision") long exchangeRevision);

}
