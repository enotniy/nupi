package com.nupi.agent.network.weather.models;

import com.google.gson.annotations.SerializedName;

public class Main {

    @SerializedName("grnd_level")
    private Number grnd_level;
    @SerializedName("humidity")
    private Number humidity;
    @SerializedName("pressure")
    private Number pressure;
    @SerializedName("sea_level")
    private Number sea_level;
    @SerializedName("temp")
    private Number temp;
    @SerializedName("temp_max")
    private Number temp_max;
    @SerializedName("temp_min")
    private Number temp_min;

    public Number getGrnd_level() {
        return this.grnd_level;
    }

    public void setGrnd_level(Number grnd_level) {
        this.grnd_level = grnd_level;
    }

    public Number getHumidity() {
        return this.humidity;
    }

    public void setHumidity(Number humidity) {
        this.humidity = humidity;
    }

    public Number getPressure() {
        return this.pressure;
    }

    public void setPressure(Number pressure) {
        this.pressure = pressure;
    }

    public Number getSea_level() {
        return this.sea_level;
    }

    public void setSea_level(Number sea_level) {
        this.sea_level = sea_level;
    }

    public Number getTemp() {
        return this.temp;
    }

    public void setTemp(Number temp) {
        this.temp = temp;
    }

    public Number getTemp_max() {
        return this.temp_max;
    }

    public void setTemp_max(Number temp_max) {
        this.temp_max = temp_max;
    }

    public Number getTemp_min() {
        return this.temp_min;
    }

    public void setTemp_min(Number temp_min) {
        this.temp_min = temp_min;
    }
}
