package com.nupi.agent.network.meteor;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasenchuk Victor on 12.01.16
 */
public class TranscriptOfPayment {
    @SerializedName("doc")
    private final String doc;
    @SerializedName("sum")
    private final double sum;

    public TranscriptOfPayment(String doc, double sum) {
        this.doc = doc;
        this.sum = sum;
    }

    public String getDoc() {
        return doc;
    }

    public double getSum() {
        return sum;
    }
}
