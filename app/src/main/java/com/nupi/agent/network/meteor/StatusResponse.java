package com.nupi.agent.network.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pasencukviktor on 23/02/16
 */
public class StatusResponse {

    @SerializedName("endpoints")
    @Expose
    private Endpoints endpoints;

    public Endpoints getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(Endpoints endpoints) {
        this.endpoints = endpoints;
    }

}
