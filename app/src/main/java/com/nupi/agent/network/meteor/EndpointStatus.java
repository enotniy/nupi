package com.nupi.agent.network.meteor;

/**
 * Created by pasencukviktor on 23/02/16
 */
public enum EndpointStatus {
    OK,
    RESET
}
