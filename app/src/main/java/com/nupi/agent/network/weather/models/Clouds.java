package com.nupi.agent.network.weather.models;

import com.google.gson.annotations.SerializedName;

public class Clouds {
    @SerializedName("all")
    private Number all;

    public Number getAll() {
        return this.all;
    }

    public void setAll(Number all) {
        this.all = all;
    }
}
