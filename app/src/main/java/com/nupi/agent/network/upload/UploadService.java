package com.nupi.agent.network.upload;

import android.os.Handler;

import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.application.service.GeolocationService;
import com.nupi.agent.database.RealmRegistryOperations;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.network.meteor.BaseRequest;
import com.nupi.agent.network.meteor.BaseResponse;
import com.nupi.agent.network.meteor.NupiMeteorApi;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.network.meteor.OrderRequest;
import com.nupi.agent.network.meteor.PaymentRequest;
import com.nupi.agent.network.meteor.TranscriptOfPayment;
import com.nupi.agent.network.nupi.NupiServerApi;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by Pasenchuk Victor on 15.01.15
 */

public class UploadService {


    private static final long FIVE_SECONDS = 5_000;
    private static final long SIX_MINUTES = 6 * 60_000;

    private final Handler handler;
    private final NupiApp nupiApp;
    private final List<OrderRequest> orderRequestsList;
    private final List<PaymentRequest> paymentRequestList;
    @Inject
    NupiMeteorApi nupiMeteorApi;
    @Inject
    NupiPreferences sharedPreferences;
    @Inject
    GeolocationService geolocationService;
    @Inject
    RealmRegistryOperations realmRegistryOperations;
    @Inject
    NupiServerApi nupiServerApi;
    private boolean isUpdating = false;


    public UploadService(NupiApp app) {
        this.nupiApp = app;
        app.getAppComponent().inject(this);
        orderRequestsList = sharedPreferences.getOrderRequests();
        paymentRequestList = sharedPreferences.getPaymentRequests();
        handler = new Handler();
    }

    public void sendDataToServer() {
        if (!isUpdating && (orderRequestsList.size() > 0 || paymentRequestList.size() > 0)) {
            isUpdating = true;

            nupiMeteorApi
                    .sendOrders(orderRequestsList)
                    .flatMap(new Func1<BaseResponse, Observable<BaseResponse>>() {
                        @Override
                        public Observable<BaseResponse> call(BaseResponse baseResponse) {
                            processResponse(baseResponse, orderRequestsList);
                            sharedPreferences.setOrderRequests(orderRequestsList);
                            return nupiMeteorApi.sendPayments(paymentRequestList);
                        }
                    })
                    .subscribe(new Subscriber<BaseResponse>() {
                        @Override
                        public void onCompleted() {
                            isUpdating = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            isUpdating = false;
                        }

                        @Override
                        public void onNext(BaseResponse baseResponse) {
                            processResponse(baseResponse, paymentRequestList);
                            sharedPreferences.setPaymentRequests(paymentRequestList);
                        }
                    });
        } else
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendDataToServer();
                }
            }, FIVE_SECONDS);
    }

    private void processResponse(BaseResponse baseResponse, List<? extends BaseRequest> baseRequests) {
        try (Realm realm = realmRegistryOperations.getRealm()) {
            for (String guid : baseResponse.getAcceptedGuids()) {
                realmRegistryOperations.updateReportEntryWasSent(realm, guid);
                removeGuidFromList(baseRequests, guid);
            }
            for (String guid : baseResponse.getErrorGuids().keySet()) {
                realmRegistryOperations.updateReportEntryWasRejected(realm, guid);
                removeGuidFromList(baseRequests, guid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void removeGuidFromList(List<? extends BaseRequest> baseRequests, String guid) {
        for (int i = 0; i < baseRequests.size(); i++) {
            final BaseRequest orderRequest = baseRequests.get(i);
            if (orderRequest.getGuid().equals(guid)) {
                baseRequests.remove(i);
                break;
            }
        }
    }

    public void sendOrderDocToServer(final OrderRequest orderRequest) {
        final String counterAgentGuid = orderRequest.getCounterAgent();
        orderRequestsList.add(orderRequest);
        sharedPreferences.setOrderRequests(orderRequestsList);

        float sum = 0;
        for (OrderItem orderItem : orderRequest.getOrderItems())
            sum += orderItem.getSum();

        Realm realm = realmRegistryOperations.getRealm();
        realmRegistryOperations.addReportEntry(realm, orderRequest.getGuid(), counterAgentGuid, sum, RegistryEntry.REALISATION, orderRequest.getOrganization(), orderRequest.getOrderItems(), orderRequest.getComment());
        realm.close();

        sendDataToServer();

    }

    public void sendPaymentDocsToServer(final List<PaymentRequest> paymentRequests) {
        for (PaymentRequest paymentRequest : paymentRequests)
            addPaymentDoc(paymentRequest);

        sharedPreferences.setPaymentRequests(paymentRequestList);
        sendDataToServer();
    }

    public void sendPaymentDocToServer(final PaymentRequest paymentRequest) {
        addPaymentDoc(paymentRequest);
        sendDataToServer();
    }

    private void addPaymentDoc(PaymentRequest paymentRequest) {
        paymentRequestList.add(paymentRequest);
        TranscriptOfPayment transcriptOfPayment = paymentRequest.getTranscriptOfPayment().get(0);
        Realm realm = realmRegistryOperations.getRealm();
        realmRegistryOperations.addReportEntry(realm, paymentRequest.getGuid(), paymentRequest.getCounterAgent(), transcriptOfPayment.getSum(), RegistryEntry.PAYMENT, paymentRequest.getOrganization(), new LinkedList<OrderItem>(), "");
        realm.close();
    }

}
