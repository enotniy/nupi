package com.nupi.agent.network.weather.models;

import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("deg")
    private Number deg;
    @SerializedName("speed")
    private Number speed;

    public Number getDeg() {
        return this.deg;
    }

    public void setDeg(Number deg) {
        this.deg = deg;
    }

    public Number getSpeed() {
        return this.speed;
    }

    public void setSpeed(Number speed) {
        this.speed = speed;
    }
}
