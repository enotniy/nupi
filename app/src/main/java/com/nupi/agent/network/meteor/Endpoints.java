package com.nupi.agent.network.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pasencukviktor on 23/02/16
 */
public class Endpoints {

    @SerializedName("counterAgents")
    @Expose
    private EndpointInfo counterAgents;
    @SerializedName("nomenclature")
    @Expose
    private EndpointInfo nomenclature;
    @SerializedName("units")
    @Expose
    private EndpointInfo units;
    @SerializedName("nomenclaturePrices")
    @Expose
    private EndpointInfo nomenclaturePrices;
    @SerializedName("paymentsByOrders")
    @Expose
    private EndpointInfo paymentsByOrders;
    @SerializedName("paymentsByOrders/expanded")
    @Expose
    private EndpointInfo paymentsByOrdersExpanded;
    @SerializedName("payments")
    @Expose
    private EndpointInfo payments;
    @SerializedName("payments/expanded")
    @Expose
    private EndpointInfo paymentsExpanded;
    @SerializedName("stocks")
    @Expose
    private EndpointInfo stocks;
    @SerializedName("organizations")
    @Expose
    private EndpointInfo organizations;
    @SerializedName("contracts")
    @Expose
    private EndpointInfo contracts;
    @SerializedName("sale")
    @Expose
    private EndpointInfo sale;
    @SerializedName("realization")
    @Expose
    private EndpointInfo realization;
    @SerializedName("orders")
    @Expose
    private EndpointInfo orders;
    @SerializedName("cashInOrders")
    @Expose
    private EndpointInfo cashInOrders;

    public EndpointInfo getCounterAgents() {
        return counterAgents;
    }

    public void setCounterAgents(EndpointInfo counterAgents) {
        this.counterAgents = counterAgents;
    }

    public EndpointInfo getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(EndpointInfo nomenclature) {
        this.nomenclature = nomenclature;
    }

    public EndpointInfo getUnits() {
        return units;
    }

    public void setUnits(EndpointInfo units) {
        this.units = units;
    }

    public EndpointInfo getNomenclaturePrices() {
        return nomenclaturePrices;
    }

    public void setNomenclaturePrices(EndpointInfo nomenclaturePrices) {
        this.nomenclaturePrices = nomenclaturePrices;
    }

    public EndpointInfo getPaymentsByOrders() {
        return paymentsByOrders;
    }

    public void setPaymentsByOrders(EndpointInfo paymentsByOrders) {
        this.paymentsByOrders = paymentsByOrders;
    }

    public EndpointInfo getPaymentsByOrdersExpanded() {
        return paymentsByOrdersExpanded;
    }

    public void setPaymentsByOrdersExpanded(EndpointInfo paymentsByOrdersExpanded) {
        this.paymentsByOrdersExpanded = paymentsByOrdersExpanded;
    }

    public EndpointInfo getPayments() {
        return payments;
    }

    public void setPayments(EndpointInfo payments) {
        this.payments = payments;
    }

    public EndpointInfo getPaymentsExpanded() {
        return paymentsExpanded;
    }

    public void setPaymentsExpanded(EndpointInfo paymentsExpanded) {
        this.paymentsExpanded = paymentsExpanded;
    }

    public EndpointInfo getStocks() {
        return stocks;
    }

    public void setStocks(EndpointInfo stocks) {
        this.stocks = stocks;
    }

    public EndpointInfo getOrganizations() {
        return organizations;
    }

    public void setOrganizations(EndpointInfo organizations) {
        this.organizations = organizations;
    }

    public EndpointInfo getContracts() {
        return contracts;
    }

    public void setContracts(EndpointInfo contracts) {
        this.contracts = contracts;
    }

    public EndpointInfo getSale() {
        return sale;
    }

    public void setSale(EndpointInfo sale) {
        this.sale = sale;
    }

    public EndpointInfo getRealization() {
        return realization;
    }

    public void setRealization(EndpointInfo realization) {
        this.realization = realization;
    }

    public EndpointInfo getOrders() {
        return orders;
    }

    public void setOrders(EndpointInfo orders) {
        this.orders = orders;
    }

    public EndpointInfo getCashInOrders() {
        return cashInOrders;
    }

    public void setCashInOrders(EndpointInfo cashInOrders) {
        this.cashInOrders = cashInOrders;
    }
}
