package com.nupi.agent.network.nupi.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 20.10.2015.
 */
public class SendingChatMessage {

    public static final String
            TYPE_TEXT = "TEXT",
            TYPE_GEOLOCATION = "GEOLOCATION",
            TYPE_IMAGE = "IMAGE";


    @SerializedName("type")
    public String type;
    @SerializedName("topic")
    public String topic;
    @SerializedName("message")
    public String message;
    @SerializedName("latitude")
    public Double latitude;
    @SerializedName("longitude")
    public Double longitude;
    @SerializedName("altitude")
    public Double altitude;

    public SendingChatMessage(String message, String type) {
        this.type = type;
        this.message = message;
    }
}
