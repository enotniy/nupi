package com.nupi.agent.network.nupi;

import com.nupi.agent.network.nupi.models.Auth;
import com.nupi.agent.network.nupi.models.Token;

import retrofit.http.Body;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by Pasenchuk Victor on 17.07.15
 */
public interface NupiAuthApi {

    @POST("/device/auth/")
    Observable<Token> getToken(@Body Auth auth);

}
