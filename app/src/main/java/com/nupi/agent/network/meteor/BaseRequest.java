package com.nupi.agent.network.meteor;

import com.google.gson.annotations.SerializedName;
import com.nupi.agent.helpers.GuidGenerator;

/**
 * Created by Pasenchuk Victor on 12.01.16
 */
public abstract class BaseRequest {
    @SerializedName("guid")
    private String guid;
    @SerializedName("counterAgent")
    private String counterAgent;
    @SerializedName("organization")
    private String organization;
    @SerializedName("contract")
    private String contract;
    @SerializedName("comment")
    private String comment;


    public BaseRequest(String counterAgent) {
        this();
        this.counterAgent = counterAgent;
    }

    public BaseRequest() {
        setRandomGuid();
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCounterAgent() {
        return counterAgent;
    }

    public void setCounterAgent(String counterAgent) {
        this.counterAgent = counterAgent;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public void setRandomGuid() {
        setGuid(GuidGenerator.randomGUID().toLowerCase());
    }

}
