package com.nupi.agent.network.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.helpers.RealmGsonHelper;
import com.nupi.agent.ui.controllers.chat.activities.ChatActivity;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by wildf on 15.10.2015.
 */
public class NupiGcmListenerService extends GcmListenerService {

    public static final String
            TYPE_CHAT_MESSAGE = "CHAT_MESSAGE",
            TYPE_CHAT_GROUP = "CHAT_GROUP";

    public static final String
            TYPE = "TYPE",
            DATA = "DATA";

    @Override
    public void onCreate() {
        Log.d("GCM MESSAGE", "GCM Listener created");
        super.onCreate();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        final String type = data.getString(TYPE);
        final String dataString = data.getString(DATA);

        if (type != null && dataString != null) {
            Log.d("GCM MESSAGE", dataString);
            Log.d("GCM MESSAGE", type);
            switch (type) {
                case TYPE_CHAT_MESSAGE:
                    receiveMessage(dataString);
                    break;
                case TYPE_CHAT_GROUP:
                    receiveNewGroupInfo(dataString);
                    break;
            }
        }

    }

    private void receiveMessage(final String dataString) {
        //TODO: inject

        final RealmChatOperations realmChatOperations = new RealmChatOperations(getApplication());
        final Gson gson = RealmGsonHelper.getRealmGson();
        final ChatMessage chatMessage = gson.fromJson(dataString, ChatMessage.class);

        if (chatMessage != null) {
            final Realm chatRealm = realmChatOperations.getRealm();
            final boolean updateMessage = realmChatOperations.isMessageExists(chatRealm, chatMessage.getId());

            realmChatOperations.copyToRealmOrUpdateChatMessage(chatRealm, chatMessage);

            final String user_id = new NupiPreferences(getApplicationContext()).getChatId();

            final long unreadMessageCount = realmChatOperations.getUnreadMessageCount(chatRealm, user_id);
            chatRealm.close();

            if (!updateMessage && !chatMessage.getSender().equals(user_id))
                showNotification(chatMessage, unreadMessageCount);
        }
    }

    private void receiveNewGroupInfo(final String dataString) {
        //TODO: inject


        final RealmChatOperations realmChatOperations = new RealmChatOperations(getApplication());
        final Gson gson = RealmGsonHelper.getRealmGson();
        final ChatGroup chatGroup = gson.fromJson(dataString, ChatGroup.class);

        if (chatGroup != null) {

            List<ChatGroup> chatGroups = new ArrayList<>();
            chatGroups.add(chatGroup);

            try (Realm realm = realmChatOperations.getRealm()) {
                realmChatOperations.copyToRealmOrUpdateChatGroups(realm, chatGroups);
            }

            // TODO: 01/03/16 inform about new group
        }
    }

    private void showNotification(ChatMessage chatMessage, long unreadMessageCount) {
        String message = "";


        Intent intent = new Intent(this, ChatActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.putExtra(ChatActivity.KEY_IS_FROM_NOTIFICATION, true);

        if (chatMessage != null) {
            message = chatMessage.getMessage();
            intent.putExtra(ChatActivity.KEY_IS_FROM_NOTIFICATION, true);
            intent.putExtra(ChatActivity.KEY_IS_PRIVATE_MESSAGE, chatMessage.getGroup() == null);
            intent.putExtra(ChatActivity.KEY_THREAD_ID, chatMessage.getGroup() != null ? chatMessage.getGroup() : chatMessage.getSender());
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

// TODO: 18/01/16 change text: show all messages
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(unreadMessageCount == 1 ? getString(R.string.incoming_message) : getString(R.string.incoming_messages))
                .setContentText(unreadMessageCount == 1 ? message : String.format(getString(R.string.incoming_messages_count), unreadMessageCount))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

}
