package com.nupi.agent.network.meteor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pasencukviktor on 23/02/16
 */
public class EndpointInfo {

    @SerializedName("lastRevision")
    @Expose
    private long lastRevision;

    @SerializedName("filterRevision")
    @Expose
    private long filterRevision;

    @SerializedName("status")
    @Expose
    private EndpointStatus endpointStatus;

    public long getLastRevision() {
        return lastRevision;
    }

    public void setLastRevision(long lastRevision) {
        this.lastRevision = lastRevision;
    }

    public long getFilterRevision() {
        return filterRevision;
    }

    public void setFilterRevision(long filterRevision) {
        this.filterRevision = filterRevision;
    }

    public EndpointStatus getEndpointStatus() {
        return endpointStatus;
    }

    public void setEndpointStatus(EndpointStatus endpointStatus) {
        this.endpointStatus = endpointStatus;
    }
}
