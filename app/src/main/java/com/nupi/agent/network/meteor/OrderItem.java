package com.nupi.agent.network.meteor;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasenchuk Victor on 29.12.15
 */
public class OrderItem {

    @SerializedName("nomenclature")
    private String nomenclature;
    @SerializedName("unit")
    private String unit;
    @SerializedName("sum")
    private double sum;
    @SerializedName("price")
    private double price;
    @SerializedName("quantity")
    private double quantity;

    public OrderItem(String nomenclature) {
        this.nomenclature = nomenclature;
    }

    public OrderItem(String nomenclature, String unit, double price, double quantity) {
        this.nomenclature = nomenclature;
        this.unit = unit;
        this.price = price;
        this.quantity = quantity;
        updateSum();
    }

    public String getNomenclature() {
        return nomenclature;
    }

    public void setNomenclature(String nomenclature) {
        this.nomenclature = nomenclature;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getSum() {
        return sum;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
        updateSum();
    }

    private void updateSum() {
        sum = price * quantity;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
        updateSum();
    }

}
