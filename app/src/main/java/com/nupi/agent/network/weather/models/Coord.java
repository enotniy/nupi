package com.nupi.agent.network.weather.models;

import com.google.gson.annotations.SerializedName;

public class Coord {
    @SerializedName("lat")
    private Number lat;
    @SerializedName("lon")
    private Number lon;

    public Number getLat() {
        return this.lat;
    }

    public void setLat(Number lat) {
        this.lat = lat;
    }

    public Number getLon() {
        return this.lon;
    }

    public void setLon(Number lon) {
        this.lon = lon;
    }
}
