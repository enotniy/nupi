package com.nupi.agent.network.weather;

import com.nupi.agent.network.weather.models.CurrentWeather;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Pasenchuk Victor on 16.01.15
 */
public interface OpenWeatherMapWebService {
    @GET("/weather?units=metric&APPID=8d534fe230133fe6b34cfc5dc684baf3")
    Observable<CurrentWeather> fetchCurrentWeather(@Query("lon") double longitude,
                                                   @Query("lat") double latitude);

    @GET("/forecast/daily?units=metric&cnt=7&APPID=8d534fe230133fe6b34cfc5dc684baf3")
    Observable<CurrentWeather> fetchWeatherForecasts(
            @Query("lon") double longitude, @Query("lat") double latitude);
}
