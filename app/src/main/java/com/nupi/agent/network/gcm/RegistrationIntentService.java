package com.nupi.agent.network.gcm;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.network.nupi.NupiServerApi;

import java.io.IOException;

import javax.inject.Inject;

import retrofit.client.Response;
import rx.functions.Action1;

/**
 * Created by wildf on 15.10.2015.
 */
public class RegistrationIntentService extends IntentService {

    public static final String SENDER_ID = "725160552840";
    @Inject
    NupiServerApi nupiServerApi;

    public RegistrationIntentService() {
        super("RegistrationIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((NupiApp) getApplication()).getAppComponent().inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final NupiPreferences prefs = new NupiPreferences(getApplicationContext());
        String registration_id = null;
        String name = android.os.Build.MODEL;
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            registration_id = instanceID.getToken(SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

        } catch (IOException e) {
            prefs.setGcmIsNupiRegistered(false);
        }

        if (registration_id != null) {
            prefs.setGcmRegistrationId(registration_id);

            nupiServerApi.gcmRegister(name, null, registration_id)
                    .subscribe(new Action1<Response>() {
                                   @Override
                                   public void call(Response response) {
                                       prefs.setGcmIsNupiRegistered(true);
                                   }
                               }, new Action1<Throwable>() {
                                   @Override
                                   public void call(Throwable throwable) {
                                       prefs.setGcmIsNupiRegistered(false);
                                   }
                               }
                    );
        } else
            prefs.setGcmIsNupiRegistered(false);


    }

}
