package com.nupi.agent.helpers;

import com.nupi.agent.database.models.meteor.Nomenclature;

import java.util.Comparator;

/**
 * Created by Pasenchuk Victor on 27.04.15
 */
public class NomenclatureComparator implements Comparator<Nomenclature> {
    @Override
    public int compare(Nomenclature ref_price, Nomenclature t1) {
        if (ref_price.getName() != null) {
            if (t1.getName() != null) {
                return ref_price.getName().compareToIgnoreCase(t1.getName());
            }
            return 1;
        }
        return -1;
    }
}

