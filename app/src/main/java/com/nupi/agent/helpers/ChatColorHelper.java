package com.nupi.agent.helpers;

import android.content.Context;
import android.graphics.Color;

import com.nupi.agent.R;

/**
 * Created by Pasenchuk Victor on 15.01.16
 */
public class ChatColorHelper {

    private final String[] colorArray;

    public ChatColorHelper(Context context) {
        colorArray = context.getResources().getStringArray(R.array.colors_chat);
    }

    public int getColor(String id) {
        return Color.parseColor(colorArray[Math.abs(id.hashCode() % colorArray.length)]);
    }
}
