package com.nupi.agent.helpers;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.speech.RecognizerIntent;

import com.nupi.agent.utils.PlayMarketUtils;

import java.util.List;

/**
 * Created by Pasenchuk Victor on 02.01.15
 */
public class VoiceSearchHelper {
    private final Fragment fragment;
    private final int requestCode;
    private final Activity activity;

    public VoiceSearchHelper(Fragment fragment, int requestCode) {
        this.fragment = fragment;
        this.requestCode = requestCode;
        activity = fragment.getActivity();
    }

    public void runVoiceSearch() {

        if (isSpeechRecognitionActivityPresented()) {
            startRecognitionActivity();
        } else {
            installGoogleVoiceSearch();
        }
    }

    private boolean isSpeechRecognitionActivityPresented() {
        // получаем экземпляр менеджера пакетов
        PackageManager pm = activity.getPackageManager();
        // получаем список активити способных обработать запрос на
        // распознавание
        List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);

        return activities.size() != 0;

    }

    // установка с маркета
    private void installGoogleVoiceSearch() {

        final String appPackageName = "com.google.android.googlequicksearchbox";
        PlayMarketUtils.openPlayMarket(activity, appPackageName);
    }


    private void startRecognitionActivity() {

        // создаем Intent с действием RecognizerIntent.ACTION_RECOGNIZE_SPEECH
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        // добаляем дополнительные параметры:
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Голосовой поиск"); // текстовая подсказка пользователю
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH); // модель
        // распознавания
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1); // количество
        // резальтатов,
        // которое мы
        // хотим
        // получить
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ru-RU");

        // стартуем активити и ждем от нее результата
        fragment.startActivityForResult(intent, requestCode);
    }


}