package com.nupi.agent.helpers;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by Pasenchuk Victor on 15.01.15
 */
public class GuidGenerator {
    private static final SecureRandom random = new SecureRandom();

    public static String randomGUID() {
        String str = new BigInteger(128, random).toString(16);
        while (str.length() < 32)
            str = "0" + str;
        return new StringBuilder(str)
                .insert(8, "-")
                .insert(13, "-")
                .insert(18, "-")
                .insert(23, "-")
                .toString();
    }
}
