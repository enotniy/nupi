package com.nupi.agent.helpers;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.support.annotation.NonNull;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.BuildConfig;

import java.math.BigInteger;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.util.Calendar;

import javax.security.auth.x500.X500Principal;

/**
 * Created by wildf on 29.02.2016
 */

public class KeyStoreHelper {

    public static final int VALID_YEARS = 100;
    public static final int KEY_SIZE = 1024;
    private static final String
            NUPI_CERT_PASSWORD = "ions721*fibs";
    private KeyStore keyStore = null;
    private Context context;

    public KeyStoreHelper(Context context) {

        this.context = context;

        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
        } catch (Exception e) {
            logException(e);
        }

    }

    private void logException(Exception e) {
        Crashlytics.logException(e);
        if (BuildConfig.DEBUG)
            e.printStackTrace();
    }


    public byte[] getPrivateKey(@NonNull String keyAlias) {
        try {
            if (!keyStore.containsAlias(keyAlias))
                createKeyEntry(keyAlias);
            else
                Log.d("KEY EXISTS", keyAlias);

            return getPrivateKeyBytes(keyAlias);
        } catch (Exception e) {
            logException(e);
        }
        return null;
    }

    private byte[] getPrivateKeyBytes(@NonNull String keyAlias) throws Exception {

        final KeyStore.PrivateKeyEntry keyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(keyAlias, null);

        final PublicKey publicKey = keyEntry.getCertificate().getPublicKey();

        final MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(publicKey.getEncoded());
        return md.digest();
    }

    private void createKeyEntry(String keyAlias) throws Exception {

        Log.d("CREATE KEY", keyAlias);

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 1);
        KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                .setAlias(keyAlias)
                .setSubject(new X500Principal(String.format("CN=%s, O=Cardinal Directions LLC", context.getPackageName())))
                .setSerialNumber(BigInteger.ONE)
                .setStartDate(start.getTime())
                .setEndDate(end.getTime())
                .build();
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        generator.initialize(spec);
        generator.generateKeyPair();
    }

}
