package com.nupi.agent.helpers;

import com.nupi.agent.database.models.meteor.CounterAgent;

import java.util.Comparator;

/**
 * Created by Pasenchuk Victor on 27.04.15
 */
public class CounterAgentComparator implements Comparator<CounterAgent> {
    @Override
    public int compare(CounterAgent t0, CounterAgent t1) {
        if (t0.getName() != null) {
            if (t1.getName() != null) {
                return t0.getName().compareToIgnoreCase(t1.getName());
            }
            return 1;
        }
        return -1;
    }
}

