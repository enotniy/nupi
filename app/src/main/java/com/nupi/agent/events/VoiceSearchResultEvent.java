package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 02.01.15
 */
public class VoiceSearchResultEvent {

    private CharSequence searchString;

    public VoiceSearchResultEvent(CharSequence searchString) {
        this.searchString = searchString;
    }

    public CharSequence getSearchString() {
        return searchString;
    }
}
