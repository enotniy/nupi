package com.nupi.agent.events;

import com.nupi.agent.ui.controllers.analytics.activities.ReportAnalyticsActivity;

/**
 * Created by Pasenchuk Victor on 01.11.14
 */
public class ReportAnalyticsButtonEvent {


    private int typeReport = ReportAnalyticsActivity.REPORT_TYPE_GOODS;

    public ReportAnalyticsButtonEvent(int type) {
        this.typeReport = type;
    }

    public int getTypeReport() {
        return typeReport;
    }
}
