package com.nupi.agent.events;

import com.nupi.agent.enums.UpdateStatus;

/**
 * Created by Pasenchuk Victor on 02.03.15
 */
public class UpdateWorkflowEvent {
    public final UpdateStatus updateStatus;

    public UpdateWorkflowEvent(UpdateStatus updateStatus) {
        this.updateStatus = updateStatus;
    }
}
