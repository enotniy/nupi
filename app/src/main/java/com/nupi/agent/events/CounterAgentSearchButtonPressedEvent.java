package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 12.11.14
 */

public class CounterAgentSearchButtonPressedEvent {

    private boolean activate = true;

    public CounterAgentSearchButtonPressedEvent() {
    }

    public CounterAgentSearchButtonPressedEvent(boolean activate) {
        this.activate = activate;
    }

    public boolean isActivate() {
        return activate;
    }
}
