package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 24.05.15
 */
public class OpenGalleryEvent {
    public final String clientName;

    public OpenGalleryEvent(String clientName) {
        this.clientName = clientName;
    }
}
