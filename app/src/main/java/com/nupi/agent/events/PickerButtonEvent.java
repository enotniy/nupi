package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 01.11.14
 */
public class PickerButtonEvent {


    private int type;

    public PickerButtonEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
