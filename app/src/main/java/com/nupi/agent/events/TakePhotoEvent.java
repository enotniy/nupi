package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 20.05.15
 */
public class TakePhotoEvent {
    public final String clientName;

    public TakePhotoEvent(String path) {
        this.clientName = path;
    }
}
