package com.nupi.agent.events;

import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;

import java.util.List;

/**
 * Created by Pasenchuk Victor on 15.10.14
 */
public class PlannerCreateRouteEvent {

    private PlannerRoute route;
    private List<ClientInfo> clientInfo;

    public PlannerCreateRouteEvent() {
    }

    public PlannerCreateRouteEvent(List<ClientInfo> clientInfoList, PlannerRoute route) {
        this.route = route;
        this.clientInfo = clientInfoList;
    }

    public List<ClientInfo> getClientInfo() {
        return clientInfo;
    }

    public PlannerRoute getRoute() {
        return route;
    }

}
