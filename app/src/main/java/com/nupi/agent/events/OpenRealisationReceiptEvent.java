package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 08.09.15
 */
public class OpenRealisationReceiptEvent {
    public String realisationGuid;

    public OpenRealisationReceiptEvent(String realisationGuid) {
        this.realisationGuid = realisationGuid;
    }
}
