package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 14.11.14
 */
public class SearchStringEvent {

    private CharSequence searchString;

    public SearchStringEvent(CharSequence searchString) {
        this.searchString = searchString;
    }

    public CharSequence getSearchString() {
        return searchString;
    }
}
