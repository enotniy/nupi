package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 15.10.14
 */
public class PlannerRouteEvent {

    private String route;

    public PlannerRouteEvent(String route) {
        this.route = route;
    }

    public String getRoute() {
        return route;
    }
}
