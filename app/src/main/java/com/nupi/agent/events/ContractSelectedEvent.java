package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 25.01.15
 */
public class ContractSelectedEvent {

    private final String contractId;

    public ContractSelectedEvent(String contractId) {
        this.contractId = contractId;
    }

    public String getContractId() {
        return contractId;
    }


}
