package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 25.01.15
 */
public class RealisationSelectedEvent {
    public final String docId;

    public RealisationSelectedEvent(String docId) {
        this.docId = docId;
    }
}
