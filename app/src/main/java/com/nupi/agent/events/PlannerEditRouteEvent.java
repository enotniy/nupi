package com.nupi.agent.events;

import com.nupi.agent.database.models.planner.PlannerRoute;

/**
 * Created by Pasenchuk Victor on 15.10.14
 */
public class PlannerEditRouteEvent {

    private PlannerRoute route;

    public PlannerEditRouteEvent(PlannerRoute route) {
        this.route = route;
    }

    public PlannerRoute getRoute() {
        return route;
    }

}
