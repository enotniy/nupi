package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 28.09.15
 */
public class OpenGeolocationEvent {

    private OpenType openType = OpenType.NONE;

    public OpenGeolocationEvent(OpenType type) {
        this.openType = type;
    }

    public OpenType getOpenType() {
        return openType;
    }

    public enum OpenType {POINT, ROUTE, NONE}
}
