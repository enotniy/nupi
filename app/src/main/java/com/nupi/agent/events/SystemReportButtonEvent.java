package com.nupi.agent.events;

/**
 * Created by Pasenchuk Victor on 01.11.14
 */
public class SystemReportButtonEvent {

    private boolean inThePocket;

    public SystemReportButtonEvent() {
        this.inThePocket = false;
    }

    public SystemReportButtonEvent(boolean inThePocket) {
        this.inThePocket = inThePocket;
    }

    public boolean getInThePocket() {
        return inThePocket;
    }
}
