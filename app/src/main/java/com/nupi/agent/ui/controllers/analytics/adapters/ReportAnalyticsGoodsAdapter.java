package com.nupi.agent.ui.controllers.analytics.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.application.models.Tree;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.enums.SelectionType;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;


public class ReportAnalyticsGoodsAdapter extends RecyclerView.Adapter<ReportAnalyticsGoodsAdapter.ViewHolder> {

    List<Nomenclature> listClients;
    Picker picker;
    @Inject
    NupiPreferences preferences;
    @Inject
    RealmExchangeOperations realmExchangeOperations;
    private Runnable onElementsListChangeListener;
    private Realm realm;
    private Tree<String> tree;

    public ReportAnalyticsGoodsAdapter(Activity activity, Picker data, Realm realm, Runnable onElementsListChangeListener) {
        NupiApp nupiApp = (NupiApp) activity.getApplicationContext();
        nupiApp.getAppComponent().inject(this);
        this.picker = data;
        this.realm = realm;
        this.tree = realmExchangeOperations.getTreeIdNomenclature(realm);
        RealmResults<Nomenclature> prices = realmExchangeOperations.getNonFolderNomenclature(realm);
        listClients = new LinkedList<>();
        for (Nomenclature price : prices) {
            if (picker.getTypeSelection(price.getGuid()) == SelectionType.ALL && !price.isFolder())
                listClients.add(price);
        }
        this.onElementsListChangeListener = onElementsListChangeListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_report_filter_child, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return listClients.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Nomenclature item = listClients.get(position);
        holder.name.setText(String.valueOf(item.getName()));
        holder.setRef_price(listClients.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Nomenclature> getItems() {
        return listClients;
    }

    public void clear() {
        listClients.clear();
    }

    public Picker getPicker() {
        return picker;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.name)
        TextView name;
        @InjectView(R.id.remove)
        ImageView remove;
        @InjectView(R.id.row)
        LinearLayout row;
        private Nomenclature ref_price;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void setRef_price(Nomenclature ref_price) {
            this.ref_price = ref_price;
        }

        @OnClick(R.id.remove)
        void onItemclick() {
            if (listClients.contains(ref_price)) {
                listClients.remove(ref_price);
                picker.selectElement(tree.getTree(ref_price.getGuid()), SelectionType.NONE);
                if (onElementsListChangeListener != null) {
                    onElementsListChangeListener.run();
                }
                preferences.setPickerPrice(picker);
                notifyDataSetChanged();
            }
        }
    }
}

