package com.nupi.agent.ui.controllers.geolocation.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.NinePatchDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;
import com.nupi.agent.events.PlannerCreateRouteEvent;
import com.nupi.agent.events.PlannerRouteEvent;
import com.nupi.agent.helpers.RecycleViewItemClickSupport;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.geolocation.activities.GeolocationActivity;
import com.nupi.agent.ui.controllers.geolocation.adapters.GeolocationCounterAgentsMeteorAdapter;
import com.nupi.agent.ui.controllers.geolocation.adapters.GeolocationRouteMeteorAdapter;
import com.nupi.agent.ui.controllers.geolocation.adapters.GeolocationRoutesMeteorAdapter;
import com.nupi.agent.utils.ExternalFilesUtils;
import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.SKMaps;
import com.skobbler.ngx.SKMapsInitSettings;
import com.skobbler.ngx.map.SKAnimationSettings;
import com.skobbler.ngx.map.SKAnnotation;
import com.skobbler.ngx.map.SKAnnotationView;
import com.skobbler.ngx.map.SKCalloutView;
import com.skobbler.ngx.map.SKCoordinateRegion;
import com.skobbler.ngx.map.SKMapCustomPOI;
import com.skobbler.ngx.map.SKMapInternationalizationSettings;
import com.skobbler.ngx.map.SKMapPOI;
import com.skobbler.ngx.map.SKMapSettings;
import com.skobbler.ngx.map.SKMapSurfaceListener;
import com.skobbler.ngx.map.SKMapSurfaceView;
import com.skobbler.ngx.map.SKMapViewHolder;
import com.skobbler.ngx.map.SKMapViewStyle;
import com.skobbler.ngx.map.SKPOICluster;
import com.skobbler.ngx.map.SKScreenPoint;
import com.skobbler.ngx.navigation.SKAdvisorSettings;
import com.skobbler.ngx.navigation.SKNavigationListener;
import com.skobbler.ngx.navigation.SKNavigationManager;
import com.skobbler.ngx.navigation.SKNavigationSettings;
import com.skobbler.ngx.navigation.SKNavigationState;
import com.skobbler.ngx.positioner.SKCurrentPositionListener;
import com.skobbler.ngx.positioner.SKCurrentPositionProvider;
import com.skobbler.ngx.positioner.SKPosition;
import com.skobbler.ngx.positioner.SKPositionerManager;
import com.skobbler.ngx.routing.SKRouteInfo;
import com.skobbler.ngx.routing.SKRouteJsonAnswer;
import com.skobbler.ngx.routing.SKRouteListener;
import com.skobbler.ngx.routing.SKRouteManager;
import com.skobbler.ngx.routing.SKRouteSettings;
import com.skobbler.ngx.routing.SKViaPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class GeolocationFragment extends NupiFragment implements SKMapSurfaceListener, SKCurrentPositionListener, SKRouteListener, SKNavigationListener {


    private static final int LOCATION_PERMISSIONS_CHECK = 1;
    private static final String[] LOCATION_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    public boolean isRouteSelect = false;
    public boolean isPointSelect = false;


    @InjectView(R.id.route_name)
    TextView routeName;
    @InjectView(R.id.time_route)
    TextView timeRoute;
    @InjectView(R.id.length_route)
    TextView lengthRoute;
    @InjectView(R.id.list_route)
    RecyclerView listRoute;
    @InjectView(R.id.route)
    LinearLayout route;
    @InjectView(R.id.list_routes)
    RecyclerView listRoutes;
    @InjectView(R.id.routes)
    LinearLayout routes;
    @InjectView(R.id.list_points)
    RecyclerView listPoints;
    @InjectView(R.id.points)
    LinearLayout points;
    @InjectView(R.id.show_all_points)
    TextView showAllPoints;
    @InjectView(R.id.chess_board_background)
    View chessBackground;
    @InjectView(R.id.drive_button)
    Button driveButton;
    @InjectView(R.id.calculate_route)
    ImageView calculateRouteButton;
    @InjectView(R.id.map_display_mode)
    Button mapDisplayModeButton;
    String mapsFolder;
    @InjectView(R.id.routes_button)
    LinearLayout routesButton;
    @InjectView(R.id.points_button)
    LinearLayout pointsButton;
    @InjectView(R.id.pathImage)
    ImageView pathImage;
    @InjectView(R.id.pointImage)
    ImageView pointImage;
    @InjectView(R.id.planner_route)
    ImageView plannerRoute;
    @InjectView(R.id.add_route)
    ImageView addRoute;
    @InjectView(R.id.headline_points)
    LinearLayout headlinePoints;
    @InjectView(R.id.close_search)
    ImageView closeSearch;
    @InjectView(R.id.search)
    EditText search;
    @InjectView(R.id.clear_search)
    ImageView clearSearch;
    @InjectView(R.id.search_panel_points)
    LinearLayout searchPanelPoints;
    @InjectView(R.id.map_surface_holder)
    SKMapViewHolder mapSurfaceHolder;
    @InjectView(R.id.position_me_navigation_ui_button)
    ImageView positionMeNavigationUiButton;
    @InjectView(R.id.map_zoom_plus)
    ImageView mapZoomPlus;
    @InjectView(R.id.map_zoom_minus)
    ImageView mapZoomMinus;
    @InjectView(R.id.map_frameLayout)
    FrameLayout mapFrameLayout;
    private selectedPanel currentPanel = selectedPanel.NONE;
    private SKMapSurfaceView mapView;
    private SKCoordinate selectedCounterAgentLocation;
    private SKMapViewHolder mapHolder;
    private SKCurrentPositionProvider currentPositionProvider = null;
    private TextView popupTitleView;
    private TextView popupDescriptionView;
    private SKCalloutView mapPopup;
    private SKPosition currentPosition;
    private TextToSpeech textToSpeechEngine;
    private boolean navigationInProgress = false;
    private boolean followMe = false;
    private GeolocationCounterAgentsMeteorAdapter geolocationCounterAgentsMeteorAdapter;
    private GeolocationRoutesMeteorAdapter geolocationRoutesMeteorAdapter;
    private boolean isAllPointsShow = false;
    private NaviMode naviMode = NaviMode.REAL;
    private HashMap<Integer, CounterAgent> stringHashMap;
    private MarkerType CURRENT_MARKER_TYPE;

    public static GeolocationFragment newInstance(String type) {
        GeolocationFragment geolocationFragment = new GeolocationFragment();
        if (type.equals(GeolocationActivity.START_GEOLOCATION_ROUTE))
            geolocationFragment.isRouteSelect = true;
        else if (type.equals(GeolocationActivity.START_GEOLOCATION_POINT))
            geolocationFragment.isPointSelect = true;
        return geolocationFragment;
    }

    /**
     * Checks if the current device has a GPS module (hardware)
     *
     * @return true if the current device has GPS
     */
    public static boolean hasGpsModule(final Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        for (final String provider : locationManager.getAllProviders()) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the current device has a  NETWORK module (hardware)
     *
     * @return true if the current device has NETWORK
     */
    public static boolean hasNetworkModule(final Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        for (final String provider : locationManager.getAllProviders()) {
            if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mapView != null) {
            preferences.setLastPositionLatitude((float) mapView.getCurrentMapRegion().getCenter().getLatitude());
            preferences.setLastPositionLongitude((float) mapView.getCurrentMapRegion().getCenter().getLongitude());
        }
        ButterKnife.reset(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initSKMapLibrary();
        View view = inflater.inflate(R.layout.fragment_geolocation, container, false);
        initMap(view, inflater);
        LayoutInflater inflater1 = inflater;
        ButterKnife.inject(this, view);
        setPoints();
        setRoutes();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkLocationPermission();
    }

    @OnClick(R.id.homeButton)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }

    void showRouteList() {
        if (selectedItemsHolder.getSelectedRouteGuid() != null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            listRoute.setLayoutManager(linearLayoutManager);
            listRoute.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
            listRoute.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
            GeolocationRouteMeteorAdapter geolocationRouteMeteorAdapter = new GeolocationRouteMeteorAdapter(realmPlannerOperations.getRealm(), realmExchangeOperations, realmPlannerOperations, selectedItemsHolder.getSelectedRouteGuid());
            listRoute.setAdapter(geolocationRouteMeteorAdapter);
            routeName.setText(geolocationRouteMeteorAdapter.getRouteName());
            calculateStoredRoute();
        }
    }

    private void calculateStoredRoute() {

        selectedItemsHolder.setSelectedCounterAgentGuid(null);
        final PlannerRoute plannerRoute = realmPlannerOperations.getRouteByGuid(realmPlannerOperations.getRealm(), selectedItemsHolder.getSelectedRouteGuid());
        final List<ClientInfo> clientInfos = plannerRoute.getClientsList();

        int routePointsCount = clientInfos.size();

        if (routePointsCount <= 1) return;

        mapView.deleteAllAnnotationsAndCustomPOIs();

        CounterAgent endPoint = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), clientInfos.get(routePointsCount - 1).getClientId(), CounterAgent.class);
        if (endPoint == null || (endPoint.getLatitude() == 0f && endPoint.getLongitude() == 0f)) {
            showToast(R.string.not_coord_in_route);
            return;
        }

        SKCoordinate endPointLocation;
        endPointLocation = new SKCoordinate(endPoint.getLatitude(), endPoint.getLongitude());
        SKAnnotation endPointAnnotation = new SKAnnotation(endPoint.getGuid().hashCode());
        endPointAnnotation.setLocation(endPointLocation);
        endPointAnnotation.setMininumZoomLevel(5);
        SKAnnotationView pointAnnotationView = new SKAnnotationView();
        pointAnnotationView.setView(createMark(markType.RED, String.valueOf(routePointsCount), endPoint, endPointAnnotation));
        endPointAnnotation.setAnnotationView(pointAnnotationView);
        mapView.addAnnotation(endPointAnnotation, SKAnimationSettings.ANIMATION_NONE);

        List<SKViaPoint> viaPoints = new ArrayList<>();

        int currentPointId = 0;
        for (int i = 0; i < routePointsCount - 1; i++) {
            final CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), clientInfos.get(i).getClientId(), CounterAgent.class);
            if (counterAgent.getLatitude() != 0f || counterAgent.getLongitude() != 0f) {
                currentPointId++;
                SKCoordinate pointLocation = new SKCoordinate(counterAgent.getLatitude(), counterAgent.getLongitude());
                SKViaPoint viaPoint = new SKViaPoint(i, pointLocation);
                viaPoints.add(viaPoint);

                SKAnnotation pointAnnotation = new SKAnnotation(counterAgent.getGuid().hashCode());
                pointAnnotation.setLocation(pointLocation);
                pointAnnotation.setMininumZoomLevel(5);
                SKAnnotationView annotationView = new SKAnnotationView();
                markType markColor = markType.BLUE;
                if (currentPointId == 1) markColor = markType.GREEN;
                annotationView.setView(createMark(markColor, String.valueOf(currentPointId), counterAgent, pointAnnotation));
                pointAnnotation.setAnnotationView(annotationView);
                mapView.addAnnotation(pointAnnotation, SKAnimationSettings.ANIMATION_NONE);
                Log.d("ROUTES", "Point: " + i + "  " + realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), clientInfos.get(i).getClientId(), CounterAgent.class).getName());
            } else {
                showToast(R.string.not_coord_in_route);
                return;
            }
        }

        if (currentPosition != null)
            launchRouteCalculation(currentPosition.getCoordinate(), endPointLocation, viaPoints);
        else
            showToast(R.string.no_current_coord);
    }

    private void initSKMapLibrary() {
        SKMapsInitSettings initMapSettings = new SKMapsInitSettings();
        ExternalFilesUtils externalFilesUtils = new ExternalFilesUtils(this.getActivity(), "Maps");
        mapsFolder = externalFilesUtils.GetFilesDir().toString();
        initMapSettings.setMapResourcesPaths(mapsFolder, new SKMapViewStyle(mapsFolder + "/daystyle/", "daystyle.json"));
        SKMaps.getInstance().initializeSKMaps(this.getActivity(), initMapSettings);
    }

    private void initPositionProvider() {
        currentPositionProvider = new SKCurrentPositionProvider(this.getActivity());
        currentPositionProvider.setCurrentPositionListener(this);
        currentPositionProvider.requestLocationUpdates(hasGpsModule(this.getActivity()), hasNetworkModule(this.getActivity()), false);
    }

    private boolean initData(String counterAgentGuid) {
        selectedCounterAgentLocation = null;
        if (counterAgentGuid == null)
            return false;
        CounterAgent selectedClient = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), counterAgentGuid, CounterAgent.class);
        if (selectedClient != null && (selectedClient.getLongitude() != 0f || selectedClient.getLatitude() != 0f)) {
            selectedCounterAgentLocation = new SKCoordinate(selectedClient.getLatitude(), selectedClient.getLongitude());
            return true;
        } else {
            Toast.makeText(this.getActivity(), getString(R.string.not_coord_in_counteragent), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        togglePanel(selectedPanel.NONE);
    }

    private void initMap(View view, LayoutInflater inflater) {
        mapHolder = (SKMapViewHolder) view.findViewById(R.id.map_surface_holder);
        mapHolder.setMapSurfaceListener(this);
        mapPopup = mapHolder.getCalloutView();
        View popupView = inflater.inflate(R.layout.skmap_popup, null);
        popupTitleView = (TextView) popupView.findViewById(R.id.top_text);
        popupDescriptionView = (TextView) popupView.findViewById(R.id.bottom_text);
        mapPopup.setCustomView(popupView);
    }

    private void showMarker(CounterAgent counterAgent, markType type) {

        if (counterAgent != null && (counterAgent.getLatitude() != 0f || counterAgent.getLongitude() != 0f)) {
            selectedCounterAgentLocation = new SKCoordinate(counterAgent.getLatitude(), counterAgent.getLongitude());
            SKAnnotation selectedAgentAnnotation = new SKAnnotation(counterAgent.getGuid().hashCode());
            selectedAgentAnnotation.setLocation(selectedCounterAgentLocation);
            selectedAgentAnnotation.setMininumZoomLevel(5);
            SKAnnotationView annotationView = new SKAnnotationView();
            annotationView.setView(createMark(type, null, counterAgent, selectedAgentAnnotation));
            selectedAgentAnnotation.setAnnotationView(annotationView);
            mapView.addAnnotation(selectedAgentAnnotation, SKAnimationSettings.ANIMATION_NONE);
            //onAnnotationSelected(selectedAgentAnnotation);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mapHolder.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapHolder.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (currentPositionProvider != null)
            currentPositionProvider.stopLocationUpdates();
        if (mapView != null) {
            mapView.clearAllOverlays();
            mapView.deleteAllAnnotationsAndCustomPOIs();
        }
        SKRouteManager.getInstance().clearCurrentRoute();
        if (navigationInProgress)
            SKNavigationManager.getInstance().stopNavigation();
        SKMaps.getInstance().destroySKMaps();
        if (textToSpeechEngine != null) {
            textToSpeechEngine.stop();
            textToSpeechEngine.shutdown();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void setPoints() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listPoints.setLayoutManager(linearLayoutManager);
        listPoints.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
        listPoints.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
        geolocationCounterAgentsMeteorAdapter = new GeolocationCounterAgentsMeteorAdapter(getActivity(), getMeteorRealm());
        listPoints.setAdapter(geolocationCounterAgentsMeteorAdapter);

        stringHashMap = new HashMap<>();
        List<CounterAgent> counterAgents = realmExchangeOperations.getNonFolderCounterAgents(getMeteorRealm());
        for (CounterAgent counterAgent : counterAgents) {
            stringHashMap.put(counterAgent.getGuid().hashCode(), counterAgent);
        }

        RecycleViewItemClickSupport
                .addTo(listPoints)
                .setOnItemClickListener(new RecycleViewItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        mapPopup.hide();
                        CounterAgent counterAgent = geolocationCounterAgentsMeteorAdapter.getItem(position);
                        mapView.deleteAllAnnotationsAndCustomPOIs();
                        SKRouteManager.getInstance().clearCurrentRoute();
                        isRouteSelect = false;
                        isAllPointsShow = false;
                        if (initData(counterAgent.getGuid())) {
                            selectedItemsHolder.setSelectedCounterAgentGuid(counterAgent.getGuid());
                            showMarker(counterAgent, markType.RED);
                            if (currentPosition != null)
                                launchRouteCalculation(currentPosition.getCoordinate(), selectedCounterAgentLocation, null);
                            geolocationCounterAgentsMeteorAdapter.notifyDataSetChanged();
                        } else
                            selectedItemsHolder.setSelectedCounterAgentGuid(null);
                    }
                });
    }

    public void setRoutes() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listRoutes.setLayoutManager(linearLayoutManager);
        listRoutes.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
        listRoutes.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
        geolocationRoutesMeteorAdapter = new GeolocationRoutesMeteorAdapter(realmPlannerOperations.getRealm(), realmPlannerOperations);
        listRoutes.setAdapter(geolocationRoutesMeteorAdapter);
        RecycleViewItemClickSupport
                .addTo(listRoutes)
                .setOnItemClickListener(new RecycleViewItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        selectedItemsHolder.setSelectedRouteGuid(geolocationRoutesMeteorAdapter.getItem(position).getGuid());
                        isRouteSelect = true;
                        isAllPointsShow = false;
                        showRouteList();
                        togglePanel(selectedPanel.NONE);
                        timeRoute.setText("");
                        lengthRoute.setText("");
                        togglePanel(selectedPanel.ROUTE);
                    }
                });
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this.getActivity(), getString(R.string.need_permission_location), Toast.LENGTH_LONG).show();
            } else {
                FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSIONS_CHECK);
            }
        } else {
            initPositionProvider();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSIONS_CHECK) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initPositionProvider();
            } else {
                Toast.makeText(this.getActivity(), getString(R.string.need_permission_location), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActionPan() {
    }

    @Override
    public void onActionZoom() {
        if (mapView != null) {
            float zoomLevel = mapView.getZoomLevel();
            int ZOOM_TO_CHANGE_MARKER_TYPE = 10;
            if (zoomLevel > ZOOM_TO_CHANGE_MARKER_TYPE && CURRENT_MARKER_TYPE == MarkerType.SMALL) {
                CURRENT_MARKER_TYPE = MarkerType.BIG;
                resizeAnnotationsIcons();
            } else if (zoomLevel <= ZOOM_TO_CHANGE_MARKER_TYPE && CURRENT_MARKER_TYPE == MarkerType.BIG) {
                CURRENT_MARKER_TYPE = MarkerType.SMALL;
                resizeAnnotationsIcons();
            }
        }
    }

    private void resizeAnnotationsIcons() {
        List<SKAnnotation> annotations = mapView.getAllAnnotations();

        for (SKAnnotation annotation : annotations) {
            SKAnnotationView annotationView = annotation.getAnnotationView();
            TextView tv = (TextView) annotationView.getView().findViewById(R.id.mark_text);
            String annotationText = (tv != null && tv.getText() != null) ? tv.getText().toString() : "";
            markType markColor = (markType) annotationView.getView().getTag();
            View view = createMark(markColor, annotationText, null, null);
            annotationView.setView(view);
            annotation.setAnnotationView(annotationView);
            mapView.updateAnnotation(annotation);
        }
    }

    @Override
    public void onSurfaceCreated(SKMapViewHolder skMapViewHolder) {
        if (chessBackground != null)
            chessBackground.setVisibility(View.GONE);
        mapView = mapHolder.getMapSurfaceView();
        applySettingsOnMapView();


        if (currentPosition != null) {
            mapView.getMapSettings().setCurrentPositionShown(true);
            mapView.centerMapOnCurrentPosition();

            if (isRouteSelect) {
                CURRENT_MARKER_TYPE = MarkerType.BIG;
                calculateStoredRoute();
                togglePanel(selectedPanel.ROUTE);
            } else if (isPointSelect)
                if (initData(selectedItemsHolder.getSelectedCounterAgentGuid())) {
                    CURRENT_MARKER_TYPE = MarkerType.BIG;
                    final CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class);
                    selectedItemsHolder.setSelectedCounterAgentGuid(counterAgent.getGuid());
                    showMarker(counterAgent, markType.RED);
                    if (currentPosition != null)
                        launchRouteCalculation(currentPosition.getCoordinate(), selectedCounterAgentLocation, null);
                    geolocationCounterAgentsMeteorAdapter.notifyDataSetChanged();
                }

            preferences.setLastPositionLatitude((float) mapView.getCurrentMapRegion().getCenter().getLatitude());
            preferences.setLastPositionLongitude((float) mapView.getCurrentMapRegion().getCenter().getLongitude());
        } else {
            Toast.makeText(mapView.getContext(), "Can't get my position", Toast.LENGTH_LONG).show();
            mapView.centerMapOnPosition(new SKCoordinate(preferences.getLastPositionLatitude(), preferences.getLastPositionLongitude()));
        }
        mapView.setZoom(10);
        CURRENT_MARKER_TYPE = MarkerType.BIG;

        clearRouteFromCache();
    }

    private void applySettingsOnMapView() {
        SKMapInternationalizationSettings internationalisationSettings = new SKMapInternationalizationSettings();
        internationalisationSettings.setPrimaryLanguage(SKMaps.SKLanguage.LANGUAGE_RU);
        internationalisationSettings.setFallbackLanguage(SKMaps.SKLanguage.LANGUAGE_EN);
        internationalisationSettings.setShowBothLabels(true);
        mapView.getMapSettings().setMapInternationalizationSettings(internationalisationSettings);
        mapView.getMapSettings().setMapRotationEnabled(true);
        mapView.getMapSettings().setMapZoomingEnabled(true);
        mapView.getMapSettings().setMapPanningEnabled(true);
        mapView.getMapSettings().setZoomWithAnchorEnabled(true);
        mapView.getMapSettings().setInertiaRotatingEnabled(true);
        mapView.getMapSettings().setInertiaZoomingEnabled(true);
        mapView.getMapSettings().setInertiaPanningEnabled(true);
        mapView.getMapSettings().setCompassPosition(new SKScreenPoint(mapView.getWidth() - 130, mapView.getHeight() - 130));
        mapView.getMapSettings().setCompassShown(true);
        mapView.getMapSettings().setMapDisplayMode(SKMapSettings.SKMapDisplayMode.MODE_2D);
    }

    @Override
    public void onMapRegionChanged(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onMapRegionChangeStarted(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onMapRegionChangeEnded(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onDoubleTap(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onSingleTap(SKScreenPoint skScreenPoint) {
        mapPopup.hide();
        togglePanel(selectedPanel.NONE);
    }

    @Override
    public void onRotateMap() {

    }

    @Override
    public void onLongPress(SKScreenPoint skScreenPoint) {
        if (isVisible() && mapView != null) {
            togglePanel(selectedPanel.NONE);
            mapView.deleteAllAnnotationsAndCustomPOIs();
            SKRouteManager.getInstance().clearCurrentRoute();

            isAllPointsShow = false;
            isRouteSelect = false;
            SKScreenPoint pointCoord = skScreenPoint;
            int FREE_POINT_ANNOTATION_ID = 1000;
            mapView.deleteAnnotation(FREE_POINT_ANNOTATION_ID);
            SKCoordinate skCoordinate = mapView.pointToCoordinate(skScreenPoint);
            SKAnnotation freeAnnotation = new SKAnnotation(FREE_POINT_ANNOTATION_ID);
            freeAnnotation.setLocation(skCoordinate);
            freeAnnotation.setMininumZoomLevel(5);
            SKAnnotationView annotationView = new SKAnnotationView();
            annotationView.setView(createMark(markType.RED, null, null, null));
            freeAnnotation.setAnnotationView(annotationView);
            mapView.addAnnotation(freeAnnotation, SKAnimationSettings.ANIMATION_NONE);
            if (currentPosition != null & skCoordinate != null)
                launchRouteCalculation(currentPosition.getCoordinate(), skCoordinate, null);
        }
    }

    public View createMark(markType viewType, String markText, CounterAgent counterAgent, SKAnnotation skAnnotation) {
        //TODO: Add here big/small icon set depending from zoom size
        int layout = R.layout.view_mark_red;

        switch (CURRENT_MARKER_TYPE) {
            case BIG:
                switch (viewType) {
                    case RED:
                        layout = R.layout.view_mark_red;
                        break;
                    case GREEN:
                        layout = R.layout.view_mark_green;
                        break;
                    case BLUE:
                        layout = R.layout.view_mark_blue;
                        break;
                }
                break;

            case SMALL:
                switch (viewType) {
                    case RED:
                        layout = R.layout.view_mark_blue_small;
                        break;
                    case GREEN:
                        layout = R.layout.view_mark_blue_small;
                        break;
                    case BLUE:
                        layout = R.layout.view_mark_blue_small;
                        break;
                }
                break;
        }

        View view = LayoutInflater.from(this.getActivity()).inflate(layout, null);
        view.setTag(viewType);
        ViewHolderMark viewHolderMark = new ViewHolderMark(view);

        if (markText != null) {
            viewHolderMark.markText.setText(markText);
        }
        return view;
    }

    @Override
    public void onInternetConnectionNeeded() {
    }

    @Override
    public void onMapActionDown(SKScreenPoint skScreenPoint) {
    }

    @Override
    public void onMapActionUp(SKScreenPoint skScreenPoint) {
        followMe = false;
    }

    @Override
    public void onPOIClusterSelected(SKPOICluster skpoiCluster) {

    }

    @Override
    public void onMapPOISelected(SKMapPOI skMapPOI) {

    }

    @OnClick(R.id.map_surface_holder)
    public void onMapClick() {
    }

    @Override
    public void onAnnotationSelected(SKAnnotation annotation) {
        if (stringHashMap.containsKey(annotation.getUniqueID()))
            showPopup(annotation, stringHashMap.get(annotation.getUniqueID()));
    }

    private void showPopup(SKAnnotation annotation, CounterAgent counterAgent) {
        float annotationOffset = annotation.getOffset().getY();
        int annotationHeight = (int) (64 * getResources().getDisplayMetrics().density);
        if (counterAgent != null) {
            popupTitleView.setText(counterAgent.getName());
            String text = String.valueOf(counterAgent.getLatitude()) + " " + String.valueOf(counterAgent.getLongitude());
            popupDescriptionView.setText(text);
            mapPopup.setVerticalOffset(-annotationOffset + annotationHeight / 2);
            mapPopup.showAtLocation(annotation.getLocation(), true);
        }
    }

    @Override
    public void onCustomPOISelected(SKMapCustomPOI skMapCustomPOI) {
        mapPopup.hide();
    }

    @Override
    public void onCompassSelected() {
        followMe = true;
        positionMe();
    }

    @Override
    public void onCurrentPositionSelected() {

    }

    @Override
    public void onObjectSelected(int i) {

    }

    @Override
    public void onInternationalisationCalled(int i) {

    }

    @Override
    public void onBoundingBoxImageRendered(int i) {

    }

    @Override
    public void onGLInitializationError(String s) {

    }

    @Override
    public void onScreenshotReady(Bitmap bitmap) {

    }

    @Override
    public void onCurrentPositionUpdate(SKPosition currentPosition) {
        this.currentPosition = currentPosition;
        if (this.currentPosition != null) {
            SKPositionerManager.getInstance().reportNewGPSPosition(this.currentPosition);
            if (!navigationInProgress & followMe) {
                positionMe();
            }
            if (selectedCounterAgentLocation != null && calculateRouteButton.getVisibility() == View.GONE && !navigationInProgress && SKRouteManager.getInstance().getNumberOfRoutes() == 0) {
                calculateRouteButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onRouteCalculationCompleted(SKRouteInfo skRouteInfo) {
        int length = skRouteInfo.getDistance() / 1000;
        lengthRoute.setText(String.format(getString(R.string.lenght_route), length));
        int hours = skRouteInfo.getEstimatedTime() / (60 * 60);
        int minutes = (skRouteInfo.getEstimatedTime() - (hours * 60 * 60)) / 60;
        timeRoute.setText(String.format(getString(R.string.time_route), hours, minutes));
    }

    @Override
    public void onRouteCalculationFailed(SKRoutingErrorCode skRoutingErrorCode) {
        Toast.makeText(mapView.getContext(), "Route calculation failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAllRoutesCompleted() {
        SKRouteManager.getInstance().zoomToRoute(1, 1, 8, 8, 8, 8);
        if (driveButton != null)
            driveButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onServerLikeRouteCalculationCompleted(SKRouteJsonAnswer skRouteJsonAnswer) {

    }

    @Override
    public void onOnlineRouteComputationHanging(int i) {

    }

    private void launchRouteCalculation(SKCoordinate startPoint, SKCoordinate endPoint, List<SKViaPoint> viaPoints) {
        if (calculateRouteButton != null)
            calculateRouteButton.setVisibility(View.GONE);
        SKRouteManager.getInstance().clearCurrentRoute();
        clearRouteFromCache();
        SKRouteSettings route = new SKRouteSettings();
        route.setStartCoordinate(startPoint);
        if (viaPoints != null)
            route.setViaPoints(viaPoints);
        route.setDestinationCoordinate(endPoint);
        route.setNoOfRoutes(1);
        route.setRouteMode(SKRouteSettings.SKRouteMode.CAR_FASTEST);
        route.setRouteExposed(true);
        SKRouteManager.getInstance().setRouteListener(this);
        SKRouteManager.getInstance().calculateRoute(route);
    }

    private void launchNavigation() {

        SKNavigationSettings navigationSettings = new SKNavigationSettings();
        switch (naviMode) {
            case SIMULATION:
                navigationSettings.setNavigationType(SKNavigationSettings.SKNavigationType.SIMULATION);
                break;
            case REAL:
                navigationSettings.setNavigationType(SKNavigationSettings.SKNavigationType.REAL);
                break;
        }

        SKNavigationManager navigationManager = SKNavigationManager.getInstance();
        navigationManager.setMapView(mapView);
        navigationManager.setNavigationListener(this);
        navigationManager.startNavigation(navigationSettings);
        navigationInProgress = true;
        mapView.getMapSettings().setMapDisplayMode(SKMapSettings.SKMapDisplayMode.MODE_3D);
        mapDisplayModeButton.setVisibility(View.VISIBLE);
        mapDisplayModeButton.setText("2D");
    }

    private void stopNavigation() {
        navigationInProgress = false;
        if (textToSpeechEngine != null) {
            textToSpeechEngine.stop();
        }
        SKRouteManager.getInstance().clearCurrentRoute();
        SKNavigationManager.getInstance().stopNavigation();
        if (mapView.getMapSettings().getMapDisplayMode() == SKMapSettings.SKMapDisplayMode.MODE_3D)
            mapView.getMapSettings().setMapDisplayMode(SKMapSettings.SKMapDisplayMode.MODE_2D);
        mapDisplayModeButton.setVisibility(View.GONE);
    }

    private void setAdvicesAndStartNavigation(MapAdvices currentMapAdvices) {
        final SKAdvisorSettings advisorSettings = new SKAdvisorSettings();
        advisorSettings.setLanguage(SKAdvisorSettings.SKAdvisorLanguage.LANGUAGE_RU);
        advisorSettings.setAdvisorConfigPath(mapsFolder + "/Advisor");
        advisorSettings.setResourcePath(mapsFolder + "/Advisor/Languages");
        advisorSettings.setAdvisorVoice("ru");
        switch (currentMapAdvices) {
            case AUDIO_FILES:
                advisorSettings.setAdvisorType(SKAdvisorSettings.SKAdvisorType.AUDIO_FILES);
                break;
            case TEXT_TO_SPEECH:
                advisorSettings.setAdvisorType(SKAdvisorSettings.SKAdvisorType.TEXT_TO_SPEECH);
                break;
        }
        SKRouteManager.getInstance().setAudioAdvisorSettings(advisorSettings);
        launchNavigation();
    }

    public void clearRouteFromCache() {
        SKRouteManager.getInstance().clearAllRoutesFromCache();
    }

    @Override
    public void onDestinationReached() {

    }

    @Override
    public void onSignalNewAdviceWithInstruction(String instruction) {
        Toast.makeText(mapView.getContext(), instruction, Toast.LENGTH_SHORT).show();
        textToSpeechEngine.speak(instruction, TextToSpeech.QUEUE_ADD, null);
    }

    @Override
    public void onSignalNewAdviceWithAudioFiles(String[] strings, boolean b) {

    }

    @Override
    public void onSpeedExceededWithAudioFiles(String[] strings, boolean b) {

    }

    @Override
    public void onSpeedExceededWithInstruction(String s, boolean b) {

    }

    @Override
    public void onUpdateNavigationState(SKNavigationState skNavigationState) {
    }

    @Override
    public void onReRoutingStarted() {
    }

    @Override
    public void onFreeDriveUpdated(String s, String s1, String s2, SKNavigationState.SKStreetType skStreetType, double v, double v1) {
    }

    @Override
    public void onViaPointReached(int i) {
    }

    @Override
    public void onVisualAdviceChanged(boolean b, boolean b1, SKNavigationState skNavigationState) {
    }

    @Override
    public void onTunnelEvent(boolean b) {

    }

    @OnClick(R.id.position_me_navigation_ui_button)
    public void positionMe() {
        if (navigationInProgress)
            return;
        if (currentPosition != null) {
            mapView.getMapSettings().setCurrentPositionShown(true);
            mapView.centerMapOnCurrentPositionSmooth(15, 1000);
            mapView.getMapSettings().setOrientationIndicatorType(
                    SKMapSurfaceView.SKOrientationIndicatorType.DEFAULT);
            mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.NONE);
        } else {
            Toast.makeText(getActivity(), "No position available", Toast.LENGTH_LONG).show();
        }
        togglePanel(selectedPanel.NONE);
    }

    @OnClick(R.id.planner_route)
    public void onPlannerClick() {
        bus.post(new PlannerRouteEvent(selectedItemsHolder.getSelectedRouteGuid()));
    }

    @OnClick(R.id.add_route)
    public void onPlannerRouteCreateClick() {
        bus.post(new PlannerCreateRouteEvent());
    }

    @OnClick(R.id.drive_button)
    public void startStopNavigation() {
        if (navigationInProgress) {
            driveButton.setBackground(getResources().getDrawable(R.drawable.nupi_drive));
            stopNavigation();
            positionMe();
            if (isRouteSelect)
                calculateStoredRoute();
            else if (isPointSelect)
                initData(selectedItemsHolder.getSelectedCounterAgentGuid());
        } else {
            driveButton.setBackground(getResources().getDrawable(R.drawable.selector_nupi_drive));
            if (textToSpeechEngine == null) {
                textToSpeechEngine = new TextToSpeech(mapView.getContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {

                        if (status == TextToSpeech.SUCCESS) {
                            Locale locale = new Locale("ru");
                            int result = textToSpeechEngine.setLanguage(locale);
                            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                                Toast.makeText(getActivity(), "This Language is not supported", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Text to speech initialisation failed", Toast.LENGTH_SHORT).show();
                        }

                        setAdvicesAndStartNavigation(MapAdvices.TEXT_TO_SPEECH);
                    }
                });
            } else {
                setAdvicesAndStartNavigation(MapAdvices.TEXT_TO_SPEECH);
            }
        }
        togglePanel(selectedPanel.NONE);
    }

    @OnClick(R.id.calculate_route)
    public void onClickCalculateRoute() {
        launchRouteCalculation(currentPosition.getCoordinate(), selectedCounterAgentLocation, null);
        togglePanel(selectedPanel.NONE);
    }

    @OnClick(R.id.map_display_mode)
    public void toggleMapDisplayMode() {
        if (mapView.getMapSettings().getMapDisplayMode() == SKMapSettings.SKMapDisplayMode.MODE_3D) {
            mapDisplayModeButton.setText("3D");
            mapView.getMapSettings().setMapDisplayMode(SKMapSettings.SKMapDisplayMode.MODE_2D);
        } else if (mapView.getMapSettings().getMapDisplayMode() == SKMapSettings.SKMapDisplayMode.MODE_2D) {
            mapDisplayModeButton.setText("2D");
            mapView.getMapSettings().setMapDisplayMode(SKMapSettings.SKMapDisplayMode.MODE_3D);
        }
        togglePanel(selectedPanel.NONE);
    }

    @OnClick(R.id.routes_button)
    public void onRouteButtonClick() {
        if (isRouteSelect && currentPanel == selectedPanel.ROUTE) {
            isRouteSelect = false;
            mapView.deleteAllAnnotationsAndCustomPOIs();
            SKRouteManager.getInstance().clearCurrentRoute();
            selectedItemsHolder.setSelectedRouteGuid(null);
            togglePanel(selectedPanel.ROUTES);
        } else if (isRouteSelect) {
            togglePanel(selectedPanel.ROUTE);
        } else {
            togglePanel(selectedPanel.ROUTES);
        }

    }

    @OnClick(R.id.points_button)
    public void onPointButtonClick() {
        togglePanel(selectedPanel.POINTS);
    }

    @OnClick(R.id.map_zoom_plus)
    public void onPlusClick() {
        mapView.setZoom(mapView.getZoomLevel() + 1);
        togglePanel(selectedPanel.NONE);
    }

    @OnClick(R.id.map_zoom_minus)
    public void onMinusClick() {
        mapView.setZoom(mapView.getZoomLevel() - 1);
        togglePanel(selectedPanel.NONE);
    }

    @OnClick(R.id.show_all_points)
    public void onAllPointButtonClick() {
        isAllPointsShow = !isAllPointsShow;
        if (isAllPointsShow) {
            mapView.deleteAllAnnotationsAndCustomPOIs();
            SKRouteManager.getInstance().clearCurrentRoute();
            showAllPoints();
            showAllPoints.setTextColor(getResources().getColor(R.color.text_list));
            showAllPoints.setBackground(getResources().getDrawable(R.drawable.selector_list_item));
        } else {
            mapView.deleteAllAnnotationsAndCustomPOIs();
            SKRouteManager.getInstance().clearCurrentRoute();
            showAllPoints.setTextColor(getResources().getColor(R.color.text_button));
            showAllPoints.setBackground(getResources().getDrawable(R.drawable.selector_nupi_blue_dark));
            if (isRouteSelect)
                showRouteList();
            selectedItemsHolder.setSelectedCounterAgentGuid(null);
        }
        togglePanel(selectedPanel.NONE);

    }

    private void checkShowAll() {
        if (isAllPointsShow) {
            showAllPoints.setTextColor(getResources().getColor(R.color.text_list));
            showAllPoints.setBackground(getResources().getDrawable(R.drawable.selector_list_item));
        } else {

            showAllPoints.setTextColor(getResources().getColor(R.color.text_button));
            showAllPoints.setBackground(getResources().getDrawable(R.drawable.selector_nupi_blue_dark));

        }
    }

    private void showAllPoints() {
        if (isRouteSelect) {
            isRouteSelect = false;
            isPointSelect = false;
        }
        selectedItemsHolder.setSelectedCounterAgentGuid(null);

        for (CounterAgent client : realmExchangeOperations.getCounterAgents(getMeteorRealm()))
            showMarker(client, markType.BLUE);

    }

    private void togglePanel(selectedPanel panel) {
        points.setVisibility(View.GONE);
        routes.setVisibility(View.GONE);
        route.setVisibility(View.GONE);
        pointsButton.setBackground(getResources().getDrawable(R.drawable.selector_nupi_blue_dark));
        pathImage.setImageResource(R.drawable.nupi_routs_active);
        routesButton.setBackground(getResources().getDrawable(R.drawable.selector_nupi_blue_dark));
        pointImage.setImageResource(R.drawable.nupi_points_active);

        if (panel == currentPanel) {
            currentPanel = selectedPanel.NONE;
        } else {
            currentPanel = panel;
        }

        switch (currentPanel) {
            case ROUTES:
                routes.setVisibility(View.VISIBLE);
                routesButton.setBackground(getResources().getDrawable(R.drawable.selector_list_item));
                pathImage.setImageResource(R.drawable.nupi_routs);
                break;
            case ROUTE:
                route.setVisibility(View.VISIBLE);
                routesButton.setBackground(getResources().getDrawable(R.drawable.selector_list_item));
                pathImage.setImageResource(R.drawable.nupi_routs);
                showRouteList();
                break;
            case POINTS:
                points.setVisibility(View.VISIBLE);
                pointsButton.setBackground(getResources().getDrawable(R.drawable.selector_list_item));
                pointImage.setImageResource(R.drawable.nupi_points_passive);
                checkShowAll();
                geolocationCounterAgentsMeteorAdapter.notifyDataSetChanged();
                break;
        }
    }

    private enum MarkerType {BIG, SMALL}

    private enum markType {BLUE, RED, GREEN}

    private enum selectedPanel {NONE, ROUTES, POINTS, SEARCH, ROUTE}

    private enum MapAdvices {
        TEXT_TO_SPEECH, AUDIO_FILES
    }

    private enum NaviMode {
        SIMULATION, REAL
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'view_mark_red.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class ViewHolderMark {
        @InjectView(R.id.mark_text)
        TextView markText;
        @InjectView(R.id.custom_layout)
        RelativeLayout customLayout;

        ViewHolderMark(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
