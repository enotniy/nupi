package com.nupi.agent.ui.controllers.receipt.activities;

import android.content.Intent;
import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.SystemReportButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.receipt.fragments.ReceiptFragment;
import com.nupi.agent.ui.controllers.registry.activities.RegistryActivity;
import com.squareup.otto.Subscribe;


public class ReceiptActivity extends NupiActivity {

    private static String guidKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        Intent intent = getIntent();
        guidKey = intent.getStringExtra(RegistryActivity.GUID_KEY);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, ReceiptFragment.newInstance(guidKey))
                    .commit();
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
        bus.post(new SystemReportButtonEvent(true));
    }
}
