package com.nupi.agent.ui.controllers.analytics.activities;

import android.content.Intent;
import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.ChatButtonEvent;
import com.nupi.agent.events.ReportAnalyticsButtonEvent;
import com.nupi.agent.events.SystemReportButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.analytics.fragments.AnalyticsFragment;
import com.nupi.agent.ui.controllers.registry.activities.RegistryActivity;
import com.squareup.otto.Subscribe;


public class AnalyticsActivity extends NupiActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytics);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new AnalyticsFragment())
                    .commit();

    }

    @Subscribe
    public void onSystemReportButtonEvent(SystemReportButtonEvent event) {
        Intent intent = new Intent(this, RegistryActivity.class);
        intent.putExtra(IN_THE_POCKET_FILTER, event.getInThePocket());
        startActivity(intent);
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

    @Override
    @Subscribe
    public void onReportAnalyticsButton(ReportAnalyticsButtonEvent event) {
        super.onReportAnalyticsButton(event);
    }

    @Override
    @Subscribe
    public void onChatButtonEvent(ChatButtonEvent event) {
        super.onChatButtonEvent(event);
    }
}
