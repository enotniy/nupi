package com.nupi.agent.ui.controllers.analytics.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.models.NomenclatureSaleGroup;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.utils.StringUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.Sort;


public class ReportAnalyticsResultAdapter extends RecyclerView.Adapter<ReportAnalyticsResultAdapter.ViewHolder> {

    public static final int COUNT_ABC_TYPE_VIEW = 3;
    public static final int TYPE_VIEW_NOT_ABC = 0;
    public static final int TYPE_VIEW_A = 1;
    public static final int TYPE_VIEW_B = 2;
    public static final int TYPE_VIEW_C = 3;
    public static final int TYPE_VIEW_NULL = 4;

    @Inject
    protected RealmExchangeOperations realmExchangeOperations;
    private boolean isAbc;
    private Context context;
    private List<Nomenclature> nomenclatures;
    private List<CounterAgent> counterAgents;
    private Realm realm;
    private Date dateFrom;
    private Date dateTo;
    private List<NomenclatureSaleGroup> filtredNomenclatureSaleGroups;
    private List<NomenclatureSaleGroup> nomenclatureSaleGroups;
    private Sort sort = Sort.ASCENDING;
    private int nonEmptyGoods = 0;
    private String query = "";

    public ReportAnalyticsResultAdapter(Context context, Realm realm, List<Nomenclature> nomenclatures, List<CounterAgent> counterAgents, Date dateFrom, Date dateTo, boolean isABC) {
        NupiApp nupiApp = (NupiApp) context.getApplicationContext();
        nupiApp.getAppComponent().inject(this);
        this.isAbc = isABC;
        this.context = context;
        this.nomenclatures = nomenclatures;
        this.realm = realm;
        this.counterAgents = counterAgents;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        initGroup();
    }

    public void setData() {
        if (query == null || query.equals(""))
            filtredNomenclatureSaleGroups = nomenclatureSaleGroups;
        else {
            filtredNomenclatureSaleGroups = new LinkedList<>();
            for (NomenclatureSaleGroup nomenclatureSaleGroup : nomenclatureSaleGroups) {
                if (nomenclatureSaleGroup.getNomenclature().getLowerCaseName().contains(query))
                    filtredNomenclatureSaleGroups.add(nomenclatureSaleGroup);
            }
        }
    }

    public Sort getSort() {
        return sort;
    }

    private void initGroup() {
        nomenclatureSaleGroups = new LinkedList<>();
        nonEmptyGoods = 0;
        Log.d("ANAL", "Start query");
        nomenclatureSaleGroups =
                realmExchangeOperations.getNomenclatureSaleGroup(
                        realm, nomenclatures, counterAgents, dateFrom, dateTo);
        Log.d("ANAL", "Stop query, sorting");

        Log.d("ANAL", "Sorting finished");
        for (NomenclatureSaleGroup nomenclatureSaleGroup : nomenclatureSaleGroups) {
            if (nomenclatureSaleGroup.getSum() > 0f)
                nonEmptyGoods++;
        }
        sortGroups();
        for (int position = 0; position < nomenclatureSaleGroups.size(); position++) {
            if (sort == Sort.DESCENDING)
                position = getItemCount() - position - 1;
            int type;
            if (position < nonEmptyGoods / 3) {
                type = TYPE_VIEW_A;
            } else if (position < 2 * nonEmptyGoods / 3)
                type = TYPE_VIEW_B;
            else if (position < nonEmptyGoods)
                type = TYPE_VIEW_C;
            else
                type = TYPE_VIEW_NULL;
            nomenclatureSaleGroups.get(position).setType(type);
        }

        setData();
        Log.d("ANAL", "Sorting non empty counted");
    }

    private void sortGroups() {
        Collections.sort(nomenclatureSaleGroups, new Comparator<NomenclatureSaleGroup>() {
            @Override
            public int compare(NomenclatureSaleGroup lhs, NomenclatureSaleGroup rhs) {
                int result;
                if (lhs.getSum() < rhs.getSum())
                    result = 1;
                else if (lhs.getSum() > rhs.getSum())
                    result = -1;
                else {
                    result = lhs.getNomenclature().getName().compareTo(rhs.getNomenclature().getName());
                }
                return sort == Sort.ASCENDING ? result : -result;
            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final NomenclatureSaleGroup nomenclatureSaleGroup = filtredNomenclatureSaleGroups.get(position);
        holder.goodName.setText(nomenclatureSaleGroup.getNomenclature().getName());
        holder.goodQuantity.setText(StringUtils.getStockFormat(nomenclatureSaleGroup.getCount()));
        holder.goodCost.setText(StringUtils.getMoneyFormat(nomenclatureSaleGroup.getSum()));
    }

    @Override
    public int getItemCount() {
        return filtredNomenclatureSaleGroups.size();
    }

    public void setFilterText(String text) {
        query = text.toLowerCase();
        setData();
        notifyDataSetChanged();
    }

    public void changeSort() {
        if (Sort.ASCENDING == sort)
            this.sort = Sort.DESCENDING;
        else
            this.sort = Sort.ASCENDING;
        sortGroups();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        int layout = 0;
        switch (viewType) {
            case TYPE_VIEW_NOT_ABC:
                layout = R.layout.list_item_report_filter_result;
                break;
            case TYPE_VIEW_A:
                layout = R.layout.list_item_report_filter_result_a;
                break;
            case TYPE_VIEW_B:
                layout = R.layout.list_item_report_filter_result_b;
                break;
            case TYPE_VIEW_C:
                layout = R.layout.list_item_report_filter_result_c;
                break;
            case TYPE_VIEW_NULL:
                layout = R.layout.list_item_report_filter_result_zero;
                break;
        }
        final View v = inflater.inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    public void setIsAbc(boolean isAbc) {
        this.isAbc = isAbc;
        notifyDataSetChanged();
    }

    public float getSum() {
        float sum = 0;
        for (NomenclatureSaleGroup nomenclatureSaleGroup : filtredNomenclatureSaleGroups) {
            sum += nomenclatureSaleGroup.getSum();
        }
        return sum;
    }

    @Override
    public int getItemViewType(int position) {
        return isAbc ? filtredNomenclatureSaleGroups.get(position).getType() : TYPE_VIEW_NOT_ABC;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.good_name)
        TextView goodName;
        @InjectView(R.id.good_quantity)
        TextView goodQuantity;
        @InjectView(R.id.good_cost)
        TextView goodCost;
        @InjectView(R.id.goods_row)
        LinearLayout goodsRow;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}

