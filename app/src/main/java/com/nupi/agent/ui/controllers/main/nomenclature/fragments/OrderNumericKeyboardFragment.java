package com.nupi.agent.ui.controllers.main.nomenclature.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.database.models.meteor.NomenclaturePrice;
import com.nupi.agent.database.models.meteor.Unit;
import com.nupi.agent.events.DismissNumPadEvent;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.ui.base.BaseNumericKeyboardFragment;
import com.nupi.agent.utils.StringUtils;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class OrderNumericKeyboardFragment extends BaseNumericKeyboardFragment {

    @InjectView(R.id.typeCalc)
    Spinner spinnerCalc;
    @InjectView(R.id.btn_remove)
    Button btnRemove;
    @InjectView(R.id.btn_dot)
    LinearLayout btnDot;
    private RealmChangeListener listener;
    private RealmResults<Unit> units;
    private double baseCost;

    public OrderNumericKeyboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.numeric_keyboard, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSpinner();
        double quantity = selectedItemsHolder.getSelectedGood().getQuantity();
        if (quantity != 0)
            calcText.setText(String.valueOf(quantity));

        (preferences.isFloatQuantityAllowed() ? btnDot : btnRemove).setVisibility(View.VISIBLE);
        (!preferences.isFloatQuantityAllowed() ? btnDot : btnRemove).setVisibility(View.GONE);

        listener = new RealmChangeListener() {
            @Override
            public void onChange() {
                if (isVisible())
                    initSpinner();
            }
        };
        getMeteorRealm().addChangeListener(listener);
    }

    private void initSpinner() {
        OrderItem orderItem = selectedItemsHolder.getSelectedGood();

        final Nomenclature nomenclature =
                realmExchangeOperations.getNomenclatureById(getMeteorRealm(),
                        orderItem.getNomenclature());

        if (nomenclature == null) {
            showToast(R.string.wait_nomenclature);
            return;
        }

        units = realmExchangeOperations.getUnitsForGood(getMeteorRealm(), nomenclature.getGuid());
        final Unit storeUnit = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), nomenclature.getStoreUnit(), Unit.class);
        if (storeUnit != null) {
            final NomenclaturePrice nomenclaturePrice =
                    realmExchangeOperations.getAndCompressNomenclaturePriceForUnit(getMeteorRealm(), storeUnit.getGuid());
            if (nomenclaturePrice != null)
                baseCost = nomenclaturePrice.getPrice() / storeUnit.getCoefficient();
        }

        final String[] unitCaptions = new String[units.size()];

        int selectedUnitPosition = -1;
        int defaultUnitPosition = 0;

        final int defaultUnit = preferences.getDefaultUnit();

        for (int i = 0; i < units.size(); i++) {
            Unit unit = units.get(i);
            if (unit.getGuid().equals(selectedItemsHolder.getSelectedGood().getUnit()))
                selectedUnitPosition = i;
            if ((defaultUnit == NupiPreferences.REPORT_UNIT && unit.getGuid().equals(nomenclature.getReportUnit())) ||
                    (defaultUnit == NupiPreferences.STORE_UNIT && unit.getGuid().equals(nomenclature.getStoreUnit())))
                defaultUnitPosition = i;
            unitCaptions[i] = String.format(getString(R.string.unit_format), unit.getName(),
                    formatFloat(unit.getCoefficient()), formatFloat(unit.getCoefficient() * baseCost));
        }
        if (selectedUnitPosition == -1)
            selectedUnitPosition = defaultUnitPosition;
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getActivity(), R.layout.list_item_spinner, unitCaptions);
        adapter.setDropDownViewResource(R.layout.list_item_spinner_dropdown);

        spinnerCalc.setAdapter(adapter);

        spinnerCalc.setSelection(selectedUnitPosition);
    }

    private String formatFloat(double d) {
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return StringUtils.getMoneyFormat(d);
    }


    @OnClick(R.id.btn_remove)
    public void onCancelKey() {
        sendButtonEvent("Cancel Good", true);
        orderApi.removeOrderForClient(selectedItemsHolder.getSelectedCounterAgentGuid(),
                selectedItemsHolder.getSelectedGood().getNomenclature());
        preferences.setOrdersApi(orderApi);
        if (orderApi.hasOrders(selectedItemsHolder.getSelectedCounterAgentGuid()))
            writeNewHasOrdersValueIfNeeded(false);
        bus.post(new DismissNumPadEvent());
    }


    @OnClick(R.id.btnOK)
    public void onOkKey() {
        sendButtonEvent("Add Good", true);
        // TODO: 29.12.15 from meteor
        try {
            float value = Float.parseFloat(calcText.getText().toString());
            if (value == 0) {
                orderApi.removeOrderForClient(selectedItemsHolder.getSelectedCounterAgentGuid(),
                        selectedItemsHolder.getSelectedGood().getNomenclature());
                if (orderApi.hasOrders(selectedItemsHolder.getSelectedCounterAgentGuid()))
                    writeNewHasOrdersValueIfNeeded(false);
            } else if (value > 0) {
                Unit selectedUnit = units.get(spinnerCalc.getSelectedItemPosition());
                orderApi.addToOrderForClient(selectedItemsHolder.getSelectedCounterAgentGuid(),
                        selectedItemsHolder.getSelectedGood().getNomenclature(),
                        selectedUnit.getGuid(), selectedUnit.getCoefficient() * baseCost, value);

                writeNewHasOrdersValueIfNeeded(true);
            }
        } catch (NumberFormatException e) {
            showToast(R.string.incorrect_number);
        }
        preferences.setOrdersApi(orderApi);
        bus.post(new DismissNumPadEvent());
    }

    private void writeNewHasOrdersValueIfNeeded(boolean newHasOrdersValue) {
        if (!selectedItemsHolder.getSelectedCounterAgentGuid().equals(CounterAgent.NEW_POINT)) {
            final Realm meteorRealm = getMeteorRealm();
            final CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(
                    meteorRealm,
                    selectedItemsHolder.getSelectedCounterAgentGuid(),
                    CounterAgent.class
            );
            if (newHasOrdersValue ^ counterAgent.isHasOrders()) {
                meteorRealm.beginTransaction();
                try {
                    counterAgent.setHasOrders(newHasOrdersValue);
                    meteorRealm.commitTransaction();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                    meteorRealm.cancelTransaction();
                }
            }
        }
    }
}
