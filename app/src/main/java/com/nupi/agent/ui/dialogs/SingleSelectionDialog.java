package com.nupi.agent.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 21.03.14
 * Time: 13:05
 */
public abstract class SingleSelectionDialog implements SingleSelectionDialogDelegate {
    public void show(Activity activity, int position) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder
                .setTitle(getDialogMessage())
                .setSingleChoiceItems(getItemsSource(), position, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        onItemClick(item);
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
