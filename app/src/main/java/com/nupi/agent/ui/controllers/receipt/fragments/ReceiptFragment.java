package com.nupi.agent.ui.controllers.receipt.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Organization;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.receipt.adapters.ReceiptEntryAdapter;
import com.nupi.agent.utils.StringUtils;

import butterknife.InjectView;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by Pasenchuk Victor on 08.09.15
 */
public class ReceiptFragment extends NupiFragment {
    @State
    String realisationGuid;

    @InjectView(R.id.sum_info)
    TextView sumInfo;
    @InjectView(R.id.comment_info)
    TextView commentInfo;
    @InjectView(R.id.price_entries)
    ListView priceEntries;
    @InjectView(R.id.realisation_header)
    TextView realisationHeader;
    @InjectView(R.id.sum)
    TextView sum;
    @InjectView(R.id.organization_info)
    TextView organizationInfo;
    @InjectView(R.id.point_info)
    TextView pointInfo;
    @InjectView(R.id.status_info)
    TextView statusInfo;

    public static ReceiptFragment newInstance(String realisationGuid) {
        ReceiptFragment receiptFragment = new ReceiptFragment();
        receiptFragment.realisationGuid = realisationGuid;
        return receiptFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_receipt, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final RegistryEntry registryEntry =
                realmRegistryOperations.getRegistryEntryById(getRegistryRealm(), realisationGuid);
        if (registryEntry != null) {
            if (registryEntry.getEntryType() == RegistryEntry.REALISATION ||
                    registryEntry.getEntryType() == RegistryEntry.ORDER) {
                realisationHeader.setText(
                        registryEntry.getDocInfo() != null
                                ? registryEntry.getDocInfo()
                                : "");
                sum.setText(StringUtils.getMoneyFormat(registryEntry.getSum()));

                priceEntries.setAdapter(new ReceiptEntryAdapter(getActivity(), getMeteorRealm(), realmRegistryOperations.getPriceEntriesOfRealisation(getRegistryRealm(), registryEntry)));
                sumInfo.setText(String.format(getString(R.string.receipt_sum), registryEntry.getSum()));

                CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), registryEntry.getCounterAgent(), CounterAgent.class);
                Organization organization = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), registryEntry.getOrganization(), Organization.class);

                organizationInfo.setText(organization != null ? organization.getName() : "");
                if (counterAgent != null && counterAgent.getName() != null)
                    pointInfo.setText(counterAgent.getName());
                else
                    pointInfo.setText("");
                statusInfo.setText(getStatus(registryEntry));
                String comment = registryEntry.getComment();
                commentInfo.setText(String.format(getString(R.string.comment_receipt), comment != null ? comment : ""));
            } else {
                showToast(R.string.no_realisation_in_db);
                getActivity().finish();
            }
        } else {
            showToast(R.string.no_realisation_in_db);
            getActivity().finish();
        }
    }


    private String getStatus(RegistryEntry item) {
        switch (item.getEntryStatus()) {
            case RegistryEntry.SKIPPED:
                return getResources().getString(R.string.skipped);
            case RegistryEntry.NOT_SENT:
                return getResources().getString(R.string.not_sent);
            case RegistryEntry.SENT:
                return getResources().getString(R.string.sent);
            case RegistryEntry.RECEIVED:
                return getResources().getString(R.string.received);
            case RegistryEntry.REJECTED:
                return getResources().getString(R.string.rejected);
            case RegistryEntry.ACCEPTED:
                return getResources().getString(R.string.accepted);
            default:
                return getResources().getString(R.string.unknown);
        }
    }

    @OnClick(R.id.backBtn)
    void onBackClick() {
        getActivity().finish();
    }

}
