package com.nupi.agent.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 21.03.14
 * Time: 13:05
 */
public abstract class ListDialog implements ListDialogDelegate {
    public void show(Activity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder
                .setTitle(getDialogMessage())
                .setItems(getItemsSource(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        onItemClick(item);
                    }
                })
                .setCancelable(true)
                .show();
    }
}
