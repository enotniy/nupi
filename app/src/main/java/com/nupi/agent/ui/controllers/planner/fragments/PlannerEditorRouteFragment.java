package com.nupi.agent.ui.controllers.planner.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.nupi.agent.R;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.PlannerButtonHomeRouteEvent;
import com.nupi.agent.events.PlannerCreateRouteEvent;
import com.nupi.agent.events.PlannerEvent;
import com.nupi.agent.events.PlannerRouteEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.planner.adapters.DraggablePointsAdapter;
import com.nupi.agent.ui.dialogs.YesNoDialog;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;

public class PlannerEditorRouteFragment extends NupiFragment {


    @InjectView(R.id.home_button)
    ImageView homeButton;
    @InjectView(R.id.plannerList)
    RecyclerView mRecyclerView;
    @InjectView(R.id.delete_point)
    ImageView deletePoint;
    @InjectView(R.id.add_point)
    ImageView addPoint;
    @InjectView(R.id.down_point)
    ImageView downPoint;
    @InjectView(R.id.up_point)
    ImageView upPoint;
    @InjectView(R.id.show_path)
    TextView showPath;
    @InjectView(R.id.save_path)
    TextView savePath;
    @InjectView(R.id.route_name)
    TextView routeName;

    private RecyclerView.LayoutManager mLayoutManager;
    private DraggablePointsAdapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewDragDropManager mRecyclerViewDragDropManager;
    private PlannerRoute route;

    private Realm plannerRealm;


    public static PlannerEditorRouteFragment newInstance(PlannerRoute route) {
        PlannerEditorRouteFragment receiptFragment = new PlannerEditorRouteFragment();
        receiptFragment.route = route;
        return receiptFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        plannerRealm = realmPlannerOperations.getRealm();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        plannerRealm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_planner_route_editor, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && (keyEvent.getAction() == KeyEvent.ACTION_UP)) {
                    onHomeButtonPress();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        realmPlannerOperations.addClients(plannerRealm, realmExchangeOperations.getCounterAgents(getMeteorRealm()));
        setClientsAdapter();
        fillHeadline();
    }

    private void fillHeadline() {
        routeName.setText(route.getRouteName());
    }

    @Override
    public void onPause() {
//        mRecyclerViewDragDropManager.cancelDrag();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setClientsAdapter() {
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        if (preferences.isLoggedIn()) {
            mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();
            mRecyclerViewDragDropManager.setDraggingItemShadowDrawable(
                    (NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z3));

            final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

            mAdapter = new DraggablePointsAdapter(getActivity(), route);
            mWrappedAdapter = mRecyclerViewDragDropManager.createWrappedAdapter(mAdapter);

            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mWrappedAdapter);
            mRecyclerView.setItemAnimator(animator);
            if (supportsViewElevation()) {
                // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
            } else {
                mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
            }
            mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));

            mRecyclerViewDragDropManager.attachRecyclerView(mRecyclerView);
        }
    }

    @Override
    public void onDestroyView() {
        if (mRecyclerViewDragDropManager != null) {
            mRecyclerViewDragDropManager.release();
            mRecyclerViewDragDropManager = null;
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        mAdapter = null;
        mLayoutManager = null;
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        if (checkChange())
            bus.post(new PlannerButtonHomeRouteEvent());
        else {
            new YesNoDialog(getActivity(), getString(R.string.picker_leave)) {
                @Override
                public void handlePositiveButton() {
                    bus.post(new PlannerButtonHomeRouteEvent());
                }
            }.show();
        }
    }

    private boolean checkChange() {
        List<ClientInfo> currentRoutePoints = mAdapter.getClients();
        List<ClientInfo> routePoints = route.getClientsList();

        if (currentRoutePoints.size() != routePoints.size())
            return false;

        for (int i = 0; i < currentRoutePoints.size(); i++)
            if (currentRoutePoints.get(i) != routePoints.get(i))
                return false;

        return true;
    }

    @OnClick(R.id.edit_route)
    void onDeleteRoutePress() {
        new YesNoDialog(getActivity(), getString(R.string.delete_route)) {
            @Override
            public void handlePositiveButton() {
                final String name = route.getRouteName();
                plannerRealm.beginTransaction();
                route.removeFromRealm();
                plannerRealm.commitTransaction();
                Toast
                        .makeText(getActivity(),
                                String.format(getString(R.string.route_deleted), name),
                                Toast.LENGTH_SHORT)
                        .show();
                bus.post(new PlannerEvent());
            }
        }.show();
    }

    @OnClick(R.id.delete_point)
    void onDeletePress() {
        if (mAdapter.getSelectedIndex() > -1)
            if (mAdapter.getItemCount() < 2)
                showToast(R.string.empty_route);
            else
                mAdapter.removeSelectedPoint();

        else
            showToast(R.string.select_item_for_remove);
    }

    @OnClick(R.id.show_path)
    void onShowPathPress() {
        if (mAdapter.getSelectedIndex() > -1) {
            selectedItemsHolder.setSelectedCounterAgentGuid(mAdapter.getSelectedItemGuid());
            bus.post(new OpenGeolocationEvent(OpenGeolocationEvent.OpenType.POINT));
        } else
            showToast(R.string.select_item);
    }


    @OnClick(R.id.up_point)
    void onUpPress() {
        if (mAdapter.getSelectedIndex() > -1) {
            mAdapter.upSelectedPoint();
        } else
            showToast(R.string.select_item_or_drag);
    }

    @OnClick(R.id.down_point)
    void onDownPress() {
        if (mAdapter.getSelectedIndex() > -1) {
            mAdapter.downSelectedPoint();
        } else
            showToast(R.string.select_item_or_drag);
    }

    @OnClick(R.id.route_name)
    void onNamePress() {
        final EditText editText = new EditText(getActivity());
        editText.setText(route.getRouteName());
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.edit_name_route)
                .setView(editText)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!editText.getText().toString().equals("")) {
                            final String newString = editText.getText().toString();
                            if (realmPlannerOperations.getRouteByName(plannerRealm, newString) == null) {
                                plannerRealm.beginTransaction();
                                route.setRouteName(newString);
                                plannerRealm.commitTransaction();
                                routeName.setText(newString);
                            } else {
                                Toast.makeText(getActivity(), R.string.route_exist, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), R.string.route_empty, Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .show();
    }


    @OnClick(R.id.add_point)
    void onAddPress() {
        bus.post(new PlannerCreateRouteEvent(mAdapter.getClients(), route));
    }

    @OnClick(R.id.save_path)
    void onSavePress() {
        sendButtonEvent("Save path", true);
        RealmList<ClientInfo> clientInfo = new RealmList<>();
        for (ClientInfo client : mAdapter.getClients())
            clientInfo.add(client);
        plannerRealm.beginTransaction();
        route.setClientsList(clientInfo);
        plannerRealm.commitTransaction();
        showToast(R.string.save_change);
        bus.post(new PlannerRouteEvent(route.getGuid()));
    }

}
