package com.nupi.agent.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.nupi.agent.R;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 01.12.13
 * Time: 21:32
 */
public abstract class MessageBoxWithAction {
    public void show(CharSequence message, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder
                .setMessage(message)
                .setCancelable(true)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        onOKButtonClick();
                    }
                })
                .show();
    }

    public abstract void onOKButtonClick();

}
