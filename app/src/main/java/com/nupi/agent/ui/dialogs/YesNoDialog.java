package com.nupi.agent.ui.dialogs;

import android.content.Context;

import com.nupi.agent.R;

/**
 * Created with IntelliJ IDEA.
 * User: Pasencuk Victor
 * Date: 18.03.14
 * Time: 17:33
 */
public abstract class YesNoDialog implements YesNoDialogDelegate {
    private final Context context;
    private final String message;
    private String positiveButtonName;
    private String negativeButtonName;

    public YesNoDialog(Context context, String message) {
        this.context = context;
        this.message = message;
        this.positiveButtonName = context.getString(R.string.yes);
        this.negativeButtonName = context.getString(R.string.no);
    }

    public void setPositiveButtonName(String buttonName) {
        this.positiveButtonName = buttonName;
    }

    public void setNegativeButtonName(String buttonName) {
        this.negativeButtonName = buttonName;
    }

    public void show() {

        new ComplexDialog() {

            @Override
            public String getDialogMessage() {
                return message;
            }

            @Override
            public String getPositiveButtonName() {
                return positiveButtonName;
            }

            @Override
            public String getNegativeButtonName() {
                return negativeButtonName;
            }

            @Override
            public String getNeutralButtonName() {
                return null;
            }

            @Override
            public void handlePositiveButton() {
                YesNoDialog.this.handlePositiveButton();
            }

            @Override
            public void handleNeutralButton() {
            }
        }.show(context, false);
    }

}
