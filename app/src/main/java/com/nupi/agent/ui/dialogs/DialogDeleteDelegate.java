package com.nupi.agent.ui.dialogs;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 18.03.14
 * Time: 17:08
 */
interface DialogDeleteDelegate {
    void delete();
}
