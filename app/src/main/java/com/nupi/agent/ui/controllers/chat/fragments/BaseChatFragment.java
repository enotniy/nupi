package com.nupi.agent.ui.controllers.chat.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.network.nupi.NupiServerApi;
import com.nupi.agent.ui.base.NupiFragment;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Pasenchuk Victor on 04.12.15
 */
public abstract class BaseChatFragment extends NupiFragment {

    @Inject
    protected NupiServerApi nupiServerApi;

    private Subscription chatSubscription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        if (chatSubscription != null && !chatSubscription.isUnsubscribed())
            chatSubscription.unsubscribe();
        super.onPause();
    }

    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
    }

    @NonNull
    public Observable<Void> reloadMessages(final List<ChatGroup> chatGroups, final String myId) {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                final Realm realm = realmChatOperations.getRealm();
                List<ChatMessage> chatMessages;
                long dbMessageCount;
                String id;
                try {
                    for (int i = 0; i < realmChatOperations.getChatUsers(realm).size(); i++) {
                        final ChatUser chatUser = realmChatOperations.getChatUsers(realm).get(i);
                        id = chatUser.getUser();
                        if (!chatUser.getUser().equals(myId)) {
                            dbMessageCount =
                                    realmChatOperations.getPrivateChatMessagesCount(realm, id);
                            if (dbMessageCount < chatUser.getMessageCount()) {
                                chatMessages = nupiServerApi.getChatUserMessagesSync(id);
                                realmChatOperations.copyToRealmOrUpdateChatMessages(realm, chatMessages);
                            }
                        }
                    }

                    for (int i = 0; i < chatGroups.size(); i++) {
                        final ChatGroup chatGroup = chatGroups.get(i);
                        id = chatGroup.getId();
                        dbMessageCount =
                                realmChatOperations.getGroupChatMessagesCount(realm, id);
                        if (dbMessageCount < chatGroup.getMessageCount()) {
                            chatMessages = nupiServerApi.getChatGroupMessagesSync(id);
                            realmChatOperations.copyToRealmOrUpdateChatMessages(realm, chatMessages);
                        }
                    }
                    realm.close();
                    subscriber.onCompleted();
                } catch (Exception e) {
                    realm.close();
                    subscriber.onError(e);
                }
            }
        });
    }
}
