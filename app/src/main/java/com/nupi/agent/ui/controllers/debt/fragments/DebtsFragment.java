package com.nupi.agent.ui.controllers.debt.fragments;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.enums.SortType;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.debt.adapters.DebtsAdapter;
import com.nupi.agent.ui.controllers.general.fragments.ButtonsFragment;
import com.nupi.agent.ui.controllers.general.fragments.SmallChatFragment;
import com.nupi.agent.utils.StringUtils;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.RealmChangeListener;
import io.realm.Sort;


/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class DebtsFragment extends NupiFragment implements RealmChangeListener {

    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";

    @InjectView(R.id.counterAgentsListView)
    RecyclerView counterAgentsListView;

    @InjectView(R.id.sumDebt)
    TextView textViewSumDebtList;

    @InjectView(R.id.imageSortByDebt)
    ImageView imageSortByDebt;

    @InjectView(R.id.sortAbc)
    ImageView sortAbc;

    @InjectView(R.id.sortDebt)
    LinearLayout linearLayoutSortDebt;

    @InjectView(R.id.textSortDebt)
    TextView textViewSortDebt;

    @Inject
    NupiPreferences sharedPreferences;

    private DebtsAdapter debtsAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_counteragents, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.two_buttons, new ButtonsFragment())
                .replace(R.id.bottomChat, new SmallChatFragment())
                .commit();
        initList(savedInstanceState);
        setSumDebt();
    }

    private void initList(Bundle savedInstanceState) {
        mLayoutManager = new LinearLayoutManager(getActivity());
        final Parcelable eimSavedState = (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);
        mRecyclerViewExpandableItemManager.setOnGroupExpandListener(new RecyclerViewExpandableItemManager.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition, boolean fromUser) {
                if (fromUser) {
                    int childItemHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.list_row_height);
                    int topMargin = (int) (getActivity().getResources().getDisplayMetrics().density * 16); // top-spacing: 16dp
                    final int bottomMargin = topMargin;
                    mRecyclerViewExpandableItemManager.scrollToGroup(groupPosition, childItemHeight, topMargin, bottomMargin);
                }
            }
        });
        mRecyclerViewExpandableItemManager.setOnGroupCollapseListener(new RecyclerViewExpandableItemManager.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition, boolean fromUser) {
            }
        });

        debtsAdapter = new DebtsAdapter(getActivity(), getMeteorRealm());

        mWrappedAdapter = mRecyclerViewExpandableItemManager.createWrappedAdapter(debtsAdapter);// wrap for expanding

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        animator.setSupportsChangeAnimations(false);
        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Need to disable them when using animation indicator.

        counterAgentsListView.setLayoutManager(mLayoutManager);
        counterAgentsListView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        counterAgentsListView.setItemAnimator(animator);
        counterAgentsListView.setHasFixedSize(false);

        // additional decorations
        //noinspection StatementWithEmptyBody
        if (supportsViewElevation()) {
            // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
        } else {
            counterAgentsListView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
        }
        counterAgentsListView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));

        mRecyclerViewExpandableItemManager.attachRecyclerView(counterAgentsListView);
        // mRecyclerViewExpandableItemManager.expandAll(false);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save current state to support screen rotation, etc...
        if (mRecyclerViewExpandableItemManager != null) {
            outState.putParcelable(
                    SAVED_STATE_EXPANDABLE_ITEM_MANAGER,
                    mRecyclerViewExpandableItemManager.getSavedState());
        }
    }

    @Override
    public void onDestroyView() {
        if (mRecyclerViewExpandableItemManager != null) {
            mRecyclerViewExpandableItemManager.release();
            mRecyclerViewExpandableItemManager = null;
        }

        if (counterAgentsListView != null) {
            counterAgentsListView.setItemAnimator(null);
            counterAgentsListView.setAdapter(null);
            counterAgentsListView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        mLayoutManager = null;

        super.onDestroyView();
    }

    public void setSortButtonDebt(boolean active, Sort order) {
        if (active) {
            switch (order) {
                case ASCENDING:
                    imageSortByDebt.setImageResource(R.drawable.nupi_sort_1_9);
                    break;
                case DESCENDING:
                    imageSortByDebt.setImageResource(R.drawable.nupi_sort_9_1);
                    break;
            }
            imageSortByDebt.setBackgroundColor(getResources().getColor(R.color.background_list));
        } else {
            imageSortByDebt.setImageResource(R.drawable.nupi_sort_9_1_disable);
            imageSortByDebt.setBackgroundResource(R.drawable.selector_nupi_blue_dark);
        }
    }

    public void setSortButtonName(boolean active, Sort order) {
        if (active) {
            switch (order) {
                case ASCENDING:
                    sortAbc.setImageResource(R.drawable.nupi_sort_a_ya_active);
                    break;
                case DESCENDING:
                    sortAbc.setImageResource(R.drawable.nupi_sort_ya_a_active);
                    break;
            }
            sortAbc.setBackgroundColor(getResources().getColor(R.color.background_list));
        } else {
            sortAbc.setImageResource(R.drawable.nupi_sort_a_ya_disable);
            sortAbc.setBackgroundResource(R.drawable.selector_nupi_blue_dark);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onChange();
        getRegistryRealm().addChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getRegistryRealm().removeChangeListener(this);
    }


    private void setSumDebt() {
        textViewSumDebtList.setText(StringUtils.getMoneyFormat(debtsAdapter.getSumDebt()));
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }

    @OnClick(R.id.sortAbc)
    void onSortAbcButtonPress() {
        if (debtsAdapter != null) {
            sendButtonEvent("Sort ABC", true);
            if (debtsAdapter.getSortType() == SortType.ABC_ORDER) {
                debtsAdapter.changeOrder();
            }

            setSortButtonName(true, debtsAdapter.getOrder());
            setSortButtonDebt(false, Sort.DESCENDING);

            debtsAdapter.sort(SortType.ABC_ORDER);
            preferences.setSortOrder(debtsAdapter.getOrder());
            preferences.setSortType(SortType.ABC_ORDER);
        } else {
            sendButtonEvent("Sort ABC", false);
        }
    }

    @OnClick(R.id.sortDebt)
    void onSortDebtButtonPress() {
        if (debtsAdapter != null) {
            sendButtonEvent("Sort Debt", true);
            if (debtsAdapter.getSortType() == SortType.DEBT_ORDER)
                debtsAdapter.changeOrder();

            setSortButtonDebt(true, debtsAdapter.getOrder());
            setSortButtonName(false, debtsAdapter.getOrder());

            debtsAdapter.sort(SortType.DEBT_ORDER);
            preferences.setSortOrder(debtsAdapter.getOrder());
            preferences.setSortType(SortType.DEBT_ORDER);
        } else {
            sendButtonEvent("Sort Debt", false);
        }
    }

    @Override
    public void onChange() {
        setSumDebt();
    }

}
