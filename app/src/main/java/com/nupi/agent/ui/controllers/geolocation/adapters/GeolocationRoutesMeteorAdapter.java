package com.nupi.agent.ui.controllers.geolocation.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.RealmPlannerOperations;
import com.nupi.agent.database.models.planner.PlannerRoute;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by User on 20.10.2015
 */


public class GeolocationRoutesMeteorAdapter extends RecyclerView.Adapter<GeolocationRoutesMeteorAdapter.ViewHolder> {

    private final RealmPlannerOperations realmPlannerOperations;
    private Realm realm;
    private RealmResults<PlannerRoute> routes;

    public GeolocationRoutesMeteorAdapter(Realm realm, RealmPlannerOperations plannerOperations) {
        this.realmPlannerOperations = plannerOperations;
        this.realm = realm;
        setHasStableIds(true);
        updateCounterAgents();
    }

    private void updateCounterAgents() {
        routes = realmPlannerOperations.getPlannerRoutes(realm);
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return routes.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.list_item_clients_geolocation, parent, false));
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final PlannerRoute plannerRoute = getItem(position);
        viewHolder.clientName.setText(plannerRoute.getRouteName());
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getRouteName().hashCode();
    }

    public PlannerRoute getItem(int i) {
        return routes.get(i);
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_clients.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.clientName)
        TextView clientName;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
