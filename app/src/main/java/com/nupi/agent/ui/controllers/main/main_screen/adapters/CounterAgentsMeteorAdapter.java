package com.nupi.agent.ui.controllers.main.main_screen.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.enums.SortType;
import com.nupi.agent.utils.HighlighterUtils;
import com.nupi.agent.utils.StringUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by User on 20.10.2015
 */


public class CounterAgentsMeteorAdapter extends RecyclerView.Adapter<CounterAgentsMeteorAdapter.ViewHolder> {

    private static final int
            POINT = 0,
            POINT_WITH_REALISATION = 1;
    private final RealmExchangeOperations realmExchangeOperations;
    private Sort order = Sort.ASCENDING;
    private SortType sortType = SortType.ABC_ORDER;
    private String searchQuery = "";
    private Realm realm;
    private OrderApi orderApi;
    private RealmResults<CounterAgent> counterAgents;
    private Subscription subscription;

    public CounterAgentsMeteorAdapter(Realm realm, RealmExchangeOperations realmExchangeOperations, OrderApi orderApi, Sort sort, SortType sortType) {
        this.realmExchangeOperations = realmExchangeOperations;
        this.orderApi = orderApi;
        this.realm = realm;
        this.order = sort;
        this.sortType = sortType;
        setHasStableIds(true);

        updateCounterAgents();

    }

    private void updateCounterAgents() {
        if (subscription != null)
            subscription.unsubscribe();
        counterAgents = realmExchangeOperations.getNonFolderCounterAgentsWithOrders(realm, searchQuery, sortType, order);
        subscription = counterAgents
                .asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RealmResults<CounterAgent>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(RealmResults<CounterAgent> counterAgents) {
                        notifyDataSetChanged();
                    }
                });
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return getItem(position).isHasOrders() ? POINT_WITH_REALISATION : POINT;
    }

    @Override
    public int getItemCount() {
        return counterAgents.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == POINT)
            return new ViewHolder(inflater.inflate(R.layout.list_item_clients, parent, false));
        else
            return new ViewHolder(inflater.inflate(R.layout.list_item_clients_with_realisation, parent, false));
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final CounterAgent counterAgent = getItem(position);
        viewHolder.clientName.setText(counterAgent.getName());
        viewHolder.clientDebt.setText(StringUtils.getMoneyFormat(counterAgent.getDebt()));
        HighlighterUtils.highlightSearchString(counterAgent.getName(), viewHolder.clientName, searchQuery, Color.BLUE);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getGuid().hashCode();
    }

    public CounterAgent getItem(int i) {
        return counterAgents.get(i);
    }

    public void sort(final SortType sortType) {
        this.sortType = sortType;
        updateCounterAgents();
    }


    public void changeOrder() {
        order = order.getValue() ? Sort.DESCENDING : Sort.ASCENDING;
    }

    public SortType getSortType() {
        return sortType;
    }


    public void setStringFilter(String query) {
        if (query != null && !query.equals(searchQuery)) {
            searchQuery = query.toLowerCase();
            updateCounterAgents();
        }
    }

    public Sort getOrder() {
        return order;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_clients.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.clientName)
        TextView clientName;
        @InjectView(R.id.clientDebt)
        TextView clientDebt;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
