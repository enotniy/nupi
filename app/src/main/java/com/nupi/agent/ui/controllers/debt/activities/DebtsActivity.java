package com.nupi.agent.ui.controllers.debt.activities;

import android.content.Intent;
import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.ChatButtonEvent;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.events.SystemReportButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.debt.fragments.DebtsFragment;
import com.nupi.agent.ui.controllers.registry.activities.RegistryActivity;
import com.squareup.otto.Subscribe;


public class DebtsActivity extends NupiActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counteragents);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, new DebtsFragment())
                    .commit();
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

    @Override
    @Subscribe
    public void onRealisationReceiptEvent(OpenRealisationReceiptEvent event) {
        super.onRealisationReceiptEvent(event);
    }

    @Subscribe
    public void onSystemReportButtonEvent(SystemReportButtonEvent event) {
        Intent intent = new Intent(this, RegistryActivity.class);
        intent.putExtra(IN_THE_POCKET_FILTER, event.getInThePocket());
        startActivity(intent);
    }

    @Override
    @Subscribe
    public void onChatButtonEvent(ChatButtonEvent event) {
        super.onChatButtonEvent(event);
    }
}
