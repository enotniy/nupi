package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nupi.agent.R;
import com.nupi.agent.events.CounterAgentSearchButtonPressedEvent;
import com.nupi.agent.events.SearchStringEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.general.fragments.SmallChatFragment;
import com.squareup.otto.Subscribe;

/**
 * Created by Pasenchuk Victor on 14.10.14
 */
public class MainRightFragment extends NupiFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_right, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initFragments();
    }

    private void initFragments() {
        if (getChildFragmentManager().findFragmentById(R.id.centerControls) == null)
            getChildFragmentManager().beginTransaction()
                    .add(R.id.centerControls, new CenterControlsFragment())
                    .commit();

        getChildFragmentManager().beginTransaction()
                .replace(R.id.topStatus, new TopStatusFragment())
                .replace(R.id.bottomChat, new SmallChatFragment())
                .commit();
    }

    @Subscribe
    public void onCounterAgentsSearchButtonPressedEvent(CounterAgentSearchButtonPressedEvent event) {
        if (preferences.getSearchVisible()) {
            bus.post(new SearchStringEvent(""));
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.centerControls, new CenterControlsFragment())
                    .commit();
            preferences.changeSearchVisible();
        } else {
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.centerControls, new SearchFragment())
                    .commit();
            preferences.changeSearchVisible();
        }
    }
}
