package com.nupi.agent.ui.controllers.analytics.activities;

import android.content.Intent;
import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.events.PickerButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.analytics.fragments.ReportClientFragment;
import com.nupi.agent.ui.controllers.analytics.fragments.ReportGoodsFragment;
import com.squareup.otto.Subscribe;


public class ReportAnalyticsActivity extends NupiActivity {

    public static final String REPORT_TYPE = "REPORT_TYPE";
    public static final int REPORT_TYPE_GOODS = 0;
    public static final int REPORT_TYPE_CLIENTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytics);
        if (savedInstanceState == null) {
            int type = getIntent().getIntExtra(REPORT_TYPE, 0);
            if (type == REPORT_TYPE_GOODS) {
                getFragmentManager().beginTransaction()
                        .add(R.id.container, ReportGoodsFragment.getInstance(type))
                        .commit();
            } else if (type == REPORT_TYPE_CLIENTS) {
                getFragmentManager().beginTransaction()
                        .add(R.id.container, new ReportClientFragment())
                        .commit();
            }
        }
    }

    @Override
    @Subscribe
    public void onRealisationReceiptEvent(OpenRealisationReceiptEvent event) {
        super.onRealisationReceiptEvent(event);
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

    @Subscribe
    public void onPickerButtonEvent(PickerButtonEvent event) {
        Intent intent = new Intent(this, PickerActivity.class);
        intent.putExtra(Picker.TYPE_PICKER, event.getType());
        startActivity(intent);
    }
}
