package com.nupi.agent.ui.base;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.application.service.SelectedItemsHolder;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.RealmPlannerOperations;
import com.nupi.agent.database.RealmRegistryOperations;
import com.nupi.agent.events.VoiceSearchResultEvent;
import com.squareup.otto.Bus;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.ButterKnife;
import icepick.Icepick;
import io.realm.Realm;

/**
 * Created by Pasenchuk Victor on 24.09.14
 */

public class NupiFragment extends Fragment {

    public static final int VOICE_SEARCH_REQUEST_CODE = 70153;
    public static final String NEW_COUNTER_AGENT_GUID = "00000000-caca-caca-caca-cacacacacaca";
    public static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";

    @Inject
    protected Bus bus;

    @Inject
    protected SelectedItemsHolder selectedItemsHolder;

    @Inject
    protected OrderApi orderApi;

    @Inject
    protected RealmRegistryOperations realmRegistryOperations;

    @Inject
    protected RealmChatOperations realmChatOperations;

    @Inject
    protected RealmExchangeOperations realmExchangeOperations;

    @Inject
    protected RealmPlannerOperations realmPlannerOperations;

    @Inject
    protected NupiPreferences preferences;

    private Realm registryRealm;

    private Realm chatRealm;

    private Realm meteorRealm;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        performInjections(savedInstanceState);

        initDatabaseHelpers();

        initProgressDialog();

        Log.d("LifeCycle onCreate", getSimpleClassName());
    }

    private void performInjections(Bundle savedInstanceState) {
        Icepick.restoreInstanceState(this, savedInstanceState != null ? savedInstanceState : getArguments());
        NupiApp nupiApp = getNupiApp();
        nupiApp.getAppComponent().inject(this);
    }

    private void initDatabaseHelpers() {
        registryRealm = realmRegistryOperations.getRealm();
        chatRealm = realmChatOperations.getRealm();
        meteorRealm = realmExchangeOperations.getRealm();
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading_wait));
        progressDialog.setCancelable(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bus.register(this);

        if (requestCode == VOICE_SEARCH_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            ArrayList<String> matches = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            bus.post(new VoiceSearchResultEvent(matches.get(0)));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    public void setIcepickArguments() {
        final Bundle bundle = new Bundle();
        Icepick.saveInstanceState(this, bundle);
        setArguments(bundle);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);

    }

    @Override
    public void onResume() {
        super.onResume();
        dismissProgressDialog();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        dismissProgressDialog();
        bus.unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registryRealm.close();
        chatRealm.close();
        meteorRealm.close();
        Log.d("LifeCycle onDestroy", getSimpleClassName());
    }

    public Realm getRegistryRealm() {
        return registryRealm;
    }

    public Realm getChatRealm() {
        return chatRealm;
    }

    public Realm getMeteorRealm() {
        return meteorRealm;
    }

    public void showToast(int id) {
        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
    }

    public void showProgressDialog() {
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    public void sendButtonEvent(String whichButton, boolean success) {
        Answers.getInstance().logCustom(getButtonEvent(whichButton, success));
    }

    public void sendNetworkEvent(String name) {
        Answers.getInstance().logCustom(getNetworkEvent(name));
    }

    public CustomEvent getButtonEvent(String whichButton, boolean success) {
        return new CustomEvent("Button pressed")
                .putCustomAttribute("Button", whichButton)
                .putCustomAttribute("Location", getSimpleClassName())
                .putCustomAttribute("Result", success ? "success" : "fail");
    }

    @NonNull
    private String getSimpleClassName() {
        return this.getClass().getSimpleName();
    }

    public CustomEvent getNetworkEvent(String networkProcess) {
        return new CustomEvent("Network operation")
                .putCustomAttribute("Operation", networkProcess)
                .putCustomAttribute("Location", this.getClass().getName())
                .putCustomAttribute("Kind", "network");
    }

    public NupiApp getNupiApp() {
        return (NupiApp) getActivity().getApplication();
    }

    public boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }
}
