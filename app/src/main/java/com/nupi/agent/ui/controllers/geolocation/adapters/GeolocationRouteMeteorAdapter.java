package com.nupi.agent.ui.controllers.geolocation.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.RealmPlannerOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by User on 20.10.2015
 */


public class GeolocationRouteMeteorAdapter extends RecyclerView.Adapter<GeolocationRouteMeteorAdapter.ViewHolder> {

    private static final int TYPE_START = 0;
    private static final int TYPE_ALL = 1;
    private static final int TYPE_FINISH = 2;
    private static final int TYPE_DISACTIVE = 3;

    private final RealmExchangeOperations realmExchangeOperations;
    private final RealmPlannerOperations realmPlannerOperations;
    private Realm realm;
    private PlannerRoute plannerRoute;
    private RealmList<ClientInfo> clientInfos;

    public GeolocationRouteMeteorAdapter(Realm realm, RealmExchangeOperations realmExchangeOperations, RealmPlannerOperations realmPlannerOperations, String routeGuid) {
        this.realmExchangeOperations = realmExchangeOperations;
        this.realmPlannerOperations = realmPlannerOperations;
        this.realm = realm;
        plannerRoute = realmPlannerOperations.getRouteByGuid(realm, routeGuid);
        clientInfos = plannerRoute.getClientsList();
        setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {
        ClientInfo clientInfo = clientInfos.get(position);
        CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(realmExchangeOperations.getRealm(), clientInfo.getClientId(), CounterAgent.class);
        if (counterAgent != null && counterAgent.getLatitude() == 0f && counterAgent.getLongitude() == 0)
            return TYPE_DISACTIVE;
        else if (position == 0)
            return TYPE_START;
        else if (position == (clientInfos.size() - 1))
            return TYPE_FINISH;
        else
            return TYPE_ALL;
    }

    @Override
    public int getItemCount() {
        return clientInfos.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        int layout;
        switch (viewType) {
            case TYPE_START:
                layout = R.layout.list_item_point_geolocation_start;
                break;
            case TYPE_FINISH:
                layout = R.layout.list_item_point_geolocation_end;
                break;
            case TYPE_DISACTIVE:
                layout = R.layout.list_item_point_geolocation_disactive;
                break;
            case TYPE_ALL:
            default:
                layout = R.layout.list_item_point_geolocation;
                break;
        }
        return new ViewHolder(inflater.inflate(layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final ClientInfo item = getItem(position);
        viewHolder.clientName.setText(item.getClientName());
        viewHolder.textMarker.setText(String.valueOf(position + 1));
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getClientId().hashCode();
    }

    public ClientInfo getItem(int i) {
        return clientInfos.get(i);
    }

    public String getRouteName() {
        return plannerRoute.getRouteName();
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_point_geolocation.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.image_marker)
        ImageView imageMarker;
        @InjectView(R.id.text_marker)
        TextView textMarker;
        @InjectView(R.id.clientName)
        TextView clientName;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_clients.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */

}
