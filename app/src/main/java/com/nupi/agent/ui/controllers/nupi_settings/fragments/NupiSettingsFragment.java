package com.nupi.agent.ui.controllers.nupi_settings.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nupi.agent.BuildConfig;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.events.OpenSupervisorEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.utils.ChatMessageUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class NupiSettingsFragment extends NupiFragment {

    @InjectView(R.id.today_radio_button)
    RadioButton todayRadioButton;
    @InjectView(R.id.analytics_radio_button)
    RadioButton analyticsRadioButton;
    @InjectView(R.id.other_radio_button)
    RadioButton otherRadioButton;
    @InjectView(R.id.allow_float_quantity)
    CheckBox allowFloatQuantityCheckBox;
    @InjectView(R.id.keep_breadcrumbs)
    CheckBox keepBreadcrumbs;
    @InjectView(R.id.version)
    TextView textViewVersion;
    @InjectView(R.id.store_unit)
    RadioButton storeUnit;
    @InjectView(R.id.report_unit)
    RadioButton reportUnit;
    @InjectView(R.id.change_base_url)
    TextView changeBaseUrl;
    @InjectView(R.id.route)
    TextView route;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nupi_settings, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (preferences.isLoggedIn() && preferences.getChatId() != null) {
            final ChatUser chatUser = realmChatOperations.getChatUserById(getChatRealm(), preferences.getChatId());
            if (chatUser != null) {
                if (chatUser.getChatRoute() == null)
                    route.setText(ChatMessageUtils.getNickname(chatUser));
                else
                    route.setText(String.format(getResources().getString(R.string.route_chat), ChatMessageUtils.getNickname(chatUser), chatUser.getChatRoute().getName()));
            } else
                route.setVisibility(View.GONE);
        } else
            route.setVisibility(View.GONE);

        if (BuildConfig.DEBUG)
            changeBaseUrl.setVisibility(View.VISIBLE);

        textViewVersion.setText(String.format(getResources().getString(R.string.version_nupi), BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));

        allowFloatQuantityCheckBox.setChecked(preferences.isFloatQuantityAllowed());

        keepBreadcrumbs.setChecked(preferences.getKeepBreadcrumbs());

        switch (preferences.getMainTab()) {
            case NupiPreferences.TAB_TODAY:
                todayRadioButton.toggle();
                break;
            case NupiPreferences.TAB_ANALYTICS:
                analyticsRadioButton.toggle();
                break;
            case NupiPreferences.TAB_OTHER:
                otherRadioButton.toggle();
                break;
        }

        switch (preferences.getDefaultUnit()) {
            case NupiPreferences.STORE_UNIT:
                storeUnit.toggle();
                break;
            case NupiPreferences.REPORT_UNIT:
                reportUnit.toggle();
                break;
        }
    }

    @OnClick(R.id.closeButton)
    void closeButtonClick() {
        getActivity().finish();
    }

    @OnClick(R.id.today_radio_button)
    void todayRadioButtonClick() {
        preferences.setMainTab(NupiPreferences.TAB_TODAY);
    }

    @OnClick(R.id.analytics_radio_button)
    void analyticsRadioButtonClick() {
        preferences.setMainTab(NupiPreferences.TAB_ANALYTICS);
    }

    @OnClick(R.id.other_radio_button)
    void otherRadioButtonClick() {
        preferences.setMainTab(NupiPreferences.TAB_OTHER);
    }

    @OnClick(R.id.store_unit)
    void storeUnitRadioButtonClick() {
        preferences.setDefaultUnit(NupiPreferences.STORE_UNIT);
    }

    @OnClick(R.id.report_unit)
    void reportUnitRadioButtonClick() {
        preferences.setDefaultUnit(NupiPreferences.REPORT_UNIT);
    }

    @OnClick(R.id.supervisor)
    void onSupervisorClick() {
        sendButtonEvent("Supervisor", true);

        bus.post(new OpenSupervisorEvent());
//        AuthDialog.show(getActivity(), getString(R.string.NUPI_admin_sign_in)).
//                flatMap(new Func1<Auth, Observable<Token>>() {
//                    @Override
//                    public Observable<Token> call(Auth auth) {
//                        return nupiAuthApi.getToken(auth);
//                    }
//                })
//                .flatMap(new Func1<Token, Observable<NupiPermissions>>() {
//                    @Override
//                    public Observable<NupiPermissions> call(Token token) {
//                        return nupiAuthApi.getNupiPermissions("Token " + token.token);
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<NupiPermissions>() {
//                               @Override
//                               public void call(NupiPermissions routeInfo) {
//                                   if (routeInfo.organisation_admin_status)
//                                       bus.post(new OpenSupervisorEvent());
//                                   else
//                                       MessageBox.show(getString(R.string.access_denied), getActivity());
//                               }
//                           },
//                        new Action1<Throwable>() {
//                            @Override
//                            public void call(Throwable throwable) {
//                                if (throwable instanceof RetrofitError) {
//                                    Response response = ((RetrofitError) throwable).getResponse();
//                                    if (response != null) {
//                                        switch (response.getStatus()) {
//
//                                        }
//                                    }
//                                }
//                            }
//                        });
    }

    @OnClick(R.id.special_options)
    void onSpecialOptionsClick() {
        sendButtonEvent("Accessibility", true);
        startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
    }

    @OnClick(R.id.change_base_url)
    void onChangeBaseUrlClick() {
        sendButtonEvent("Change base URL", true);
        final EditText input = new EditText(getActivity());
        input.setText(preferences.getBaseUrl());
        new AlertDialog.Builder(getActivity())
                .setTitle("Change base URL")
                .setView(input)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        preferences.setBaseUrl(input.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    @OnClick(R.id.allow_float_quantity)
    void onAllowFloatQuantityClick() {
        sendButtonEvent("Allow float quantity", true);
        preferences.setFloatQuantityAllowed(allowFloatQuantityCheckBox.isChecked());
    }

    @OnClick(R.id.keep_breadcrumbs)
    void onKeepBreadcrumbsClick() {
        sendButtonEvent("Keep breadcrumbs", true);
        preferences.setKeepBreadcrumbs(keepBreadcrumbs.isChecked());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
