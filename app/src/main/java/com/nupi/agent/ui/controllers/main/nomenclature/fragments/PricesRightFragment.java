package com.nupi.agent.ui.controllers.main.nomenclature.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Organization;
import com.nupi.agent.events.ClientDebtEvent;
import com.nupi.agent.events.CounterAgentSearchButtonPressedEvent;
import com.nupi.agent.events.DismissNumPadEvent;
import com.nupi.agent.events.HomeButtonEvent;
import com.nupi.agent.events.OpenGalleryEvent;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.ShowPriceNumPadEvent;
import com.nupi.agent.events.TakePhotoEvent;
import com.nupi.agent.events.UpdatePriceRealisationEvent;
import com.nupi.agent.network.meteor.OrderRequest;
import com.nupi.agent.network.upload.UploadService;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.main.main_screen.fragments.SearchFragment;
import com.nupi.agent.ui.dialogs.ListDialog;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.utils.StringUtils;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class PricesRightFragment extends NupiFragment {

    @Inject
    UploadService uploadService;
    @InjectView(R.id.currentClientDebt)
    TextView clientDebtTextView;
    @InjectView(R.id.comment_text)
    TextView commentTextTextView;
    @InjectView(R.id.comment_icon)
    ImageView commentIcon;
    @InjectView(R.id.send_btn)
    LinearLayout sendButton;
    @InjectView(R.id.clients_debt)
    View clients_debt;
    @InjectView(R.id.send_btn_image)
    ImageView imageViewSend;
    @InjectView(R.id.send_btn_text)
    TextView textViewSend;

    @InjectView(R.id.contract_text)
    TextView textViewSelectionMethod;
    @InjectView(R.id.contract_icon)
    ImageView imageViewSelectionMethod;

    private OrderRequest orderRequest;
    private RealmResults<Contract> contracts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prices_right, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Animation anim = AnimationUtils.loadAnimation(
                getActivity(), R.anim.blink_anim);
        clients_debt.startAnimation(anim);

        showReceiptFragment();
        orderRequest = orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedCounterAgentGuid());

        contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
        if (contracts.size() == 1) {
            setContract(contracts.get(0), orderRequest);
        }

        checkSendButtonAvailability();
        checkComment(orderRequest.getComment());
        setDebt();
    }

    public void setDebt() {
        if (!selectedItemsHolder.getSelectedCounterAgentGuid().equals(CounterAgent.NEW_POINT)) {
            clients_debt.setClickable(true);
            clients_debt.setBackground(getResources().getDrawable(R.drawable.selector_nupi_red));
            clientDebtTextView.setText(StringUtils.getMoneyFormat(realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class).getDebt()));
            textViewSelectionMethod.setText(getString(R.string.contract));

        } else {
            clients_debt.setClickable(false);
            clients_debt.setBackgroundColor(getResources().getColor(R.color.grey));
            clientDebtTextView.setText(StringUtils.getMoneyFormat(0f));
            textViewSelectionMethod.setText(getString(R.string.organization));
        }
    }

    public void showReceiptFragment() {
        getChildFragmentManager().beginTransaction()
                .replace(R.id.priceCenterContainer, new PriceReceiptFragment())
                .commit();
    }

    public void checkComment(String comment) {
        if (getActivity() != null)
            if (TextUtils.isEmpty(comment)) {
                commentIcon.setImageResource(R.drawable.nupi_no_comment);
                commentTextTextView.setTextColor(Color.WHITE);
            } else {
                commentIcon.setImageResource(R.drawable.nupi_comment);
                commentTextTextView.setTextColor(getActivity().getResources().getColor(R.color.yellow));
            }
    }

    @OnClick(R.id.camera_button)
    void onCameraButton() {
        if (!selectedItemsHolder.getSelectedCounterAgentGuid().equals(CounterAgent.NEW_POINT)) {
            sendButtonEvent("Camera", true);
            bus.post(new TakePhotoEvent(realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class).getName()));
        } else
            showToast(R.string.not_available);
    }

    @OnClick(R.id.gallery_button)
    void onGalleryButton() {
        if (!selectedItemsHolder.getSelectedCounterAgentGuid().equals(CounterAgent.NEW_POINT)) {
            sendButtonEvent("Gallery", true);
            bus.post(new OpenGalleryEvent(realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class).getName()));
        } else
            showToast(R.string.not_available);
    }

    @OnClick(R.id.map_button)
    void onGeolocationButton() {
        if (!selectedItemsHolder.getSelectedCounterAgentGuid().equals(CounterAgent.NEW_POINT)) {
            sendButtonEvent("Geolocation", true);
            bus.post(new OpenGeolocationEvent(OpenGeolocationEvent.OpenType.POINT));
        } else
            showToast(R.string.not_available);
    }

    @OnClick(R.id.contract_selection)
    void onContractSelectionClick() {
        if (selectedItemsHolder.getSelectedCounterAgentGuid().equals(CounterAgent.NEW_POINT)) {
            final RealmResults<Organization> organizations = realmExchangeOperations.getRealmObjects(getMeteorRealm(), Organization.class);
            new ListDialog() {
                @Override
                public String getDialogMessage() {
                    return getString(R.string.choose_organization);
                }

                @Override
                public CharSequence[] getItemsSource() {
                    final CharSequence[] sequences = new String[organizations.size()];
                    for (int i = 0; i < sequences.length; i++) {
                        final Organization organization = organizations.get(i);
                        sequences[i] = organization.getName();
                    }
                    return sequences;
                }

                @Override
                public void onItemClick(int itemId) {
                    orderRequest.setOrganization(organizations.get(itemId).getGuid());
                    orderApi.setRealisationForClient(selectedItemsHolder.getSelectedCounterAgentGuid(), orderRequest);
                    checkSendButtonAvailability();
                }
            }.show(getActivity());
        } else if (contracts.size() > 0)
            new ListDialog() {
                @Override
                public String getDialogMessage() {
                    return getString(R.string.choose_contract);
                }

                @Override
                public CharSequence[] getItemsSource() {
                    final CharSequence[] sequences = new String[contracts.size()];
                    for (int i = 0; i < sequences.length; i++) {
                        final Contract contract = contracts.get(i);
                        final Organization organization = realmExchangeOperations.getRealmObjectByGuid(
                                getMeteorRealm(),
                                contract.getOrganization(),
                                Organization.class
                        );
                        if (organization != null && organization.getName() != null)
                            sequences[i] = String.format("%s (%s)", contract.getName(), organization.getName());
                        else
                            sequences[i] = contract.getName();
                    }
                    return sequences;
                }

                @Override
                public void onItemClick(int itemId) {
                    setContract(contracts.get(itemId), orderRequest);
                }
            }.show(getActivity());
        else
            MessageBox.show(getString(R.string.no_contracts_for_counter_agent), getActivity());
    }

    @OnClick(R.id.clients_debt)
    void onClientsDebtSelect() {
        sendButtonEvent("Debt", true);
        bus.post(new ClientDebtEvent());
    }

    @OnClick(R.id.comment)
    void onCommentClick() {
        final Context context = getActivity();
        final EditText input = new EditText(context);
        input.setText(orderRequest.getComment());
        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.write_comment))
                .setView(input)
                .setPositiveButton(context.getString(R.string.write), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        sendButtonEvent("Comment", true);
                        String comment = input.getText().toString().replace("\n", " ").replace("\t", " ").replace("\r", " ");
                        orderRequest.setComment(comment);
                        checkComment(comment);
                    }
                }).setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                sendButtonEvent("Comment", false);
            }
        }).show();
    }


    @OnClick(R.id.send_btn)
    void onSendButtonClick() {
        if (orderRequest.getRecordsCount() != 0) {
            if (!TextUtils.isEmpty(orderRequest.getOrganization())) {
                if (selectedItemsHolder.getSelectedCounterAgentGuid().equals(CounterAgent.NEW_POINT)) {
                    if (TextUtils.isEmpty(orderRequest.getComment())) {
                        sendButtonEvent("Send Order", false);
                        Toast.makeText(getActivity(), R.string.need_permission_comment, Toast.LENGTH_LONG).show();
                    } else {
                        sendButtonEvent("Send Order", true);
                        String clientGuid = selectedItemsHolder.getSelectedCounterAgentGuid();
                        orderRequest.setContract(CounterAgent.NEW_POINT);
                        uploadService.sendOrderDocToServer(orderApi.removeOrderRequestForClient(clientGuid));
                        preferences.setOrdersApi(orderApi);
                        removeHasOrdersMarkFromCounterAgent();
                        MessageBox.show(getString(R.string.sending_request), getActivity());
                        bus.post(new HomeButtonEvent());
                    }
                } else {
                    sendButtonEvent("Send Order", true);
                    String clientGuid = selectedItemsHolder.getSelectedCounterAgentGuid();
                    uploadService.sendOrderDocToServer(orderApi.removeOrderRequestForClient(clientGuid));
                    preferences.setOrdersApi(orderApi);
                    removeHasOrdersMarkFromCounterAgent();
                    MessageBox.show(getString(R.string.sending_request), getActivity());
                    bus.post(new HomeButtonEvent());
                }
            } else {
                sendButtonEvent("Send Order", false);
                showToast(R.string.need_to_select_organisation);
            }
        } else {
            sendButtonEvent("Send Order", false);
            showToast(R.string.need_to_add_goods);
        }
    }

    private void removeHasOrdersMarkFromCounterAgent() {
        final Realm meteorRealm = getMeteorRealm();
        final CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(
                meteorRealm,
                selectedItemsHolder.getSelectedCounterAgentGuid(),
                CounterAgent.class
        );
        meteorRealm.beginTransaction();
        try {
            counterAgent.setHasOrders(false);
            meteorRealm.commitTransaction();
        } catch (Exception e) {
            Crashlytics.logException(e);
            meteorRealm.cancelTransaction();
        }
    }

    @Subscribe
    public void onCounterAgentsSearchButtonPressedEvent(CounterAgentSearchButtonPressedEvent event) {
        if (event.isActivate())
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.priceCenterContainer, new SearchFragment())
                    .commit();
    }


    @Subscribe
    public void onShowPriceNumPad(ShowPriceNumPadEvent event) {
        getChildFragmentManager().beginTransaction()
                .replace(R.id.priceCenterContainer, new OrderNumericKeyboardFragment())
                .commit();
    }


    @Subscribe
    public void onUpdateRealisation(UpdatePriceRealisationEvent event) {
        orderRequest = orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedCounterAgentGuid());
        checkSendButtonAvailability();
        checkComment(orderRequest.getComment());
        contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
        clientDebtTextView.setText(StringUtils.getMoneyFormat(realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class).getDebt()));
        setDebt();
    }


    @Subscribe
    public void onDismissNumPad(DismissNumPadEvent event) {
        checkSendButtonAvailability();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.priceCenterContainer, new PriceReceiptFragment())
                .commit();
    }

    private void setContract(Contract contract, OrderRequest orderRequest) {
        orderRequest.setContract(contract.getGuid());
        orderRequest.setOrganization(contract.getOrganization());
        orderApi.setRealisationForClient(selectedItemsHolder.getSelectedCounterAgentGuid(), orderRequest);
        checkSendButtonAvailability();
    }

    private void checkSendButtonAvailability() {
        if (orderRequest.getRecordsCount() != 0) {
            imageViewSend.setImageResource(R.drawable.nupi_complete_operation);
            textViewSend.setTextColor(getResources().getColor(R.color.text_button));
        } else {
            imageViewSend.setImageResource(R.drawable.nupi_complete_operation_lock);
            textViewSend.setTextColor(getResources().getColor(R.color.text_button_disable));
        }

        if (!TextUtils.isEmpty(orderRequest.getOrganization())) {
            textViewSelectionMethod.setTextColor(getResources().getColor(R.color.text_yellow));
            imageViewSelectionMethod.setImageResource(R.drawable.nupi_contract_yellow);
        } else {
            textViewSelectionMethod.setTextColor(getResources().getColor(R.color.text_button));
            imageViewSelectionMethod.setImageResource(R.drawable.nupi_contract_white);
        }
    }

}