package com.nupi.agent.ui.controllers.presenter.activities;

import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.presenter.fragments.PresenterFragment;
import com.squareup.otto.Subscribe;


public class PresenterActivity extends NupiActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_presenter);

        if (savedInstanceState == null)
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new PresenterFragment())
                    .commit();

    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

}
