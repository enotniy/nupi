package com.nupi.agent.ui.base;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public abstract class BaseAdapter<T> extends ArrayAdapter<T> {

    protected String filterText = "";
    private ArrayList<T> original;
    private ArrayList<T> filtered_items;
    private Filter filter;
    private int layoutId;
    private Runnable onFilterFinished = null;

    public BaseAdapter(Context context, int layoutId, List<T> data) {
        super(context, layoutId, data);
        this.layoutId = layoutId;
        original = new ArrayList<>(data);
        filtered_items = new ArrayList<>(original);
    }

    public BaseAdapter(Context context, int layoutId, List<T> data, @Nullable Runnable onFilterFinished) {
        this(context, layoutId, data);
        this.onFilterFinished = onFilterFinished;
    }

    public ArrayList<T> getFiltered_items() {
        return filtered_items;
    }

    @Override
    public long getItemId(int position) {
        return original.indexOf(filtered_items.get(position));
    }


    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new LiveSearchFilter();

        return filter;
    }

    public abstract String getItemFilterableValue(T item);

    public abstract void sort();

    public void removeElement(T element) {
        remove(element);
        original.remove(element);
        filtered_items.remove(element);
        notifyDataSetChanged();
    }

    private class LiveSearchFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            filterText = constraint.toString().toLowerCase();
            ArrayList<T> list = new ArrayList<>(original);

            if (filterText.length() == 0) {
                results.values = list;
                results.count = list.size();
            } else {
                ArrayList<T> filtered_list = new ArrayList<>();

                for (final T item : list) {
                    final String value = getItemFilterableValue(item).toLowerCase();

                    if (value.contains(filterText)) {
                        filtered_list.add(item);
                    }
                }
                results.values = filtered_list;
                results.count = filtered_list.size();
            }
            return results;
        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (ArrayList<T>) results.values;

            clear();
            for (T item : filtered_items) {
                add(item);
            }
            sort();
            if (onFilterFinished != null)
                onFilterFinished.run();
        }

    }
}

