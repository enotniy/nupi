package com.nupi.agent.ui.controllers.chat.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.R;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.events.ChatThreadSelectEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.controllers.chat.adapters.ChatGroupsAdapter;
import com.nupi.agent.ui.controllers.chat.adapters.ChatUsersAdapter;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.utils.NetworkUtils;
import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import icepick.State;
import io.realm.Realm;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by User on 19.10.2015.
 */
public class ChatListFragment extends BaseChatFragment {

    @InjectView(R.id.home_button)
    ImageView homeButton;
    @InjectView(R.id.personal_list)
    TextView personalList;
    @InjectView(R.id.group_list)
    TextView groupList;
    @InjectView(R.id.list_users)
    ListView listUsers;
    @InjectView(R.id.list_groups)
    ListView listGroups;
    @State
    boolean fromNotification = false;
    @State
    boolean openPrivate = true;
    @State
    String selectedId;
    private ChatUsersAdapter chatUsersAdapter;
    private ChatGroupsAdapter chatGroupsAdapter;

    public static ChatListFragment getPrivateInstance(String id) {
        return getInstance(id, true);
    }

    public static ChatListFragment getGroupsInstance(String id) {
        return getInstance(id, false);
    }

    @NonNull
    public static ChatListFragment getInstance(String id, boolean openPrivate) {
        final ChatListFragment chatListFragment = new ChatListFragment();
        chatListFragment.fromNotification = true;
        chatListFragment.selectedId = id;
        chatListFragment.openPrivate = openPrivate;
        chatListFragment.setIcepickArguments();
        return chatListFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat_list, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        openPrivate = fromNotification ? openPrivate : selectedItemsHolder.isChatUsersSelected();
        setChatButton(openPrivate);

        String index = selectedItemsHolder.getChatSelectedId();

        String userId = preferences.getChatId();
        chatUsersAdapter = new ChatUsersAdapter(getActivity(), realmChatOperations, getChatRealm(), realmChatOperations.getChatUsers(getChatRealm(), userId), index, userId);
        listUsers.setAdapter(chatUsersAdapter);


        chatGroupsAdapter = new ChatGroupsAdapter(getActivity(), realmChatOperations, getChatRealm(), realmChatOperations.getChatGroups(getChatRealm()), index, userId);
        listGroups.setAdapter(chatGroupsAdapter);
    }

    @Subscribe
    @Override
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        super.onUpdateWorkflowEvent(event);
    }

    private void setChatButton(boolean userMessages) {
        openPrivate = userMessages;
        if (userMessages) {
            personalList.setBackgroundColor(getResources().getColor(R.color.white));
            personalList.setTextColor(getResources().getColor(R.color.text_title_light));
            groupList.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_nupi_blue_dark));
            groupList.setTextColor(getResources().getColor(R.color.text_button));
            listUsers.setVisibility(View.VISIBLE);
            listGroups.setVisibility(View.GONE);
        } else {
            groupList.setBackgroundColor(getResources().getColor(R.color.white));
            groupList.setTextColor(getResources().getColor(R.color.text_title_light));
            personalList.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_nupi_blue_dark));
            personalList.setTextColor(getResources().getColor(R.color.text_button));
            listUsers.setVisibility(View.GONE);
            listGroups.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }

    @OnClick(R.id.group_list)
    void onButtonGroupList() {
        setChatButton(false);
    }


    @OnClick(R.id.personal_list)
    void onButtonPersonalList() {
        setChatButton(true);
    }


    @OnItemClick({R.id.list_groups, R.id.list_users})
    void onUserSelected(int position) {

        selectedItemsHolder.setChatUsersSelected(openPrivate);
        final String id = openPrivate ?
                chatUsersAdapter.getItem(position).getUser() :
                chatGroupsAdapter.getItem(position).getId();
        final long messageCount = openPrivate ?
                chatUsersAdapter.getItem(position).getMessageCount() :
                chatGroupsAdapter.getItem(position).getMessageCount();
        final long dbMessageCount = openPrivate ?
                realmChatOperations.getPrivateChatMessagesCount(getChatRealm(), id) :
                realmChatOperations.getGroupChatMessagesCount(getChatRealm(), id);

        if (!chatGroupsAdapter.isEmpty() && !chatUsersAdapter.isEmpty()) {
            if (openPrivate) {
                chatUsersAdapter.setSelectedIndex(id);
                chatUsersAdapter.notifyDataSetChanged();
                chatGroupsAdapter.unselectedTab();
                chatGroupsAdapter.notifyDataSetChanged();
            } else {
                chatGroupsAdapter.setSelectedIndex(id);
                chatGroupsAdapter.notifyDataSetChanged();
                chatUsersAdapter.unselectedTab();
                chatUsersAdapter.notifyDataSetChanged();
            }
        }

        fromNotification = false;
        selectedItemsHolder.setChatSelectedId(id);
        bus.post(new ChatThreadSelectEvent());


        if (dbMessageCount < messageCount)
            (selectedItemsHolder.isChatUsersSelected() ?
                    nupiServerApi.getChatUserMessages(selectedItemsHolder.getChatSelectedId()) :
                    nupiServerApi.getChatGroupMessages(selectedItemsHolder.getChatSelectedId()))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<ChatMessage>>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (getActivity() != null) {
                                switch (NetworkUtils.getNetworkError(e)) {
                                    case NetworkUtils.CODE_ERROR:
                                        Crashlytics.logException(e);
                                        MessageBox.show(getString(R.string.go_to_admins), getActivity());
                                        break;
                                    case NetworkUtils.NETWORK_ERROR:
                                        MessageBox.show(getString(R.string.no_server_connection), getActivity());
                                        break;
                                    case 403:
                                        MessageBox.show(getString(R.string.forbidden), getActivity());
                                        break;
                                    case 401:
                                        MessageBox.show(getString(R.string.unauthorized), getActivity());
                                        break;
                                    default:
                                        MessageBox.show(getString(R.string.server_error), getActivity());
                                }
                            }
                        }

                        @Override
                        public void onNext(List<ChatMessage> chatMessages) {

                            try (Realm realm = realmChatOperations.getRealm()) {
                                realmChatOperations.copyToRealmOrUpdateChatMessages(realm, chatMessages);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
    }

}
