package com.nupi.agent.ui.keyboard;

import android.inputmethodservice.KeyboardView;
import android.view.KeyEvent;
import android.view.inputmethod.BaseInputConnection;
import android.widget.EditText;

/**
 * Created by Pasenchuk Victor on 12.11.14
 */
public abstract class KeyboardActionListener implements KeyboardView.OnKeyboardActionListener {

    private static final int ENTER_KEY = 50000, CANCEL_KEY = 50001, VOICE_SEARCH_KEY = 50002, LANGUAGE_EN_KEY = 50003, LANGUAGE_NATIVE_KEY = 50004, LANGUAGE_NUMERIC_KEY = 50005, BACKSPACE_KEY = 50006;

    private EditText editText;
    private BaseInputConnection textFieldInputConnection;

    protected KeyboardActionListener(EditText editText) {
        this.editText = editText;
        textFieldInputConnection = new BaseInputConnection(editText, true);
    }

    public abstract void onEnterKey();

    public abstract void onCancelKey();

    public abstract void onVoiceSearchKey();

    public abstract void onLanguageEnKey();

    public abstract void onLanguageNativeKey();

    public abstract void onLanguageNumericKey();

    @Override
    public void onKey(int code, int[] codes) {
        if (!editText.isFocused())
            editText.requestFocus();
        switch (code) {
            case ENTER_KEY:
                onEnterKey();
                return;
            case CANCEL_KEY:
                onCancelKey();
                return;
            case BACKSPACE_KEY:
                textFieldInputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
                return;
            case VOICE_SEARCH_KEY:
                onVoiceSearchKey();
                return;
            case LANGUAGE_EN_KEY:
                onLanguageEnKey();
                return;
            case LANGUAGE_NATIVE_KEY:
                onLanguageNativeKey();
                return;
            case LANGUAGE_NUMERIC_KEY:
                onLanguageNumericKey();
                return;
            default: {
                int start = editText.getSelectionStart();
                editText.getText().insert(start, getCharString(code));
            }
        }
    }

    private String getCharString(int code) {
        return Character.toString((char) code);
    }

    @Override
    public void onPress(int i) {

    }

    @Override
    public void onRelease(int i) {

    }

    @Override
    public void onText(CharSequence charSequence) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
