package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.analytics.fragments.SmallAnalyticsFragment;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class CenterControlsFragment extends NupiFragment {

    @InjectView(R.id.other_image)
    ImageView other_image;

    @InjectView(R.id.other)
    LinearLayout other;

    @InjectView(R.id.analytics)
    TextView analytics;

    @InjectView(R.id.offers)
    TextView offers;

    @InjectView(R.id.today)
    TextView today;

    public CenterControlsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_controls, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        openDefaultTab();
    }

    @Override
    public void onResume() {
        super.onResume();
        openDefaultTab();
    }

    private void openDefaultTab() {
        NupiFragment fragment;
        switch (preferences.getMainTab()) {
            case NupiPreferences.TAB_TODAY:
                View tabViews[] = {analytics, other, offers};
                switchControlButton(today, tabViews);
                fragment = new TodayFragment();
                break;
            case NupiPreferences.TAB_ANALYTICS:
                tabViews = new View[]{other, today, offers};
                switchControlButton(analytics, tabViews);
                fragment = new SmallAnalyticsFragment();
                break;
            default:
                tabViews = new View[]{analytics, today, offers};
                switchControlButton(other, tabViews);
                fragment = new OtherFragment();
                break;
        }

        getChildFragmentManager().beginTransaction()
                .replace(R.id.specialArea, fragment)
                .commit();
    }


    @OnClick(R.id.today)
    void onTodayClick() {
        if (preferences.isLoggedIn()) {
            View tabViews[] = {offers, analytics, other};
            switchControlButton(today, tabViews);
            sendButtonEvent("Today", true);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.specialArea, new TodayFragment())
                    .commit();
        } else
            sendButtonEvent("Today", false);
    }

    @OnClick(R.id.offers)
    void onOffersClick() {
        View tabViews[] = {analytics, today, other};
        switchControlButton(offers, tabViews);
        sendButtonEvent("Offers", true);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.specialArea, new OffersFragment())
                .commit();
    }

    @OnClick(R.id.analytics)
    void onAnalyticsClick() {
        View tabViews[] = {offers, today, other};
        switchControlButton(analytics, tabViews);
        sendButtonEvent("Small analytics", true);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.specialArea, new SmallAnalyticsFragment())
                .commit();
    }

    @OnClick(R.id.other)
    void onOtherClick() {
        View TabViews[] = {analytics, today, offers};
        switchControlButton(other, TabViews);
        sendButtonEvent("Other", true);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.specialArea, new OtherFragment())
                .commit();
    }

    private void switchControlButton(View activated, View[] disactivated) {
        for (View v : disactivated) {
            v.setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_nupi_blue));
            if (v instanceof TextView)
                ((TextView) v).setTextColor(getResources().getColor(R.color.text_button));
            else if (v instanceof LinearLayout)
                other_image.setImageResource(R.drawable.nupi_other_etc_white);
        }

        activated.setBackgroundColor(getResources().getColor(R.color.white));
        if (activated instanceof TextView)
            ((TextView) activated).setTextColor(getResources().getColor(R.color.text_title_light));
        else if (activated instanceof LinearLayout)
            other_image.setImageResource(R.drawable.nupi_other_etc);
    }
}
