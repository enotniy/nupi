package com.nupi.agent.ui.controllers.planner.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.RealmList;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class PointsEditorAdapter extends RecyclerView.Adapter<PointsEditorAdapter.ViewHolder> {

    protected Context context;
    protected ArrayList<ClientInfo> points;
    @Inject
    OrderApi orderApi;
    private RealmList<ClientInfo> selectedPoint;

    public PointsEditorAdapter(Context context, List<ClientInfo> data, List<ClientInfo> selectedPoints) {
        NupiApp nupiApp = (NupiApp) context.getApplicationContext();
        nupiApp.getAppComponent().inject(this);
        points = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            points.add(data.get(i));
        }
        this.context = context;

        this.selectedPoint = new RealmList<>();
        if (selectedPoints != null)
            for (ClientInfo clientInfo : selectedPoints)
                this.selectedPoint.add(clientInfo);
        setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_planner_editor_create, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ClientInfo item = points.get(position);

        // set text
        holder.setClientInfo(item);
        holder.shop.setText(String.valueOf(item.getClientName()));
        if (item.getWorkingBeginTime() != null && item.getWorkingEndTime() != null)
            holder.workingTime.setText(String.format(context.getString(R.string.working_time), TimeUtils.timeFormat(item.getWorkingBeginTime()), TimeUtils.timeFormat(item.getWorkingEndTime())));
        else
            holder.workingTime.setText("");

        if (item.getLunchBeginTime() != null && item.getLunchEndTime() != null)
            holder.lunchTime.setText(String.format(context.getString(R.string.lunch_time), TimeUtils.timeFormat(item.getLunchBeginTime()), TimeUtils.timeFormat(item.getLunchEndTime())));
        else
            holder.lunchTime.setText("");

        holder.visitFrequency.setText(item.getVisitFrequency() != null ? String.valueOf(item.getVisitFrequency()) : "");
        holder.responsiblePerson.setText(item.getResponsiblePerson() != null ? String.valueOf(item.getResponsiblePerson()) : "");
        String phone = item.getContacts() != null ? String.valueOf(item.getContacts()) : "";
        String additional_phone = item.getContacts() != null ? String.valueOf(item.getAdditionalPhone()) : "";
        if (!phone.equals("") && !additional_phone.equals(""))
            phone += "\n";
        String email = item.getContacts() != null ? String.valueOf(item.getEmail()) : "";
        if ((!phone.equals("") || !additional_phone.equals("")) && !email.equals(""))
            additional_phone += "\n";
        holder.contacts.setText(String.format("%s%s%s", phone, additional_phone, email));
        int index = selectedPoint.indexOf(item);
        if (index > -1) {
            holder.number.setText(String.valueOf(index + 1));
            holder.clientRow.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item_selected));
        } else {
            holder.number.setText("");
            holder.clientRow.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item));
        }

        // set background resource (target view ID: container)
        final int dragState = holder.getDragStateFlags();

        if (((dragState & DraggableItemConstants.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;

            if ((dragState & DraggableItemConstants.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.drawable.selector_list_item_selected;

                // need to clear drawable state here to get correct appearance of the dragging item.
                Drawable drawable = holder.container.getBackground();
                if (drawable != null)
                    drawable.setState(new int[]{});
            } else if ((dragState & DraggableItemConstants.STATE_FLAG_DRAGGING) != 0) {
                bgResId = R.drawable.selector_list_item_selected;
            } else {
                bgResId = R.drawable.selector_list_item;
            }

            holder.container.setBackgroundResource(bgResId);
        }
    }

    @Override
    public int getItemCount() {
        return points.size();
    }

    @Override
    public long getItemId(int position) {
        return points.get(position).hashCode();
    }

    public RealmList<ClientInfo> getSelectedPoints() {
        return selectedPoint;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_planner.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */

    class ViewHolder extends AbstractDraggableItemViewHolder {

        @InjectView(R.id.number)
        TextView number;
        @InjectView(R.id.shop)
        TextView shop;
        @InjectView(R.id.working_time)
        TextView workingTime;
        @InjectView(R.id.lunch_time)
        TextView lunchTime;
        @InjectView(R.id.info)
        LinearLayout info;
        @InjectView(R.id.visit_frequency)
        TextView visitFrequency;
        @InjectView(R.id.responsible_person)
        TextView responsiblePerson;
        @InjectView(R.id.contacts)
        TextView contacts;
        @InjectView(R.id.container)
        FrameLayout container;
        @InjectView(R.id.clientRow)
        RelativeLayout clientRow;
        ClientInfo clientInfo;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public ClientInfo getClientInfo() {
            return clientInfo;
        }

        public void setClientInfo(ClientInfo clientInfo) {
            this.clientInfo = clientInfo;
        }

        @OnClick(R.id.container)
        void onPointClick() {
            if (selectedPoint.contains(clientInfo))
                selectedPoint.remove(clientInfo);
            else
                selectedPoint.add(clientInfo);
            notifyDataSetChanged();
        }

    }
}

