package com.nupi.agent.ui.controllers.geolocation.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.service.SelectedItemsHolder;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by User on 20.10.2015
 */


public class GeolocationCounterAgentsMeteorAdapter extends RecyclerView.Adapter<GeolocationCounterAgentsMeteorAdapter.ViewHolder> {

    @Inject
    public RealmExchangeOperations realmExchangeOperations;
    @Inject
    public SelectedItemsHolder selectedItemsHolder;
    private Realm realm;
    private RealmResults<CounterAgent> counterAgents;
    private Activity context;

    public GeolocationCounterAgentsMeteorAdapter(Activity context, Realm realm) {
        NupiApp nupiApp = (NupiApp) context.getApplicationContext();
        nupiApp.getAppComponent().inject(this);
        this.realm = realm;
        this.context = context;
        setHasStableIds(true);
        updateCounterAgents();
    }

    private void updateCounterAgents() {
        counterAgents = realmExchangeOperations.getNonFolderCounterAgents(realm);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return counterAgents.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.list_item_clients_geolocation, parent, false));
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final CounterAgent counterAgent = getItem(position);
        viewHolder.clientName.setText(counterAgent.getName());
        if (selectedItemsHolder.getSelectedCounterAgentGuid() != null && selectedItemsHolder.getSelectedCounterAgentGuid().equals(counterAgent.getGuid()))
            viewHolder.clientRow.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item_selected));
        else
            viewHolder.clientRow.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item));
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getGuid().hashCode();
    }

    public CounterAgent getItem(int i) {
        return counterAgents.get(i);
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_clients.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.clientName)
        TextView clientName;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
