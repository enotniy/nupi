package com.nupi.agent.ui.controllers.main.nomenclature.fragments;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.nupi.agent.R;
import com.nupi.agent.events.CounterAgentSearchButtonPressedEvent;
import com.nupi.agent.events.DismissNumPadEvent;
import com.nupi.agent.events.PricesSearchStopEvent;
import com.nupi.agent.events.SearchStringEvent;
import com.nupi.agent.events.ShowPriceNumPadEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.main.nomenclature.adapters.NomenclatureSearchMeteorAdapter;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class PricesSearchFragment extends NupiFragment implements ExpandableListView.OnChildClickListener {

    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";


    @InjectView(R.id.goodsListView)
    RecyclerView goodsListView;

    private NomenclatureSearchMeteorAdapter nomenclatureSearchMeteorAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prices_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initList(savedInstanceState);

    }

    private void initList(Bundle savedInstanceState) {
        mLayoutManager = new LinearLayoutManager(getActivity());
        final Parcelable eimSavedState = (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);
        mRecyclerViewExpandableItemManager.setOnGroupExpandListener(new RecyclerViewExpandableItemManager.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition, boolean fromUser) {
                if (fromUser) {
                    int childItemHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.list_row_height);
                    int margin = (int) (getActivity().getResources().getDisplayMetrics().density * 16); // top-spacing: 16dp
                    mRecyclerViewExpandableItemManager.scrollToGroup(groupPosition, childItemHeight, margin, margin);
                }
            }
        });
        mRecyclerViewExpandableItemManager.setOnGroupCollapseListener(new RecyclerViewExpandableItemManager.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition, boolean fromUser) {
            }
        });
        nomenclatureSearchMeteorAdapter = new NomenclatureSearchMeteorAdapter(getActivity(), getMeteorRealm(), orderApi, selectedItemsHolder);

        mWrappedAdapter = mRecyclerViewExpandableItemManager.createWrappedAdapter(nomenclatureSearchMeteorAdapter);// wrap for expanding

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        // Change animations are enabled by default since support-v7-recyclerview v22.
        // Need to disable them when using animation indicator.
        animator.setSupportsChangeAnimations(false);

        goodsListView.setLayoutManager(mLayoutManager);
        goodsListView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        goodsListView.setItemAnimator(animator);
        goodsListView.setHasFixedSize(false);

        // additional decorations
        //noinspection StatementWithEmptyBody
        if (supportsViewElevation()) {
            // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
        } else {
            goodsListView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
        }
        goodsListView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));


        mRecyclerViewExpandableItemManager.attachRecyclerView(goodsListView);
        mRecyclerViewExpandableItemManager.expandAll();
    }

    private void setUpPricesAdapter(String searchString) {
        nomenclatureSearchMeteorAdapter.setFilterString(searchString);
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        setUpPricesAdapter("");
        bus.post(new CounterAgentSearchButtonPressedEvent());
        bus.post(new PricesSearchStopEvent());
    }


    @Subscribe
    public void filterByString(SearchStringEvent searchStringEvent) {
        String searchString;
        if (searchStringEvent.getSearchString() == null) {
            searchString = "";
        } else
            searchString = searchStringEvent.getSearchString().toString().toLowerCase();
        setUpPricesAdapter(searchString);
        mRecyclerViewExpandableItemManager.expandAll();
    }

    @Subscribe
    public void onDismissNumPad(DismissNumPadEvent event) {
        nomenclatureSearchMeteorAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        bus.post(new ShowPriceNumPadEvent());
        return false;
    }
}
