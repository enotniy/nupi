package com.nupi.agent.ui.controllers.presenter.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nupi.agent.R;
import com.nupi.agent.ui.base.NupiFragment;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by User on 22.10.2015.
 */
public class PresenterFragment extends NupiFragment {

    @InjectView(R.id.topPanel)
    LinearLayout linearLayoutTopPanel;
    @InjectView(R.id.searchPanel)
    LinearLayout linearLayoutSearchPanel;
    @InjectView(R.id.close_search)
    ImageView imageViewCloseSearch;
    @InjectView(R.id.text_search)
    EditText editTextSearch;
    private Boolean isSeach = false;

    public PresenterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_presenter_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.homeButton)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }


    @OnClick(R.id.search)
    void onSeachButtonPress() {
        sendButtonEvent("Search presenter open", true);
        linearLayoutTopPanel.setVisibility(View.GONE);
        linearLayoutSearchPanel.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.close_search)
    void onSeachCloseButtonPress() {
        sendButtonEvent("Search presenter close", true);
        linearLayoutTopPanel.setVisibility(View.VISIBLE);
        linearLayoutSearchPanel.setVisibility(View.GONE);
        editTextSearch.setText("");
    }
}
