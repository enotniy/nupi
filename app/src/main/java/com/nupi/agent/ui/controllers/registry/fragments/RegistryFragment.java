package com.nupi.agent.ui.controllers.registry.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.events.InSystemReportButtonEvent;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.general.fragments.ButtonsFragment;
import com.nupi.agent.ui.controllers.general.fragments.SmallChatFragment;
import com.nupi.agent.ui.controllers.registry.adapters.ReportEntryAdapter;
import com.nupi.agent.ui.dialogs.CalendarPickerDialog;
import com.nupi.agent.ui.dialogs.SelectClientDialog;
import com.nupi.agent.ui.dialogs.SingleSelectionDialog;
import com.nupi.agent.utils.StringUtils;
import com.nupi.agent.utils.TimeUtils;
import com.squareup.otto.Subscribe;

import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import icepick.State;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class RegistryFragment extends NupiFragment {

    public static final int
            ALL_DOCS = 0,
            CREDIT_DOCS = 1,
            DEBT_DOCS = 2,
            REALISATIONS_DOCS = 3,
            PAYMENT_DOCS = 4,
            ORDER_DOCS = 5,
            PAYMENT_IN_DOCS = 6,
            IN_THE_POCKET_DOCS = 7;
    private static final String TAG = "Calendar";
    private static final int DEFAULT_REPORT_RANGE = 7;
    private final CalendarPickerDialog calendarPickerDialog = new CalendarPickerDialog();
    @InjectView(R.id.logEntryListView)
    ListView logEntryListView;
    @InjectView(R.id.date_range)
    TextView dateRange;
    @InjectView(R.id.sumLayout)
    LinearLayout linearLayoutSum;
    @InjectView(R.id.sumNumber)
    TextView textViewSum;
    @InjectView(R.id.filterResultsText)
    TextView filterResultsText;
    @InjectView(R.id.calendarButtonText)
    TextView calendarButtonText;
    @InjectView(R.id.filter_clients_text)
    TextView filterClientsText;
    @InjectView(R.id.two_buttons)
    LinearLayout twoButtons;
    @InjectView(R.id.bottomChat)
    FrameLayout bottomChat;
    @State
    int selectedChatFilter = 0;
    private ReportEntryAdapter reportEntryAdapter;
    private Date fromDate = TimeUtils.getPastDate(DEFAULT_REPORT_RANGE);
    private Date toDate = TimeUtils.getEndOfDay();
    private Integer entryType = null;
    private CounterAgent selectedClient;

    public RegistryFragment() {
    }

    public static RegistryFragment newInstance(Boolean inThePocket) {
        RegistryFragment registryFragment = new RegistryFragment();
        if (inThePocket) {
            registryFragment.selectedChatFilter = IN_THE_POCKET_DOCS;
        }
        registryFragment.setIcepickArguments();
        return registryFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registry, container, false);
        ButterKnife.inject(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.two_buttons, new ButtonsFragment())
                .replace(R.id.bottomChat, new SmallChatFragment())
                .commit();

        fillEntriesList();

        fillDateLabels();

    }


    private void fillDateLabels() {
        dateRange.setText(String.format(" %s - %s", TimeUtils.dateFormat(fromDate), TimeUtils.dateFormat(toDate)));
    }

    @OnItemClick(R.id.logEntryListView)
    void onEntrySelect(int item) {
        Answers.getInstance().logCustom(new CustomEvent("Item click, registry"));
        final RegistryEntry registryEntry = reportEntryAdapter.getItem(item);


        if (registryEntry.getEntryType() == RegistryEntry.REALISATION ||
                registryEntry.getEntryType() == RegistryEntry.ORDER) {
            bus.post(new OpenRealisationReceiptEvent(registryEntry.getGuid()));
        }

    }


    @OnClick(R.id.filter_clients)
    void onClientFilterClick() {
        sendButtonEvent("Change Client", true);

        String selectedClientId = selectedClient != null ? selectedClient.getGuid() : "";

        final SelectClientDialog selectClientDialog = new SelectClientDialog(getActivity(), selectedClientId, new SelectClientDialog.OnClientSelectedListener() {
            @Override
            public void onClientSelected(CounterAgent client) {
                selectedClient = client;
                selectedItemsHolder.setSelectedCounterAgentGuid(selectedClient.getGuid());
                fillEntriesList();
            }
        });
        selectClientDialog.addClearButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedClient = null;
                selectedItemsHolder.setSelectedCounterAgentGuid(null);
                fillEntriesList();
            }
        });
        selectClientDialog.show();
    }

    @OnClick(R.id.calendarButton)
    void onCalendarClick() {
        calendarPickerDialog.show(this, fromDate, toDate, new CalendarPickerDialog.OnDatesSelectedListener() {
            @Override
            public void onDatesSelected(Date newFromDate, Date newToDate) {
                fromDate = newFromDate;
                toDate = newToDate;
                fillDateLabels();
                fillEntriesList();
            }
        });

    }

    private void fillEntriesList() {
        fillFilters();
        int[] entryStatuses = getFilterTypeAndStatuses();

        RealmResults<RegistryEntry> registryEntries = selectedChatFilter == IN_THE_POCKET_DOCS ?
                realmRegistryOperations.getInThePocketEntries(getRegistryRealm()) :
                realmRegistryOperations.getRegistryEntries(getRegistryRealm(), fromDate, toDate, entryType, entryStatuses, false, selectedClient);
        reportEntryAdapter = new ReportEntryAdapter(getActivity(),
                getMeteorRealm(),
                realmExchangeOperations,
                registryEntries,
                true);
        logEntryListView.setAdapter(reportEntryAdapter);

        checkAndShowSum();
    }

    private int[] getFilterTypeAndStatuses() {
        int[] entryStatuses = RegistryEntry.ALL_STATUSES;
        switch (selectedChatFilter) {
            case ALL_DOCS:
                entryType = null;
                break;
            case CREDIT_DOCS:
                entryType = RegistryEntry.REALISATION;
                entryStatuses = RegistryEntry.ALL_STATUSES;
                break;
            case DEBT_DOCS:
                entryType = RegistryEntry.PAYMENT;
                entryStatuses = RegistryEntry.ALL_STATUSES;
                break;
            case REALISATIONS_DOCS:
                entryType = RegistryEntry.REALISATION;
                entryStatuses = RegistryEntry.ALL_STATUSES;
                break;
            case PAYMENT_DOCS:
                entryType = RegistryEntry.PAYMENT;
                entryStatuses = RegistryEntry.ONLY_PROCESSED_STATUSES;
                break;
            case ORDER_DOCS:
                entryType = RegistryEntry.ORDER;
                entryStatuses = RegistryEntry.ALL_STATUSES;
                break;
            case PAYMENT_IN_DOCS:
                entryType = RegistryEntry.PAYMENT;
                entryStatuses = RegistryEntry.ONLY_PROCESSING_STATUSES;
                break;
            case IN_THE_POCKET_DOCS:
                entryType = RegistryEntry.PAYMENT;
                entryStatuses = RegistryEntry.IN_THE_POCKET_STATUSES;
                break;
        }
        return entryStatuses;
    }

    @Subscribe
    public void onSystemReportButtonEvent(InSystemReportButtonEvent event) {
        selectedChatFilter = IN_THE_POCKET_DOCS;
        showToast(R.string.in_the_pocket_filter);
        fillEntriesList();
    }


    private void checkAndShowSum() {
        if (entryType == null) {
            linearLayoutSum.setVisibility(View.GONE);
        } else {
            linearLayoutSum.setVisibility(View.VISIBLE);
            textViewSum.setText(StringUtils.getMoneyFormat(reportEntryAdapter.getSum()));
        }
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }

    @OnClick(R.id.filterResults)
    void onFilterResultsPress() {
        sendButtonEvent("Filter Registry", true);
        new SingleSelectionDialog() {
            @Override
            public String getDialogMessage() {
                return getActivity().getString(R.string.choose_document_type);
            }

            @Override
            public CharSequence[] getItemsSource() {
                return new CharSequence[]{
                        getActivity().getString(R.string.all),
                        getActivity().getString(R.string.credit),
                        getActivity().getString(R.string.pay),
                        getActivity().getString(R.string.realisaton),
                        getActivity().getString(R.string.order_pay),
                        getActivity().getString(R.string.order),
                        getActivity().getString(R.string.payment_in),
                        getActivity().getString(R.string.in_pocket)
                };
            }

            @Override
            public void onItemClick(int itemId) {
                selectedChatFilter = itemId;
                fillEntriesList();
            }
        }.show(getActivity(), selectedChatFilter);
    }

    private void fillFilters() {

        filterResultsText.setTextColor(getResources().getColor(selectedChatFilter != ALL_DOCS ? R.color.text_yellow : R.color.text_button));
        if (TimeUtils.getPastDate(DEFAULT_REPORT_RANGE).equals(fromDate) && TimeUtils.getEndOfDay().equals(toDate)) {
            calendarButtonText.setTextColor(getResources().getColor(R.color.text_button));
            dateRange.setTextColor(getResources().getColor(R.color.text_button));
        } else {
            calendarButtonText.setTextColor(getResources().getColor(R.color.text_yellow));
            dateRange.setTextColor(getResources().getColor(R.color.text_yellow));
        }
        filterClientsText.setTextColor(getResources().getColor(selectedClient != null ? R.color.text_yellow : R.color.text_button));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
