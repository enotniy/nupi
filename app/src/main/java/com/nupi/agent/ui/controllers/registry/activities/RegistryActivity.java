package com.nupi.agent.ui.controllers.registry.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.nupi.agent.R;
import com.nupi.agent.events.ChatButtonEvent;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.chat.activities.ChatActivity;
import com.nupi.agent.ui.controllers.registry.fragments.RegistryFragment;
import com.squareup.otto.Subscribe;


public class RegistryActivity extends NupiActivity {

    private static boolean openedActivity = false;

    public static boolean isOpenedActivity() {
        return openedActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        boolean inThePocket = intent.getBooleanExtra(RegistryActivity.IN_THE_POCKET_FILTER, false);
        setContentView(R.layout.activity_registry);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, RegistryFragment.newInstance(inThePocket))
                    .commit();

    }

    @Override
    protected void onStart() {
        super.onStart();
        openedActivity = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        openedActivity = false;
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

    @Subscribe
    public void onChatButtonEvent(ChatButtonEvent event) {
        Log.d("chat", "chat");
        startActivity(new Intent(this, ChatActivity.class));
    }

    @Override
    @Subscribe
    public void onRealisationReceiptEvent(OpenRealisationReceiptEvent event) {
        super.onRealisationReceiptEvent(event);
    }
}
