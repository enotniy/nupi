package com.nupi.agent.ui.controllers.general.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nupi.agent.R;
import com.nupi.agent.events.ChatButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.controllers.chat.fragments.BaseChatFragment;
import com.nupi.agent.ui.controllers.general.adapters.ChatSmallMessageAdapter;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;
import butterknife.OnClick;
import uk.co.androidalliance.edgeeffectoverride.ListView;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class SmallChatFragment extends BaseChatFragment {

    @InjectView(R.id.chatMessages)
    ListView listViewMessages;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_small_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initChat();
    }


    @Subscribe
    @Override
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        super.onUpdateWorkflowEvent(event);
        initChat();
    }

    @OnClick(R.id.chatButton)
    void onChatButtonClick() {
        sendButtonEvent("Chat", true);
        bus.post(new ChatButtonEvent());
    }

    public void initChat() {
        listViewMessages.setAdapter(new ChatSmallMessageAdapter(getActivity(), realmChatOperations.getChatMessages(getChatRealm())));
    }


}
