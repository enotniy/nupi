package com.nupi.agent.ui.controllers.chat.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.helpers.ChatColorHelper;
import com.nupi.agent.utils.ChatMessageUtils;
import com.nupi.agent.utils.TimeUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

/**
 * Created by User on 19.10.2015
 */
public class ChatGroupsAdapter extends RealmBaseAdapter<ChatGroup> {

    private final ChatColorHelper chatColorHelper;
    protected RealmChatOperations realmChatOperations;
    private Context context;
    private Realm realm;
    private String selectedIndex = null;
    private boolean selectedTab = true;
    private String userId;


    public ChatGroupsAdapter(Context context, RealmChatOperations realmChatOperations, Realm realm, RealmResults<ChatGroup> objects, String index, String userId) {
        super(context, objects, true);
        chatColorHelper = new ChatColorHelper(context);
        selectedIndex = index;
        this.realmChatOperations = realmChatOperations;
        this.realm = realm;
        this.userId = userId;
        this.context = context;
    }

    public void setSelectedIndex(String selectedIndex) {
        this.selectedIndex = selectedIndex;
        this.selectedTab = true;
    }

    public void unselectedTab() {
        this.selectedTab = false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item_chat_group, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        final ChatGroup chatGroup = getItem(position);
        final String headline = chatGroup.getHeadline();
        final String groupId = chatGroup.getId();
        final long messageCount = realmChatOperations.getUnreadGroupChatMessagesCount(realm, groupId, userId);

        if (chatGroup.getLastMessageDate() != null) {
            viewHolder.timeMessage.setText(TimeUtils.timeShortFormat(chatGroup.getLastMessageDate()));
            viewHolder.timeMessage.setVisibility(View.VISIBLE);
        } else
            viewHolder.timeMessage.setVisibility(View.GONE);

        if (selectedTab && selectedIndex != null && selectedIndex.equals(groupId))
            convertView.setBackgroundColor(context.getResources().getColor(R.color.background_chat));
        else
            convertView.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item));

        if (!TextUtils.isEmpty(headline)) {
            fillHeadline(viewHolder, headline, groupId, messageCount);
        } else {
            fillHeadline(viewHolder, "—", groupId, messageCount);
        }
        return convertView;
    }

    private void fillHeadline(ViewHolder viewHolder, String headline, String groupId, long countMessage) {
        viewHolder.personName.setText(headline);
        viewHolder.firstSymbol.setText(ChatMessageUtils.getFirstSymbols(headline));
        viewHolder.firstSymbol.setBackgroundResource(R.drawable.selector_chat_group);
        GradientDrawable gd = (GradientDrawable) viewHolder.firstSymbol.getBackground().getCurrent();

        gd.setColor(chatColorHelper.getColor(groupId));
        if (countMessage > 0) {
            viewHolder.messageCount.setText(String.valueOf(countMessage));
            viewHolder.messageCount.setVisibility(View.VISIBLE);
        } else
            viewHolder.messageCount.setVisibility(View.GONE);

        RealmResults<ChatMessage> chatMessages = realmChatOperations.getGroupChatMessages(realm, groupId);
        ChatMessage chatMessage = chatMessages.size() > 0 ? chatMessages.last() : null;
        if (chatMessage != null) {
            String nickname = !chatMessage.getUserFrom().getUser().equals(userId) ? ChatMessageUtils.getNickname(chatMessage.getUserFrom()) : context.getResources().getString(R.string.i);
            Spanned message = Html.fromHtml(String.format(context.getString(R.string.chat_message_last),
                    nickname,
                    chatMessage.getMessage() != null ? chatMessage.getMessage() : "",
                    (chatMessage.getType().compareTo(ChatMessage.TYPE_GEOLOCATION) == 0 ? context.getString(R.string.geolocation_in_small_chat) : ""),
                    (chatMessage.getType().compareTo(ChatMessage.TYPE_IMAGE) == 0 ? context.getString(R.string.img_in_small_chat) : ""))
            );

            viewHolder.textMessage.setText(message);
            viewHolder.textMessage.setVisibility(View.VISIBLE);
            viewHolder.timeMessage.setText(TimeUtils.timeShortFormat(TimeUtils.getChatTime(chatMessage.getDate())));
            viewHolder.timeMessage.setVisibility(View.VISIBLE);
        } else {
            viewHolder.timeMessage.setVisibility(View.GONE);
            viewHolder.textMessage.setText("");
            //viewHolder.textMessage.setVisibility(View.GONE);
        }
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_chat_group.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class ViewHolder {
        @InjectView(R.id.first_symbol)
        TextView firstSymbol;
        @InjectView(R.id.personName)
        TextView personName;
        @InjectView(R.id.textMessage)
        TextView textMessage;
        @InjectView(R.id.timeMessage)
        TextView timeMessage;
        @InjectView(R.id.chatRow)
        LinearLayout chatRow;
        @InjectView(R.id.message_count)
        TextView messageCount;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
