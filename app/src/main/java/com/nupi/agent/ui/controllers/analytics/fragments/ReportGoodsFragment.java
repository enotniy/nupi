package com.nupi.agent.ui.controllers.analytics.fragments;

import android.content.Context;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.nupi.agent.R;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.events.PickerButtonEvent;
import com.nupi.agent.events.VoiceSearchResultEvent;
import com.nupi.agent.helpers.VoiceSearchHelper;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportAnalyticsClientsAdapter;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportAnalyticsGoodsAdapter;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportAnalyticsResultAdapter;
import com.nupi.agent.ui.dialogs.CalendarPickerDialog;
import com.nupi.agent.utils.StringUtils;
import com.nupi.agent.utils.TimeUtils;
import com.squareup.otto.Subscribe;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import icepick.State;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by User on 15.10.2015
 */

public class ReportGoodsFragment extends NupiFragment {

    static final private boolean DEFAULT_ABC_FILTER_ENABLED = true;
    private final CalendarPickerDialog calendarPickerDialog = new CalendarPickerDialog();
    @InjectView(R.id.home_button)
    ImageView homeButton;
    @InjectView(R.id.name_filter_goods)
    TextView nameFilterGoods;
    @InjectView(R.id.add_goods)
    ImageView addGoods;
    @InjectView(R.id.goods_filter)
    LinearLayout goodsFilter;
    @InjectView(R.id.entity_filter_goods)
    RecyclerView entityFilterGoods;
    @InjectView(R.id.name_filter_clients)
    TextView nameFilterClients;
    @InjectView(R.id.clients_filter)
    LinearLayout clientsFilter;
    @InjectView(R.id.entity_filter_clients)
    RecyclerView entityFilterClients;
    @InjectView(R.id.arrow_filter_goods)
    LinearLayout arrowFilterGoods;
    @InjectView(R.id.arrow_filter_clients)
    LinearLayout arrowFilterClients;
    @InjectView(R.id.layout_goods)
    LinearLayout layoutGoods;
    @InjectView(R.id.layout_clients)
    LinearLayout layoutClients;
    @InjectView(R.id.goods_empty)
    TextView goodsEmpty;
    @InjectView(R.id.clients_empty)
    TextView clientsEmpty;
    @InjectView(R.id.add_clients)
    ImageView addClients;
    @InjectView(R.id.sum_report)
    TextView sumReport;
    @InjectView(R.id.report_entity_list)
    RecyclerView reportEntityList;
    @InjectView(R.id.clear_search)
    ImageView clearSearch;
    @InjectView(R.id.close_search)
    ImageView closeSearch;
    @InjectView(R.id.abc_filter)
    TextView abcFilter;
    @InjectView(R.id.sort)
    LinearLayout sort;
    @InjectView(R.id.sort_image)
    ImageView sort_image;
    @InjectView(R.id.search)
    EditText search;
    @InjectView(R.id.searchButton)
    RelativeLayout searchButton;
    @InjectView(R.id.headline)
    TextView headline;
    @InjectView(R.id.search_panel)
    LinearLayout searchPanel;
    @InjectView(R.id.calendarButtonText)
    TextView calendarButtonText;
    @InjectView(R.id.date_range)
    TextView dateRange;
    @InjectView(R.id.calendarButton)
    LinearLayout calendarButton;
    @State
    Date fromDate = TimeUtils.getFirstDay();
    @State
    Date toDate = TimeUtils.getEndOfDay();
    @State
    boolean isAsc;
    @State
    boolean isAbcFilter;
    @State
    boolean isSearch = false;
    @State
    boolean isFilterGoodExpand = true;
    private ReportAnalyticsClientsAdapter clientsAdapter;
    private ReportAnalyticsGoodsAdapter goodsAdapter;
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;
    private Picker clients;
    private Picker goods;
    private ReportAnalyticsResultAdapter resultAdapter;

    public static ReportGoodsFragment getInstance(int typeReport) {
        final ReportGoodsFragment reportListFragment = new ReportGoodsFragment();
        reportListFragment.isAsc = true;
        reportListFragment.isAbcFilter = true;
        reportListFragment.setIcepickArguments();
        return reportListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_report_goods, container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fillAbcFilter();
        fillSort();
        setFilterText();
        fillDateLabels();
        setExpandFilters();
    }

    @Override
    public void onResume() {
        super.onResume();
        initFilters();
        setFilterText();
        fillHeadline();
    }


    @OnClick(R.id.clear_search)
    public void onClickClearSearch() {
        search.setText("");
    }

    @OnTextChanged(R.id.search)
    public void onSearchTextChanged(CharSequence text) {
        if (resultAdapter != null) {
            resultAdapter.setFilterText(text.toString());
            fillHeadline();
        }
    }

    @OnClick(R.id.searchButton)
    public void onSearchClick() {
        if (!isSearch) {
            headline.setVisibility(View.GONE);
            searchButton.setVisibility(View.GONE);
            searchPanel.setVisibility(View.VISIBLE);
            search.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    @OnClick(R.id.close_search)
    public void onCloseSearchClick() {
        if (!isSearch) {
            headline.setVisibility(View.VISIBLE);
            searchButton.setVisibility(View.VISIBLE);
            searchPanel.setVisibility(View.GONE);
            search.setText("");
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(search.getApplicationWindowToken(), 0);
        }
    }

    @OnClick(R.id.sort)
    public void onSortClick() {
        resultAdapter.changeSort();
        resultAdapter.setData();
        fillSort();
    }

    private void fillSort() {
        if (resultAdapter != null)
            sort_image.setImageResource(resultAdapter.getSort() == Sort.ASCENDING ? R.drawable.nupi_sort_9_1 : R.drawable.nupi_sort_1_9);
    }

    @OnClick(R.id.abc_filter)
    public void onAbcFilter() {
        isAbcFilter = !isAbcFilter;
        resultAdapter.setIsAbc(isAbcFilter);
        fillAbcFilter();
    }

    private void fillAbcFilter() {
        abcFilter.setBackground(getResources().getDrawable(isAbcFilter ? R.drawable.selector_list_item : R.drawable.selector_filter));
        abcFilter.setTextColor(getResources().getColor(isAbcFilter ? R.color.text_list : R.color.text_button));
    }


    @OnClick(R.id.voice)
    public void onVoiceClick() {
        VoiceSearchHelper voiceSearchHelper = new VoiceSearchHelper(this, VOICE_SEARCH_REQUEST_CODE);
        voiceSearchHelper.runVoiceSearch();
        resultAdapter.notifyDataSetChanged();
    }

    void fillHeadline() {
        sumReport.setText(StringUtils.getMoneyFormat(resultAdapter.getSum()));
    }

    @Subscribe
    public void onVoiceSearchResult(VoiceSearchResultEvent event) {
        final CharSequence searchString = event.getSearchString();
        search.setText(searchString);
        onSearchTextChanged(searchString);
        Log.d("Search", searchString.toString());
        resultAdapter.notifyDataSetChanged();
    }

    private void setFilterText() {
        nameFilterClients.setText(String.format(getResources().getString(R.string.goods_filter), clientsAdapter != null ? clientsAdapter.getItemCount() : 0));
        nameFilterGoods.setText(String.format(getResources().getString(R.string.clients_filter), goodsAdapter != null ? goodsAdapter.getItemCount() : 0));
    }

    private void setExpandFilters() {

        if (isFilterGoodExpand) {
            layoutClients.setVisibility(View.GONE);
            layoutGoods.setVisibility(View.VISIBLE);
            arrowFilterClients.setScaleY(1);
            arrowFilterGoods.setScaleY(-1);
        } else {
            layoutClients.setVisibility(View.VISIBLE);
            layoutGoods.setVisibility(View.GONE);
            arrowFilterClients.setScaleY(-1);
            arrowFilterGoods.setScaleY(1);
        }
    }

    @OnClick(R.id.calendarButton)
    void onCalendarClick() {
        calendarPickerDialog.show(this, fromDate, toDate, new CalendarPickerDialog.OnDatesSelectedListener() {
            @Override
            public void onDatesSelected(Date newFromDate, Date newToDate) {
                fromDate = newFromDate;
                toDate = newToDate;
                fillDateLabels();
                updateData();
            }
        });

    }

    private void fillDateLabels() {
        dateRange.setText(String.format(" %s - %s", TimeUtils.dateFormat(fromDate), TimeUtils.dateFormat(toDate)));
    }

    private List<CounterAgent> getListClient(List<String> listId) {
        List<CounterAgent> counterAgents = new LinkedList<>();
        for (String id : listId) {
            CounterAgent client = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), id, CounterAgent.class);
            if (client != null)
                counterAgents.add(client);
        }
        return counterAgents;
    }

    void updateData() {

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
        LinearLayoutManager mLayoutManagerClients = new LinearLayoutManager(getActivity());
        mLayoutManagerClients.setOrientation(LinearLayoutManager.VERTICAL);

        reportEntityList.setLayoutManager(mLayoutManagerClients);
        reportEntityList.setItemAnimator(animator);
        resultAdapter = new ReportAnalyticsResultAdapter(getActivity(), getMeteorRealm(), goodsAdapter.getItems(), clientsAdapter.getItems(), fromDate, toDate, isAsc);
        reportEntityList.setAdapter(resultAdapter);
        reportEntityList.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
        reportEntityList.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
        goodsAdapter.notifyDataSetChanged();
        clientsAdapter.notifyDataSetChanged();

        reportEntityList.setAdapter(resultAdapter);
        fillHeadline();
    }

    private void initFilters() {

        goods = preferences.getPickerPrice();
        clients = preferences.getPickerClients();

        if (goods == null) {
            RealmResults<Nomenclature> nomenclatures = realmExchangeOperations.getNomenclature(getMeteorRealm());
            goods = new Picker(nomenclatures.toArray(new Nomenclature[nomenclatures.size()]), false);
            preferences.setPickerPrice(goods);
        }

        if (clients == null) {
            RealmResults<CounterAgent> nomenclatures = realmExchangeOperations.getCounterAgents(getMeteorRealm());
            clients = new Picker(nomenclatures.toArray(new CounterAgent[nomenclatures.size()]), false);
            preferences.setPickerClients(clients);
        }
        changeFilters();
    }

    private void changeFilters() {
        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        final Runnable onElementsListPriceChangeListener = new Runnable() {
            @Override
            public void run() {
                updateData();
                setFilterText();
                preferences.setPickerPrice(goodsAdapter.getPicker());
            }
        };

        final Runnable onElementsListClientChangeListener = new Runnable() {
            @Override
            public void run() {
                updateData();
                setFilterText();
                preferences.setPickerClients(clientsAdapter.getPicker());
            }
        };

        Realm realm = getMeteorRealm();
        if (goods != null) {
            LinearLayoutManager mLayoutManagerGoods = new LinearLayoutManager(getActivity());
            mLayoutManagerGoods.setOrientation(LinearLayoutManager.VERTICAL);

            entityFilterGoods.setLayoutManager(mLayoutManagerGoods);
            entityFilterGoods.setItemAnimator(animator);
            goodsAdapter = new ReportAnalyticsGoodsAdapter(getActivity(), goods, realm, onElementsListPriceChangeListener);
            entityFilterGoods.setAdapter(goodsAdapter);
            entityFilterGoods.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
            entityFilterGoods.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
            goodsAdapter.notifyDataSetChanged();
        }

        if (clients != null) {
            LinearLayoutManager mLayoutManagerClients = new LinearLayoutManager(getActivity());
            mLayoutManagerClients.setOrientation(LinearLayoutManager.VERTICAL);

            entityFilterClients.setLayoutManager(mLayoutManagerClients);
            entityFilterClients.setItemAnimator(animator);
            clientsAdapter = new ReportAnalyticsClientsAdapter(getActivity(), clients, realm, onElementsListClientChangeListener);
            entityFilterClients.setAdapter(clientsAdapter);
            entityFilterClients.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
            entityFilterClients.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
            goodsAdapter.notifyDataSetChanged();
            clientsAdapter.notifyDataSetChanged();
        }

        //checkEmptyAdapter();
        updateData();
    }

    @OnClick(R.id.goods_filter)
    void onGoodsFilterPress() {
        sendButtonEvent("Analytics goods filter", true);
        isFilterGoodExpand = !isFilterGoodExpand;
        setExpandFilters();

    }

    @OnClick(R.id.clients_filter)
    void onClientsFilterPress() {
        sendButtonEvent("Analytics clients filter", true);
        isFilterGoodExpand = !isFilterGoodExpand;
        setExpandFilters();

    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }

    @OnClick(R.id.add_clients)
    void onAddClientPress() {
        bus.post(new PickerButtonEvent(Picker.TYPE_PICKER_CLIENTS));
    }

    @OnClick(R.id.add_goods)
    void onAddGoodsPress() {
        bus.post(new PickerButtonEvent(Picker.TYPE_PICKER_GOODS));
    }

    private void checkEmptyAdapter() {
        if (clientsAdapter == null || clientsAdapter.getItemCount() == 0)
            clientsFilter.setVisibility(View.INVISIBLE);
        else
            clientsFilter.setVisibility(View.VISIBLE);

        if (goodsAdapter == null || goodsAdapter.getItemCount() == 0)
            goodsFilter.setVisibility(View.INVISIBLE);
        else
            goodsFilter.setVisibility(View.VISIBLE);
    }
}
