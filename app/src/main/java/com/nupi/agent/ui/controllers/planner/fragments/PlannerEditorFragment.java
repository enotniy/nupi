package com.nupi.agent.ui.controllers.planner.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.nupi.agent.R;
import com.nupi.agent.database.RealmPlannerOperations;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;
import com.nupi.agent.events.PlannerButtonHomeRouteEvent;
import com.nupi.agent.helpers.GuidGenerator;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.planner.adapters.PointsEditorAdapter;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;

public class PlannerEditorFragment extends NupiFragment {

    @Inject
    RealmPlannerOperations realmPlannerOperations;

    @InjectView(R.id.home_button)
    ImageView homeButton;
    @InjectView(R.id.plannerList)
    RecyclerView mRecyclerView;
    @InjectView(R.id.create_path)
    TextView createPath;

    private RecyclerView.LayoutManager mLayoutManager;
    private PointsEditorAdapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewDragDropManager mRecyclerViewDragDropManager;
    private List<ClientInfo> selectedPoints;
    private Realm plannerRealm;
    private boolean isNewRoute = true;
    private PlannerRoute route;
    private String query  = "";


    public static PlannerEditorFragment newInstance(List<ClientInfo> points, PlannerRoute route) {
        PlannerEditorFragment plannerEditorFragment = new PlannerEditorFragment();
        plannerEditorFragment.selectedPoints = points;
        plannerEditorFragment.isNewRoute = false;
        plannerEditorFragment.route = route;
        return plannerEditorFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
        plannerRealm = realmPlannerOperations.getRealm();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        plannerRealm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_planner_editor, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fillHeadline();
    }


    @Override
    public void onStart() {
        super.onStart();
        realmPlannerOperations.addClients(plannerRealm, realmExchangeOperations.getCounterAgents(getMeteorRealm()));

        setClientsAdapter();
    }

    @Override
    public void onPause() {
//        mRecyclerViewDragDropManager.cancelDrag();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void fillHeadline() {
        if (isNewRoute)
            createPath.setText(getString(R.string.create_path));
        else
            createPath.setText(getString(R.string.save_path));
    }

    private void setClientsAdapter() {
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        if (preferences.isLoggedIn()) {
            List<ClientInfo> points = realmPlannerOperations.getClientInfoList(plannerRealm, query);
            final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(animator);

            mAdapter = new PointsEditorAdapter(getActivity(), points, selectedPoints);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        if (mRecyclerViewDragDropManager != null) {
            mRecyclerViewDragDropManager.release();
            mRecyclerViewDragDropManager = null;
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        mAdapter = null;
        mLayoutManager = null;
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        bus.post(new PlannerButtonHomeRouteEvent());
    }

    @OnClick(R.id.create_path)
    void onCreatePathPress() {
        final RealmList<ClientInfo> selectedPoints = mAdapter.getSelectedPoints();
        if (selectedPoints == null || selectedPoints.size() < 1) {
            showToast(R.string.empty_route);
            return;
        }
        if (isNewRoute) {
            final EditText input = new EditText(getActivity());
            input.setText("");
            input.setHint(R.string.new_route);
            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.edit_name_route))
                    .setView(input)
                    .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PlannerRoute plannerRoute = new PlannerRoute();
                            plannerRoute.setGuid(GuidGenerator.randomGUID());
                            plannerRoute.setClientsList(selectedPoints);
                            plannerRoute.setCreationDate(new Date());
                            plannerRoute.setRouteName(!input.getText().toString().equals("") ? input.getText().toString() : getResources().getString(R.string.new_route));
                            if (!input.getText().toString().equals("")) {
                                if (realmPlannerOperations.getRouteByName(plannerRealm, plannerRoute.getRouteName()) == null) {
                                    plannerRealm.beginTransaction();
                                    plannerRealm.copyToRealm(plannerRoute);
                                    plannerRealm.commitTransaction();
                                    dismissProgressDialog();
                                    getChildFragmentManager().beginTransaction()
                                            .replace(R.id.container, PlannerRouteFragment.newInstance(plannerRoute.getGuid()))
                                            .commit();
                                } else {
                                    Toast.makeText(getActivity(), R.string.route_exist, Toast.LENGTH_LONG).show();
                                    input.setText("");
                                }
                            } else {
                                Toast.makeText(getActivity(), R.string.route_empty, Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        } else {
            plannerRealm.beginTransaction();
            route.setClientsList(selectedPoints);
            plannerRealm.commitTransaction();
            dismissProgressDialog();
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.container, PlannerEditorRouteFragment.newInstance(route))
                    .commit();
        }
    }
}
