package com.nupi.agent.ui.controllers.analytics.fragments;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.nupi.agent.R;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.application.models.Tree;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.enums.SelectionType;
import com.nupi.agent.events.DismissNumPadEvent;
import com.nupi.agent.helpers.CounterAgentComparator;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.analytics.adapters.TreePickerClientAdapter;
import com.nupi.agent.ui.dialogs.YesNoDialog;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.RealmResults;
import uk.co.androidalliance.edgeeffectoverride.ListView;

/**
 * Created by User on 15.10.2015
 */

public class PickerClientFragment extends NupiFragment implements ListView.OnItemClickListener {

    final static boolean IS_ALL_DEFAULT_SELECTED_ITEM = false;

    @InjectView(R.id.closeButton)
    FrameLayout closeButton;
    @InjectView(R.id.goodsListView)
    RecyclerView goodsListView;
    @InjectView(R.id.hierarchy)
    LinearLayout buttonsContainer;
    @InjectView(R.id.buttonsRibbon)
    HorizontalScrollView buttonsRibbon;
    @InjectView(R.id.count_item)
    TextView countItem;
    @InjectView(R.id.btnOK)
    TextView btnOK;
    @InjectView(R.id.select_all)
    TextView selectAll;
    @InjectView(R.id.title)
    TextView title;

    private Tree<String> clientsIdTree;
    private List<View> hierarchyButtonList;
    private List<CounterAgent> priceList;
    private TreePickerClientAdapter pricesAdapter;
    private Picker picker;
    private boolean isAllSelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_picker, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        clientsIdTree = realmExchangeOperations.getTreeIdCounterAgent(getMeteorRealm());
        picker = preferences.getPickerClients();
        if (picker == null) {
            RealmResults<CounterAgent> counterAgents = realmExchangeOperations.getCounterAgents(getMeteorRealm());
            picker = new Picker(counterAgents.toArray(new CounterAgent[counterAgents.size()]), false);
        }

        hierarchyButtonList = new LinkedList<>();

        if (clientsIdTree != null)
            openChildNode(clientsIdTree.getHead());

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && (keyEvent.getAction() == KeyEvent.ACTION_UP)) {
                    onHomeButtonPress();
                    return true;
                }
                return false;
            }
        });
        fillHeadline();
        title.setText(R.string.shops);
    }

    private void fillHeadline() {
        List<String> selectedItems = pricesAdapter.getPicker().getSelectedIds();
        int countAll = 0;
        int countSelected = 0;
        for (CounterAgent client : realmExchangeOperations.getCounterAgents(getMeteorRealm())) {
            if (!client.isFolder()) {
                countAll++;
                if (selectedItems.contains(client.getGuid()))
                    countSelected++;
            }
        }

        isAllSelected = (countAll == countSelected);
        fillButtonSelect();

        countItem.setText(String.format(getResources().getString(R.string.count_element_pick), countSelected, countAll));
    }

    private void fillButtonSelect() {
        if (isAllSelected) {
            selectAll.setTextColor(getResources().getColor(R.color.text_button));
            selectAll.setBackground(getResources().getDrawable(R.drawable.selector_nupi_blue_dark));
        } else {
            selectAll.setTextColor(getResources().getColor(R.color.text_title_dark));
            selectAll.setBackground(getResources().getDrawable(R.drawable.selector_list_item));
        }
    }


    private void openChildNode(String parentId) {
        addButton(parentId);
        setUpPricesAdapter(parentId);
        fillHeadline();
    }

    private void addButton(final String priceId) {
        final int buttonCount = hierarchyButtonList.size();

        final View breadCrumbsView = initNewBreadCrumbsNode(priceId);

        breadCrumbsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Answers.getInstance().logCustom(new CustomEvent("Node Button, breadcrumbs"));
                setUpPricesAdapter(priceId);
                bus.post(new DismissNumPadEvent());
                for (int i = hierarchyButtonList.size() - 1; i > buttonCount; i--) {
                    buttonsContainer.removeView(hierarchyButtonList.remove(i));
                }
                recalculateColors();
                breadCrumbsView.setClickable(false);
                scrollToRight();
            }
        });


        hierarchyButtonList.add(breadCrumbsView);
        buttonsContainer.addView(breadCrumbsView);

        recalculateColors();

        scrollToRight();
    }

    @Override
    public void onResume() {
        super.onResume();
        fillHeadline();
    }

    private void recalculateColors() {
        for (int i = 0; i < hierarchyButtonList.size(); i++) {
            final ViewHolder viewHolder = new ViewHolder(hierarchyButtonList.get(i));
            switch (hierarchyButtonList.size() - i - 1) {
                case 0:
                    viewHolder.backgroundView.setClickable(false);
                    viewHolder.imageView.setVisibility(View.GONE);
                    viewHolder.backgroundView.setBackgroundColor(getResources().getColor(R.color.transparent));
                    break;
                case 1:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_1);
                    break;
                case 2:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_2);
                    break;
                case 3:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_3);
                    break;
                case 4:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_4);
                    break;
                default:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_5);
                    break;
            }
        }
    }

    @NonNull
    private View initNewBreadCrumbsNode(String priceId) {
        final View breadCrumbsView = getActivity().getLayoutInflater().inflate(!Nomenclature.ROOT_GUID.equals(priceId) ? R.layout.view_bread_crumbs : R.layout.view_bread_crumbs_root, null);
        final ViewHolder viewHolder = new ViewHolder(breadCrumbsView);

        breadCrumbsView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));


        final Nomenclature nomenclature = realmExchangeOperations.getNomenclatureById(getMeteorRealm(), priceId);
        if (Nomenclature.ROOT_GUID.equals(priceId)) {
            viewHolder.textView.setText("");
        } else if (nomenclature != null)
            viewHolder.textView.setText(nomenclature.getName());
        else
            viewHolder.textView.setText("/");
        return breadCrumbsView;
    }

    private void scrollToRight() {
        ViewTreeObserver vto = buttonsRibbon.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (isVisible()) {
                    buttonsRibbon.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    buttonsRibbon.scrollTo(buttonsContainer.getWidth(), 0);
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Answers.getInstance().logCustom(new CustomEvent("Item click, price tree"));
        CounterAgent client = priceList.get(i);
        if (client.isFolder()) {
            openChildNode(client.getGuid());
        }
    }

    private void setUpPricesAdapter(String head) {
        Collection<String> priceIds = clientsIdTree.getSuccessors(head);
        priceList = new LinkedList<>();

        if (preferences.isLoggedIn()) {
            for (String clientId : priceIds) {
                CounterAgent client = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), clientId, CounterAgent.class);
                if (!client.isFolder())
                    priceList.add(client);
                else if (clientsIdTree.getSuccessors(client.getGuid()).size() > 0)
                    priceList.add(client);
            }
        }
        Collections.sort(priceList, new CounterAgentComparator());

        Runnable onchangeAdapterListener = new Runnable() {
            @Override
            public void run() {
                fillHeadline();
            }
        };

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
        LinearLayoutManager mLayoutManagerGoods = new LinearLayoutManager(getActivity());
        mLayoutManagerGoods.setOrientation(LinearLayoutManager.VERTICAL);

        goodsListView.setLayoutManager(mLayoutManagerGoods);
        goodsListView.setItemAnimator(animator);
        pricesAdapter = new TreePickerClientAdapter(priceList, clientsIdTree, picker, onchangeAdapterListener);
        goodsListView.setAdapter(pricesAdapter);
        goodsListView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
        goodsListView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
    }

    @OnClick(R.id.closeButton)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        new YesNoDialog(getActivity(), getString(R.string.picker_leave)) {
            @Override
            public void handlePositiveButton() {
                getActivity().finish();
            }
        }.show();
    }


    @OnClick(R.id.btnOK)
    void onButtonOKPress() {
        sendButtonEvent("Home", true);
        preferences.setPickerClients(picker);
        getActivity().finish();
    }

    @OnClick(R.id.select_all)
    void onButtonSelectAllPress() {
        pricesAdapter.getPicker().selectElement(clientsIdTree, isAllSelected ? SelectionType.NONE : SelectionType.ALL);
        fillHeadline();
        pricesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    static class ViewHolder {
        @InjectView(R.id.textView)
        TextView textView;
        @InjectView(R.id.imageView)
        ImageView imageView;
        @InjectView(R.id.backgroundView)
        LinearLayout backgroundView;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
