package com.nupi.agent.ui.controllers.receipt.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.database.models.meteor.Unit;
import com.nupi.agent.database.models.registry.PriceEntry;
import com.nupi.agent.utils.StringUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class ReceiptEntryAdapter extends ArrayAdapter<PriceEntry> {

    final Realm realm;
    @Inject
    RealmExchangeOperations realmExchangeOperations;

    public ReceiptEntryAdapter(Activity context, Realm realm, List<PriceEntry> prices) {
        super(context, R.layout.list_item_price_entry, prices);
        this.realm = realm;
        ((NupiApp) context.getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {


        PriceEntry priceListItem = getItem(position);

        if (view == null) {
            view = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_price_entry, null);
        }

        fillItemView(view, priceListItem, position);

        return view;
    }

    public void fillItemView(View view, PriceEntry item, int position) {
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.number
                .setText(String.valueOf(position + 1));

        double quantity = item.getQuantity();


        Unit unit = realmExchangeOperations.getRealmObjectByGuid(realm, item.getUnit(), Unit.class);

        viewHolder.unit
                .setText(unit != null ? unit.getName() :
                        item.getUnitName() != null ?
                                item.getUnitName() : getContext().getString(R.string.unknown));

        Nomenclature nomenclature = realmExchangeOperations.getRealmObjectByGuid(
                realm,
                item.getNomenclature(),
                Nomenclature.class
        );

        viewHolder.nomenclature
                .setText(nomenclature != null ? nomenclature.getName() :
                        item.getNomName() != null ?
                                item.getNomName() : getContext().getString(R.string.not_in_db));

        viewHolder.quantity
                .setText(StringUtils.getStockFormat(quantity));

        if (quantity != 0)
            viewHolder.price
                    .setText(StringUtils.getMoneyFormat(item.getSum() / quantity));

        viewHolder.sum
                .setText(StringUtils.getMoneyFormat(item.getSum()));
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_price_entry.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.number)
        TextView number;
        @InjectView(R.id.nomenclature)
        TextView nomenclature;
        @InjectView(R.id.quantity)
        TextView quantity;
        @InjectView(R.id.unit)
        TextView unit;
        @InjectView(R.id.price)
        TextView price;
        @InjectView(R.id.sum)
        TextView sum;
        @InjectView(R.id.orderRow)
        LinearLayout orderRow;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

