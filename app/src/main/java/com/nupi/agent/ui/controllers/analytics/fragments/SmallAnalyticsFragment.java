package com.nupi.agent.ui.controllers.analytics.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nupi.agent.R;
import com.nupi.agent.events.AnalyticsButtonEvent;
import com.nupi.agent.events.ReportAnalyticsButtonEvent;
import com.nupi.agent.ui.controllers.analytics.activities.ReportAnalyticsActivity;

import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class SmallAnalyticsFragment extends BaseAnalyticsFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_small_analytics, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupChart();
    }

    private void setupChart() {
        initChart();
        setBarData();
        setLabel();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupChart();
    }

    @Override
    public void onChange() {
        if (isVisible())
            setupChart();
    }

    @OnClick(R.id.bar_chart)
    void onBarChartClick() {
        sendButtonEvent("Small analytics", true);
        if (preferences.isLoggedIn())
            bus.post(new AnalyticsButtonEvent());
    }

    @OnClick(R.id.report_goods)
    void onReportGoodsClick() {
        sendButtonEvent("Report goods analytics", true);
        if (preferences.isLoggedIn())
            bus.post(new ReportAnalyticsButtonEvent(ReportAnalyticsActivity.REPORT_TYPE_GOODS));
    }

    @OnClick(R.id.report_client)
    void onReportClientsClick() {
        sendButtonEvent("Report client analytics", true);
        if (preferences.isLoggedIn())
            bus.post(new ReportAnalyticsButtonEvent(ReportAnalyticsActivity.REPORT_TYPE_CLIENTS));
    }

}
