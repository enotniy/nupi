package com.nupi.agent.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 18.03.14
 * Time: 0:12
 */
public abstract class ComplexDialog implements ComplexDialogDelegate {
    public void show(Context context, boolean hasThirdButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(getDialogMessage());
        builder.setPositiveButton(getPositiveButtonName(),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        handlePositiveButton();
                        dialog.cancel();
                    }
                });
        if (hasThirdButton) {
            builder.setNeutralButton(getNeutralButtonName(),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            handleNeutralButton();
                            dialog.cancel();
                        }
                    });
        }
        builder.setNegativeButton(getNegativeButtonName(),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        dialog.cancel();
                    }
                });

        // выводим диалоговое окно
        builder.show();
    }

}
