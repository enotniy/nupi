package com.nupi.agent.ui.dialogs;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 18.03.14
 * Time: 17:33
 */
interface YesNoDialogDelegate {
    void handlePositiveButton();
}
