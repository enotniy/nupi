package com.nupi.agent.ui.controllers.geolocation.activities;

import android.content.Intent;
import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.PlannerCreateRouteEvent;
import com.nupi.agent.events.PlannerRouteEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.geolocation.fragments.GeolocationFragment;
import com.nupi.agent.ui.controllers.planner.activities.PlannerActivity;
import com.squareup.otto.Subscribe;

public class GeolocationActivity extends NupiActivity {

    public static final String START_GEOLOCATION_POINT = "POINT";
    public static final String START_GEOLOCATION_ROUTE = "ROUTE";
    public static final String START_GEOLOCATION_NONE = "NONE";
    public static final String START_GEOLOCATION_TYPE = "START_GEOLOCATION_TYPE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geolocation);
        Intent intent = getIntent();
        String type = intent.getStringExtra(START_GEOLOCATION_TYPE);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, GeolocationFragment.newInstance(type))
                    .commit();
    }

    @Subscribe
    public void onPlannerRouteEvent(PlannerRouteEvent event) {
        Intent intent = new Intent(getApplicationContext(), PlannerActivity.class);
        intent.putExtra(PlannerActivity.START_PLANNER_TYPE, PlannerActivity.START_PLANNER_ROUTE);
        startActivity(intent);
    }

    @Subscribe
    public void onPlannerCreateRouteEvent(PlannerCreateRouteEvent event) {
        Intent intent = new Intent(getApplicationContext(), PlannerActivity.class);
        intent.putExtra(PlannerActivity.START_PLANNER_TYPE, PlannerActivity.START_PLANNER_CREATE_ROUTE);
        startActivity(intent);
    }

}
