package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nupi.agent.R;
import com.nupi.agent.ui.base.NupiFragment;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class OffersFragment extends NupiFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_offers, container, false);
    }
}
