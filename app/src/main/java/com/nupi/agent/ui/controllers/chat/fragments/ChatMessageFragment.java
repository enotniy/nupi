package com.nupi.agent.ui.controllers.chat.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.service.GeolocationService;
import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.events.ChatThreadSelectEvent;
import com.nupi.agent.helpers.CameraHandler;
import com.nupi.agent.helpers.ChatColorHelper;
import com.nupi.agent.network.nupi.NupiServerApi;
import com.nupi.agent.network.nupi.models.SendingChatMessage;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.chat.adapters.ChatMessageAdapter;
import com.nupi.agent.ui.dialogs.ListDialog;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.utils.BitmapUtils;
import com.nupi.agent.utils.ChatMessageUtils;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import icepick.State;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.mime.TypedFile;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by User on 19.10.2015
 */

public class ChatMessageFragment extends NupiFragment {

    public static final String CHAT_FOLDER = "chat";
    @Inject
    protected NupiServerApi nupiServerApi;
    @Inject
    GeolocationService geolocationService;
    @InjectView(R.id.first_symbol)
    TextView firstSymbol;

    @InjectView(R.id.user_name)
    TextView userName;

    @InjectView(R.id.attach)
    ImageView attach;

    @InjectView(R.id.send_message)
    EditText sendMessage;

    @InjectView(R.id.attach_button)
    LinearLayout attachButton;

    @InjectView(R.id.preview)
    RelativeLayout preview;

    @InjectView(R.id.messageArea)
    ListView listViewMessages;
    @State
    String photoName;
    @State
    String typeMessage;
    @State
    boolean fromNotification = false;
    @State
    boolean openPrivate = true;
    @State
    String selectedId;
    private CameraHandler cameraHandler;
    private ChatColorHelper chatColorHelper;
    private boolean messageBlock = false;
    private Bitmap attachmentImage;


    public static ChatMessageFragment getPrivateInstance(String id) {
        return getInstance(id, true);
    }

    public static ChatMessageFragment getGroupsInstance(String id) {
        return getInstance(id, false);
    }

    @NonNull
    public static ChatMessageFragment getInstance(String id, boolean openPrivate) {
        final ChatMessageFragment chatListFragment = new ChatMessageFragment();
        chatListFragment.fromNotification = true;
        chatListFragment.selectedId = id;
        chatListFragment.openPrivate = openPrivate;
        chatListFragment.setIcepickArguments();
        return chatListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat_message, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        chatColorHelper = new ChatColorHelper(getActivity());
        typeMessage = ChatMessage.TYPE_TEXT;
        cameraHandler = new CameraHandler(this);
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.view_message_divider, null);
        listViewMessages.addFooterView(v);
        listViewMessages.addHeaderView(v);
    }


    @Override
    public void onResume() {
        super.onResume();

        initListView();

        if (ChatMessage.TYPE_IMAGE.equals(typeMessage) && photoName != null)
            loadAttachmentImage(photoName);
    }

    @Override
    public void onPause() {

        if (attachmentImage != null) {
            attachmentImage.recycle();
        }
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == CameraHandler.REQUEST_IMAGE_CAPTURE) && (resultCode == Activity.RESULT_OK)) {
            cameraHandler.addToGallery(photoName);
            loadAttachmentImage(photoName);
            typeMessage = ChatMessage.TYPE_IMAGE;
            preview.setVisibility(View.VISIBLE);
        } else if ((requestCode == CameraHandler.REQUEST_OPEN_IMAGE) && (resultCode == Activity.RESULT_OK) && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                attachmentImage = BitmapFactory.decodeFile(picturePath);
                typeMessage = ChatMessage.TYPE_IMAGE;
                preview.setVisibility(View.VISIBLE);
                attach.setImageResource(R.drawable.nupi_placeholder_icon);
                //loadAttachmentImage(picturePath);
            } else {
                showToast(R.string.cant_load_image);
                clearAttach();
            }
        } else {
            clearAttach();
        }
    }

    private void initListView() {
        if (selectedItemsHolder.getChatSelectedId() != null || fromNotification) {

            selectedId = fromNotification ? selectedId : selectedItemsHolder.getChatSelectedId();

            boolean chatUsersSelected = fromNotification ? openPrivate : selectedItemsHolder.isChatUsersSelected();

            RealmResults<ChatMessage> chatMessageRealmResults = chatUsersSelected ?
                    realmChatOperations.getPrivateChatMessages(getChatRealm(), selectedId) :
                    realmChatOperations.getGroupChatMessages(getChatRealm(), selectedId);

            long count = 0;
            for (int i = 0; i < chatMessageRealmResults.size(); i++)
                if (!chatMessageRealmResults.get(i).isWasLocallyRead())
                    count++;

            ChatMessageAdapter messageAdapter = new ChatMessageAdapter(getActivity(), chatMessageRealmResults, preferences.getChatId(), nupiServerApi);
            listViewMessages.setAdapter(messageAdapter);
            if (count > 0)
                listViewMessages.setSelection((int) (chatMessageRealmResults.size() - count));

            if (chatUsersSelected) {
                final ChatUser chatUser = realmChatOperations.getChatUserById(getChatRealm(), selectedId);
                if (chatUser != null) {
                    firstSymbol.setText(ChatMessageUtils.getFirstSymbols(ChatMessageUtils.getNickname(chatUser)));
                    firstSymbol.setBackground(getResources().getDrawable(R.drawable.selector_chat));
                    GradientDrawable gd = (GradientDrawable) firstSymbol.getBackground().getCurrent();
                    gd.setColor(chatColorHelper.getColor(chatUser.getUser()));
                    if (chatUser.getChatRoute() == null)
                        userName.setText(ChatMessageUtils.getNickname(chatUser));
                    else
                        userName.setText(String.format(getResources().getString(R.string.route_chat), ChatMessageUtils.getNickname(chatUser), chatUser.getChatRoute().getName()));
                }
            } else {
                final ChatGroup chatGroup = realmChatOperations.getChatGroupById(getChatRealm(), selectedId);
                if (chatGroup != null) {
                    firstSymbol.setText(ChatMessageUtils.getFirstSymbols(ChatMessageUtils.getHeadlineGroup(chatGroup)));
                    firstSymbol.setBackground(getResources().getDrawable(R.drawable.selector_chat_group));
                    GradientDrawable gd = (GradientDrawable) firstSymbol.getBackground().getCurrent();
                    gd.setColor(chatColorHelper.getColor(chatGroup.getId()));
                    userName.setText(ChatMessageUtils.getHeadlineGroup(chatGroup));
                }
            }
        }
    }

    private long getMessageCount() {

        return selectedItemsHolder.isChatUsersSelected() ?
                realmChatOperations.getGroupChatMessagesCount(getChatRealm(), selectedId) :
                realmChatOperations.getGroupChatMessagesCount(getChatRealm(), selectedId);
    }


    @Subscribe
    public void onChatUserSelectedEvent(ChatThreadSelectEvent event) {
        fromNotification = false;
        initListView();
    }

    @OnClick(R.id.preview)
    void onPreviewPress() {
        clearAttach();
    }

    @OnClick(R.id.sending_message)
    void onSendMessageButtonPress() {
        if (!messageBlock) {
            final String id = selectedItemsHolder.getChatSelectedId();

            if (id != null && ((!sendMessage.getText().toString().isEmpty() && ChatMessage.TYPE_TEXT.equals(typeMessage)) || (!ChatMessage.TYPE_TEXT.equals(typeMessage)))) {
                sendButtonEvent("Send message", true);

                final Location location = geolocationService.getLastKnownLocation();
                final SendingChatMessage sendingChatMessage = new SendingChatMessage(sendMessage.getText().toString(), typeMessage);
                sendMessage.setText(null);
                if (location != null) {
                    sendingChatMessage.latitude = location.getLatitude();
                    sendingChatMessage.longitude = location.getLongitude();
                    sendingChatMessage.altitude = location.getAltitude();
                } else if (ChatMessage.TYPE_GEOLOCATION.equals(typeMessage)) {
                    MessageBox.show(getString(R.string.enable_location), getActivity());
                    clearAttach();
                    sendingChatMessage.type = ChatMessage.TYPE_TEXT;
                    return;
                }

                File tempImage = null;
                TypedFile typedFile = null;
                if (attachmentImage != null && !attachmentImage.isRecycled()) {
                    tempImage = BitmapUtils.saveBitmapToCache(attachmentImage, getActivity());
                    typedFile = new TypedFile("image/jpeg", tempImage);
                }

                messageBlock = true;
                final File finalTempImage = tempImage;
                ChatGroup dialog = null,
                        newConversation = null;
                if (selectedItemsHolder.isChatUsersSelected()) {
                    dialog = realmChatOperations.getDialogByUserId(getChatRealm(), id);
                    if (dialog == null) {
                        newConversation = new ChatGroup();
                        List<String> ids = new ArrayList<>(2);
                        ids.add(preferences.getChatId());
                        ids.add(id);
                        newConversation.setUsers(ids);
                        newConversation.setType(ChatGroup.TYPE_DIALOG);
                    }
                }

                final ChatGroup finalNewConversation = newConversation;
                (selectedItemsHolder.isChatUsersSelected() ?
                        // TODO: 03/03/16 investigate
//                        dialog != null ?
//                                nupiServerApi.sendChatGroupMessage(dialog.getId(), sendingChatMessage, typedFile) :
                        nupiServerApi.sendChatUserMessage(id, sendingChatMessage, typedFile) :
                        nupiServerApi.sendChatGroupMessage(id, sendingChatMessage, typedFile))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<ChatMessage>() {
                            @Override
                            public void onCompleted() {
                                deleteTempFile();
                                messageBlock = false;
                                typeMessage = ChatMessage.TYPE_TEXT;
                            }

                            @Override
                            public void onError(Throwable e) {
                                deleteTempFile();
                                messageBlock = false;
                            }

                            @Override
                            public void onNext(ChatMessage chatMessage) {

                                if (isVisible()) {
                                    sendMessage.setText("");
                                    preview.setVisibility(View.GONE);
                                    attach.setImageResource(R.drawable.nupi_attach);
                                }
                                typeMessage = ChatMessage.TYPE_TEXT;
                                attachmentImage.recycle();
                                attachmentImage = null;

                                Realm realm = realmChatOperations.getRealm();
                                chatMessage.setSent(true);
                                realmChatOperations.copyToRealmOrUpdateChatMessage(realm, chatMessage);
                                if (finalNewConversation != null) {
                                    finalNewConversation.setId(chatMessage.getId());
                                    realmChatOperations.postProcessChatGroup(realm, finalNewConversation);
                                    realm.beginTransaction();
                                    try {
                                        realm.copyToRealmOrUpdate(finalNewConversation);
                                    } finally {
                                        realm.commitTransaction();
                                    }

                                }
                                realm.close();
                            }

                            private void deleteTempFile() {
                                if (finalTempImage != null) {
                                    finalTempImage.delete();
                                }
                            }
                        });
            }

        }
    }

    @OnClick(R.id.attach_button)
    void onAttachClick() {

        new ListDialog() {
            @Override
            public String getDialogMessage() {
                return getString(R.string.attach);
            }

            @Override
            public CharSequence[] getItemsSource() {
                return new String[]{
                        getString(R.string.do_photo),
                        getString(R.string.gallery),
                        getString(R.string.geolocation),
                        getString(R.string.cancel)
                };
            }

            @Override
            public void onItemClick(int itemId) {
                switch (itemId) {
                    case 0:
                        photoName = cameraHandler.openCamera(CHAT_FOLDER);
                        return;
                    case 1:
                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, CameraHandler.REQUEST_OPEN_IMAGE);
                        return;
                    case 2:
                        typeMessage = ChatMessage.TYPE_GEOLOCATION;
                        attach.setImageResource(R.drawable.nupi_other_geolocation);
                        preview.setVisibility(View.VISIBLE);
                        return;
                    case 3:
                        clearAttach();
                }
            }
        }.show(getActivity());
    }

    private void loadAttachmentImage(String path) {
        attach.setImageResource(R.drawable.nupi_placeholder_icon);
        preview.setVisibility(View.VISIBLE);
        sendMessage.setHint(R.string.add_comment);
    }

    void clearAttach() {
        preview.setVisibility(View.GONE);
        attach.setImageResource(R.drawable.nupi_attach);
        typeMessage = ChatMessage.TYPE_TEXT;
        sendMessage.setHint(R.string.write_message);
        if (attachmentImage != null)
            attachmentImage.recycle();
    }
}
