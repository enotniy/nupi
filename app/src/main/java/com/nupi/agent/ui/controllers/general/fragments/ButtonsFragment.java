package com.nupi.agent.ui.controllers.general.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.enums.UpdateStatus;
import com.nupi.agent.events.InSystemReportButtonEvent;
import com.nupi.agent.events.SystemReportButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.registry.activities.RegistryActivity;
import com.nupi.agent.utils.StringUtils;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.RealmChangeListener;


/**
 * A placeholder fragment containing a simple view.
 */
public class ButtonsFragment extends NupiFragment implements RealmChangeListener {

    @InjectView(R.id.ImageWallet)
    ImageView ImageWallet;
    @InjectView(R.id.walletButton)
    LinearLayout walletButton;
    @InjectView(R.id.in_the_wallet)
    TextView inTheWallet;
    @InjectView(R.id.showRealisationWallet)
    LinearLayout showRealisationWallet;
    @InjectView(R.id.sum_debt_value)
    TextView sumDebtValue;
    @InjectView(R.id.sum_debt)
    LinearLayout sumDebt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_buttons, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        getRegistryRealm().removeChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getRegistryRealm().addChangeListener(this);
        onChange();
    }

    @OnClick(R.id.showRealisationWallet)
    void onWalletRegistryClick() {
        sendButtonEvent("Registry from pocket", true);
        if (!RegistryActivity.isOpenedActivity()) {
            showProgressDialog();
            bus.post(new SystemReportButtonEvent(true));
        } else
            bus.post(new InSystemReportButtonEvent());
    }

    @OnClick(R.id.walletButton)
    void onWalletButtonClick() {
        preferences.changeWalletVisible();
        setVisibleWallet();
    }

    @Override
    public void onChange() {
        setVisibleWallet();
        setDebtValue();
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        if (event.updateStatus == UpdateStatus.UPDATE_FINISHED && isVisible()) {
            onChange();
        }
    }

    private void setDebtValue() {
        if (preferences.isLoggedIn()) {
            sumDebtValue.setText(
                    String.format(
                            getResources().getString(R.string.debtorArrearsLevel),
                            StringUtils.getMoneyFormat(realmExchangeOperations.getDebtSumForAllCounterAgent(getMeteorRealm()))
                    )
            );
        }
    }

    private void setVisibleWallet() {
        if (preferences.isWalletVisible()) {
            inTheWallet.setText(
                    String.format(
                            getResources().getString(R.string.nowInPocket),
                            StringUtils.getMoneyFormat(realmRegistryOperations.getInThePocketValue(getRegistryRealm()))
                    )
            );
            ImageWallet.setImageResource(R.drawable.nupi_eye_open);
            ImageWallet.setPadding(0, 0, 0, 0);
        } else {
            inTheWallet.setText(getResources().getString(R.string.nowInPocketGone));
            ImageWallet.setImageResource(R.drawable.nupi_eye_closed);
            ImageWallet.setPadding(0, (int) getResources().getDimension(R.dimen.padding_half_small), 0, 0);
        }
    }

}
