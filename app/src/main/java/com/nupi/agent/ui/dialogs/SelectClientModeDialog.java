package com.nupi.agent.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.nupi.agent.R;
import com.nupi.agent.events.ClientDebtEvent;
import com.nupi.agent.events.ClientSelectedEvent;
import com.squareup.otto.Bus;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 19.11.15
 */
public class SelectClientModeDialog {

    private AlertDialog alertDialog;
    private Bus bus;

    public SelectClientModeDialog(Activity activity, Bus bus) {
        this.bus = bus;

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.dialog_choose_mode, null);
        new ViewHolder(view);

        alertDialog = new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.choose_mode))
                .setView(view)
                .create();
    }

    public void show() {
        alertDialog.show();
    }

    class ViewHolder {

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }


        @OnClick(R.id.goto_price)
        public void onGoToPriceClick() {
            bus.post(new ClientSelectedEvent());
            alertDialog.dismiss();
        }

        @OnClick(R.id.goto_cash)
        public void onGoToCashClick() {
            bus.post(new ClientDebtEvent());
            alertDialog.dismiss();
        }
    }
}
