package com.nupi.agent.ui.controllers.analytics.adapters;

import android.app.Activity;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.ExpandedPayment;
import com.nupi.agent.enums.SortType;
import com.nupi.agent.utils.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by enotniy on
 */
public class ReportClientAdapter extends AbstractExpandableItemAdapter<ReportClientAdapter.GroupViewHolder, ReportClientAdapter.ItemViewHolder> {

    @Inject
    RealmExchangeOperations realmExchangeOperations;

    private Date dateFrom;
    private Date dateTo;
    private List<CounterAgent> counterAgents;
    private HashMap<String, Pair<RealmResults<ExpandedPayment>, Double>> payments;
    private Realm realm;

    public ReportClientAdapter(Activity activity, Realm realm, Date from, Date to) {
        this.dateFrom = from;
        this.dateTo = to;
        this.realm = realm;
        ((NupiApp) activity.getApplication()).getAppComponent().inject(this);
        counterAgents = realmExchangeOperations.getNonFolderCounterAgents(realm);
        initData();
        setHasStableIds(true);
    }

    private void initData() {
        payments = new HashMap<>();
        for (CounterAgent counterAgent : realmExchangeOperations.getNonFolderCounterAgents(realm)) {
            double tail = realmExchangeOperations.getDebtByCounterAgentSumFromDateToNow(realm, dateTo, counterAgent);
            payments.put(counterAgent.getGuid(), new Pair<>(realmExchangeOperations.getExpandedPaymentByCounterAgent(realm, counterAgent.getGuid(), dateFrom, dateTo), tail));
        }
    }

    public void setFilterText(final String query) {
        counterAgents = realmExchangeOperations.getNonFolderCounterAgents(realm, query.toLowerCase(), SortType.ABC_ORDER, Sort.ASCENDING);
    }

    public void changeGroups(Date from, Date to) {
        this.dateFrom = from;
        this.dateTo = to;
        initData();
        notifyDataSetChanged();
    }

    public float getSum() {
        float sum = 0;
        for (CounterAgent counterAgent : counterAgents) {
            sum += (counterAgent.getDebt() - payments.get(counterAgent.getGuid()).second);
        }
        return sum;
    }

    public void setSelectedCounterAgent(CounterAgent selectedCounterAgent) {
        if (selectedCounterAgent == null)
            counterAgents = realmExchangeOperations.getNonFolderCounterAgents(realm);
        else {
            counterAgents = new LinkedList<>();
            counterAgents.add(selectedCounterAgent);
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindGroupViewHolder(GroupViewHolder holder, int groupPosition, int viewType) {
        final CounterAgent counterAgent = counterAgents.get(groupPosition);
        final RealmResults<ExpandedPayment> expandedPayments = payments.get(counterAgent.getGuid()).first;
        final double tail = payments.get(counterAgent.getGuid()).second;

        final Number numberExpense = expandedPayments.where().equalTo("type", ExpandedPayment.TYPE_EXPENSE).sum("sum");
        final double plusDebtPeriod = numberExpense == null ? 0 : numberExpense.floatValue();
        final Number numberReceipt = expandedPayments.where().equalTo("type", ExpandedPayment.TYPE_RECEIPT).sum("sum");
        final double minusDebtPeriod = numberReceipt == null ? 0 : numberReceipt.floatValue();

        holder.clientName.setText(counterAgent.getName());
        holder.startDebt.setText(StringUtils.getMoneyFormat(counterAgent.getDebt() - minusDebtPeriod + plusDebtPeriod - tail));
        holder.finishDebt.setText(StringUtils.getMoneyFormat(counterAgent.getDebt() - tail));
        holder.downDebt.setText(plusDebtPeriod > 0f ? StringUtils.getMoneyFormat(plusDebtPeriod) : "");
        holder.upDebt.setText(minusDebtPeriod > 0f ? StringUtils.getMoneyFormat(minusDebtPeriod) : "");

        holder.itemView.setClickable(true);
    }

    public double getDebtByDoc(CounterAgent counterAgent, List<ExpandedPayment> expandedPayments, double tail, int position) {
        double result = counterAgent.getDebt() - tail;
        for (int i = expandedPayments.size() - 2; i >= position; i--) {
            result += expandedPayments.get(i + 1).getType().equals(ExpandedPayment.TYPE_EXPENSE) ? expandedPayments.get(i + 1).getSum() : -expandedPayments.get(i + 1).getSum();
        }
        return result;
    }

    @Override
    public void onBindChildViewHolder(ItemViewHolder holder, int groupPosition, int childPosition, int viewType) {
        final CounterAgent counterAgent = counterAgents.get(groupPosition);
        final List<ExpandedPayment> expandedPayments = payments.get(counterAgent.getGuid()).first;
        final ExpandedPayment item = expandedPayments.get(childPosition);

        holder.docName.setText(item.getRegistrarInfo());

        holder.startDebt.setText("");
        holder.finishDebt.setText(StringUtils.getMoneyFormat(getDebtByDoc(counterAgent, expandedPayments, payments.get(counterAgent.getGuid()).second, childPosition)));
        if (item.getType().equals(ExpandedPayment.TYPE_EXPENSE))
            holder.downDebt.setText(StringUtils.getMoneyFormat(item.getSum()));
        else
            holder.downDebt.setText("");

        if (item.getType().equals(ExpandedPayment.TYPE_RECEIPT))
            holder.upDebt.setText(StringUtils.getMoneyFormat(item.getSum()));
        else
            holder.upDebt.setText("");
    }


    @Override
    public int getGroupCount() {
        return counterAgents.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        final CounterAgent counterAgent = counterAgents.get(groupPosition);
        return payments.get(counterAgent.getGuid()).first.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return counterAgents.get(groupPosition).getGuid().hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public GroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_report_group, parent, false);
        return new GroupViewHolder(v);
    }

    @Override
    public ItemViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_report_item, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(GroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return holder.itemView.isEnabled() && holder.itemView.isClickable();
    }

    static class GroupViewHolder extends AbstractExpandableItemViewHolder {
        @InjectView(R.id.clientName)
        TextView clientName;
        @InjectView(R.id.start_debt)
        TextView startDebt;
        @InjectView(R.id.up_debt)
        TextView upDebt;
        @InjectView(R.id.down_debt)
        TextView downDebt;
        @InjectView(R.id.finish_debt)
        TextView finishDebt;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        GroupViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    class ItemViewHolder extends AbstractExpandableItemViewHolder {
        @InjectView(R.id.docName)
        TextView docName;
        @InjectView(R.id.start_debt)
        TextView startDebt;
        @InjectView(R.id.up_debt)
        TextView upDebt;
        @InjectView(R.id.down_debt)
        TextView downDebt;
        @InjectView(R.id.finish_debt)
        TextView finishDebt;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

    }
}