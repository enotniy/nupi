package com.nupi.agent.ui.controllers.chat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import com.nupi.agent.R;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.helpers.CameraHandler;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.chat.fragments.ChatListFragment;
import com.nupi.agent.ui.controllers.chat.fragments.ChatMessageFragment;
import com.squareup.otto.Subscribe;


public class ChatActivity extends NupiActivity {

    public static final String
            KEY_IS_FROM_NOTIFICATION = "KEY_IS_FROM_NOTIFICATION",
            KEY_IS_PRIVATE_MESSAGE = "KEY_IS_PRIVATE_MESSAGE",
            KEY_THREAD_ID = "KEY_THREAD_ID";

    public static final String PHOTO_PATH = String.format("%s/DCIM/chat-cache/", Environment.getExternalStorageDirectory());

    private CameraHandler cameraHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);

        cameraHandler = new CameraHandler(this);
        if (savedInstanceState == null)
            setupFragments(getIntent());

    }

    private void setupFragments(Intent intent) {

        ChatListFragment chatListFragment;
        ChatMessageFragment chatMessageFragment;

        final String id = intent.getStringExtra(KEY_THREAD_ID);

        if (id != null && intent.getBooleanExtra(KEY_IS_FROM_NOTIFICATION, false)) {
            if (intent.getBooleanExtra(KEY_IS_PRIVATE_MESSAGE, true)) {
                chatListFragment = ChatListFragment.getPrivateInstance(id);
                chatMessageFragment = ChatMessageFragment.getPrivateInstance(id);
            } else {
                chatListFragment = ChatListFragment.getGroupsInstance(id);
                chatMessageFragment = ChatMessageFragment.getGroupsInstance(id);
            }
        } else {
            chatListFragment = new ChatListFragment();
            chatMessageFragment = new ChatMessageFragment();
        }
        getFragmentManager().beginTransaction()
                .replace(R.id.leftContainer, chatListFragment)
                .replace(R.id.rightContainer, chatMessageFragment)
                .commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

}
