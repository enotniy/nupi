package com.nupi.agent.ui.controllers.main.cash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class CashAdapter extends RealmBaseAdapter<PaymentByOrder> {


    private final Context context;
    private ArrayList<Integer> selectedIndexs = new ArrayList<>();
    private List<PaymentByOrder> sortPaymentsByOrders;
    private Contract contract;


    public CashAdapter(Context context, RealmResults<PaymentByOrder> paymentsByOrders, Contract contract) {
        super(context, paymentsByOrders, true);
        this.context = context;
        this.contract = contract;
        initData(paymentsByOrders);
    }

    private void initData(RealmResults<PaymentByOrder> paymentsByOrders) {
        this.sortPaymentsByOrders = new LinkedList<>();
        for (PaymentByOrder paymentByOrder : paymentsByOrders) {
            sortPaymentsByOrders.add(paymentByOrder);
        }
        Collections.sort(sortPaymentsByOrders, new Comparator<PaymentByOrder>() {
            @Override
            public int compare(PaymentByOrder lhs, PaymentByOrder rhs) {
                return TimeUtils.calculateDate(lhs).compareTo(TimeUtils.calculateDate(rhs));
            }
        });
    }

    public void addSelectedIndex(int selectedIndex) {
        if (selectedIndexs.contains(selectedIndex))
            selectedIndexs.remove(selectedIndexs.indexOf(selectedIndex));
        else
            selectedIndexs.add(selectedIndex);
    }

    @Override
    public PaymentByOrder getItem(int i) {
        return sortPaymentsByOrders.get(i);
    }

    @Override
    public void updateRealmResults(RealmResults<PaymentByOrder> queryResults) {
        super.updateRealmResults(queryResults);
        initData(queryResults);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        PaymentByOrder paymentByOrder = getItem(position);

        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(context)
                    .inflate(R.layout.list_item_cash, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.debt.setText(String.format("%.2f", paymentByOrder.getSum()));
        viewHolder.docName.setText(paymentByOrder.getDocInfo());

        long delay = getDelay(paymentByOrder);
        viewHolder.delay.setText(String.valueOf(delay));
        viewHolder.delay.setTextColor(context.getResources().getColor(delay > 0 ? R.color.red : R.color.text_list));

        viewHolder.clientRow.setBackground(context.getResources().getDrawable(
                selectedIndexs.contains(position) ? R.drawable.selector_list_item_selected : R.drawable.selector_list_item));

        return view;
    }

    private long getDelay(PaymentByOrder paymentByOrder) {
        if (contract != null && contract.isDebtDaysControl()) {
            long diff = (TimeUtils.currentTime() - TimeUtils.calculateDate(paymentByOrder).getTime());
            long delay = (diff / 1000 / 60 / 60 / 24) - contract.getDebtDaysMax();
            return delay > 0 ? delay : 0;
        }
        return 0;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_cash.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.docName)
        TextView docName;
        @InjectView(R.id.age)
        TextView age;
        @InjectView(R.id.delay)
        TextView delay;
        @InjectView(R.id.debt)
        TextView debt;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

