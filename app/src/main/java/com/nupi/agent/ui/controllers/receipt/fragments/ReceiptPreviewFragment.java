package com.nupi.agent.ui.controllers.receipt.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Organization;
import com.nupi.agent.network.meteor.OrderRequest;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.receipt.adapters.ReceiptPreviewEntryAdapter;
import com.nupi.agent.utils.StringUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 08.09.15
 */
public class ReceiptPreviewFragment extends NupiFragment {

    @InjectView(R.id.sum_info)
    TextView sumInfo;
    @InjectView(R.id.comment_info)
    TextView commentInfo;
    @InjectView(R.id.price_entries)
    ListView priceEntries;
    @InjectView(R.id.realisation_header)
    TextView realisationHeader;
    @InjectView(R.id.sum)
    TextView sum;
    @InjectView(R.id.backBtn)
    FrameLayout backBtn;
    @InjectView(R.id.organization_info)
    TextView organizationInfo;
    @InjectView(R.id.point_info)
    TextView pointInfo;
    @InjectView(R.id.status_info)
    TextView statusInfo;
    @InjectView(R.id.comment_scroll)
    ScrollView scrollViewComment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipt, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!selectedItemsHolder.getSelectedCounterAgentGuid().equals(NEW_COUNTER_AGENT_GUID)) {
            final CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class);
            pointInfo.setText(counterAgent.getName());
        } else
            pointInfo.setText(getString(R.string.not_counteragent));

        final OrderRequest orderRequest = orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedCounterAgentGuid());

        realisationHeader.setText(R.string.preview);

        ReceiptPreviewEntryAdapter receiptPreviewEntryAdapter = new ReceiptPreviewEntryAdapter(getActivity(), orderRequest.getOrderItems(), getMeteorRealm());
        priceEntries.setAdapter(receiptPreviewEntryAdapter);

        sum.setText(StringUtils.getMoneyFormat(receiptPreviewEntryAdapter.getSum()));
        sumInfo.setText(StringUtils.getMoneyFormat(receiptPreviewEntryAdapter.getSum()));

        Organization firms = null;
        if (orderRequest.getOrganization() != null)
            firms = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), orderRequest.getOrganization(), Organization.class);

        organizationInfo.setText(firms != null ? firms.getName() : "");

        statusInfo.setText(R.string.preview);

        if (orderRequest.getComment() != null)
            commentInfo.setText(String.format(getString(R.string.comment_receipt), orderRequest.getComment()));
    }

    @OnClick(R.id.backBtn)
    void onBackClick() {
        getActivity().finish();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
