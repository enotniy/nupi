package com.nupi.agent.ui.controllers.main.main_screen.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.utils.StringUtils;

import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;


public class TodayAdapter extends RealmBaseAdapter<RegistryEntry> implements ListAdapter {

    private int resId;

    public TodayAdapter(Context context, int resId, RealmResults<RegistryEntry> realmResults, boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
        this.resId = resId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(resId, parent, false);
        }

        RegistryEntry item = realmResults.get(position);
        ((TextView) convertView.findViewById(R.id.todayRow)).setText(String.format("%d: %S", position + 1, StringUtils.getMoneyFormat(item.getSum())));
        fillItemStatus(item, convertView.findViewById(R.id.status_text_report));
        return convertView;
    }

    private void fillItemStatus(RegistryEntry item, View view) {
        switch (item.getEntryStatus()) {
            case RegistryEntry.SKIPPED:
                view.setBackgroundResource(R.drawable.selector_status_circle_skipped);
                break;
            case RegistryEntry.NOT_SENT:
                view.setBackgroundResource(R.drawable.selector_status_circle_not_send);
                break;
            case RegistryEntry.SENT:
                view.setBackgroundResource(R.drawable.selector_status_circle_send);
                break;
            case RegistryEntry.RECEIVED:
                view.setBackgroundResource(R.drawable.selector_status_circle_received);
                break;
            case RegistryEntry.REJECTED:
                view.setBackgroundResource(R.drawable.selector_status_circle_rejected);
                break;
            case RegistryEntry.ACCEPTED:
                view.setBackgroundResource(R.drawable.selector_status_circle_accepted);
                break;
        }
    }


}
