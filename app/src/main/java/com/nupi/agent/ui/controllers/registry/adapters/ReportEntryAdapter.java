package com.nupi.agent.ui.controllers.registry.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.utils.StringUtils;
import com.nupi.agent.utils.TimeUtils;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;


public class ReportEntryAdapter extends RealmBaseAdapter<RegistryEntry> implements ListAdapter {

    private RealmExchangeOperations realmExchangeOperations;
    private Realm realm;

    public ReportEntryAdapter(Context context, Realm realm, RealmExchangeOperations realmExchangeOperations, RealmResults<RegistryEntry> realmResults, boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
        this.realmExchangeOperations = realmExchangeOperations;
        this.realm = realm;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_report_entry, parent, false);
            viewHolder = getViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        RegistryEntry item = realmResults.get(position);

        Date creationDate = item.getDate();
        fillItemsFields(viewHolder, item, creationDate);
        fillItemStatus(viewHolder, item);
        return convertView;
    }

    private void fillItemsFields(ViewHolder viewHolder, RegistryEntry item, Date creationDate) {
        viewHolder.sum_text_report.setText(StringUtils.getMoneyFormat(item.getSum()));
        viewHolder.date_text_report.setText(TimeUtils.dateFormat(creationDate));
        viewHolder.time_text_report.setText(TimeUtils.timeFormat(creationDate));
        CounterAgent client = realmExchangeOperations.getRealmObjectByGuid(realm, item.getCounterAgent(), CounterAgent.class);
        viewHolder.counter_agent__text_report.setText(client != null ? client.getName() : context.getString(R.string.unknown));
        viewHolder.doc_type_text_report.setText(item.getDocInfo());

    }

    private void fillItemStatus(ViewHolder viewHolder, RegistryEntry item) {
        switch (item.getEntryStatus()) {
            case RegistryEntry.SKIPPED:
                viewHolder.status_text_report.setBackgroundResource(R.drawable.selector_status_circle_skipped);
                break;
            case RegistryEntry.NOT_SENT:
                viewHolder.status_text_report.setBackgroundResource(R.drawable.selector_status_circle_not_send);
                break;
            case RegistryEntry.SENT:
                viewHolder.status_text_report.setBackgroundResource(R.drawable.selector_status_circle_send);
                break;
            case RegistryEntry.RECEIVED:
                viewHolder.status_text_report.setBackgroundResource(R.drawable.selector_status_circle_received);
                break;
            case RegistryEntry.REJECTED:
                viewHolder.status_text_report.setBackgroundResource(R.drawable.selector_status_circle_rejected);
                break;
            case RegistryEntry.ACCEPTED:
                viewHolder.status_text_report.setBackgroundResource(R.drawable.selector_status_circle_accepted);
                break;
        }
    }

    public float getSum() {
        Number sum = realmResults.where().sum("sum");
        if (sum != null)
            return sum.floatValue();
        else return 0;
    }

    private ViewHolder getViewHolder(View convertView) {
        ViewHolder viewHolder;
        viewHolder = new ViewHolder();
        viewHolder.status_text_report = convertView.findViewById(R.id.status_text_report);
        viewHolder.doc_type_text_report = (TextView) convertView.findViewById(R.id.doc_type_text_report);
        viewHolder.date_text_report = (TextView) convertView.findViewById(R.id.date_text_report);
        viewHolder.time_text_report = (TextView) convertView.findViewById(R.id.time_text_report);
        viewHolder.sum_text_report = (TextView) convertView.findViewById(R.id.sum_text_report);
        viewHolder.counter_agent__text_report = (TextView) convertView.findViewById(R.id.counter_agent__text_report);
        return viewHolder;
    }

    private static class ViewHolder {
        View status_text_report;
        TextView doc_type_text_report;
        TextView date_text_report;
        TextView time_text_report;
        TextView sum_text_report;
        TextView counter_agent__text_report;
    }

}
