package com.nupi.agent.ui.controllers.chat.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.helpers.ChatColorHelper;
import com.nupi.agent.utils.ChatMessageUtils;
import com.nupi.agent.utils.TimeUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import rx.Subscription;

/**
 * Created by User on 19.10.2015.
 */
public class ChatUsersAdapter extends RealmBaseAdapter<ChatUser> {

    private final ChatColorHelper chatColorHelper;
    private RealmChatOperations realmChatOperations;
    private Realm realm;
    private Context context;
    private String selectedIndex = null;
    private boolean selectedTab = true;
    private String userId;
    private Subscription subscription;

    public ChatUsersAdapter(Context context, RealmChatOperations realmChatOperations, Realm realm, RealmResults<ChatUser> objects, String selectedPosition, String userId) {
        super(context, objects, true);
        chatColorHelper = new ChatColorHelper(context);
        selectedIndex = selectedPosition;
        this.realmChatOperations = realmChatOperations;
        this.realm = realm;
        this.userId = userId;
        this.context = context;
    }


    public void setSelectedIndex(String selectedIndex) {
        this.selectedIndex = selectedIndex;
        this.selectedTab = true;
    }

    public void unselectedTab() {
        this.selectedTab = false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item_chat_person, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        final ChatUser chatUser = getItem(position);
        final String chatUserId = chatUser.getUser();

        if (selectedTab && selectedIndex != null && selectedIndex.equals(chatUserId))
            convertView.setBackgroundColor(context.getResources().getColor(R.color.background_chat));
        else
            convertView.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item));

        final long countMessage = realmChatOperations.getUnreadPrivateChatMessagesCount(realm, chatUserId, userId);
        fillHeadline(viewHolder, ChatMessageUtils.getNickname(chatUser), chatUserId, countMessage);

        return convertView;
    }

    void fillHeadline(ViewHolder viewHolder, String name, String chatUserId, long countMessage) {
        viewHolder.personName.setText(name);
        viewHolder.firstSymbol.setText(ChatMessageUtils.getFirstSymbols(name));
        viewHolder.firstSymbol.setBackgroundResource(R.drawable.selector_chat);
        GradientDrawable gd = (GradientDrawable) viewHolder.firstSymbol.getBackground().getCurrent();
        gd.setColor(chatColorHelper.getColor(chatUserId));
        if (countMessage > 0) {
            viewHolder.messageCount.setText(String.valueOf(countMessage));
            viewHolder.messageCount.setVisibility(View.VISIBLE);
        } else
            viewHolder.messageCount.setVisibility(View.GONE);

        RealmResults<ChatMessage> chatMessages = realmChatOperations.getPrivateChatMessages(realm, chatUserId);
        ChatMessage chatMessage = chatMessages.size() > 0 ? chatMessages.last() : null;
        if (chatMessage != null) {
            String nickname = !chatMessage.getUserFrom().getUser().equals(userId) ? ChatMessageUtils.getNickname(chatMessage.getUserFrom()) : context.getResources().getString(R.string.i);
            Spanned message = Html.fromHtml(String.format(context.getString(R.string.chat_message_last),
                    nickname,
                    chatMessage.getMessage() != null ? chatMessage.getMessage() : "",
                    (chatMessage.getType().compareTo(ChatMessage.TYPE_GEOLOCATION) == 0 ? context.getString(R.string.geolocation_in_small_chat) : ""),
                    (chatMessage.getType().compareTo(ChatMessage.TYPE_IMAGE) == 0 ? context.getString(R.string.img_in_small_chat) : ""))
            );

            viewHolder.textMessage.setText(message);
            viewHolder.textMessage.setVisibility(View.VISIBLE);

            viewHolder.timeMessage.setText(TimeUtils.dateFormatChat(TimeUtils.getChatTime(chatMessage.getDate())));
            viewHolder.timeMessage.setVisibility(View.VISIBLE);
        } else {
            viewHolder.textMessage.setText("");
             //viewHolder.textMessage.setVisibility(View.GONE);
             viewHolder.timeMessage.setVisibility(View.GONE);
        }
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_chat_group.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {

        @InjectView(R.id.message_count)
        TextView messageCount;
        @InjectView(R.id.first_symbol)
        TextView firstSymbol;
        @InjectView(R.id.personName)
        TextView personName;
        @InjectView(R.id.textMessage)
        TextView textMessage;
        @InjectView(R.id.timeMessage)
        TextView timeMessage;
        @InjectView(R.id.chatRow)
        LinearLayout chatRow;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


}
