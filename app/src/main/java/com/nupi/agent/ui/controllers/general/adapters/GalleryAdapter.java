package com.nupi.agent.ui.controllers.general.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.helpers.CameraHandler;
import com.nupi.agent.ui.base.BaseAdapter;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Pasenchuk Victor on 24.05.15
 */
public class GalleryAdapter extends BaseAdapter<File> {

    private ThreadPoolExecutor threadPoolExecutor;

    public GalleryAdapter(Context context, String clientName) {
        super(context, R.layout.list_photo, getFiles(clientName));
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(9);
    }

    public static List<File> getFiles(String clientName) {
        File agentDir = new File(CameraHandler.PHOTO_PATH + clientName + "/");
        agentDir.mkdirs();
        File[] children = agentDir.listFiles();
        Arrays.sort(children, new Comparator<File>() {
            @Override
            public int compare(File lhs, File rhs) {
                return Long.valueOf(rhs.lastModified()).compareTo(lhs.lastModified());
            }
        });
        return new LinkedList<>(Arrays.asList(children));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        final File file = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_photo, null);
            viewHolder = new ViewHolder();
            viewHolder.imagePhoto = (ImageView) convertView.findViewById(R.id.imagePhoto);
            viewHolder.textPhotoName = (TextView) (convertView.findViewById(R.id.textPhotoName));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textPhotoName.setText(file.getName());
        viewHolder.imagePhoto.setImageResource(R.drawable.placeholder);
        viewHolder.imagePhoto.setTag(file);

        threadPoolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    final byte[] imageData = new ExifInterface(file.getPath()).getThumbnail();

                    if (imageData != null) {
                        final Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
                        postBitmap(bitmap);

                    } else if (file.length() < 2048_00) {
                        final Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
                        postBitmap(bitmap);
                    }

                } catch (Exception ignored) {
                }
            }


            private void postBitmap(final Bitmap bitmap) {
                if (viewHolder.imagePhoto != null && viewHolder.imagePhoto.getTag().equals(file)) {
                    viewHolder.imagePhoto.post(new Runnable() {
                        @Override
                        public void run() {
                            viewHolder.imagePhoto.setImageBitmap(bitmap);
                        }
                    });
                }
            }
        });


        return convertView;
    }

    @Override
    public String getItemFilterableValue(File item) {
        return item.getName();
    }

    @Override
    public void sort() {

    }

    private class ViewHolder {
        TextView textPhotoName;
        ImageView imagePhoto;
    }
}
