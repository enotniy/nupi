package com.nupi.agent.ui.dialogs;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 21.03.14
 * Time: 13:04
 */
interface ListDialogDelegate {
    String getDialogMessage();

    CharSequence[] getItemsSource();

    void onItemClick(int itemId);
}
