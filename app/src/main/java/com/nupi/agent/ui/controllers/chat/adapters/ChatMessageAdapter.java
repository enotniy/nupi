package com.nupi.agent.ui.controllers.chat.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.database.models.chat.ChatUser;
import com.nupi.agent.di.AppModule;
import com.nupi.agent.helpers.ChatColorHelper;
import com.nupi.agent.network.nupi.NupiServerApi;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.utils.ChatMessageUtils;
import com.nupi.agent.utils.PicassoCacheUtils;
import com.nupi.agent.utils.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by User on 20.10.2015
 */

public class ChatMessageAdapter extends RealmBaseAdapter<ChatMessage> {

    public static final int TIMEOUT = 2;
    private static final int
            MY_MESSAGE = 0,
            GROUP_MESSAGE = 1,
            PRIVATE_MESSAGE = 2;
    private static final int MAP_SIZE_SMALL = 255;
    private static final int MAP_SIZE_BIG_WIDTH = 640;
    private static final int MAP_SIZE_BIG_HEIGHT = 600;
    private final ChatColorHelper chatColorHelper;
    @Inject
    RealmChatOperations realmChatOperations;
    private String userId;
    private Activity context;
    private List<String> readMessageIds = new LinkedList<>();
    private MessageReader messageReader = new MessageReader();
    private AlertDialog photoPreviewDialog;

    public ChatMessageAdapter(Activity context, RealmResults<ChatMessage> objects, String userId, final NupiServerApi nupiServerApi) {
        super(context, objects, true);
        ((NupiApp) context.getApplication()).getAppComponent().inject(this);
        chatColorHelper = new ChatColorHelper(context);
        this.userId = userId;
        this.context = context;

        Observable
                .create(new Observable.OnSubscribe<Void>() {
                    @Override
                    public void call(final Subscriber<? super Void> subscriber) {
                        final MessageReader.OnMessageReadListener listener = new MessageReader.OnMessageReadListener() {
                            @Override
                            public void onReadMessage(String messageId) {
                                if (!readMessageIds.contains(messageId))
                                    readMessageIds.add(messageId);
                                subscriber.onNext(null);
                            }
                        };
                        messageReader.addListener(listener);

                        subscriber.add(new Subscription() {
                            @Override
                            public void unsubscribe() {
                                messageReader.removeListener(listener);
                            }

                            @Override
                            public boolean isUnsubscribed() {
                                return false;
                            }
                        });
                    }
                })
                .debounce(TIMEOUT, TimeUnit.SECONDS)
                .flatMap(new Func1<Void, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(Void aVoid) {

                        for (String guid : readMessageIds) {
                            realmChatOperations.setMessageReadStatus(realmChatOperations.getRealm(), guid, true);
                        }

                        return nupiServerApi
                                .updateReadingStatus(readMessageIds)
                                .onErrorResumeNext(Observable.<Void>empty());
                    }
                })
                .observeOn(Schedulers.computation())
                .subscribeOn(Schedulers.computation())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Void aVoid) {
                    }
                });

        if (realmResults != null && realmResults.size() > 0 && getItemViewType(realmResults.size() - 1) == MY_MESSAGE)
            for (int i = 0; i < realmResults.size(); i++) {
                realmChatOperations.setMessageReadStatus(realmChatOperations.getRealm(), realmResults.get(i).getId(), true);
            }
    }

    private String getMapUrl(Double lat, Double lon, int width, int height) {
        return String.format("%s/staticmap?center=%s,%s&zoom=15&maptype=hybrid&size=%dx%d&scale=1&language=%s&sensor=false&markers=%s,%s", AppModule.HTTP_API_GOOGLE_MAPS, lat, lon, width, height, "RU", lat, lon);
    }

    @Override
    public int getItemViewType(int position) {
        final ChatMessage chatMessage = getItem(position);
        if (chatMessage.getUserFrom() != null && chatMessage.getUserFrom().getUser().equals(userId))
            return MY_MESSAGE;
        if (chatMessage.getChatGroup() != null)
            return GROUP_MESSAGE;
        else
            return PRIVATE_MESSAGE;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        final ChatMessage chatMessage = getItem(position);

        if (!chatMessage.isWasRead() && !chatMessage.getUserFrom().getUser().equals(userId))
            messageReader.readMessage(chatMessage.getId());

        final int type = getItemViewType(position);

        if (convertView == null) {
            convertView = getInflatedLayoutForType(type);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        if (type == GROUP_MESSAGE) {
            try {
                ChatUser chatUser = chatMessage.getUserFrom();
                int color = chatColorHelper.getColor(chatUser.getUser());
                viewHolder.firstSymbol.setText(ChatMessageUtils.getFirstSymbols(ChatMessageUtils.getNickname(chatUser)));
                viewHolder.sender.setVisibility(View.VISIBLE);
                viewHolder.sender.setText(ChatMessageUtils.getNickname(chatUser));
                viewHolder.sender.setTextColor(color);

                viewHolder.firstSymbol.setBackgroundResource(R.drawable.selector_chat);
                GradientDrawable gd = (GradientDrawable) viewHolder.firstSymbol.getBackground().getCurrent();
                gd.setColor(chatColorHelper.getColor(chatUser.getUser()));
            } catch (Throwable t) {
                MessageBox.show(String.valueOf(t), context);
            }
        }

        if (type == MY_MESSAGE) {
            viewHolder.readStatus.setVisibility(chatMessage.isWasRead() ? View.VISIBLE : View.GONE);
            viewHolder.unreadStatus.setVisibility(chatMessage.isWasRead() ? View.GONE : View.VISIBLE);
        } else if (getItemViewType(realmResults.size() - 1) == MY_MESSAGE || realmChatOperations.getWasReadMessage(realmChatOperations.getRealm(), chatMessage.getId())) {
            viewHolder.backgroundView.setBackground(context.getResources().getDrawable(R.drawable.selector_nupi_message_other_person));
        } else {
            viewHolder.backgroundView.setBackground(context.getResources().getDrawable(chatMessage.isWasLocallyRead() ? R.drawable.selector_nupi_message_other_person : R.drawable.selector_nupi_message_new_other_person));
        }

        if (ChatMessage.TYPE_GEOLOCATION.equals(chatMessage.getType()) && chatMessage.getLatitude() != null && chatMessage.getLongitude() != null) {
            viewHolder.attach.setVisibility(View.VISIBLE);
            PicassoCacheUtils.loadCachedImage(
                    context,
                    getMapUrl(chatMessage.getLatitude(), chatMessage.getLongitude(), MAP_SIZE_SMALL, MAP_SIZE_SMALL),
                    viewHolder.attach
            );
            viewHolder.setFile(getMapUrl(chatMessage.getLatitude(), chatMessage.getLongitude(), MAP_SIZE_BIG_WIDTH, MAP_SIZE_BIG_HEIGHT));
        } else if (ChatMessage.TYPE_IMAGE.equals(chatMessage.getType())) {
            viewHolder.attach.setVisibility(View.VISIBLE);
            PicassoCacheUtils.loadCachedImage(
                    context,
                    chatMessage.getAttachment(),
                    viewHolder.attach
            );
            viewHolder.setFile(chatMessage.getAttachment());
        } else {
            viewHolder.attach.setVisibility(View.GONE);
        }

        final Date time = TimeUtils.getChatTime(chatMessage.getDate());

        if (position == 0 || (new SimpleDateFormat("yyyy.MM.dd", Locale.US).format(time))
                .compareTo(new SimpleDateFormat("yyyy.MM.dd", Locale.US).format( TimeUtils.getChatTime(getItem(position - 1).getDate()))) != 0)
            viewHolder.date.setVisibility(View.VISIBLE);
        else
            viewHolder.date.setVisibility(View.GONE);

        if (TextUtils.isEmpty(chatMessage.getMessage()))
            viewHolder.textMessage.setVisibility(View.GONE);
        else
            viewHolder.textMessage.setVisibility(View.VISIBLE);
        String message = TextUtils.isEmpty(chatMessage.getMessage()) ? "" : chatMessage.getMessage();
        viewHolder.textMessage.setText(message);

        Locale locale = new Locale("ru");
        Locale.setDefault(locale);
        viewHolder.date.setText(new SimpleDateFormat("d MMMM").format(time));
        viewHolder.timeMessage.setText(TimeUtils.timeShortFormat(time));
        return convertView;
    }

    private View getInflatedLayoutForType(int type) {
        switch (type) {
            case MY_MESSAGE:
                return LayoutInflater.from(context).inflate(R.layout.view_message_my, null);
            case GROUP_MESSAGE:
                return LayoutInflater.from(context).inflate(R.layout.view_message_other_group, null);
            default:
                return LayoutInflater.from(context).inflate(R.layout.view_message_other, null);
        }
    }

    private static class MessageReader {
        private List<OnMessageReadListener> listeners = new LinkedList<>();

        private void addListener(OnMessageReadListener listener) {
            listeners.add(listener);
        }

        private void removeListener(OnMessageReadListener listener) {
            listeners.remove(listener);
        }

        private void readMessage(String messageId) {
            for (OnMessageReadListener listener : listeners)
                listener.onReadMessage(messageId);
        }

        private interface OnMessageReadListener {
            void onReadMessage(String messageId);
        }
    }

    class ViewHolder {

        @InjectView(R.id.container)
        View container;

        @InjectView(R.id.textMessage)
        TextView textMessage;

        @InjectView(R.id.timeMessage)
        TextView timeMessage;

        @InjectView(R.id.first_symbol)
        TextView firstSymbol;

        @InjectView(R.id.attach)
        ImageView attach;

        @InjectView(R.id.sender)
        TextView sender;

        @InjectView(R.id.date)
        TextView date;

        @InjectView(R.id.backgroundView)
        LinearLayout backgroundView;


        @Optional
        @InjectView(R.id.readStatus)
        ImageView readStatus;

        @Optional
        @InjectView(R.id.unreadStatus)
        ImageView unreadStatus;

        private String file;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

        public void setFile(String file) {
            this.file = file;
        }

        @OnClick(R.id.attach)
        void onAttachClick() {
            if (file != null) {
                photoPreviewDialog = new AlertDialog.Builder(context)
                        .create();
                View photoPreview = context.getLayoutInflater().inflate(R.layout.dialog_photo_chat, null);
                ViewHolderPhoto viewHolderPhoto = new ViewHolderPhoto(photoPreview);
                PicassoCacheUtils.loadCachedImage(
                        context,
                        file,
                        viewHolderPhoto.photo
                );

                photoPreviewDialog.setView(photoPreview);
                photoPreviewDialog.show();
            }
        }
    }

    class ViewHolderPhoto {
        @InjectView(R.id.photo)
        ImageView photo;

        ViewHolderPhoto(View view) {
            ButterKnife.inject(this, view);
        }

        @OnClick(R.id.photo)
        void onAttachClick() {
            if (photoPreviewDialog != null)
                photoPreviewDialog.dismiss();
        }
    }
}
