package com.nupi.agent.ui.controllers.planner.adapters;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.database.RealmPlannerOperations;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;
import com.nupi.agent.utils.HighlighterUtils;
import com.nupi.agent.utils.TimeUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class PointsAdapter extends RecyclerView.Adapter<PointsAdapter.ViewHolder> {

    protected Context context;
    protected RealmList<ClientInfo> points;
    @Inject
    OrderApi orderApi;
    private PlannerRoute plannerRoute;
    private RealmPlannerOperations realmPlannerOperations;

    public PointsAdapter(Context context, RealmPlannerOperations realmPlannerOperations, List<ClientInfo> data) {
        NupiApp nupiApp = (NupiApp) context.getApplicationContext();
        nupiApp.getAppComponent().inject(this);
        points = new RealmList<>();
        for (int i = 0; i < data.size(); i++) {
            points.add(data.get(i));
        }
        this.context = context;
        this.realmPlannerOperations = realmPlannerOperations;
        setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_planner, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ClientInfo item = points.get(position);

        // set text
        holder.setClientInfo(item);
        holder.shop.setText(String.valueOf(item.getClientName()));
        if (item.getWorkingBeginTime() != null && item.getWorkingEndTime() != null) {
            final String time = String.format(context.getString(R.string.working_time), TimeUtils.timeFormat(item.getWorkingBeginTime()), TimeUtils.timeFormat(item.getWorkingEndTime()));
            HighlighterUtils.highlightSearchString(time, holder.workingTime, context.getString(R.string.working), context.getResources().getColor(R.color.text_hint));
        } else
            holder.workingTime.setText("");

        if (item.getLunchBeginTime() != null && item.getLunchEndTime() != null) {
            final String lunch = String.format(context.getString(R.string.lunch_time), TimeUtils.timeFormat(item.getLunchBeginTime()), TimeUtils.timeFormat(item.getLunchEndTime()));
            HighlighterUtils.highlightSearchString(lunch, holder.lunchTime, context.getString(R.string.lunch), context.getResources().getColor(R.color.text_hint));
        } else
            holder.lunchTime.setText("");

        if (holder.workingTime.getText().equals("") && holder.lunchTime.getText().equals("")) {
            holder.info.setBackgroundColor(context.getResources().getColor(R.color.background_hint));
            holder.workingTime.setLines(2);
            holder.lunchTime.setVisibility(View.GONE);
        } else {
            holder.workingTime.setLines(1);
            holder.lunchTime.setVisibility(View.VISIBLE);
            holder.info.setBackgroundColor(context.getResources().getColor(R.color.background_list));
        }


        if (item.getVisitFrequency() != null) {
            holder.visitFrequency.setText(String.valueOf(item.getVisitFrequency()));
            holder.visitFrequency.setBackgroundColor(context.getResources().getColor(R.color.background_list));
        } else {
            holder.visitFrequency.setText("");
            holder.visitFrequency.setBackgroundColor(context.getResources().getColor(R.color.background_hint));
        }

        if (item.getResponsiblePerson() != null) {
            holder.responsiblePerson.setText(String.valueOf(item.getResponsiblePerson()));
            holder.responsiblePerson.setBackgroundColor(context.getResources().getColor(R.color.background_list));
        } else {
            holder.responsiblePerson.setText("");
            holder.responsiblePerson.setBackgroundColor(context.getResources().getColor(R.color.background_hint));
        }

        String phone = item.getContacts() != null ? String.valueOf(item.getContacts()) : "";
        String additional_phone = item.getContacts() != null ? String.valueOf(item.getAdditionalPhone()) : "";
        if (!phone.equals("") && !additional_phone.equals(""))
            phone += "\n";
        String email = item.getContacts() != null ? String.valueOf(item.getEmail()) : "";
        if ((!phone.equals("") || !additional_phone.equals("")) && !email.equals(""))
            additional_phone += "\n";
        holder.contacts.setText(String.format("%s%s%s", phone, additional_phone, email));
        if (phone.equals("") && additional_phone.equals("") && email.equals(""))
            holder.contacts.setBackgroundColor(context.getResources().getColor(R.color.background_hint));
        else
            holder.contacts.setBackgroundColor(context.getResources().getColor(R.color.background_list));


        // set background resource (target view ID: container)
        final int dragState = holder.getDragStateFlags();

        if (((dragState & DraggableItemConstants.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;

            if ((dragState & DraggableItemConstants.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.drawable.selector_list_item_selected;

                // need to clear drawable state here to get correct appearance of the dragging item.
                Drawable drawable = holder.container.getBackground();
                if (drawable != null)
                    drawable.setState(new int[]{});
            } else if ((dragState & DraggableItemConstants.STATE_FLAG_DRAGGING) != 0) {
                bgResId = R.drawable.selector_list_item_selected;
            } else {
                bgResId = R.drawable.selector_list_item;
            }

            holder.container.setBackgroundResource(bgResId);
        }
    }


    public void setFilterText(String query) {

    }

    @Override
    public int getItemCount() {
        return points.size();
    }

    @Override
    public long getItemId(int position) {
        return points.get(position).hashCode();
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_planner.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */

    public class ViewHolder extends AbstractDraggableItemViewHolder {

        @InjectView(R.id.shop)
        TextView shop;
        @InjectView(R.id.working_time)
        TextView workingTime;
        @InjectView(R.id.lunch_time)
        TextView lunchTime;
        @InjectView(R.id.info)
        LinearLayout info;
        @InjectView(R.id.visit_frequency)
        TextView visitFrequency;
        @InjectView(R.id.responsible_person)
        TextView responsiblePerson;
        @InjectView(R.id.contacts)
        TextView contacts;
        @InjectView(R.id.container)
        FrameLayout container;
        @InjectView(R.id.clientRow)
        RelativeLayout clientRow;
        ClientInfo clientInfo;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void setClientInfo(ClientInfo clientInfo) {
            this.clientInfo = clientInfo;
        }


        @OnClick(R.id.responsible_person)
        void onResponsiblePersonClick() {
            final EditText input = new EditText(context);
            input.setText(clientInfo.getResponsiblePerson());
            final AlertDialog alertDialog = new AlertDialog.Builder(context)
                    .setTitle(context.getResources().getString(R.string.contact_person))
                    .setView(input)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Realm realm = realmPlannerOperations.getRealm();
                            realm.beginTransaction();
                            clientInfo.setResponsiblePerson(input.getText().toString());
                            realm.commitTransaction();
                            notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        }

        @OnClick(R.id.contacts)
        void onVisitFrequencyClick() {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.dialog_planner_contact, null);
            final ViewHolderContact viewHolderContact = new ViewHolderContact(linearLayout);
            viewHolderContact.email.setText(clientInfo.getEmail());
            viewHolderContact.phoneNumber.setText(clientInfo.getContacts());
            viewHolderContact.phoneNumber2.setText(clientInfo.getAdditionalPhone());

            final AlertDialog alertDialog = new AlertDialog.Builder(context)
                    .setTitle(context.getResources().getString(R.string.phone))
                    .setView(linearLayout)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Realm realm = realmPlannerOperations.getRealm();
                            realm.beginTransaction();
                            clientInfo.setEmail(viewHolderContact.email.getText().toString());
                            clientInfo.setContacts(viewHolderContact.phoneNumber.getText().toString());
                            clientInfo.setAdditionalPhone(viewHolderContact.phoneNumber2.getText().toString());
                            realm.commitTransaction();
                            notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        }

        @OnClick(R.id.visit_frequency)
        void onContectClick() {
            final String[] visit_type = context.getResources().getStringArray(R.array.visit_array);
            String currentValue = clientInfo.getVisitFrequency();
            int index = currentValue == null ? -1 : Arrays.asList(visit_type).indexOf(currentValue);
            final AlertDialog alertDialog = new AlertDialog.Builder(context)
                    .setTitle(context.getResources().getString(R.string.phone))
                    .setSingleChoiceItems(visit_type, index, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Realm realm = realmPlannerOperations.getRealm();
                            visitFrequency.setText(visit_type[i]);
                            realm.beginTransaction();
                            clientInfo.setVisitFrequency(visit_type[i]);
                            realm.commitTransaction();
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        }

        @OnClick(R.id.info)
        void onInfoClick() {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.dialog_planner_time, null);
            final ViewHolderTime viewHolderTime = new ViewHolderTime(linearLayout);
            viewHolderTime.setWorkFromDate(clientInfo.getWorkingBeginTime());
            viewHolderTime.setWorkToDate(clientInfo.getWorkingEndTime());
            viewHolderTime.setLunchFromDate(clientInfo.getLunchBeginTime());
            viewHolderTime.setLunchToDate(clientInfo.getLunchEndTime());

            final AlertDialog alertDialog = new AlertDialog.Builder(context)
                    .setTitle(context.getResources().getString(R.string.phone))
                    .setView(linearLayout)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Realm realm = realmPlannerOperations.getRealm();
                            realm.beginTransaction();
                            clientInfo.setWorkingBeginTime(viewHolderTime.getWorkFromDate());
                            clientInfo.setWorkingEndTime(viewHolderTime.getWorkToDate());
                            clientInfo.setLunchBeginTime(viewHolderTime.getLunchFromDate());
                            clientInfo.setLunchEndTime(viewHolderTime.getLunchToDate());
                            realm.commitTransaction();
                            notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        }

    }

    public class ViewHolderContact {
        @InjectView(R.id.phone_number)
        EditText phoneNumber;
        @InjectView(R.id.phone_number_2)
        EditText phoneNumber2;
        @InjectView(R.id.email)
        EditText email;

        ViewHolderContact(View view) {
            ButterKnife.inject(this, view);
        }
    }


    public class ViewHolderTime {

        @InjectView(R.id.work_from)
        TextView workFrom;
        @InjectView(R.id.work_to)
        TextView workTo;
        @InjectView(R.id.lunch_from)
        TextView lunchFrom;
        @InjectView(R.id.lunch_to)
        TextView lunchTo;

        private Date workFromDate;
        private Date workToDate;
        private Date lunchFromDate;
        private Date lunchToDate;

        ViewHolderTime(View view) {
            ButterKnife.inject(this, view);
        }

        public Date getWorkToDate() {
            return workToDate;
        }

        public void setWorkToDate(Date workToDate) {
            if (workToDate != null)
                this.workToDate = workToDate;
            else
                this.workToDate = new Date(0, 0, 0, 0, 0);
            workTo.setText(TimeUtils.timeFormat(workToDate));
        }

        public Date getWorkFromDate() {
            return workFromDate;
        }

        public void setWorkFromDate(Date workFromDate) {
            if (workFromDate != null)
                this.workFromDate = workFromDate;
            else
                this.workFromDate = new Date(0, 0, 0, 0, 0);
            workFrom.setText(TimeUtils.timeFormat(workFromDate));
        }

        public Date getLunchFromDate() {
            return lunchFromDate;
        }

        public void setLunchFromDate(Date lunchFromDate) {
            if (lunchFromDate != null)
                this.lunchFromDate = lunchFromDate;
            else
                this.lunchFromDate = new Date(0, 0, 0, 0, 0);
            lunchFrom.setText(TimeUtils.timeFormat(lunchFromDate));
        }

        public Date getLunchToDate() {
            return lunchToDate;
        }

        public void setLunchToDate(Date lunchToDate) {
            if (lunchToDate != null)
                this.lunchToDate = lunchToDate;
            else
                this.lunchToDate = new Date(0, 0, 0, 0, 0);
            lunchTo.setText(TimeUtils.timeFormat(lunchToDate));
        }

        @OnClick(R.id.work_from)
        void onWorkFromClick() {
            final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    setWorkFromDate(new Date(0, 0, 0, hourOfDay, minute));
                }
            }, workFromDate.getHours(), workFromDate.getMinutes(), true);
            timePickerDialog.show();
        }

        @OnClick(R.id.work_to)
        void onWorkToClick() {
            final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    setWorkToDate(new Date(0, 0, 0, hourOfDay, minute));
                }
            }, workToDate.getHours(), workToDate.getMinutes(), true);
            timePickerDialog.show();
        }

        @OnClick(R.id.lunch_from)
        void onLunchFromClick() {
            final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    setLunchFromDate(new Date(0, 0, 0, hourOfDay, minute));
                }
            }, lunchFromDate.getHours(), lunchFromDate.getMinutes(), true);
            timePickerDialog.show();
        }

        @OnClick(R.id.lunch_to)
        void onLunchToClick() {
            final TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    setLunchToDate(new Date(0, 0, 0, hourOfDay, minute));
                }
            }, lunchToDate.getHours(), lunchToDate.getMinutes(), true);
            timePickerDialog.show();
        }
    }
}

