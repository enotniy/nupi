package com.nupi.agent.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 19.11.15
 */
public class SelectClientDialog {

    @Inject
    RealmExchangeOperations realmExchangeOperations;
    private AlertDialog alertDialog;
    private Activity activity;

    // TODO: 29.12.15 show meteor clients
    public SelectClientDialog(Activity activity, String selectedClientId, final @NonNull OnClientSelectedListener listener) {
        this.activity = activity;
        ((NupiApp) activity.getApplication()).getAppComponent().inject(this);

        final RealmResults<CounterAgent> counterAgentsRealm = realmExchangeOperations.getCounterAgents(realmExchangeOperations.getRealm());

        final CounterAgent[] counterAgents = counterAgentsRealm.toArray(new CounterAgent[counterAgentsRealm.size()]);
        Arrays.sort(counterAgents, new Comparator<CounterAgent>() {
            @Override
            public int compare(CounterAgent ref_clients, CounterAgent t1) {
                return ref_clients.getLowerCaseName().compareToIgnoreCase(t1.getLowerCaseName());
            }
        });

        final List<String> client_names = new LinkedList<>();
        final List<CounterAgent> clients = new LinkedList<>();
        int selectedClientPosition = -1;
        int index = 0;
        for (CounterAgent ref_client : counterAgents) {
            if (!ref_client.isFolder()) {
                client_names.add(ref_client.getName());
                clients.add(ref_client);
                if (ref_client.getGuid().equals(selectedClientId))
                    selectedClientPosition = index;
                index++;
            }
        }

        alertDialog = new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.choose_client))
                .setSingleChoiceItems(client_names.toArray(new String[client_names.size()]), selectedClientPosition, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onClientSelected(clients.get(i));
                        dialogInterface.dismiss();
                    }
                })
                .setNeutralButton(R.string.cancel, null)
                .create();
    }

    public void addClearButton(DialogInterface.OnClickListener onClickListener) {

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, activity.getString(R.string.clear), onClickListener);
    }

    public void show() {
        alertDialog.show();
    }

    public interface OnClientSelectedListener {
        void onClientSelected(CounterAgent client);
    }
}
