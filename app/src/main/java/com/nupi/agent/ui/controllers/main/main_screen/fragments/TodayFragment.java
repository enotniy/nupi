package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.nupi.agent.R;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.main.main_screen.adapters.TodayAdapter;
import com.nupi.agent.utils.StringUtils;
import com.nupi.agent.utils.TimeUtils;

import butterknife.InjectView;
import butterknife.OnItemClick;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class TodayFragment extends NupiFragment {

    public static final int ONE_DAY = 1;
    private static final String SUMMARY_STRING = "%d: %s %s";
    @InjectView(R.id.listPayments)
    ListView listPayments;

    @InjectView(R.id.listOrders)
    ListView listOrders;

    @InjectView(R.id.ordersSummary)
    TextView ordersSummary;

    @InjectView(R.id.paymentsSummary)
    TextView paymentsSummary;

    TodayAdapter ordersAdapter;
    TodayAdapter paymentsAdapter;
    private RealmResults<RegistryEntry> todayRealisations;
    private RealmResults<RegistryEntry> todayPayments;

    private final BroadcastReceiver broadcastReceiverDateChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            initData();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_today, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initData();
    }

    private void initData() {
        performRealmQuery();

        final DataSetObserver dataSetObserver = getDataSetObserver();

        ordersAdapter = new TodayAdapter(getActivity(), R.layout.list_item_today_order, todayRealisations, true);
        paymentsAdapter = new TodayAdapter(getActivity(), R.layout.list_item_today_payment, todayPayments, true);

        ordersAdapter.registerDataSetObserver(dataSetObserver);
        paymentsAdapter.registerDataSetObserver(dataSetObserver);

        listOrders.setAdapter(ordersAdapter);
        listPayments.setAdapter(paymentsAdapter);

        updateSummery();
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_DATE_CHANGED);

        getActivity().registerReceiver(broadcastReceiverDateChange, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiverDateChange);
    }

    private void performRealmQuery() {
        todayRealisations = realmRegistryOperations.getRegistryEntries(getRegistryRealm(), TimeUtils.getPastDate(ONE_DAY), TimeUtils.getEndOfDay(), RegistryEntry.REALISATION, RegistryEntry.TODAY_STATUSES, true, null);
        todayPayments = realmRegistryOperations.getRegistryEntries(getRegistryRealm(), TimeUtils.getPastDate(ONE_DAY), TimeUtils.getEndOfDay(), RegistryEntry.PAYMENT, RegistryEntry.TODAY_STATUSES, true, null);
    }

    private DataSetObserver getDataSetObserver() {
        return new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if (isVisible()) {
                    updateSummery();
                }
            }
        };
    }

    private void updateSummery() {
        ordersSummary.setText(String.format(SUMMARY_STRING, todayRealisations.size(), getString(R.string.resultSum), StringUtils.getMoneyFormat(todayRealisations.sum("sum").floatValue())));
        paymentsSummary.setText(String.format(SUMMARY_STRING, todayPayments.size(), getString(R.string.resultSum), StringUtils.getMoneyFormat(todayPayments.sum("sum").floatValue())));
    }


    @OnItemClick(R.id.listOrders)
    void onEntrySelect(int item) {
        Answers.getInstance().logCustom(new CustomEvent("Item click, registry"));
        final RegistryEntry registryEntry = ordersAdapter.getItem(item);

        if (registryEntry.getEntryType() == RegistryEntry.REALISATION) {
            bus.post(new OpenRealisationReceiptEvent(registryEntry.getGuid()));
        }
    }
}
