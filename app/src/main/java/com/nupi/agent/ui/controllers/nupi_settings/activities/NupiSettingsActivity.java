package com.nupi.agent.ui.controllers.nupi_settings.activities;

import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.OpenSupervisorEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.nupi_settings.fragments.NupiSettingsFragment;
import com.nupi.agent.ui.controllers.nupi_settings.fragments.SupervisorFragment;
import com.squareup.otto.Subscribe;

public class NupiSettingsActivity extends NupiActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, new NupiSettingsFragment())
                    .commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }

    @Subscribe
    public void onOpenSupervisorEvent(OpenSupervisorEvent event) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new SupervisorFragment())
                .addToBackStack(null)
                .commit();
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }
}
