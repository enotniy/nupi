package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nupi.agent.R;
import com.nupi.agent.events.CounterAgentsButtonEvent;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.OpenNupiSettingsEvent;
import com.nupi.agent.events.OpenPlannerEvent;
import com.nupi.agent.events.SystemReportButtonEvent;
import com.nupi.agent.ui.base.NupiFragment;

import butterknife.OnClick;

/**
 * Created by User on 09.09.2015
 */
public class OtherFragment extends NupiFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_other, container, false);
    }

    @OnClick(R.id.nupi_settings)
    void onNupiSettingsClick() {
        sendButtonEvent("Nupi Settings", true);
        bus.post(new OpenNupiSettingsEvent());
    }

    @OnClick(R.id.planner)
    void onPlannerClick() {
        if (preferences.isLoggedIn()) {
            sendButtonEvent("Planner", true);
            bus.post(new OpenPlannerEvent());
        } else
            sendButtonEvent("Planner", true);
    }

    @OnClick(R.id.call_to_office)
    void onCallClick() {
        sendButtonEvent("Call Office", true);
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.install_dialer_alrert), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.counterAgents)
    void onCounterAgentsClick() {
        if (preferences.isLoggedIn()) {
            sendButtonEvent("Counteragents", true);
            bus.post(new CounterAgentsButtonEvent());
        } else
            sendButtonEvent("Counteragents", false);
    }


    @OnClick(R.id.registry)
    void onRegistryClick() {
        if (preferences.isLoggedIn()) {
            sendButtonEvent("Registry", true);
            bus.post(new SystemReportButtonEvent(false));
        } else
            sendButtonEvent("Registry", false);
    }

//    @OnClick(R.id.presenter)
//    void onPresenterClick() {
//        sendButtonEvent("Presenter", true);
//        if (agentPlusService.hasData())
//            bus.post(new PresenterButtonEvent());
//    }

    @OnClick(R.id.geolocation)
    void onGeolocationClick() {
        sendButtonEvent("Geolocation", true);
        if (preferences.isLoggedIn())
            bus.post(new OpenGeolocationEvent(OpenGeolocationEvent.OpenType.NONE));
    }

}
