package com.nupi.agent.ui.controllers.planner.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.nupi.agent.R;
import com.nupi.agent.database.models.planner.PlannerRoute;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.PlannerButtonHomeRouteEvent;
import com.nupi.agent.events.PlannerEditRouteEvent;
import com.nupi.agent.events.PlannerEvent;
import com.nupi.agent.events.VoiceSearchResultEvent;
import com.nupi.agent.helpers.VoiceSearchHelper;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.planner.adapters.PointsRouteAdapter;
import com.squareup.otto.Subscribe;

import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.realm.Realm;

public class PlannerRouteFragment extends NupiFragment {

    @InjectView(R.id.home_button)
    ImageView homeButton;
    @InjectView(R.id.plannerList)
    RecyclerView mRecyclerView;
    @InjectView(R.id.route_name)
    TextView routeName;
    @InjectView(R.id.route)
    LinearLayout routeFilter;
    @InjectView(R.id.show_path)
    TextView showPath;
    @InjectView(R.id.edit_path)
    TextView editPath;
    @InjectView(R.id.search_planner)
    RelativeLayout searchPlanner;
    @InjectView(R.id.title)
    LinearLayout title;
    @InjectView(R.id.no_search_panel)
    LinearLayout noSearchPanel;
    @InjectView(R.id.home_button_search)
    ImageView homeButtonSearch;
    @InjectView(R.id.close_search)
    ImageView closeSearch;
    @InjectView(R.id.search)
    EditText search;
    @InjectView(R.id.clear_search)
    ImageView clearSearch;
    @InjectView(R.id.voice)
    RelativeLayout voice;
    @InjectView(R.id.search_panel)
    LinearLayout searchPanel;
    @InjectView(R.id.container)
    RelativeLayout container;

    private RecyclerView.LayoutManager mLayoutManager;
    private PointsRouteAdapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewDragDropManager mRecyclerViewDragDropManager;
    private PlannerRoute route;
    private String routeId;
    private String query = "";

    private Realm plannerRealm;


    public static PlannerRouteFragment newInstance(String route) {
        PlannerRouteFragment receiptFragment = new PlannerRouteFragment();
        receiptFragment.setRoute(route);
        return receiptFragment;
    }

    public void setRoute(String guid) {
        this.routeId = guid;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        plannerRealm = realmPlannerOperations.getRealm();
        if (routeId != null)
            route = realmPlannerOperations.getRouteByGuid(plannerRealm, routeId);
        else
            route = realmPlannerOperations.getRouteByGuid(plannerRealm, selectedItemsHolder.getSelectedRouteGuid());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        plannerRealm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_planner_route, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        realmPlannerOperations.addClients(plannerRealm, realmExchangeOperations.getCounterAgents(getMeteorRealm()));
        setClientsAdapter();
        fillHeadline();
    }

    private void fillHeadline() {
        routeName.setText(route.getRouteName());
    }

    @Override
    public void onPause() {
//        mRecyclerViewDragDropManager.cancelDrag();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setClientsAdapter() {
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        if (preferences.isLoggedIn()) {
            final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(animator);

            mAdapter = new PointsRouteAdapter(getActivity(), realmPlannerOperations, route);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        if (mRecyclerViewDragDropManager != null) {
            mRecyclerViewDragDropManager.release();
            mRecyclerViewDragDropManager = null;
        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
            mWrappedAdapter = null;
        }
        mAdapter = null;
        mLayoutManager = null;
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        bus.post(new PlannerButtonHomeRouteEvent());
    }

    @OnClick(R.id.edit_path)
    void onEditPathPress() {
        sendButtonEvent("Edit route", true);
        bus.post(new PlannerEditRouteEvent(route));
    }

    @OnClick(R.id.show_path)
    void onShowParhPress() {
        sendButtonEvent("Show route", true);
        selectedItemsHolder.setSelectedRouteGuid(route.getGuid());
        bus.post(new OpenGeolocationEvent(OpenGeolocationEvent.OpenType.ROUTE));
    }

    @OnClick(R.id.route)
    void onRoutePress() {
        sendButtonEvent("Route filter", true);
        final List<PlannerRoute> plannerRoutes = realmPlannerOperations.getPlannerRoutes(plannerRealm);
        final List<String> routeNames = new LinkedList<>();
        routeNames.add(getString(R.string.all_shops));
        int index = 0;
        for (int i = 0; i < plannerRoutes.size(); i++)
            if (plannerRoutes.get(i).equals(route))
                index = i + 1;
        for (PlannerRoute plannerRoute : plannerRoutes) {
            routeNames.add(plannerRoute.getRouteName());
        }
        String[] arrayRouteNames = routeNames.toArray(new String[routeNames.size()]);
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.pick_route)
                .setSingleChoiceItems(arrayRouteNames, index, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i > 0) {
                            route = plannerRoutes.get(i - 1);
                            setClientsAdapter();
                            fillHeadline();
                        } else {
                            bus.post(new PlannerEvent());
                        }
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    @OnClick(R.id.search_planner)
    void onSearchPlannerPress() {
        sendButtonEvent("Search Planner", true);
        title.setVisibility(View.GONE);
        noSearchPanel.setVisibility(View.GONE);
        searchPanel.setVisibility(View.VISIBLE);
        search.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    @OnClick(R.id.clear_search)
    public void onClickClearSearch() {
        search.setText("");
    }

    @OnClick(R.id.close_search)
    public void onCloseSearchClick() {
        title.setVisibility(View.VISIBLE);
        noSearchPanel.setVisibility(View.VISIBLE);
        searchPanel.setVisibility(View.GONE);
        search.setText("");
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(search.getApplicationWindowToken(), 0);

    }

    @OnClick(R.id.home_button_search)
    public void onLogoCloseSearchClick() {
        onCloseSearchClick();
    }

    @OnClick(R.id.voice)
    public void onVoiceClick() {
        VoiceSearchHelper voiceSearchHelper = new VoiceSearchHelper(this, VOICE_SEARCH_REQUEST_CODE);
        voiceSearchHelper.runVoiceSearch();
    }

    @Subscribe
    public void onVoiceSearchResult(VoiceSearchResultEvent event) {
        final CharSequence searchString = event.getSearchString();
        search.setText(searchString);
        onSearchTextChanged(searchString);
    }

    @OnTextChanged(R.id.search)
    public void onSearchTextChanged(CharSequence text) {
        if (mAdapter != null && !text.toString().equals(query)) {
            query = text.toString();
            mAdapter.setTextFilter(query);
            mAdapter.notifyDataSetChanged();
        }
    }
}
