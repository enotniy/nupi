package com.nupi.agent.ui.controllers.nupi_settings.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.nupi.agent.ui.controllers.main.activities.MainActivity;


public class StarterActivity extends Activity {

    private static final int PERMISSIONS_CHECK = 1;

    private static final String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doRequestNupiPermissions();
    }


    private void doRequestNupiPermissions() {
        if (!doPermissionsCheck())
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_CHECK);
        else
            startMainActivity();
    }


    private boolean doPermissionsCheck() {
        for (String permission : PERMISSIONS)
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_CHECK) {
            boolean doWeStart = true;

            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    doWeStart = false;
                }
            }
            if (doWeStart) {
                startMainActivity();
            } else {
                finish();
            }
        }
    }


    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
