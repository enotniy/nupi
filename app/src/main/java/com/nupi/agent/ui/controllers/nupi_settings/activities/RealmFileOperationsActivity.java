package com.nupi.agent.ui.controllers.nupi_settings.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.nupi.agent.R;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.RealmPlannerOperations;
import com.nupi.agent.database.RealmRegistryOperations;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.main.activities.MainActivity;

import java.io.File;

import javax.inject.Inject;

public class RealmFileOperationsActivity extends NupiActivity {
    public static final String UPDATE_KEY = "UPDATE_KEY";
    @Inject
    RealmRegistryOperations realmRegistryOperations;
    @Inject
    RealmChatOperations realmChatOperations;
    @Inject
    RealmPlannerOperations realmPlannerOperations;
    @Inject
    RealmExchangeOperations realmExchangeOperations;

    public static File getRealmFile(Context context) {
        return new File(context.getFilesDir(), "default.realm");
    }

    public static File getBackupDir(Context context) {
        return new File(context.getCacheDir(), "realm_backup/");
    }

    public static File getBackupArchive(Context context) {
        return new File(context.getCacheDir(), "realm_backup/backup.zip");
    }

    public static File getExportRealm(Context context) {
        return new File(context.getCacheDir(), "realm_backup/export.realm");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
        setContentView(R.layout.activity_realm_file_operations);


        realmExchangeOperations.deleteDatabase();
        realmRegistryOperations.deleteDatabase();
        realmChatOperations.deleteDatabase();
        realmPlannerOperations.deleteDatabase();
        realmExchangeOperations.deleteDatabase();

        startActivity(new Intent(this, MainActivity.class));
        Log.d("Wipe Realm", "OK");
        finish();
    }

    @Override
    public void onBackPressed() {

    }
}
