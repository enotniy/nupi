package com.nupi.agent.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.nupi.agent.BuildConfig;
import com.nupi.agent.R;
import com.nupi.agent.network.nupi.models.Auth;
import com.nupi.agent.ui.controllers.nupi_settings.activities.NupiSettingsActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;

/**
 * Created by Pasenchuk Victor on 10.06.15
 */
public class AuthDialog {


    private final Activity activity;
    private ViewHolder viewHolder;
    private AlertDialog alertDialog;

    public AuthDialog(Activity activity) {
        this.activity = activity;
    }


    public Observable<Auth> show(final String title, final String login, final String password) {
        return Observable.create(new Observable.OnSubscribe<Auth>() {
            @Override
            public void call(final Subscriber<? super Auth> subscriber) {

                final View view = activity.getLayoutInflater().inflate(R.layout.dialog_auth, null);
                viewHolder = new ViewHolder(view);

                if (!TextUtils.isEmpty(login))
                    viewHolder.login.setText(login);
                if (!TextUtils.isEmpty(password))
                    viewHolder.password.setText(login);

                int neutralButtonTextId = BuildConfig.DEBUG ?
                        R.string.action_settings : R.string.demo;
                alertDialog = new AlertDialog.Builder(activity)
                        .setView(view)
                        .setTitle(title)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                subscriber.onNext(new Auth(
                                        viewHolder.login.getText().toString(),
                                        viewHolder.password.getText().toString()
                                ));
                                subscriber.onCompleted();
                            }
                        })
                        .setNeutralButton(neutralButtonTextId, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (BuildConfig.DEBUG)
                                    activity.startActivity(new Intent(activity, NupiSettingsActivity.class));
                                else {
                                    subscriber.onNext(new Auth(
                                            "demo",
                                            "demo"
                                    ));
                                }
                                subscriber.onCompleted();
                            }
                        })
                        .setCancelable(false)
                        .create();

                // cleaning up
                subscriber.add(Subscriptions.create(new Action0() {
                    @Override
                    public void call() {
                        alertDialog.dismiss();
                    }
                }));

                alertDialog.show();

            }
        });

    }

    public void dismiss() {
        if (alertDialog != null)
            alertDialog.dismiss();
    }

    public ViewHolder getViewHolder() {
        return viewHolder;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'dialog_auth.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    public static class ViewHolder {
        @InjectView(R.id.login)
        EditText login;
        @InjectView(R.id.password)
        EditText password;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

        public EditText getLogin() {
            return login;
        }

        public EditText getPassword() {
            return password;
        }
    }

}
