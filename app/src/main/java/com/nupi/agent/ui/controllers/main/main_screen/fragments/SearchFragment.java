package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nupi.agent.R;
import com.nupi.agent.events.PricesSearchStopEvent;
import com.nupi.agent.events.SearchStringEvent;
import com.nupi.agent.events.VoiceSearchResultEvent;
import com.nupi.agent.helpers.VoiceSearchHelper;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.keyboard.KeyboardActionListener;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class SearchFragment extends NupiFragment {

    @InjectView(R.id.searchField)
    EditText editTextSearch;

    @InjectView(R.id.keyboard_native)
    KeyboardView keyboardViewNative;

    @InjectView(R.id.keyboard_en)
    KeyboardView keyboardViewEn;

    @InjectView(R.id.keyboard_numeric)
    KeyboardView keyboardViewNumeric;

    public SearchFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextSearchSetup();

        initKeyboard();

        addTextChangedListener();
    }

    private void addTextChangedListener() {
        editTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // Когда, юзер изменяет текст он работает
                bus.post(new SearchStringEvent(cs));
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
    }


    private void editTextSearchSetup() {
        editTextSearch.setRawInputType(InputType.TYPE_CLASS_TEXT);
        editTextSearch.setTextIsSelectable(true);
    }

    private void initKeyboard() {
        keyboardViewNative.setKeyboard(new Keyboard(getActivity(), R.xml.keyboard_search_native));
        keyboardViewEn.setKeyboard(new Keyboard(getActivity(), R.xml.keyboard_search_en));
        keyboardViewNumeric.setKeyboard(new Keyboard(getActivity(), R.xml.keyboard_search_numeric));

        KeyboardActionListener keyboardActionListener = getKeyboardActionListener();

        keyboardViewNative.setOnKeyboardActionListener(keyboardActionListener);
        keyboardViewEn.setOnKeyboardActionListener(keyboardActionListener);
        keyboardViewNumeric.setOnKeyboardActionListener(keyboardActionListener);
    }


    private KeyboardActionListener getKeyboardActionListener() {
        return new KeyboardActionListener(editTextSearch) {
            @Override
            public void onEnterKey() {

            }

            @Override
            public void onCancelKey() {
                sendButtonEvent("Cancel Search", true);
                bus.post(new PricesSearchStopEvent());
            }

            @Override
            public void onVoiceSearchKey() {
                sendButtonEvent("Voice Search", true);
                VoiceSearchHelper voiceSearchHelper = new VoiceSearchHelper(SearchFragment.this, VOICE_SEARCH_REQUEST_CODE);
                voiceSearchHelper.runVoiceSearch();
            }

            @Override
            public void onLanguageEnKey() {
                sendButtonEvent("English Search", true);
                keyboardViewNative.setVisibility(View.GONE);
                keyboardViewEn.setVisibility(View.VISIBLE);
                keyboardViewNumeric.setVisibility(View.GONE);
            }

            @Override
            public void onLanguageNativeKey() {
                sendButtonEvent("Native Search", true);
                keyboardViewNative.setVisibility(View.VISIBLE);
                keyboardViewEn.setVisibility(View.GONE);
                keyboardViewNumeric.setVisibility(View.GONE);
            }

            @Override
            public void onLanguageNumericKey() {
                sendButtonEvent("Numeric Search", true);
                keyboardViewNative.setVisibility(View.GONE);
                keyboardViewEn.setVisibility(View.GONE);
                keyboardViewNumeric.setVisibility(View.VISIBLE);
            }
        };
    }

    @OnClick(R.id.searchClearButton)
    void onClearButtonClick() {
        sendButtonEvent("Clear Search", true);
        if (editTextSearch.getText().length() > 0) {
            editTextSearch.setText(editTextSearch.getText().subSequence(0, editTextSearch.getText().length() - 1));
            editTextSearch.setSelection(editTextSearch.getText().length());
        }
    }

    @OnLongClick(R.id.searchClearButton)
    boolean onClearButtonLongClick() {
        sendButtonEvent("Clear Search", true);
        editTextSearch.setText("");
        return false;
    }

    @Subscribe
    public void onVoiceSearchResult(VoiceSearchResultEvent event) {
        editTextSearch.setText(event.getSearchString());
    }
}
