package com.nupi.agent.ui.controllers.main.cash.fragments;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.events.ContractSelectedEvent;
import com.nupi.agent.events.OpenGalleryEvent;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.RealisationSelectedEvent;
import com.nupi.agent.events.TakePhotoEvent;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.main.cash.adapters.ContractAdapter;
import com.nupi.agent.utils.StringUtils;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class CashFragment extends NupiFragment implements RealmChangeListener {

    @InjectView(R.id.cashListView)
    RecyclerView realisationsListView;

    @InjectView(R.id.sumDebt)
    TextView sumDebtTextView;

    @InjectView(R.id.counterAgentCaption)
    TextView counterAgentCaptionTextView;


    @InjectView(R.id.credit_depth)
    TextView credit_depth;

    @InjectView(R.id.credit_depth_days)
    TextView credit_depth_days;

    //private CashAdapter cashAdapter;
    private ContractAdapter contractAdapter;

    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;

    public CashFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cash, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class);
        counterAgentCaptionTextView.setText(counterAgent != null ? counterAgent.getName() : getString(R.string.no_info));
        prepareData(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getMeteorRealm().addChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getMeteorRealm().removeChangeListener(this);
    }

    @Override
    public void onChange() {
        if (isVisible())
            setupSum();
    }

    public void prepareData(Bundle savedInstanceState) {
        setUpCashAdapter(savedInstanceState);
        setupSum();
    }

    private void setupSum() {
        sumDebtTextView.setText(StringUtils.getMoneyFormat(realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class).getDebt()));
    }

    private void setUpCashAdapter(Bundle savedInstanceState) {
        //RealmResults<Contract> contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
        //cashAdapter = new CashAdapter(getActivity(), realmExchangeOperations.getDebtsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid()), contract);
        //realisationsListView.setAdapter(cashAdapter);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        final Parcelable eimSavedState = (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);
        mRecyclerViewExpandableItemManager.setOnGroupExpandListener(new RecyclerViewExpandableItemManager.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition, boolean fromUser) {
                if (fromUser) {
                    int childItemHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.list_row_height);
                    int topMargin = (int) (getActivity().getResources().getDisplayMetrics().density * 16); // top-spacing: 16dp
                    final int bottomMargin = topMargin;
                    mRecyclerViewExpandableItemManager.scrollToGroup(groupPosition, childItemHeight, topMargin, bottomMargin);
                }
            }
        });
        mRecyclerViewExpandableItemManager.setOnGroupCollapseListener(new RecyclerViewExpandableItemManager.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition, boolean fromUser) {
            }
        });
        contractAdapter = new ContractAdapter(getActivity(), realmExchangeOperations.getCounterAgentGroupsByCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid()));

        RecyclerView.Adapter mWrappedAdapter = mRecyclerViewExpandableItemManager.createWrappedAdapter(contractAdapter);

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        animator.setSupportsChangeAnimations(false);

        realisationsListView.setLayoutManager(mLayoutManager);
        realisationsListView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        realisationsListView.setItemAnimator(animator);
        realisationsListView.setHasFixedSize(false);

        if (!supportsViewElevation()) {
            realisationsListView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
        }
        realisationsListView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));

        mRecyclerViewExpandableItemManager.attachRecyclerView(realisationsListView);
        mRecyclerViewExpandableItemManager.expandAll();

        RealmResults<Contract> contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
        if (contracts.size() == 1)
            contractAdapter.addSelectedContract(contracts.first().getGuid());
    }

    @Subscribe
    public void onRealisationSelected(RealisationSelectedEvent event) {
        final PaymentByOrder paymentByOrder = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), event.docId, PaymentByOrder.class);
        contractAdapter.addSelectedIndex(event.docId, paymentByOrder.getContract());
        if (contractAdapter.getCountSelectedDocs() == 0) {
            RealmResults<Contract> contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
            if (contracts.size() == 1)
                bus.post(new ContractSelectedEvent(contracts.first().getGuid()));
        }
        contractAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onContractSelected(ContractSelectedEvent event) {
        if (event.getContractId().equals(contractAdapter.getSelectedContract())) {
            RealmResults<Contract> contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
            if (contracts.size() != 1)
                contractAdapter.addSelectedContract(event.getContractId());
        } else
            contractAdapter.addSelectedContract(event.getContractId());
        contractAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.camera_button)
    void onCameraButton() {
        sendButtonEvent("Camera", true);
        bus.post(new TakePhotoEvent(realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class).getName()));
    }

    @OnClick(R.id.gallery_button)
    void onGalleryButton() {
        sendButtonEvent("Gallery", true);
        bus.post(new OpenGalleryEvent(realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), CounterAgent.class).getName()));
    }

    @OnClick(R.id.map_button)
    void onGeolocationButton() {
        sendButtonEvent("Geolocation", true);
        bus.post(new OpenGeolocationEvent(OpenGeolocationEvent.OpenType.POINT));
    }
}
