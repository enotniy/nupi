package com.nupi.agent.ui.base;

import android.widget.ImageView;

import com.nupi.agent.R;
import com.nupi.agent.ui.views.AutoResizeTextView;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public abstract class BaseNumericKeyboardFragment extends NupiFragment {

    static final int MAX_DIGIT_AFTER_DOT_STOCK = 3;
    static final int MAX_DIGIT_AFTER_DOT_MONEY = 2;
    static final int MAX_DIGIT_BEFORE_DOT = 6;
    public typeKeyboard type = typeKeyboard.STOCK;
    @InjectView(R.id.calcText)
    protected AutoResizeTextView calcText;
    @InjectView(R.id.labelCaption)
    protected AutoResizeTextView labelCaption;
    private int decimalDigit = 0;
    private int digit = 0;
    private boolean hasElderInput = true;

    @OnClick({R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9})
    public void numKey(ImageView button) {
        if (decimalDigit > 0)
            decimalDigit++;
        else
            digit++;

        final int maxDigit = type == typeKeyboard.STOCK ? MAX_DIGIT_AFTER_DOT_STOCK : MAX_DIGIT_AFTER_DOT_MONEY;

        if (decimalDigit <= maxDigit + 1 && (digit <= MAX_DIGIT_BEFORE_DOT || decimalDigit > 0)) {
            calcText.setText(calcText.getText().toString().equals("0") || hasElderInput ?
                    button.getTag().toString() : calcText.getText().toString() + button.getTag().toString());
            sendButtonEvent("Calc Number", true);
        } else
            sendButtonEvent("Calc Number", false);
        hasElderInput = false;
    }

    @OnClick(R.id.btnClear)
    public void onClearKey() {
        sendButtonEvent("Clear", true);
        calcText.setText("0");
        decimalDigit = 0;
        digit = 0;
    }

    @OnClick(R.id.btn_dot)
    public void onDotKey() {
        if (decimalDigit == 0) {
            sendButtonEvent("Dot", true);
            if (!hasElderInput)
                calcText.setText(calcText.getText().toString() + ".");
            else
                calcText.setText("0.");
            decimalDigit++;
            hasElderInput = false;
        } else
            sendButtonEvent("Dot", false);
    }

    public enum typeKeyboard {STOCK, MONEY}

}
