package com.nupi.agent.ui.dialogs;

/**
 * Created with IntelliJ IDEA.
 * User: pasencukviktor
 * Date: 17.03.14
 * Time: 23:59
 */
interface ComplexDialogDelegate {
    String getDialogMessage();

    String getPositiveButtonName();

    String getNegativeButtonName();

    String getNeutralButtonName();

    void handlePositiveButton();

    void handleNeutralButton();
}
