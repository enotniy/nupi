package com.nupi.agent.ui.controllers.analytics.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.application.models.Tree;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.enums.SelectionType;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class TreePickerPriceAdapter extends RecyclerView.Adapter<TreePickerPriceAdapter.ViewHolder> {

    public static final int TYPE_ELEMENT = 0;
    public static final int TYPE_FOLDER = 1;

    private Picker picker;
    private Tree<String> tree;
    private List<Nomenclature> prices;
    private Runnable onChangeAdapterListener;
    private Runnable onFolderClickListener;
    private String selectedItem;

    public TreePickerPriceAdapter(List<Nomenclature> prices, Tree<String> tree, Picker picker, Runnable onChangeAdapterListener, Runnable onFolderClickListener) {
        this.prices = prices;
        this.picker = picker;
        this.tree = tree;
        this.onChangeAdapterListener = onChangeAdapterListener;
        this.onFolderClickListener = onFolderClickListener;
    }

    private Nomenclature getItem(int position) {
        return prices.get(position);
    }

    @Override
    public int getItemCount() {
        return prices.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        int layout = 0;
        switch (viewType) {
            case TYPE_ELEMENT:
                layout = R.layout.list_item_picker_child;
                break;
            case TYPE_FOLDER:
                layout = R.layout.list_item_picker_group;
                break;
        }
        final View v = inflater.inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        final Nomenclature item = getItem(position);
        return item.isFolder() ? TYPE_FOLDER : TYPE_ELEMENT;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Nomenclature item = getItem(position);
        holder.name.setText(item.getName());
        holder.setElemId(item.getGuid());
        if (picker.getTypeSelection(item.getGuid()) == SelectionType.ALL)
            holder.checkbox.setImageResource(R.drawable.nupi_selection);
        else if (picker.getTypeSelection(item.getGuid()) == SelectionType.PARTIAL)
            holder.checkbox.setImageResource(R.drawable.nupi_selection_and_no_selection);
        else if (picker.getTypeSelection(item.getGuid()) == SelectionType.NONE)
            holder.checkbox.setImageResource(R.drawable.nupi_no_selection);
        else
            holder.checkbox.setImageResource(R.drawable.nupi_no_selection);
    }


    public Picker getPicker() {
        return picker;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.name)
        TextView name;
        @InjectView(R.id.checkbox)
        ImageView checkbox;
        @Optional
        @InjectView(R.id.picker)
        LinearLayout pickerLayout;


        private String elemId;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void setElemId(String elemId) {
            this.elemId = elemId;
        }

        @OnClick(R.id.checkbox)
        void onItemclick() {
            Tree<String> node = tree.getTree(elemId);
            if (picker.getTypeSelection(elemId) == SelectionType.ALL)
                picker.selectElement(node, SelectionType.NONE);
            else
                picker.selectElement(node, SelectionType.ALL);
            onChangeAdapterListener.run();
            notifyDataSetChanged();
        }

        @OnClick(R.id.picker)
        void onPickerclick() {
            selectedItem = elemId;
            onFolderClickListener.run();
            notifyDataSetChanged();
        }
    }

}

