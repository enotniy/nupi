package com.nupi.agent.ui.controllers.analytics.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.events.VoiceSearchResultEvent;
import com.nupi.agent.helpers.VoiceSearchHelper;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportClientAdapter;
import com.nupi.agent.ui.dialogs.CalendarPickerDialog;
import com.nupi.agent.ui.dialogs.SelectClientDialog;
import com.nupi.agent.utils.StringUtils;
import com.nupi.agent.utils.TimeUtils;
import com.squareup.otto.Subscribe;

import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by User on 15.10.2015
 */

public class ReportClientFragment extends NupiFragment {

    private final CalendarPickerDialog calendarPickerDialog = new CalendarPickerDialog();
    @InjectView(R.id.home_button)
    ImageView homeButton;
    @InjectView(R.id.calendarButtonText)
    TextView calendarButtonText;
    @InjectView(R.id.date_range)
    TextView dateRange;
    @InjectView(R.id.calendarButton)
    LinearLayout calendarButton;
    @InjectView(R.id.filterResultsText)
    TextView filterResultsText;
    @InjectView(R.id.filter_clients)
    LinearLayout filterClients;
    @InjectView(R.id.counterAgentsListView)
    RecyclerView counterAgentsListView;
    @InjectView(R.id.sumDebt)
    TextView sumDebt;
    @InjectView(R.id.searchButton)
    RelativeLayout searchButton;
    @InjectView(R.id.headline)
    TextView headline;
    @InjectView(R.id.close_search)
    ImageView closeSearch;
    @InjectView(R.id.search)
    EditText search;
    @InjectView(R.id.clear_search)
    ImageView clearSearch;
    @InjectView(R.id.search_panel)
    LinearLayout searchPanel;
    private ReportClientAdapter clientAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private Date fromDate = TimeUtils.getFirstDay();
    private Date toDate = TimeUtils.getEndOfDay();
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;
    private CounterAgent selectedClient;
    private boolean isSearch = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_client, container, false);
        ButterKnife.inject(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initStateExpandable(savedInstanceState);
        fillSum();
        fillFilter();
        fillDateLabels();
    }

    private void initStateExpandable(Bundle savedInstanceState) {
        final Parcelable eimSavedState = (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);
        mRecyclerViewExpandableItemManager.setOnGroupExpandListener(new RecyclerViewExpandableItemManager.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition, boolean fromUser) {
                if (fromUser) {
                    int childItemHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.list_row_height);
                    int topMargin = (int) (getActivity().getResources().getDisplayMetrics().density * 16); // top-spacing: 16dp
                    final int bottomMargin = topMargin; // bottom-spacing: 16dp
                    mRecyclerViewExpandableItemManager.scrollToGroup(groupPosition, childItemHeight, topMargin, bottomMargin);
                }
            }
        });
        clientAdapter = new ReportClientAdapter(getActivity(), getMeteorRealm(), fromDate, toDate);
        mWrappedAdapter = mRecyclerViewExpandableItemManager.createWrappedAdapter(clientAdapter);
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerViewExpandableItemManager.setOnGroupCollapseListener(new RecyclerViewExpandableItemManager.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition, boolean fromUser) {
            }
        });

        // additional decorations
        //noinspection StatementWithEmptyBody
        if (supportsViewElevation()) {
            // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
        } else {
            counterAgentsListView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
        }
        counterAgentsListView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
        mRecyclerViewExpandableItemManager.attachRecyclerView(counterAgentsListView);

        counterAgentsListView.setLayoutManager(mLayoutManager);
        counterAgentsListView.setAdapter(mWrappedAdapter);
        counterAgentsListView.setHasFixedSize(false);
    }

    private void initList() {
        clientAdapter.changeGroups(fromDate, toDate);
        fillSum();
    }


    @OnClick(R.id.filter_clients)
    void onClientFilterClick() {
        sendButtonEvent("Change Client", true);

        String selectedClientId = selectedClient != null ? selectedClient.getGuid() : "";

        final SelectClientDialog selectClientDialog = new SelectClientDialog(getActivity(), selectedClientId, new SelectClientDialog.OnClientSelectedListener() {
            @Override
            public void onClientSelected(CounterAgent client) {
                selectedClient = client;
                clientAdapter.setSelectedCounterAgent(selectedClient);
                fillSum();
                fillFilter();
            }
        });
        selectClientDialog.addClearButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedClient = null;
                clientAdapter.setSelectedCounterAgent(null);
                fillSum();
                fillFilter();
            }
        });
        selectClientDialog.show();
    }

    @OnClick(R.id.calendarButton)
    void onCalendarClick() {
        calendarPickerDialog.show(this, fromDate, toDate, new CalendarPickerDialog.OnDatesSelectedListener() {
            @Override
            public void onDatesSelected(Date newFromDate, Date newToDate) {
                fromDate = newFromDate;
                toDate = newToDate;
                initList();
                fillSum();
                fillDateLabels();
                fillFilter();
            }
        });
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }



    @OnTextChanged(R.id.search)
    public void onSearchTextChanged(CharSequence text) {
        if (clientAdapter != null) {
            clientAdapter.setFilterText(text.toString());
            fillSum();
            clientAdapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.searchButton)
    public void onSearchClick() {
        if (!isSearch) {
            headline.setVisibility(View.GONE);
            searchButton.setVisibility(View.GONE);
            searchPanel.setVisibility(View.VISIBLE);
            search.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    @OnClick(R.id.clear_search)
    public void onClickClearSearch() {
        search.setText("");
    }

    @OnClick(R.id.close_search)
    public void onCloswSearchClick() {
        if (!isSearch) {
            headline.setVisibility(View.VISIBLE);
            searchButton.setVisibility(View.VISIBLE);
            searchPanel.setVisibility(View.GONE);
            search.setText("");
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(search.getApplicationWindowToken(), 0);
        }
    }

    private void fillFilter() {
        filterResultsText.setTextColor(getResources().getColor(selectedClient != null ? R.color.text_yellow : R.color.text_button));
    }

    private void fillDateLabels() {
        if (TimeUtils.getFirstDay().equals(fromDate) && TimeUtils.getEndOfDay().equals(toDate)) {
            calendarButtonText.setTextColor(getResources().getColor(R.color.text_button));
            dateRange.setTextColor(getResources().getColor(R.color.text_button));
        } else {
            calendarButtonText.setTextColor(getResources().getColor(R.color.text_yellow));
            dateRange.setTextColor(getResources().getColor(R.color.text_yellow));
        }
        dateRange.setText(String.format(" %s - %s", TimeUtils.dateFormat(fromDate), TimeUtils.dateFormat(toDate)));
    }

    private void fillSum() {
        sumDebt.setText(StringUtils.getMoneyFormat(clientAdapter.getSum()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.voice)
    public void onVoiceClick() {
        VoiceSearchHelper voiceSearchHelper = new VoiceSearchHelper(this, VOICE_SEARCH_REQUEST_CODE);
        voiceSearchHelper.runVoiceSearch();
    }

    @Subscribe
    public void onVoiceSearchResult(VoiceSearchResultEvent event) {
        final CharSequence searchString = event.getSearchString();
        search.setText(searchString);
        onSearchTextChanged(searchString);
    }

}
