package com.nupi.agent.ui.controllers.analytics.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nupi.agent.R;
import com.nupi.agent.events.ReportAnalyticsButtonEvent;
import com.nupi.agent.ui.controllers.analytics.activities.ReportAnalyticsActivity;
import com.nupi.agent.ui.controllers.general.fragments.ButtonsFragment;
import com.nupi.agent.ui.controllers.general.fragments.SmallChatFragment;
import com.nupi.agent.utils.TimeUtils;

import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by User on 15.10.2015
 */

public class AnalyticsFragment extends BaseAnalyticsFragment {

    @InjectView(R.id.home_button)
    ImageView homeButton;
    @InjectView(R.id.lineChart)
    LinearLayout imageViewLineChart;
    @InjectView(R.id.barChart)
    LinearLayout imageViewBarChart;
    @InjectView(R.id.barChart_image)
    ImageView barChartImage;
    @InjectView(R.id.lineChart_image)
    ImageView lineChartImage;


    private boolean isBarData = true;

    @OnClick(R.id.lineChart)
    public void onLineChartClick() {
        if (isBarData) {
            isBarData = false;
            initBigChart(false);
            imageViewLineChart.setBackgroundColor(getResources().getColor(R.color.white));
            lineChartImage.setImageResource(R.drawable.nupi_analitic_line_active);
            imageViewBarChart.setBackgroundColor(getResources().getColor(R.color.background_app));
            barChartImage.setImageResource(R.drawable.nupi_analitic_bar_disactive);
        }
    }

    @OnClick(R.id.barChart)
    public void onBarChartClick() {
        if (!isBarData) {
            isBarData = true;
            initBigChart(true);
            imageViewLineChart.setBackgroundColor(getResources().getColor(R.color.background_app));
            lineChartImage.setImageResource(R.drawable.nupi_analitic_line_disactive);
            imageViewBarChart.setBackgroundColor(getResources().getColor(R.color.white));
            barChartImage.setImageResource(R.drawable.nupi_analitic_bar_active);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_analytics, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null)
            getChildFragmentManager().beginTransaction()
                    .add(R.id.two_buttons, new ButtonsFragment())
                    .add(R.id.bottomChat, new SmallChatFragment())
                    .commit();
        initBigChart(isBarData);
    }

    @Override
    public void onResume() {
        super.onResume();
        initBigChart(isBarData);
        getRegistryRealm().addChangeListener(this);
    }


    @Override
    public void onChange() {
        if (isVisible())
            initBigChart(isBarData);
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        getActivity().finish();
    }

    @OnClick(R.id.report_goods)
    void onReportGoodsClick() {
        sendButtonEvent("Report goods analytics", true);
        if (preferences.isLoggedIn())
            bus.post(new ReportAnalyticsButtonEvent(ReportAnalyticsActivity.REPORT_TYPE_GOODS));
    }

    @OnClick(R.id.report_client)
    void onReportClientsClick() {
        sendButtonEvent("Report client analytics", true);
        if (preferences.isLoggedIn())
            bus.post(new ReportAnalyticsButtonEvent(ReportAnalyticsActivity.REPORT_TYPE_CLIENTS));
    }

    private void initBigChart(boolean isBarData) {
        int weeks = 0;
        Calendar now = TimeUtils.getCalendarForAnalytic();
        Calendar date1 = TimeUtils.getCalendarForAnalytic();
        date1.add(Calendar.WEEK_OF_YEAR, -15);
        while (date1.compareTo(now) < 0) {
            weeks++;
            date1.add(Calendar.DAY_OF_YEAR, 7);
        }
        observedWeeks = weeks;
        initChart();
        if (isBarData)
            setBarData();
        else
            setLineData();
        setLabel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
