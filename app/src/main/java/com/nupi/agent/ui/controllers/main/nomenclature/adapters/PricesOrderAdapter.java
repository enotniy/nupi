package com.nupi.agent.ui.controllers.main.nomenclature.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.utils.StringUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class PricesOrderAdapter extends ArrayAdapter<OrderItem> {

    final Realm realm;
    @Inject
    RealmExchangeOperations realmExchangeOperations;

    public PricesOrderAdapter(Activity activity, List<OrderItem> prices, Realm realm) {
        super(activity, R.layout.list_item_order, prices);
        this.realm = realm;
        ((NupiApp) activity.getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {


        OrderItem item = getItem(position);

        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_order, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) view.getTag();

        viewHolder.goodNumber
                .setText(String.valueOf(position + 1));

        viewHolder.goodQuantity
                .setText(StringUtils.getStockFormat(item.getQuantity()));

        final Nomenclature nomenclature = realmExchangeOperations.getRealmObjectByGuid(realm, item.getNomenclature(), Nomenclature.class);
        if (nomenclature != null)
            viewHolder.goodName
                    .setText(nomenclature.getName());


        viewHolder.priceSum
                .setText(StringUtils.getMoneyFormat(item.getSum()));

        return view;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_order.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.goodNumber)
        TextView goodNumber;
        @InjectView(R.id.goodName)
        TextView goodName;
        @InjectView(R.id.good_quantity)
        TextView goodQuantity;
        @InjectView(R.id.priceSum)
        TextView priceSum;
        @InjectView(R.id.orderRow)
        LinearLayout orderRow;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

