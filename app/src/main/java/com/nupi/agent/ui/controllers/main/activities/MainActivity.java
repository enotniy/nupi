package com.nupi.agent.ui.controllers.main.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.events.AnalyticsButtonEvent;
import com.nupi.agent.events.ChangeNupiUserEvent;
import com.nupi.agent.events.ChatButtonEvent;
import com.nupi.agent.events.ClientDebtEvent;
import com.nupi.agent.events.ClientSelectedEvent;
import com.nupi.agent.events.CounterAgentSearchButtonPressedEvent;
import com.nupi.agent.events.CounterAgentsButtonEvent;
import com.nupi.agent.events.HomeButtonEvent;
import com.nupi.agent.events.OpenGalleryEvent;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.OpenNupiSettingsEvent;
import com.nupi.agent.events.OpenPlannerEvent;
import com.nupi.agent.events.OpenRealisationPreviewReceiptEvent;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.events.PresenterButtonEvent;
import com.nupi.agent.events.PricesSearchStartEvent;
import com.nupi.agent.events.PricesSearchStopEvent;
import com.nupi.agent.events.ReportAnalyticsButtonEvent;
import com.nupi.agent.events.SystemReportButtonEvent;
import com.nupi.agent.events.TakePhotoEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.helpers.CameraHandler;
import com.nupi.agent.network.upload.UploadService;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.analytics.activities.AnalyticsActivity;
import com.nupi.agent.ui.controllers.debt.activities.DebtsActivity;
import com.nupi.agent.ui.controllers.general.adapters.GalleryAdapter;
import com.nupi.agent.ui.controllers.geolocation.activities.GeolocationActivity;
import com.nupi.agent.ui.controllers.main.cash.fragments.CashAcceptorFragment;
import com.nupi.agent.ui.controllers.main.cash.fragments.CashFragment;
import com.nupi.agent.ui.controllers.main.main_screen.fragments.CounterAgentsFragment;
import com.nupi.agent.ui.controllers.main.main_screen.fragments.MainRightFragment;
import com.nupi.agent.ui.controllers.main.nomenclature.fragments.PricesRightFragment;
import com.nupi.agent.ui.controllers.main.nomenclature.fragments.PricesSearchFragment;
import com.nupi.agent.ui.controllers.main.nomenclature.fragments.PricesTreeFragment;
import com.nupi.agent.ui.controllers.nupi_settings.activities.LoginActivity;
import com.nupi.agent.ui.controllers.nupi_settings.activities.NupiSettingsActivity;
import com.nupi.agent.ui.controllers.planner.activities.PlannerActivity;
import com.nupi.agent.ui.controllers.presenter.activities.PresenterActivity;
import com.nupi.agent.ui.controllers.receipt.activities.ReceiptPreviewActivity;
import com.nupi.agent.ui.controllers.registry.activities.RegistryActivity;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.ui.dialogs.MessageBoxWithAction;
import com.nupi.agent.ui.dialogs.YesNoDialog;
import com.nupi.agent.ui.views.TouchImageView;
import com.nupi.agent.utils.BitmapUtils;
import com.nupi.agent.utils.ExternalFilesUtils;
import com.nupi.agent.utils.PlayMarketUtils;
import com.skobbler.ngx.SKPrepareMapTextureListener;
import com.skobbler.ngx.SKPrepareMapTextureThread;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class MainActivity extends NupiActivity {

    private static final String USER_COMMENT = "UserComment";

    private static final String STACK_CLIENT = "STACK_CLIENT";
    private static final String STACK_CLIENTS_DEBT = "STACK_CLIENTS_DEBT";
    private static final String STACK_CLIENTS_SEARCH = "STACK_CLIENTS_SEARCH";


    @Inject
    UploadService uploadService;


    @Inject
    NupiPreferences sharedPreferences;

    private CameraHandler cameraHandler;
    private String lastImage;
    private AlertDialog galleryDialog, photoPreviewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getNupiApp().getAppComponent().inject(this);

        checkGooglePlayServices();
        if (sharedPreferences.isLoggedIn())
            uploadService.sendDataToServer();

        cameraHandler = new CameraHandler(this);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null)
            getFragmentManager().beginTransaction()
                    .add(R.id.leftContainer, new CounterAgentsFragment())
                    .add(R.id.rightContainer, new MainRightFragment())
                    .commit();
    }

    private void nupiLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!sharedPreferences.isLoggedIn())
            nupiLogin();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == CameraHandler.REQUEST_IMAGE_CAPTURE) && (resultCode == Activity.RESULT_OK)) {
            Toast.makeText(this, String.format("%s\n%s", getString(R.string.photo_saved), lastImage), Toast.LENGTH_SHORT).show();
            cameraHandler.addToGallery(lastImage);
        }
    }

    @Override
    protected void onPause() {
        if (galleryDialog != null) {
            galleryDialog.dismiss();
        }
        if (photoPreviewDialog != null) {
            photoPreviewDialog.dismiss();
        }
        super.onPause();
    }

    @Subscribe
    public void onClientSelectedEvent(ClientSelectedEvent event) {
        getFragmentManager().beginTransaction()
                .replace(R.id.leftContainer, new PricesTreeFragment())
                .replace(R.id.rightContainer, new PricesRightFragment())
                .addToBackStack(STACK_CLIENT)
                .commit();
    }

    @Subscribe
    public void onPriceSearchStartEvent(PricesSearchStartEvent event) {
        getFragmentManager().beginTransaction()
                .replace(R.id.leftContainer, new PricesSearchFragment())
                .addToBackStack(STACK_CLIENTS_SEARCH)
                .commit();
    }

    @Subscribe
    public void onPriceSearchStopEvent(PricesSearchStopEvent event) {
        getFragmentManager().popBackStack(STACK_CLIENTS_SEARCH, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        bus.post(new CounterAgentSearchButtonPressedEvent());
    }


    @Subscribe
    public void onHomeButtonPressedEvent(HomeButtonEvent event) {
        FragmentManager fm = getFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Subscribe
    public void onClientDebtPressedEvent(ClientDebtEvent event) {
        getFragmentManager().beginTransaction()
                .replace(R.id.leftContainer, new CashFragment())
                .replace(R.id.rightContainer, new CashAcceptorFragment())
                .addToBackStack(STACK_CLIENTS_DEBT)
                .commit();
    }

    @Subscribe
    public void onCounterAgentsButtonPressedEvent(CounterAgentsButtonEvent event) {
        startActivity(new Intent(this, DebtsActivity.class));
    }

    @Subscribe
    public void onSystemReportButtonEvent(SystemReportButtonEvent event) {
        Intent intent = new Intent(this, RegistryActivity.class);
        intent.putExtra(IN_THE_POCKET_FILTER, event.getInThePocket());
        startActivity(intent);
    }

    @Override
    @Subscribe
    public void onChatButtonEvent(ChatButtonEvent event) {
        super.onChatButtonEvent(event);
    }

    @Subscribe
    public void onPresenterButtonEvent(PresenterButtonEvent event) {
        Log.d("Presenter", "Presenter");
        startActivity(new Intent(this, PresenterActivity.class));
    }

    @Subscribe
    public void onSystemAnalitycsButtonEvent(AnalyticsButtonEvent event) {
        startActivity(new Intent(this, AnalyticsActivity.class));
    }

    @Subscribe
    public void onNupiSettingsButtonEvent(OpenNupiSettingsEvent event) {
        startActivity(new Intent(this, NupiSettingsActivity.class));
    }

    @Subscribe
    public void onPlannerButtonEvent(OpenPlannerEvent event) {
        startActivity(new Intent(this, PlannerActivity.class));
    }

    @Subscribe
    public void onGeolocationButtonEvent(OpenGeolocationEvent event) {
        final OpenGeolocationEvent.OpenType openType = event.getOpenType();
        if (sharedPreferences.isPlayServicesAvailable()) {
            showProgressDialog();
            ExternalFilesUtils externalFilesUtils = new ExternalFilesUtils(this, "Maps");
            File mapFilesDir = externalFilesUtils.GetFilesDir();
            if (externalFilesUtils.isExternalStorageWritable()) {
                try {
                    if (!copyMapFromAssetsToSD(mapFilesDir, "SKMaps.zip", externalFilesUtils.GetFreeSpace(ExternalFilesUtils.SIZE_MB))) {
                        dismissProgressDialog();
                        MessageBox.show("Not enough free space on external storage", this);
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                new SKPrepareMapTextureThread(this, mapFilesDir.toString(), "SKMaps.zip", new SKPrepareMapTextureListener() {
                    @Override
                    public void onMapTexturesPrepared(boolean b) {
                        Intent intent = new Intent(getApplicationContext(), GeolocationActivity.class);
                        final String extra;
                        if (openType == OpenGeolocationEvent.OpenType.ROUTE)
                            extra = GeolocationActivity.START_GEOLOCATION_ROUTE;
                        else if (openType == OpenGeolocationEvent.OpenType.POINT)
                            extra = GeolocationActivity.START_GEOLOCATION_POINT;
                        else
                            extra = GeolocationActivity.START_GEOLOCATION_NONE;

                        intent.putExtra(GeolocationActivity.START_GEOLOCATION_TYPE, extra);
                        startActivity(intent);
                    }
                }).start();

            }
        } else
            Toast.makeText(this, getString(R.string.this_func_requires_google_play_services), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

    @Subscribe
    public void onChangeNupiUserEvent(ChangeNupiUserEvent event) {
        nupiLogin();
    }

    @Subscribe
    public void onTakePhotoEvent(TakePhotoEvent event) {
        lastImage = cameraHandler.openCamera(event.clientName);
    }


    @Subscribe
    public void onGalleryButton(OpenGalleryEvent event) {


        try {
            final GridView gridView = (GridView) getLayoutInflater().inflate(R.layout.dialog_grid_gallery, null);
            final GalleryAdapter galleryAdapter = new GalleryAdapter(this, event.clientName);
            gridView.setAdapter(galleryAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    createPreviewDialog(i, galleryAdapter);
                }

            });


            galleryDialog = new AlertDialog.Builder(this)
                    .setTitle(event.clientName)
                    .setView(gridView)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create();
            galleryDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            MessageBox.show(getString(R.string.external_memory_access_error), this);
        }
    }

    private void createPreviewDialog(final int startPos, final GalleryAdapter galleryAdapter) {
        photoPreviewDialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle(galleryAdapter.getItem(startPos).getName())
                .create();
        View photoPreview = getLayoutInflater().inflate(R.layout.dialog_photo_preview, null);

        final int[] finalPosition = {startPos};
        final TextView commentView = ButterKnife.findById(photoPreview, R.id.commentField);
        final ViewPager imagePager = ButterKnife.findById(photoPreview, R.id.imagePager);

        imagePager.setAdapter(getImagesPagerAdapter(galleryAdapter));

        imagePager.setOnPageChangeListener(getImagesPagerListener(galleryAdapter, finalPosition, commentView));

        imagePager.setCurrentItem(startPos);
        changePage(startPos, galleryAdapter, commentView);

        initOPenInGalleryButton(galleryAdapter, photoPreview, finalPosition);

        initShareButton(galleryAdapter, photoPreview, finalPosition);

        initDeleteButton(galleryAdapter, photoPreview, finalPosition);

        initCommentButton(photoPreview, commentView, galleryAdapter, finalPosition);

        initBackButton(photoPreview);

        photoPreviewDialog.setView(photoPreview);
        photoPreviewDialog.show();
    }

    @NonNull
    private PagerAdapter getImagesPagerAdapter(final GalleryAdapter galleryAdapter) {
        return new PagerAdapter() {
            @Override
            public int getCount() {
                return galleryAdapter.getCount();
            }

            @Override
            public View instantiateItem(final ViewGroup container, final int position) {
                final TouchImageView img = new TouchImageView(container.getContext());

                img.setImageResource(R.drawable.placeholder);

                img.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bmp = BitmapUtils.getBitmapThumbnail(galleryAdapter.getItem(position));
                        img.setImageBitmap(bmp);
                        img.setTag(bmp);
                        Log.d("Bitmap: alloc", String.valueOf(position));
                    }
                });
                container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                return img;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                ((Bitmap) ((View) object).getTag()).recycle();
                Log.d("Bitmap: recycle", String.valueOf(position));
                container.removeView((View) object);
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }
        };
    }

    @NonNull
    private ViewPager.OnPageChangeListener getImagesPagerListener(final GalleryAdapter galleryAdapter, final int[] finalPosition, final TextView commentView) {
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                finalPosition[0] = position;
                changePage(position, galleryAdapter, commentView);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    private void changePage(int position, GalleryAdapter galleryAdapter, TextView commentView) {
        photoPreviewDialog.setTitle(galleryAdapter.getItem(position).getName());
        try {
            ExifInterface exifInterface = new ExifInterface(galleryAdapter.getItem(position).getPath());

            if (exifInterface.getAttribute(USER_COMMENT) != null) {
                commentView.setText(new String(Base64.decode(exifInterface.getAttribute(USER_COMMENT), Base64.DEFAULT), "UTF-8"));
                commentView.setVisibility(View.VISIBLE);
            } else {
                commentView.setText(null);
                commentView.setVisibility(View.GONE);
            }

        } catch (Exception ignored) {
            commentView.setVisibility(View.GONE);
        }
    }


    private void initOPenInGalleryButton(final GalleryAdapter galleryAdapter, View photoPreview, final int finalPosition[]) {
        final Uri data = Uri.fromFile(galleryAdapter.getItem(finalPosition[0]));
        ButterKnife.findById(photoPreview, R.id.galleryBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(data, "image/*");
                if (intent.resolveActivity(getPackageManager()) != null)
                    startActivity(intent);
                else {
                    MessageBox.show(getString(R.string.noGallery), MainActivity.this);
                }
                photoPreviewDialog.dismiss();
                if (galleryDialog != null) {
                    galleryDialog.dismiss();
                }
            }
        });
    }


    private void initShareButton(final GalleryAdapter galleryAdapter, View photoPreview, final int finalPosition[]) {
        final String path = galleryAdapter.getItem(finalPosition[0]).getPath();
        ButterKnife.findById(photoPreview, R.id.shareBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, path);
                shareIntent.setType("image/*");
                if (shareIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.sendPhoto)));
                else {
                    MessageBox.show(getString(R.string.noGallery), MainActivity.this);
                }
                photoPreviewDialog.dismiss();
            }
        });
    }

    private void initDeleteButton(final GalleryAdapter galleryAdapter, View photoPreview, final int[] finalPosition) {
        ButterKnife.findById(photoPreview, R.id.deleteBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new YesNoDialog(MainActivity.this, getString(R.string.confirmDelete)) {
                    @Override
                    public void handlePositiveButton() {
                        if (galleryAdapter.getItem(finalPosition[0]).delete())
                            galleryAdapter.removeElement(galleryAdapter.getItem(finalPosition[0]));
                        else
                            MessageBox.show(getString(R.string.cantDelete), MainActivity.this);
                        photoPreviewDialog.dismiss();
                    }
                }.show();
            }
        });
    }

    private void initCommentButton(View photoPreview, final TextView commentView, final GalleryAdapter galleryAdapter, final int[] position) {
        ButterKnife.findById(photoPreview, R.id.commentBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    final ExifInterface exifInterface = new ExifInterface(galleryAdapter.getItem(position[0]).getPath());
                    final EditText input = new EditText(MainActivity.this);
                    if (exifInterface.getAttribute(USER_COMMENT) != null)
                        input.setText(new String(Base64.decode(exifInterface.getAttribute(USER_COMMENT), Base64.DEFAULT), "UTF-8"));

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle(getString(R.string.write_comment_photo))
                            .setView(input)
                            .setPositiveButton(getString(R.string.write), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                    try {
                                        exifInterface.setAttribute(USER_COMMENT, Base64.encodeToString(input.getText().toString().getBytes("UTF-8"), Base64.DEFAULT));
                                        exifInterface.saveAttributes();
                                        commentView.setText(new String(Base64.decode(exifInterface.getAttribute(USER_COMMENT), Base64.DEFAULT), "UTF-8"));
                                        commentView.setVisibility(View.VISIBLE);
                                    } catch (Exception ignored) {
                                        MessageBox.show(getString(R.string.cantCommentExif), MainActivity.this);
                                    }
                                }
                            }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Do nothing.
                        }
                    }).show();
                } catch (Exception ignored) {
                    Toast.makeText(MainActivity.this, R.string.loading_wait, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initBackButton(View photoPreview) {
        ButterKnife.findById(photoPreview, R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoPreviewDialog.dismiss();
            }
        });
    }


    private boolean checkGooglePlayServices() {

        int checkResult = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        switch (checkResult) {
            case ConnectionResult.SUCCESS:
            case ConnectionResult.SERVICE_UPDATING:
                sharedPreferences.setIsPlayServicesAvailable(true);
                return true;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                noPlayServicesAlertWithCancel();
                sharedPreferences.setIsPlayServicesAvailable(true);
                return true;
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.SERVICE_MISSING:
                noPlayServicesAlertWithCancel();
                sharedPreferences.setIsPlayServicesAvailable(false);
                return false;
            default:
                sharedPreferences.setIsPlayServicesAvailable(false);
                return false;
        }

        // TODO: 26.11.15 Check other statuses:

//        public static final int SUCCESS = 0;
//        public static final int SERVICE_MISSING = 1;
//        public static final int SERVICE_VERSION_UPDATE_REQUIRED = 2;
//        public static final int SERVICE_DISABLED = 3;
//        public static final int SIGN_IN_REQUIRED = 4;
//        public static final int INVALID_ACCOUNT = 5;
//        public static final int RESOLUTION_REQUIRED = 6;
//        public static final int NETWORK_ERROR = 7;
//        public static final int INTERNAL_ERROR = 8;
//        public static final int SERVICE_INVALID = 9;
//        public static final int DEVELOPER_ERROR = 10;
//        public static final int LICENSE_CHECK_FAILED = 11;
//        public static final int CANCELED = 13;
//        public static final int TIMEOUT = 14;
//        public static final int INTERRUPTED = 15;
//        public static final int API_UNAVAILABLE = 16;
//        public static final int SIGN_IN_FAILED = 17;
//        public static final int SERVICE_UPDATING = 18;
//        public static final int SERVICE_MISSING_PERMISSION = 19;
//        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS;
    }


    private void noPlayServicesAlertWithCancel() {

        YesNoDialog yesNoDialog = new YesNoDialog(this, getString(R.string.no_google_play_services_alert)) {
            @Override
            public void handlePositiveButton() {
                PlayMarketUtils.openPlayMarket(MainActivity.this, "com.google.android.gms");
            }
        };
        yesNoDialog.setPositiveButtonName(getString(R.string.install));
        yesNoDialog.setNegativeButtonName(getString(R.string.cancel));
        yesNoDialog.show();
    }

    private void noPlayServicesAlert() {
        MessageBoxWithAction messageBoxWithAction = new MessageBoxWithAction() {
            @Override
            public void onOKButtonClick() {
                PlayMarketUtils.openPlayMarket(MainActivity.this, "com.google.android.gms");
            }
        };
        messageBoxWithAction.show(getString(R.string.no_google_play_services_alert), this);
    }

    @Override
    @Subscribe
    public void onReportAnalyticsButton(ReportAnalyticsButtonEvent event) {
        super.onReportAnalyticsButton(event);
    }

    @Override
    @Subscribe
    public void onRealisationReceiptEvent(OpenRealisationReceiptEvent event) {
        super.onRealisationReceiptEvent(event);
    }

    @Subscribe
    public void onOpenRealisationPreviewReceiptEvent(OpenRealisationPreviewReceiptEvent event) {
        Intent intent = new Intent(this, ReceiptPreviewActivity.class);
        startActivity(intent);
    }

    private boolean copyMapFromAssetsToSD(File sdMapsFolder, String mapsFileName, float sdFreeSpaceAvail) throws IOException {
        int FREE_SPACE_TO_ALLOW = 150;
        String toPath = sdMapsFolder + File.separator;
        File isInstalled = new File(toPath + ".installed.txt");
        if (isInstalled.exists())
            return true;
        if (sdFreeSpaceAvail < FREE_SPACE_TO_ALLOW)
            return false;
//        new File(toPath).mkdirs();
        AssetManager assetManager = getAssets();
        InputStream in = assetManager.open(mapsFileName);
        OutputStream out = new FileOutputStream(toPath + mapsFileName);

        byte[] buffer = new byte[1024];
        int read;

        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        return true;
    }
}

