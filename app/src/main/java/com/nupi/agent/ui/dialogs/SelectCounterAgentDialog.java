package com.nupi.agent.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 19.11.15
 */
public class SelectCounterAgentDialog {

    @Inject
    RealmExchangeOperations realmExchangeOperations;
    private AlertDialog alertDialog;
    private Activity activity;

    // TODO: 29.12.15 show meteor clients
    public SelectCounterAgentDialog(Activity activity, Realm realm, String selectedClientGuid, final @NonNull OnClientSelectedListener listener) {
        this.activity = activity;
        ((NupiApp) activity.getApplication()).getAppComponent().inject(this);

        final RealmResults<CounterAgent> counterAgents = realmExchangeOperations.getCounterAgents(realm);

        final List<String> counterAgentNames = new LinkedList<>();
        final List<CounterAgent> counterAgentList = new LinkedList<>();
        int selectedClientPosition = -1;
        int index = 0;
        for (CounterAgent counterAgent : counterAgents) {
            if (!counterAgent.isFolder()) {
                counterAgentNames.add(counterAgent.getName());
                counterAgentList.add(counterAgent);
                if (counterAgent.getGuid().equals(selectedClientGuid))
                    selectedClientPosition = index;
                index++;
            }
        }

        alertDialog = new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.choose_client))
                .setSingleChoiceItems(counterAgentNames.toArray(new String[counterAgentNames.size()]), selectedClientPosition, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onClientSelected(counterAgentList.get(i));
                        dialogInterface.dismiss();
                    }
                })
                .setNeutralButton(R.string.cancel, null)
                .create();
    }

    public AlertDialog show() {
        alertDialog.show();
        return alertDialog;
    }

    public interface OnClientSelectedListener {
        void onClientSelected(CounterAgent client);
    }
}
