package com.nupi.agent.ui.controllers.main.nomenclature.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.application.service.SelectedItemsHolder;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.enums.TreeElementType;
import com.nupi.agent.events.DismissNumPadEvent;
import com.nupi.agent.events.ShowPriceNumPadEvent;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.utils.HighlighterUtils;
import com.nupi.agent.utils.StringUtils;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 16.10.14
 */
public class NomenclatureSearchMeteorAdapter extends AbstractExpandableItemAdapter<NomenclatureSearchMeteorAdapter.NomenclatureSearchGroupViewHolder, NomenclatureSearchMeteorAdapter.NomenclatureViewHolder> {

    private final RealmChangeListener listener;
    protected SelectedItemsHolder selectedItemsHolder;
    protected OrderApi orderApi;
    @Inject
    RealmExchangeOperations realmExchangeOperations;
    @Inject
    Bus bus;
    private Realm realm;

    private long selectedFolder = -1;
    private long selectedItem = -1;

    private String searchQuery = "";
    private List<NomenclatureSearchGroup> nomenclatureSearchGroups;

    public NomenclatureSearchMeteorAdapter(Activity activity, Realm realm, OrderApi orderApi, SelectedItemsHolder selectedItemsHolder) {
        this.realm = realm;
        this.selectedItemsHolder = selectedItemsHolder;
        this.orderApi = orderApi;
        listener = new RealmChangeListener() {
            @Override
            public void onChange() {
                initData(searchQuery);
            }
        };
        realm.addChangeListener(listener);
        ((NupiApp) activity.getApplication()).getAppComponent().inject(this);
        initData(null);
        setHasStableIds(true);
    }

    private void initData(String query) {
        final boolean emptyQuery = TextUtils.isEmpty(query);
        final RealmResults<Nomenclature> nonFolderNomenclature = emptyQuery ?
                realmExchangeOperations.getNonFolderNomenclature(realm) :
                realmExchangeOperations.getNomenclatureFiltered(realm, query, TreeElementType.LEAF);
        HashMap<String, Boolean> folderGuids = new HashMap<>();
        if (!emptyQuery)
            buildSearchResults(realmExchangeOperations.getNomenclatureFiltered(realm, query, TreeElementType.NODE), folderGuids);
        for (Nomenclature nomenclature : nonFolderNomenclature)
            if (!folderGuids.containsKey(nomenclature.getFolderId()))
                folderGuids.put(nomenclature.getFolderId(), false);
        nomenclatureSearchGroups = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : folderGuids.entrySet())
            nomenclatureSearchGroups.add(new NomenclatureSearchGroup(entry.getKey(), entry.getValue()));
        Collections.sort(nomenclatureSearchGroups, new Comparator<NomenclatureSearchGroup>() {
            @Override
            public int compare(NomenclatureSearchGroup lhs, NomenclatureSearchGroup rhs) {
                return lhs.path.compareTo(rhs.path);
            }
        });
        notifyDataSetChanged();
    }

    private void buildSearchResults(RealmResults<Nomenclature> nomenclatureRealmResults, HashMap<String, Boolean> folderGuids) {
        for (Nomenclature nomenclature : nomenclatureRealmResults)
            if (!nomenclature.isFolder())
                folderGuids.put(nomenclature.getFolderId(), true);
            else
                buildSearchResults(realmExchangeOperations.getNomenclatureChildren(realm, nomenclature.getGuid()), folderGuids);
    }

    @Override
    public void onBindGroupViewHolder(NomenclatureSearchGroupViewHolder holder, int groupPosition, int viewType) {
        // child item
        final NomenclatureSearchGroup item = nomenclatureSearchGroups.get(groupPosition);
        // mark as clickable
        holder.itemView.setClickable(true);

        HighlighterUtils.highlightSearchString(item.path + item.name, holder.goodName, searchQuery, Color.BLUE);

        // set background resource (target view ID: container)
        final int expandState = holder.getExpandStateFlags();

        if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0)
            holder.goodsRow.setBackgroundResource((expandState & ExpandableItemConstants.STATE_FLAG_IS_EXPANDED) != 0 ?
                    R.drawable.selector_list_item :
                    R.drawable.selector_list_item);
    }

    @Override
    public void onBindChildViewHolder(NomenclatureViewHolder holder, int groupPosition, int childPosition, int viewType) {
        final NomenclatureSearchGroup groupItem = nomenclatureSearchGroups.get(groupPosition);
        Nomenclature nomenclature = getChild(groupPosition, childPosition);

        holder.setFolder(groupPosition);
        holder.setItem(childPosition);
        holder.setNomenclatureGuid(nomenclature.getGuid());
        holder.goodName.setText(getChild(groupPosition, childPosition).getName());
        Double price = groupItem.getPrice(childPosition);
        if (price != null)
            holder.goodCost.setText(StringUtils.getMoneyFormat(price));
        else
            holder.goodCost.setText(StringUtils.getMoneyFormat(0f));
        Double stock = groupItem.getStock(childPosition);
        if (stock != null)
            holder.goodQuantity.setText(StringUtils.getStockFormat(stock));
        else
            holder.goodQuantity.setText(StringUtils.getStockFormat(0f));
        HighlighterUtils.highlightSearchString(getChild(groupPosition, childPosition).getName(), holder.goodName, searchQuery, Color.BLUE);
        if (selectedFolder == groupPosition && selectedItem == childPosition)
            holder.goodsRow.setBackgroundResource(R.color.background_list_selected_pressed);
        else
            holder.goodsRow.setBackgroundResource(R.drawable.selector_list_item_selected);

        OrderItem orderItem = orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedCounterAgentGuid()).getGood(nomenclature.getGuid());
        if (orderItem != null)
            holder.goodInOrder.setText(StringUtils.getStockFormat(orderItem.getQuantity()));
        else
            holder.goodInOrder.setText("");
    }

    private Nomenclature getChild(int groupPosition, int childPosition) {
        return nomenclatureSearchGroups.get(groupPosition).children.get(childPosition);
    }


    @Override
    public int getGroupCount() {
        return nomenclatureSearchGroups.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return nomenclatureSearchGroups.get(groupPosition).children.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return nomenclatureSearchGroups.get(groupPosition).guid.hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).getGuid().hashCode();
    }

    public String getChildGuid(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).getGuid();
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public NomenclatureSearchGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_good_folder, parent, false);
        return new NomenclatureSearchGroupViewHolder(v);
    }

    @Override
    public NomenclatureViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_good, parent, false);
        return new NomenclatureViewHolder(v);
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(NomenclatureSearchGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        // check the item is *not* pinned
        return nomenclatureSearchGroups.get(groupPosition).isPinned() && holder.itemView.isEnabled() && holder.itemView.isClickable();
    }

    public void setFilterString(String value) {
        if (!searchQuery.equals(value)) {
            searchQuery = value;
            initData(value);
        }
    }

    public boolean setSelectedItem(long selectedFolder, long selectedItem) {
        if (this.selectedItem == selectedItem && this.selectedFolder == selectedFolder) {
            this.selectedItem = -1;
            this.selectedFolder = -1;
            return false;
        } else {
            this.selectedFolder = selectedFolder;
            this.selectedItem = selectedItem;
            return true;
        }
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_counter_agents_group_view.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class NomenclatureSearchGroupViewHolder extends AbstractExpandableItemViewHolder {
        @InjectView(R.id.good_name)
        TextView goodName;
        @InjectView(R.id.goods_row)
        LinearLayout goodsRow;

        NomenclatureSearchGroupViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    private class NomenclatureSearchGroup {
        private String guid;
        private String name;
        private String path;
        private boolean pinned = true;
        private RealmResults<Nomenclature> children;
        private HashMap<String, Double> prices;
        private HashMap<String, Double> stocks;

        public NomenclatureSearchGroup(String guid, boolean addAllChildren) {
            this.guid = guid;
            prices = new HashMap<>();
            stocks = new HashMap<>();

            if (Nomenclature.ROOT_GUID.equals(guid)) {
                name = "/";
                path = "/";
            } else {
                Nomenclature nomenclatureById = realmExchangeOperations.getNomenclatureById(realm, guid);
                name = nomenclatureById.getName();
                path = "";

                String folderId = nomenclatureById.getFolderId();
                while (!Nomenclature.ROOT_GUID.equals(folderId)) {
                    final Nomenclature parent = realmExchangeOperations.getNomenclatureById(realm, folderId);
                    folderId = parent.getFolderId();
                    path = parent.getName() + "/" + path;
                }
                path = "/" + path;
            }

            children = TextUtils.isEmpty(searchQuery) || addAllChildren ?
                    realmExchangeOperations.getNomenclatureNonFolderChildren(realm, guid) :
                    realmExchangeOperations.getNomenclatureNonFolderChildren(realm, guid, searchQuery);

            for (Nomenclature nomenclature : children) {
                if (nomenclature.getStoreUnit() != null)
                    prices.put(nomenclature.getGuid(), realmExchangeOperations.getPrice(realm, nomenclature.getStoreUnit()));
                stocks.put(nomenclature.getGuid(), realmExchangeOperations.getStockSum(realm, nomenclature.getGuid()));
            }

        }

        public Double getPrice(int childPosition) {
            return prices.get(children.get(childPosition).getGuid());
        }


        public Double getStock(int childPosition) {
            return stocks.get(children.get(childPosition).getGuid());
        }

        public boolean isPinned() {
            return pinned;
        }

        public void setPinned(boolean isPinned) {
            this.pinned = isPinned;
        }
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_counter_agents.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class NomenclatureViewHolder extends AbstractExpandableItemViewHolder {


        @InjectView(R.id.good_name)
        TextView goodName;
        @InjectView(R.id.goods_row)
        LinearLayout goodsRow;

        @InjectView(R.id.good_quantity)
        TextView goodQuantity;
        @InjectView(R.id.good_cost)
        TextView goodCost;
        @InjectView(R.id.good_in_order)
        TextView goodInOrder;


        private String nomenclatureGuid;
        private long folder;
        private long item;

        NomenclatureViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        @OnClick(R.id.goods_row)
        void onItemclick() {
            boolean enable = setSelectedItem(folder, item);
            OrderItem orderItem = orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedCounterAgentGuid()).getGood(nomenclatureGuid);
            selectedItemsHolder.setSelectedGood(orderItem != null ? orderItem : new OrderItem(nomenclatureGuid));
            notifyDataSetChanged();
            if (enable)
                bus.post(new ShowPriceNumPadEvent());
            else
                bus.post(new DismissNumPadEvent());
        }

        public void setFolder(long folder) {
            this.folder = folder;
        }

        public void setItem(long item) {
            this.item = item;
        }

        public void setNomenclatureGuid(String nomenclatureGuid) {
            this.nomenclatureGuid = nomenclatureGuid;
        }
    }

}
