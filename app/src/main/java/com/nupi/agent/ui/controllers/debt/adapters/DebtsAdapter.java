package com.nupi.agent.ui.controllers.debt.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.models.ContractGroup;
import com.nupi.agent.application.models.ContractsGroup;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.enums.SortType;
import com.nupi.agent.utils.StringUtils;
import com.squareup.otto.Bus;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Pasenchuk Victor on 16.10.14
 */
public class DebtsAdapter extends AbstractExpandableItemAdapter<DebtsAdapter.CounterAgentViewHolder, DebtsAdapter.DebtViewHolder> {

    public static final int CONTRACT_TYPE = 0;
    public static final int DOCUMENT_TYPE = 1;

    @Inject
    RealmExchangeOperations meteorOperations;
    @Inject
    Bus bus;
    private Realm realm;

    private RealmResults<CounterAgent> counterAgents;
    private HashMap<String, ContractsGroup> contractsGroupHashMap = new HashMap<>();
    private Context context;
    private Sort order = Sort.ASCENDING;
    private SortType sortType = SortType.ABC_ORDER;
    private Subscription subscription;

    public DebtsAdapter(Activity activity, Realm realm) {
        this.context = activity;
        this.realm = realm;
        ((NupiApp) activity.getApplication()).getAppComponent().inject(this);
        setHasStableIds(true);
        updateCounterAgents();
    }

    public double getSumDebt() {
        return meteorOperations.getDebtSumForAllCounterAgent(realm);
    }

    private void updateCounterAgents() {
        if (subscription != null)
            subscription.unsubscribe();
        String searchQuery = "";
        counterAgents = meteorOperations.getNonFolderCounterAgents(realm, searchQuery.toLowerCase(), sortType, order);
        subscription = counterAgents
                .asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RealmResults<CounterAgent>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(RealmResults<CounterAgent> counterAgents) {
                        notifyDataSetChanged();
                    }
                });
        notifyDataSetChanged();
    }

    private ContractsGroup initCounterAgentGroup(CounterAgent counterAgent) {
        final ContractsGroup contractsGroup = meteorOperations.getContractsGroup(realm, counterAgent.getGuid());
        contractsGroupHashMap.put(counterAgent.getGuid(), contractsGroup);
        notifyDataSetChanged();
        return contractsGroup;
    }

    @Override
    public void onBindGroupViewHolder(CounterAgentViewHolder holder, int groupPosition, int viewType) {
        // child counterAgentGroup
        final CounterAgent counterAgent = counterAgents.get(groupPosition);

        holder.clientName.setText(counterAgent.getName());
        double sum = counterAgent.getDebt();
        holder.debt.setText(StringUtils.getMoneyFormat(sum));
        holder.debt.setTextColor(context.getResources().getColor(sum < -StringUtils.EPSILON ? R.color.red : R.color.text_list));
        holder.itemView.setClickable(true);
    }

    @Override
    public void onBindChildViewHolder(DebtViewHolder holder, int groupPosition, int childPosition, int viewType) {
        // group item

        final CounterAgent counterAgent = counterAgents.get(groupPosition);
        final ContractsGroup contractsGroup;
        if (contractsGroupHashMap.containsKey(counterAgent.getGuid()))
            contractsGroup = contractsGroupHashMap.get(counterAgent.getGuid());
        else
            contractsGroup = initCounterAgentGroup(counterAgent);

        ContractGroup contractGroup = contractsGroup.getGroup(childPosition);
        int ind = contractsGroup.getIndexInGroup();

        if (ind == ContractsGroup.CONTRACT_POSITION) {
            Contract contract = contractGroup.getContract();
            Payment payment = contractGroup.getPayment();

            holder.docName.setText(contract.getName());
            double sum = payment != null ? payment.getSum() : 0;
            holder.debt.setText(StringUtils.getMoneyFormat(sum));
            holder.debt.setTextColor(context.getResources().getColor(sum < 0 ? R.color.red : R.color.text_list));
            holder.divider.setVisibility(View.GONE);
            holder.delay.setVisibility(View.GONE);
            holder.age.setText(String.valueOf(contract.isDebtDaysControl() ? contract.getDebtDaysMax() : 0L));
        } else {
            PaymentByOrder paymentByOrder = contractGroup.getPaymentByOrders().get(ind);
            holder.docName.setText(paymentByOrder.getDocInfo());
            double sum = paymentByOrder.getSum();
            holder.divider.setVisibility(View.VISIBLE);
            holder.delay.setVisibility(View.VISIBLE);
            holder.debt.setText(StringUtils.getMoneyFormat(sum));
            holder.debt.setTextColor(context.getResources().getColor(sum < -StringUtils.EPSILON ? R.color.red : R.color.text_list));
            setDocDelay(contractGroup, ind, holder);
        }
    }

    @Override
    public int getGroupCount() {
        return counterAgents.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        if (contractsGroupHashMap.containsKey(counterAgents.get(groupPosition).getGuid()))
            return contractsGroupHashMap.get(counterAgents.get(groupPosition).getGuid()).getSize();
        else
            return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return counterAgents.get(groupPosition).getGuid().hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        final CounterAgent counterAgent = counterAgents.get(groupPosition);
        final ContractsGroup contractsGroup;
        if (contractsGroupHashMap.containsKey(counterAgent.getGuid()))
            contractsGroup = contractsGroupHashMap.get(counterAgent.getGuid());
        else
            contractsGroup = initCounterAgentGroup(counterAgent);
        return contractsGroup.isContract(childPosition) ? CONTRACT_TYPE : DOCUMENT_TYPE;
    }

    @Override
    public CounterAgentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_counter_agents_group_view, parent, false);
        return new CounterAgentViewHolder(v);
    }

    @Override
    public DebtViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(viewType == CONTRACT_TYPE ? R.layout.list_item_counter_agents : R.layout.list_item_debt_doc, parent, false);
        return new DebtViewHolder(v);
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(CounterAgentViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        // check the item is *not* pinned
        CounterAgent counterAgent = counterAgents.get(groupPosition);
        if (!contractsGroupHashMap.containsKey(counterAgent.getGuid())) {
            return !initCounterAgentGroup(counterAgent).isPinned() && holder.itemView.isEnabled() && holder.itemView.isClickable();
        } else
            return !contractsGroupHashMap.get(counterAgent.getGuid()).isPinned() && holder.itemView.isEnabled() && holder.itemView.isClickable();
    }

    private void setDocDelay(ContractGroup contractGroup, int ind, DebtViewHolder holder) {

        long credit = contractGroup.getContract().getDebtDaysMax();
        long delay = contractGroup.getDelay(ind);

        holder.age.setText(String.valueOf(delay));

        final TextView textViewDelay = holder.delay;
        if (credit - delay < 0) {
            writeDelay(textViewDelay, delay - credit);
            textViewDelay.setTextColor(Color.RED);
        } else {
            writeDelay(textViewDelay, 0);
            textViewDelay.setTextColor(Color.BLACK);
        }
    }

    private void writeDelay(TextView textView, long delayed) {
        textView.setText(String.format("%d", delayed));
    }

    public SortType getSortType() {
        return sortType;
    }

    public void sort(final SortType sortType) {
        this.sortType = sortType;
        updateCounterAgents();
    }

    public Sort getOrder() {
        return order;
    }

    public void changeOrder() {
        order = order.getValue() ? Sort.DESCENDING : Sort.ASCENDING;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_counter_agents_group_view.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class CounterAgentViewHolder extends AbstractExpandableItemViewHolder {
        TextView clientName;
        TextView debt;
        LinearLayout clientRow;

        CounterAgentViewHolder(View view) {
            super(view);
            clientName = (TextView) view.findViewById(R.id.clientName);
            debt = (TextView) view.findViewById(R.id.debt);
            clientRow = (LinearLayout) view.findViewById(R.id.clientRow);
        }
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_counter_agents.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class DebtViewHolder extends AbstractExpandableItemViewHolder {

        @InjectView(R.id.docName)
        TextView docName;
        @InjectView(R.id.age)
        TextView age;
        @InjectView(R.id.divider)
        TextView divider;
        @InjectView(R.id.delay)
        TextView delay;
        @InjectView(R.id.debt)
        TextView debt;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        DebtViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}