package com.nupi.agent.ui.controllers.receipt.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.database.models.meteor.Unit;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.utils.StringUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class ReceiptPreviewEntryAdapter extends ArrayAdapter<OrderItem> {

    final Realm realm;
    @Inject
    RealmExchangeOperations realmExchangeOperations;

    List<OrderItem> prices;

    public ReceiptPreviewEntryAdapter(Activity context, List<OrderItem> prices, Realm realm) {
        super(context, R.layout.list_item_price_entry, prices);
        this.realm = realm;
        this.prices = prices;
        ((NupiApp) context.getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        OrderItem priceListItem = getItem(position);

        if (view == null) {
            view = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_price_entry, null);
        }

        fillItemView(view, priceListItem, position);

        return view;
    }

    public void fillItemView(View view, OrderItem item, int position) {
        ViewHolder viewHolder = new ViewHolder(view);

        final Unit unit = realmExchangeOperations.getRealmObjectByGuid(realm, item.getUnit(), Unit.class);
        final Nomenclature nomenclature = realmExchangeOperations.getRealmObjectByGuid(realm, item.getNomenclature(), Nomenclature.class);

        viewHolder.nomenclature
                .setText(nomenclature.getName());

        viewHolder.number
                .setText(String.valueOf(position + 1));

        viewHolder.quantity
                .setText(StringUtils.getStockFormat(item.getQuantity()));

        viewHolder.price
                .setText(StringUtils.getMoneyFormat(item.getPrice()));

        viewHolder.sum
                .setText(StringUtils.getMoneyFormat(item.getSum()));

        viewHolder.unit
                .setText(unit.getName());

    }

    public float getSum() {
        float sum = 0.0f;
        for (OrderItem item : prices) {
            sum += item.getSum();
        }
        return sum;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_price_entry.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.number)
        TextView number;
        @InjectView(R.id.nomenclature)
        TextView nomenclature;
        @InjectView(R.id.quantity)
        TextView quantity;
        @InjectView(R.id.unit)
        TextView unit;
        @InjectView(R.id.price)
        TextView price;
        @InjectView(R.id.sum)
        TextView sum;
        @InjectView(R.id.orderRow)
        LinearLayout orderRow;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

