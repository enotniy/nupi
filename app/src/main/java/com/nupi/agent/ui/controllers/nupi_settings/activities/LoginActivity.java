package com.nupi.agent.ui.controllers.nupi_settings.activities;

import android.content.Intent;
import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.network.gcm.RegistrationIntentService;
import com.nupi.agent.network.nupi.NupiAuthApi;
import com.nupi.agent.network.nupi.models.Auth;
import com.nupi.agent.network.nupi.models.Token;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.dialogs.AuthDialog;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.utils.NetworkUtils;

import javax.inject.Inject;

import icepick.Icepick;
import icepick.State;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

public class LoginActivity extends NupiActivity {

    @Inject
    NupiAuthApi nupiAuthApi;

    @State
    String login;

    @State
    String password;

    private AuthDialog authDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
        setContentView(R.layout.activity_login);
        login = preferences.getLastLogin();
        Icepick.restoreInstanceState(this, savedInstanceState);
        authDialog = new AuthDialog(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        final AuthDialog.ViewHolder viewHolder = authDialog.getViewHolder();
        if (viewHolder != null) {
            login = viewHolder.getLogin().getText().toString();
            password = viewHolder.getPassword().getText().toString();
        }
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nupiLogin();
    }

    @Override
    protected void onPause() {
        authDialog.dismiss();
        super.onPause();
    }

    private void nupiLogin() {
        authDialog.show(getString(R.string.NUPI_sign_in), login, password).
                flatMap(new Func1<Auth, Observable<Token>>() {
                    @Override
                    public Observable<Token> call(Auth auth) {
                        preferences.setLastLogin(auth.getUsername());
                        return nupiAuthApi.getToken(auth);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Token>() {
                               @Override
                               public void call(Token token) {
                                   if (token.getToken() != null) {
                                       preferences.setNupiToken(token.getToken());
                                       preferences.setChatId(token.getUser());
                                       preferences.setLoggedIn(true);
                                       startService(new Intent(LoginActivity.this, RegistrationIntentService.class));

                                       Intent intent = new Intent(getApplicationContext(), RealmFileOperationsActivity.class);
                                       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                       startActivity(intent);

                                       finish();
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                try {
                                    nupiLogin();
                                    switch (NetworkUtils.getNetworkError(throwable)) {
                                        case NetworkUtils.NETWORK_ERROR:
                                            MessageBox.show(getString(R.string.no_server_connection), LoginActivity.this);
                                            break;
                                        case 403:
                                            MessageBox.show(getString(R.string.forbidden), LoginActivity.this);
                                            break;
                                        case 401:
                                            MessageBox.show(getString(R.string.unauthorized), LoginActivity.this);
                                            break;
                                    }
                                } catch (Exception ignored) {

                                }
                            }
                        });
    }

}
