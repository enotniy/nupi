package com.nupi.agent.ui.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

import com.nupi.agent.R;

/**
 * Created by User on 17.11.2015.
 */
public class LineTextView extends TextView {
    private static final int PADDING_LINE = 3;
    private Rect mRect;
    private Paint mPaint;

    // we need this constructor for LayoutInflater
    public LineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setStrokeWidth(Resources.getSystem().getDisplayMetrics().density);
        mPaint.setColor(getResources().getColor(R.color.divider_list_color_start));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //int count = getLineCount();
        int height = getHeight();
        int line_height = getLineHeight();

        int count = height / line_height;

        if (getLineCount() > count)
            count = getLineCount();//for long text with scrolling

        Rect r = mRect;
        Paint paint = mPaint;
        int baseline = getLineBounds(0, r);//first line

        for (int i = 0; i < count; i++) {

            canvas.drawLine(r.left, baseline + PADDING_LINE, r.right, baseline + PADDING_LINE, paint);
            baseline += getLineHeight();//next line
        }

        super.onDraw(canvas);
    }
}