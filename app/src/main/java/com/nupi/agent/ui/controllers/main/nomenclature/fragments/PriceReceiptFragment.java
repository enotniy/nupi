package com.nupi.agent.ui.controllers.main.nomenclature.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.events.OpenRealisationPreviewReceiptEvent;
import com.nupi.agent.events.ShowPriceNumPadEvent;
import com.nupi.agent.events.UpdatePriceRealisationEvent;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.main.nomenclature.adapters.PricesOrderAdapter;
import com.nupi.agent.ui.dialogs.SelectCounterAgentDialog;
import com.nupi.agent.utils.StringUtils;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by Pasenchuk Victor on 12.12.14
 */
public class PriceReceiptFragment extends NupiFragment implements ListView.OnItemClickListener {


    @InjectView(R.id.clientTitle)
    TextView clientTitleTextView;

    @InjectView(R.id.goodCount)
    TextView goodCountTextView;

    @InjectView(R.id.quantitySum)
    TextView quantitySumTextView;

    @InjectView(R.id.costSum)
    TextView costSumTextView;

    @InjectView(R.id.priceList)
    ListView priceListView;

    private PricesOrderAdapter pricesOrderAdapter;
    private List<OrderItem> orderItemList;

    private String clientName;
    private int discount;
    private float sum;
    private AlertDialog selectCounterAgentDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prices_order, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initClient();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (selectCounterAgentDialog != null)
            selectCounterAgentDialog.dismiss();
    }

    private void initClient() {
        String selectedUserGuid = selectedItemsHolder.getSelectedCounterAgentGuid();
        if (!selectedUserGuid.equals(NEW_COUNTER_AGENT_GUID)) {
            final CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), selectedUserGuid, CounterAgent.class);

            if (counterAgent != null) {
                clientName = counterAgent.getName();
                //discount = selectedItemsHolder.getSelectedCounterAgent().get;
                clientTitleTextView.setText(String.format(getString(R.string.nameAndDiscount), clientName.replace(",", ", ").replace("  ", " ")/*, discount*/));
            }

            orderItemList = orderApi.getOrderRequestForClient(selectedUserGuid).getOrderItems();
            double quantityCount = 0;
            sum = 0;

            for (OrderItem priceListItem : orderItemList) {
                quantityCount += priceListItem.getQuantity();
                sum += priceListItem.getSum();
            }

            goodCountTextView.setText(String.valueOf(orderItemList.size()));
            quantitySumTextView.setText(String.valueOf(quantityCount));
            costSumTextView.setText(StringUtils.getMoneyFormat(sum));

            pricesOrderAdapter = new PricesOrderAdapter(getActivity(), orderItemList, getMeteorRealm());

            priceListView.setAdapter(pricesOrderAdapter);
            priceListView.setOnItemClickListener(this);

            final int position = orderItemList.size() - 1;
            priceListView.setSelection(position);
        } else {

            clientTitleTextView.setText(String.format(getString(R.string.nameAndDiscount), getResources().getString(R.string.not_counteragent)));

            orderItemList = orderApi.getOrderRequestForClient(selectedUserGuid).getOrderItems();
            int quantityCount = 0;
            sum = 0;

            for (OrderItem priceListItem : orderItemList) {
                quantityCount += priceListItem.getQuantity();
                sum += priceListItem.getSum();
            }

            goodCountTextView.setText(String.valueOf(orderItemList.size()));
            quantitySumTextView.setText(StringUtils.getStockFormat(quantityCount));
            costSumTextView.setText(StringUtils.getMoneyFormat(sum));

            pricesOrderAdapter = new PricesOrderAdapter(getActivity(), orderItemList, getMeteorRealm());

            priceListView.setAdapter(pricesOrderAdapter);
            priceListView.setOnItemClickListener(this);

            final int position = orderItemList.size() - 1;
            priceListView.setSelection(position);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        pricesOrderAdapter.notifyDataSetChanged();
        selectedItemsHolder.setSelectedGood(orderItemList.get(i));
        bus.post(new ShowPriceNumPadEvent());
    }

    @OnClick(R.id.clientInfo)
    void onClientInfoClick() {
        sendButtonEvent("About Client", true);
        bus.post(new OpenRealisationPreviewReceiptEvent());
    }


    @OnClick(R.id.clientTitle)
    void onClientTitleClick() {
        sendButtonEvent("Change Client", true);

        selectCounterAgentDialog = new SelectCounterAgentDialog(getActivity(), getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid(), new SelectCounterAgentDialog.OnClientSelectedListener() {
            @Override
            public void onClientSelected(CounterAgent client) {
                orderApi.changeClientForRealisation(selectedItemsHolder.getSelectedCounterAgentGuid(), client.getGuid());
                writeNewHasOrdersValue(selectedItemsHolder.getSelectedCounterAgentGuid(), client.getGuid());
                selectedItemsHolder.setSelectedCounterAgentGuid(client.getGuid());
                initClient();
                bus.post(new UpdatePriceRealisationEvent());
            }
        }).show();

    }


    private void writeNewHasOrdersValue(String oldClientGuid, String newClientGuid) {
        final Realm meteorRealm = getMeteorRealm();
        CounterAgent counterAgent = realmExchangeOperations.getRealmObjectByGuid(
                meteorRealm,
                oldClientGuid,
                CounterAgent.class
        );
        writeHasOrders(meteorRealm, counterAgent);
        counterAgent = realmExchangeOperations.getRealmObjectByGuid(
                meteorRealm,
                newClientGuid,
                CounterAgent.class
        );
        writeHasOrders(meteorRealm, counterAgent);
    }

    private void writeHasOrders(Realm meteorRealm, CounterAgent counterAgent) {
        meteorRealm.beginTransaction();
        try {
            counterAgent.setHasOrders(orderApi.hasOrders(counterAgent.getGuid()));
            meteorRealm.commitTransaction();
        } catch (Exception e) {
            Crashlytics.logException(e);
            meteorRealm.cancelTransaction();
        }
    }
}
