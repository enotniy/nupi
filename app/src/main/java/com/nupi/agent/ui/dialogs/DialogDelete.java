package com.nupi.agent.ui.dialogs;

import android.app.Activity;

import com.nupi.agent.R;

/**
 * Created with IntelliJ IDEA.
 * User: pasecukviktor
 * Date: 18.03.14
 * Time: 13:24
 */
public abstract class DialogDelete implements DialogDeleteDelegate {
    public void show(final Activity activity) {
        new YesNoDialog(activity, activity.getString(R.string.delete_confirm_message)) {
            @Override
            public void handlePositiveButton() {
                delete();
            }
        }.show();
    }
}
