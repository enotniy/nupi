package com.nupi.agent.ui.controllers.analytics.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.application.models.Tree;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.enums.SelectionType;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;


public class ReportAnalyticsClientsAdapter extends RecyclerView.Adapter<ReportAnalyticsClientsAdapter.ViewHolder> {

    List<CounterAgent> listClients;
    Picker picker;
    Tree<String> tree;
    @Inject
    NupiPreferences preferences;
    @Inject
    RealmExchangeOperations realmExchangeOperations;
    private Runnable onElementsListChangeListener;

    public ReportAnalyticsClientsAdapter(Activity context, Picker clients, Realm realm, Runnable onElementsListChangeListener) {
        ((NupiApp) context.getApplication()).getAppComponent().inject(this);
        this.picker = clients;
        this.tree = realmExchangeOperations.getTreeIdCounterAgent(realm);
        RealmResults<CounterAgent> counterAgents = realmExchangeOperations.getNonFolderCounterAgents(realm);
        listClients = new LinkedList<>();
        for (CounterAgent price : counterAgents) {
            if (picker.getTypeSelection(price.getGuid()) == SelectionType.ALL)
                listClients.add(price);
        }
        this.onElementsListChangeListener = onElementsListChangeListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_report_filter_child, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return listClients.size();
    }


    public List<CounterAgent> getItems() {
        return listClients;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CounterAgent item = listClients.get(position);
        holder.name.setText(String.valueOf(item.getName()));
        holder.setRef_clients(listClients.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clear() {
        listClients.clear();
    }

    public Picker getPicker() {
        return picker;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.name)
        TextView name;
        @InjectView(R.id.remove)
        ImageView remove;
        @InjectView(R.id.row)
        LinearLayout row;
        private CounterAgent counterAgent;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void setRef_clients(CounterAgent ref_clients) {
            this.counterAgent = ref_clients;
        }

        @OnClick(R.id.remove)
        void onItemclick() {
            if (listClients.contains(counterAgent)) {
                listClients.remove(counterAgent);
                picker.selectElement(tree.getTree(counterAgent.getGuid()), SelectionType.NONE);
                if (onElementsListChangeListener != null) {
                    onElementsListChangeListener.run();
                }
                preferences.setPickerClients(picker);
                notifyDataSetChanged();
            }
        }
    }
}

