package com.nupi.agent.ui.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.events.ChatButtonEvent;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.events.PlannerCreateRouteEvent;
import com.nupi.agent.events.PlannerRouteEvent;
import com.nupi.agent.events.ReportAnalyticsButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.controllers.analytics.activities.ReportAnalyticsActivity;
import com.nupi.agent.ui.controllers.chat.activities.ChatActivity;
import com.nupi.agent.ui.controllers.geolocation.activities.GeolocationActivity;
import com.nupi.agent.ui.controllers.planner.fragments.PlannerEditorFragment;
import com.nupi.agent.ui.controllers.planner.fragments.PlannerRouteFragment;
import com.nupi.agent.ui.controllers.receipt.activities.ReceiptActivity;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.utils.ExternalFilesUtils;
import com.skobbler.ngx.SKPrepareMapTextureListener;
import com.skobbler.ngx.SKPrepareMapTextureThread;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

/**
 * Created by Pasenchuk Victor on 24.09.14
 */
public class NupiActivity extends Activity {

    public static final String GUID_KEY = "GUID_KEY";
    public static final String IN_THE_POCKET_FILTER = "IN_THE_POCKET_FILTER";


    public static final String STACK_PLANNER = "STACK_PLANNER";
    public static final String STACK_PLANNER_ROUTE = "STACK_PLANNER_ROUTE";
    public static final String STACK_PLANNER_CREATE = "STACK_PLANNER_CREATE";
    public static final String STACK_PLANNER_EDIT = "STACK_PLANNER_EDIT";
    public static final String STACK_PLANNER_ADD_POINT = "STACK_PLANNER_ADD_POINT";

    @Inject
    protected Bus bus;

    @Inject
    protected NupiPreferences preferences;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NupiApp nupiApp = getNupiApp();
        nupiANupiApppp.getAppComponent().inject(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading_wait));
        progressDialog.setCancelable(false);


    }

    public NupiApp getNupiApp() {
        return (NupiApp) getApplication();
    }

    @Override
    protected void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dismissProgressDialog();
    }

    @Override
    protected void onPause() {
        dismissProgressDialog();
        super.onPause();
    }

    protected void reactOnUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        switch (event.updateStatus) {
            case UPDATE_STARTED:
                Toast.makeText(this, R.string.data_update, Toast.LENGTH_SHORT).show();
        }
    }

    public void showProgressDialog() {
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    public NupiPreferences getPreferences() {
        return preferences;
    }


    @Subscribe
    public void onRealisationReceiptEvent(OpenRealisationReceiptEvent event) {
        Intent intent = new Intent(this, ReceiptActivity.class);
        intent.putExtra(GUID_KEY, event.realisationGuid);
        startActivity(intent);
    }

    @Subscribe
    public void onRealisationPreviewReceiptEvent() {
        Intent intent = new Intent(this, ReceiptActivity.class);
        startActivity(intent);
    }

    @Subscribe
    public void onChatButtonEvent(ChatButtonEvent event) {
        Log.d("chat", "chat");
        if (preferences.isPlayServicesAvailable())
            startActivity(new Intent(this, ChatActivity.class));
        else
            Toast.makeText(this, getString(R.string.this_func_requires_google_play_services), Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onGeolocationButtonEvent(OpenGeolocationEvent event) {
        final OpenGeolocationEvent.OpenType type = event.getOpenType();
        if (preferences.isPlayServicesAvailable()) {
            showProgressDialog();
            ExternalFilesUtils externalFilesUtils = new ExternalFilesUtils(this, "Maps");
            File mapFilesDir = externalFilesUtils.GetFilesDir();
            if (externalFilesUtils.isExternalStorageWritable()) {
                try {
                    if (!copyMapFromAssetsToSD(mapFilesDir, "SKMaps.zip", externalFilesUtils.GetFreeSpace(ExternalFilesUtils.SIZE_MB))) {
                        dismissProgressDialog();
                        MessageBox.show("Not enough free space on external storage", this);
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                new SKPrepareMapTextureThread(this, mapFilesDir.toString(), "SKMaps.zip", new SKPrepareMapTextureListener() {
                    @Override
                    public void onMapTexturesPrepared(boolean b) {
                        Intent intent = new Intent(new Intent(getApplicationContext(), GeolocationActivity.class));
                        if (type == OpenGeolocationEvent.OpenType.ROUTE)
                            intent.putExtra(GeolocationActivity.START_GEOLOCATION_TYPE, GeolocationActivity.START_GEOLOCATION_ROUTE);
                        else if (type == OpenGeolocationEvent.OpenType.POINT)
                            intent.putExtra(GeolocationActivity.START_GEOLOCATION_TYPE, GeolocationActivity.START_GEOLOCATION_POINT);
                        else
                            intent.putExtra(GeolocationActivity.START_GEOLOCATION_TYPE, GeolocationActivity.START_GEOLOCATION_NONE);
                        startActivity(intent);
                    }
                }).start();

            }
        } else
            Toast.makeText(this, getString(R.string.this_func_requires_google_play_services), Toast.LENGTH_SHORT).show();
    }


    public void onReportAnalyticsButton(ReportAnalyticsButtonEvent event) {
        Intent intent = new Intent(this, ReportAnalyticsActivity.class);
        intent.putExtra(ReportAnalyticsActivity.REPORT_TYPE, event.getTypeReport());
        startActivity(intent);
    }

    private boolean copyMapFromAssetsToSD(File sdMapsFolder, String mapsFileName, float sdFreeSpaceAvail) throws IOException {
        int FREE_SPACE_TO_ALLOW = 150;
        String toPath = sdMapsFolder + File.separator;
        File isInstalled = new File(toPath + ".installed.txt");
        if (isInstalled.exists())
            return true;
        if (sdFreeSpaceAvail < FREE_SPACE_TO_ALLOW)
            return false;
//        new File(toPath).mkdirs();
        AssetManager assetManager = getAssets();
        InputStream in = assetManager.open(mapsFileName);
        OutputStream out = new FileOutputStream(toPath + mapsFileName);

        byte[] buffer = new byte[1024];
        int read;

        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        return true;
    }

    @Subscribe
    public void onPlannerRouteEvent(PlannerRouteEvent event) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, PlannerRouteFragment.newInstance(event.getRoute()))
                .addToBackStack(STACK_PLANNER_ROUTE)
                .commit();
    }

    @Subscribe
    public void onPlannerCreateRouteEvent(PlannerCreateRouteEvent event) {
        if (event.getRoute() == null)
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new PlannerEditorFragment())
                    .addToBackStack(STACK_PLANNER_CREATE)
                    .commit();
        else
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, PlannerEditorFragment.newInstance(event.getClientInfo(), event.getRoute()))
                    .addToBackStack(STACK_PLANNER_ADD_POINT)
                    .commit();
    }
}
