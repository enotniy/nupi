package com.nupi.agent.ui.controllers.main.nomenclature.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.application.service.SelectedItemsHolder;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.utils.StringUtils;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by User on 20.10.2015
 */


public class NomenclatureTreeMeteorAdapter extends RecyclerView.Adapter<NomenclatureTreeMeteorAdapter.ViewHolder> {


    public static final int TYPE_FOLDER = 0;
    public static final int TYPE_ITEM = 1;
    private final Realm realm;
    private final RealmExchangeOperations realmExchangeOperations;
    private RealmResults<Nomenclature> nomenclatures;

    private long selectedIndex = -1;
    private HashMap<String, Double> pricesMap = new HashMap<>();
    private HashMap<String, Double> stockMap = new HashMap<>();
    private Context context;
    private OrderApi orderApi;
    private SelectedItemsHolder selectedItemsHolder;

    public NomenclatureTreeMeteorAdapter(Context context, Realm realm, RealmExchangeOperations realmExchangeOperations, OrderApi orderApi, SelectedItemsHolder selectedItemsHolder, RealmResults<Nomenclature> nomenclatures) {
        this.context = context;
        this.orderApi = orderApi;
        this.selectedItemsHolder = selectedItemsHolder;
        this.realm = realm;
        this.realmExchangeOperations = realmExchangeOperations;
        updateData(nomenclatures);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).isFolder() ? TYPE_FOLDER : TYPE_ITEM;
    }


    public Nomenclature getItem(int i) {
        if (nomenclatures == null || i < 0 || i >= nomenclatures.size()) {
            return null;
        }
        return nomenclatures.get(i);
    }

    @Override
    public int getItemCount() {
        return nomenclatures.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(viewType == TYPE_FOLDER ? R.layout.list_item_good_folder : R.layout.list_item_good, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Nomenclature nomenclature = getItem(position);

        viewHolder.goodName.setText(nomenclature.getName());
        if (!nomenclature.isFolder()) {
            // TODO: 28.12.15 optimize
            if (nomenclature.getStoreUnit() != null) {
                Double storePrice = pricesMap.get(nomenclature.getGuid());
                if (storePrice != null)
                    viewHolder.goodCost.setText(StringUtils.getMoneyFormat(storePrice));
                else
                    viewHolder.goodCost.setText(StringUtils.getMoneyFormat(0f));


                Double stock = stockMap.get(nomenclature.getGuid());
                if (stock != null)
                    viewHolder.goodQuantity.setText(StringUtils.getStockFormat(stock));
                else
                    viewHolder.goodQuantity.setText(StringUtils.getStockFormat(0f));

            }
            viewHolder.goodsRow.setBackground(context.getResources().getDrawable(selectedIndex == position ? R.color.background_list_selected_pressed : R.drawable.selector_list_item_selected));

            OrderItem orderItem = orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedCounterAgentGuid()).getGood(nomenclature.getGuid());
            if (orderItem != null)
                viewHolder.goodInOrder.setText(StringUtils.getStockFormat(orderItem.getQuantity()));
            else
                viewHolder.goodInOrder.setText("");
        }
    }

    @Override
    public long getItemId(int position) {

        final Nomenclature item = getItem(position);
        if (item != null)
            return item.getGuid().hashCode();
        return -1;
    }

    public void updateData(RealmResults<Nomenclature> realmResults) {
        this.nomenclatures = realmResults;


        nomenclatures
                .asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RealmResults<Nomenclature>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(RealmResults<Nomenclature> realmResults) {
                        onUpdate();
                    }
                });

        onUpdate();
    }

    public void onUpdate() {
        stockMap = new HashMap<>();
        pricesMap = new HashMap<>();
        for (Nomenclature nomenclature : nomenclatures) {
            stockMap.put(nomenclature.getGuid(), realmExchangeOperations.getStockSum(realm, nomenclature.getGuid()));
            if (nomenclature.getStoreUnit() != null)
                pricesMap.put(nomenclature.getGuid(), realmExchangeOperations.getPrice(realm, nomenclature.getStoreUnit()));
        }

        notifyDataSetChanged();
    }

    public boolean setSelectedIndex(long selectedIndex) {
        boolean flag = this.selectedIndex == selectedIndex;
        this.selectedIndex = flag ? -1 : selectedIndex;
        return !flag;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_good.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.good_name)
        TextView goodName;
        @InjectView(R.id.goods_row)
        LinearLayout goodsRow;

        @Optional
        @InjectView(R.id.good_quantity)
        TextView goodQuantity;
        @Optional
        @InjectView(R.id.good_cost)
        TextView goodCost;
        @Optional
        @InjectView(R.id.good_in_order)
        TextView goodInOrder;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }


    }
}
