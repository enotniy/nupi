package com.nupi.agent.ui.controllers.nupi_settings.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nupi.agent.R;
import com.nupi.agent.database.models.chat.ChatRoute;
import com.nupi.agent.database.models.registry.RegistryEntry;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.dialogs.ListDialog;
import com.nupi.agent.ui.dialogs.MessageBox;

import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A placeholder fragment containing a simple view.
 */
public class SupervisorFragment extends NupiFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_supervisor, container, false);
    }

    @OnClick(R.id.logout)
    void onLogoutClick() {
        sendButtonEvent("Logout", true);
        preferences.deleteAccount();
        getActivity().finish();
    }

    @OnClick(R.id.closeButton)
    void closeButtonClick() {
        getFragmentManager().popBackStack();
    }

    @OnClick(R.id.clearPocket)
    void onClearPocketClick() {
        sendButtonEvent("Clear Pocket", true);
        Realm realm = getRegistryRealm();

        RealmResults<RegistryEntry> registryEntries;
        do {
            registryEntries = realmRegistryOperations.getRegistryEntries(realm, null, null, RegistryEntry.PAYMENT, RegistryEntry.ONLY_NULLABLE_STATUSES, true, null);

            for (int i = 0; i < registryEntries.size(); i++) {
                realm.beginTransaction();
                registryEntries.get(i).setEntryStatus(RegistryEntry.SKIPPED);
                realm.commitTransaction();
            }
        } while (registryEntries.size() != 0);

        MessageBox.show(getString(R.string.inThePocketWasCleared), getActivity());
    }


    @OnClick(R.id.change_route)
    void onChangeRouteClick() {
        final RealmResults<ChatRoute> chatRoutes = realmChatOperations.getChatRoutes(getChatRealm());
        final String[] routeNames = new String[chatRoutes.size()];
        for (int i = 0; i < chatRoutes.size(); i++)
            routeNames[i] = chatRoutes.get(i).getName();
        new ListDialog() {
            @Override
            public String getDialogMessage() {
                return getString(R.string.change_route);
            }

            @Override
            public CharSequence[] getItemsSource() {
                return routeNames;
            }

            @Override
            public void onItemClick(int itemId) {

            }
        }.show(getActivity());
    }
}
