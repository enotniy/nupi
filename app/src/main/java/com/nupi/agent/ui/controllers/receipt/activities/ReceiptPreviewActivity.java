package com.nupi.agent.ui.controllers.receipt.activities;

import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.SystemReportButtonEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.receipt.fragments.ReceiptPreviewFragment;
import com.squareup.otto.Subscribe;


public class ReceiptPreviewActivity extends NupiActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, new ReceiptPreviewFragment())
                    .commit();
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
        bus.post(new SystemReportButtonEvent(true));
    }
}
