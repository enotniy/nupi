package com.nupi.agent.ui.controllers.main.cash.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.events.ContractSelectedEvent;
import com.nupi.agent.events.OpenRealisationReceiptEvent;
import com.nupi.agent.events.RealisationSelectedEvent;
import com.nupi.agent.network.meteor.PaymentRequest;
import com.nupi.agent.network.upload.UploadService;
import com.nupi.agent.ui.base.BaseNumericKeyboardFragment;
import com.nupi.agent.ui.dialogs.MessageBox;
import com.nupi.agent.ui.dialogs.YesNoDialog;
import com.nupi.agent.ui.views.AutoResizeTextView;
import com.nupi.agent.utils.StringUtils;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class CashAcceptorFragment extends BaseNumericKeyboardFragment {

    @Inject
    UploadService uploadService;
    @InjectView(R.id.labelCaption)
    AutoResizeTextView labelCaption;
    @InjectView(R.id.calcText)
    AutoResizeTextView calcText;


    private boolean fifoEnabled = true;
    private ArrayList<PaymentByOrder> paymentsByOrders;
    private Payment payment;
    private Contract contract = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.numeric_keyboard_cash, container, false);
        ButterKnife.inject(this, view);
        super.type = typeKeyboard.MONEY;
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paymentsByOrders = new ArrayList<>();
        Animation anim = AnimationUtils.loadAnimation(
                getActivity(), R.anim.blink_anim);
        labelCaption.startAnimation(anim);
        RealmResults<Contract> contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
        contract = contracts.size() != 1 ? null : contracts.first();
        if (contract != null)
            payment = realmExchangeOperations.getPaymentByContract(getMeteorRealm(), contract);
        setLabel();
    }

    @OnClick(R.id.labelCaption)
    public void onLabelCaptionClick() {
        if (paymentsByOrders.size() == 1) {
            bus.post(new OpenRealisationReceiptEvent(paymentsByOrders.get(0).getGuid()));
        }
    }

    @OnClick(R.id.btnOK)
    public void onOkKey() {
        if (contract != null) {
            if (!calcText.getText().toString().isEmpty()) {
                float value = Float.parseFloat(calcText.getText().toString());
                if (value != 0) {
                    if (contract.isUseCaDocuments())
                        processPaymentsByOrders(value);
                    else if (payment != null) {
                        final double firstSum = payment.getSum();
                        if (firstSum >= 0.01f) {
                            if (value > firstSum) {
                                new YesNoDialog(getActivity(), String.format(getActivity().getString(R.string.sum_exceeded_debt_contract), firstSum)) {
                                    @Override
                                    public void handlePositiveButton() {
                                        sendButtonEvent("Create contract overpay payment", true);
                                        sendDoc(firstSum);
                                    }

                                }.show();

                            } else {
                                sendButtonEvent("Create contract payment", true);
                                sendDoc(value);
                            }
                        } else {
                            sendButtonEvent("Create contract payment", false);
                            showToast(R.string.negativeDocAlertContract);
                        }
                    } else {
                        sendButtonEvent("Create payment", false);
                        Toast.makeText(getActivity(), R.string.no_payment, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    sendButtonEvent("Create payment", false);
                    Toast.makeText(getActivity(), R.string.enter_sum, Toast.LENGTH_SHORT).show();
                }
            } else {
                sendButtonEvent("Create payment", false);
                Toast.makeText(getActivity(), R.string.enter_sum, Toast.LENGTH_SHORT).show();
            }
        } else {
            sendButtonEvent("Create payment", false);
            Toast.makeText(getActivity(), R.string.no_contracts, Toast.LENGTH_SHORT).show();
        }
    }

    private void processPaymentsByOrders(float value) {
        if (value != 0) {
            if (fifoEnabled) {
                RealmResults<PaymentByOrder> fifoPaymentsByOrders;
                if (contract != null)
                    fifoPaymentsByOrders = realmExchangeOperations.getDebtsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
                else
                    fifoPaymentsByOrders = realmExchangeOperations.getPaymentsByOrdersByContract(getMeteorRealm(), contract);
                sendFifoDocs(value, fifoPaymentsByOrders);

            } else if (paymentsByOrders.size() > 1) {
                sendFifoDocs(value, paymentsByOrders);
            } else if (paymentsByOrders.size() == 1) {
                final PaymentByOrder paymentByOrder = paymentsByOrders.get(0);
                final double firstSum = paymentByOrder.getSum();
                if (firstSum > 0) {
                    if (value > firstSum) {
                        new YesNoDialog(getActivity(), String.format(getActivity().getString(R.string.sum_exceeded_debt_single_doc), firstSum)) {
                            @Override
                            public void handlePositiveButton() {
                                sendButtonEvent("Create single overpay payment", true);
                                sendDoc(paymentByOrder, firstSum);
                            }

                        }.show();

                    } else {
                        sendButtonEvent("Create single payment", true);
                        sendDoc(paymentByOrder, value);
                    }
                } else {
                    sendButtonEvent("Create single payment", false);
                    showToast(R.string.negativeDocAlert);
                }
            }
        } else {
            sendButtonEvent("Create payment", false);
            Toast.makeText(getActivity(), R.string.enter_sum, Toast.LENGTH_SHORT).show();
        }
    }

    private void sendFifoDocs(float value, List<PaymentByOrder> paymentsByOrders) {
        float docsSum = 0;
        boolean hasNegativeDocs = false;
        String negativeDocAlert = "";
        final PaymentByOrder[] payments = paymentsByOrders.toArray(new PaymentByOrder[paymentsByOrders.size()]);
        Arrays.sort(payments, new Comparator<PaymentByOrder>() {
            @Override
            public int compare(PaymentByOrder lhs, PaymentByOrder rhs) {
                return lhs.getDate().compareTo(rhs.getDate());
            }
        });
        final LinkedList<PaymentRequest> holders = new LinkedList<>();
        for (PaymentByOrder paymentByOrder : payments) {
            final double docSum = paymentByOrder.getSum();
            if (docSum > 0) {
                if (value > docSum) {
                    docsSum += docSum;
                    holders.add(new PaymentRequest(paymentByOrder, docSum));
                    value -= docSum;

                } else {
                    docsSum += value;
                    holders.add(new PaymentRequest(paymentByOrder, value));
                    value = 0;
                    break;
                }
            } else {
                hasNegativeDocs = true;
                negativeDocAlert += getString(R.string.excludedPayment, paymentByOrder.getDocInfo(), StringUtils.getMoneyFormat(paymentByOrder.getSum()));
            }
        }


        if (value > 0) {
            if (docsSum > 0) {
                final float finalDocsSum = docsSum;
                final boolean finalHasNegativeDocs = hasNegativeDocs;
                final String finalNegativeDocAlert = negativeDocAlert;
                new YesNoDialog(getActivity(), String.format(getActivity().getString(R.string.sum_exceeded_debt), finalDocsSum)) {
                    @Override
                    public void handlePositiveButton() {
                        sendButtonEvent("Create FIFO overpay payment", true);
                        sendDocs(holders);
                        notifyAboutNegativeDocs(finalHasNegativeDocs, finalNegativeDocAlert);
                    }

                }.show();
            } else {
                sendButtonEvent("Create FIFO payment", false);
                MessageBox.show(getActivity().getString(R.string.no_credit_docs), getActivity());
            }
        } else {
            sendButtonEvent("Create FIFO payment", true);
            sendDocs(holders);
            notifyAboutNegativeDocs(hasNegativeDocs, negativeDocAlert);
        }
    }

    private void notifyAboutNegativeDocs(boolean hasNegativeDocs, String negativeDocAlert) {
        if (hasNegativeDocs)
            MessageBox.show(getString(R.string.onlyPositiveDebt, negativeDocAlert), getActivity());
    }

    private void sendDoc(PaymentByOrder paymentByOrder, double value) {
        uploadService.sendPaymentDocToServer(new PaymentRequest(paymentByOrder, value));
        finishSending(getString(R.string.sending_request));
    }

    private void sendDoc(double value) {
        uploadService.sendPaymentDocToServer(new PaymentRequest(payment, value));
        finishSending(getString(R.string.sending_request));
    }

    private void finishSending(String string) {
        MessageBox.show(string, getActivity());
        getFragmentManager().popBackStack();
    }

    private void sendDocs(List<PaymentRequest> holders) {
        uploadService.sendPaymentDocsToServer(holders);
        finishSending(getString(R.string.sending_requests));
    }


    @Subscribe
    public void onRealisationSelected(RealisationSelectedEvent event) {
        final String docId = event.docId;
        final PaymentByOrder paymentByOrder = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), docId, PaymentByOrder.class);
        if (contract != null && paymentByOrder.getContract().equals(contract.getGuid())) {
            if (debtsContainId(docId))
                removeFromDebtsById(docId);
            else
                paymentsByOrders.add(paymentByOrder);
        } else {
            paymentsByOrders.clear();
            contract = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), paymentByOrder.getContract(), Contract.class);
            paymentsByOrders.add(paymentByOrder);
        }
        setLabel();
    }

    @Subscribe
    public void onContractSelected(ContractSelectedEvent event) {
        Contract selectedContract = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), event.getContractId(), Contract.class);
        paymentsByOrders = new ArrayList<>();

        if (contract != null && contract.getGuid().equals(selectedContract.getGuid())) {
            RealmResults<Contract> contracts = realmExchangeOperations.getContractsForCounterAgent(getMeteorRealm(), selectedItemsHolder.getSelectedCounterAgentGuid());
            if (contracts.size() != 1) {
                contract = null;
                fifoEnabled = false;
                payment = null;
            } else {
                fifoEnabled = true;
                contract = contracts.first();
                payment = realmExchangeOperations.getPaymentByContract(getMeteorRealm(), contract);
            }
        } else {
            contract = selectedContract;
            payment = realmExchangeOperations.getPaymentByContract(getMeteorRealm(), contract);
        }
        setLabel();
    }

    private void setLabel() {
        if (contract == null) {
            fifoEnabled = true;
            labelCaption.setText(getResources().getString(R.string.select_realisation));
        } else {
            if (paymentsByOrders.size() == 0) {
                fifoEnabled = true;
                labelCaption.setText(contract.getName());
            } else if (paymentsByOrders.size() == 1) {
                labelCaption.setText(paymentsByOrders.get(0).getDocInfo());
                fifoEnabled = false;
            } else {
                float sum = 0;
                for (PaymentByOrder paymentByOrder : paymentsByOrders)
                    sum += paymentByOrder.getSum();
                labelCaption.setText(String.format(getString(R.string.docs_selected), paymentsByOrders.size(), StringUtils.getMoneyFormat(sum)));
                fifoEnabled = false;
            }
        }
        calcText.setText("0");
    }

    public boolean debtsContainId(String guid) {
        for (PaymentByOrder paymentByOrder : paymentsByOrders)
            if (paymentByOrder.getGuid().equals(guid))
                return true;
        return false;
    }


    public void removeFromDebtsById(String guid) {
        for (int i = 0; i < paymentsByOrders.size(); i++)
            if (paymentsByOrders.get(i).getGuid().equals(guid)) {
                paymentsByOrders.remove(i);
                break;
            }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
