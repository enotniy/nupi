package com.nupi.agent.ui.controllers.planner.activities;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.events.HomeButtonEvent;
import com.nupi.agent.events.OpenGeolocationEvent;
import com.nupi.agent.events.PlannerButtonHomeRouteEvent;
import com.nupi.agent.events.PlannerCreateRouteEvent;
import com.nupi.agent.events.PlannerEditRouteEvent;
import com.nupi.agent.events.PlannerEvent;
import com.nupi.agent.events.PlannerRouteEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.planner.fragments.PlannerEditorFragment;
import com.nupi.agent.ui.controllers.planner.fragments.PlannerEditorRouteFragment;
import com.nupi.agent.ui.controllers.planner.fragments.PlannerFragment;
import com.nupi.agent.ui.controllers.planner.fragments.PlannerRouteFragment;
import com.squareup.otto.Subscribe;


public class PlannerActivity extends NupiActivity {

    public static final String START_PLANNER_TYPE = "START_PLANNER_TYPE";
    public static final String START_PLANNER = "START_PLANNER";
    public static final String START_PLANNER_CREATE_ROUTE = "START_PLANNER_CREATE_ROUTE";
    public static final String START_PLANNER_ROUTE = "START_PLANNER_ROUTE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        NupiFragment nupiFragment = new NupiFragment();
        String type = intent.getStringExtra(START_PLANNER_TYPE);
        if (type == null || type.equals(START_PLANNER))
            nupiFragment = new PlannerFragment();
        else if (type.equals(START_PLANNER_CREATE_ROUTE))
            nupiFragment = new PlannerEditorFragment();
        else if (type.equals(START_PLANNER_ROUTE))
            nupiFragment = new PlannerRouteFragment();

        setContentView(R.layout.activity_planner);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, nupiFragment)
                    .commit();
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

    @Subscribe
    public void onPlannerHomeRouteEvent(PlannerButtonHomeRouteEvent event) {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            this.finish();
    }


    @Subscribe
    public void onPlannerRouteEditEvent(PlannerEditRouteEvent event) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, PlannerEditorRouteFragment.newInstance(event.getRoute()))
                .addToBackStack(STACK_PLANNER_EDIT)
                .commit();
    }


    @Subscribe
    public void onPlannerEvent(PlannerEvent event) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, new PlannerFragment())
                .addToBackStack(STACK_PLANNER)
                .commit();
    }

    @Override
    @Subscribe
    public void onPlannerRouteEvent(PlannerRouteEvent event) {
        super.onPlannerRouteEvent(event);
    }


    @Override
    @Subscribe
    public void onPlannerCreateRouteEvent(PlannerCreateRouteEvent event) {
        super.onPlannerCreateRouteEvent(event);
    }

    @Override
    @Subscribe
    public void onGeolocationButtonEvent(OpenGeolocationEvent event) {
        super.onGeolocationButtonEvent(event);
    }
}
