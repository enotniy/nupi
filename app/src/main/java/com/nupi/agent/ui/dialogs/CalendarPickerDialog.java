package com.nupi.agent.ui.dialogs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.SystemClock;

import com.nupi.agent.R;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.utils.TimeUtils;
import com.squareup.timessquare.CalendarPickerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Pasenchuk Victor on 03.12.15
 */
public class CalendarPickerDialog {

    private long lastClickTime = 0;

    public void show(final NupiFragment nupiFragment, final Date fromDate, final Date toDate, final OnDatesSelectedListener onDatesSelectedListener) {

        // preventing double, using threshold of 1000 ms
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();

        nupiFragment.showProgressDialog();
        final Calendar lastTenYear = Calendar.getInstance();
        lastTenYear.add(Calendar.YEAR, -10);

        final Calendar today = Calendar.getInstance();
        today.add(Calendar.DATE, 1);

        final Calendar maxDateLimit = Calendar.getInstance();
        TimeUtils.truncateDayTime(maxDateLimit);
        maxDateLimit.add(Calendar.DATE, 1);
        final Date maxDate = maxDateLimit.getTime();

        Date toDateTruncated;
        if (toDate != null)
            //MAGIC!!!!
            toDateTruncated = new Date(toDate.getTime() - 100_000);
        else
            toDateTruncated = null;


        final CalendarPickerView dialogView = (CalendarPickerView) nupiFragment.getActivity().getLayoutInflater().inflate(R.layout.calendar_dialog, null, false);

        ArrayList<Date> selectedDates = new ArrayList<>(2);

        if (fromDate != null) {
            selectedDates.add(fromDate);
            if (!fromDate.equals(toDateTruncated))
                selectedDates.add(toDateTruncated);
        }
        dialogView.init(lastTenYear.getTime(), maxDate)
                .inMode(CalendarPickerView.SelectionMode.RANGE)
                .withSelectedDates(selectedDates);


        AlertDialog calendarDialog = new AlertDialog.Builder(nupiFragment.getActivity()).setTitle(nupiFragment.getActivity().getString(R.string.choose_dates_range))
                .setView(dialogView)
                .setPositiveButton(nupiFragment.getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final List<Date> limitationDates = dialogView.getSelectedDates();

                        while (limitationDates.size() > 2) {
                            limitationDates.remove(1);
                        }

                        switch (limitationDates.size()) {
                            case 0:
                                onDatesSelectedListener.onDatesSelected(null, null);
                                break;
                            case 1:
                                onDatesSelectedListener.onDatesSelected(limitationDates.get(0), limitationDates.get(0));
                                break;
                            default:
                                Date date = limitationDates.get(1);
                                date.setHours(23);
                                date.setMinutes(59);
                                date.setSeconds(59);
                                onDatesSelectedListener.onDatesSelected(limitationDates.get(0), date);
                                break;
                        }

                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(nupiFragment.getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        calendarDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialogView.fixDialogDimens();
            }
        });

        nupiFragment.dismissProgressDialog();
        nupiFragment.sendButtonEvent("Calendar Picker", true);

        calendarDialog.show();
    }

    public interface OnDatesSelectedListener {

        void onDatesSelected(Date newFromDate, Date newToDate);

    }


}
