package com.nupi.agent.ui.controllers.general.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.database.models.chat.ChatMessage;
import com.nupi.agent.utils.ChatMessageUtils;
import com.nupi.agent.utils.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

/**
 * Created by User on 20.10.2015
 */


public class ChatSmallMessageAdapter extends RealmBaseAdapter<ChatMessage> {

    @Inject
    NupiPreferences preferences;
    private Context context;

    public ChatSmallMessageAdapter(Activity context, RealmResults<ChatMessage> objects) {
        super(context, objects, true);
        this.context = context;
        ((NupiApp) context.getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        final ChatMessage chatMessage = getItem(position);

        String user_id = preferences.getChatId();
        String nickname = !user_id.equals(chatMessage.getUserFrom().getUser()) ? ChatMessageUtils.getNickname(chatMessage.getUserFrom()) : context.getResources().getString(R.string.i);

        Spanned message = Html.fromHtml(String.format(context.getString(R.string.small_chat_message),
                nickname,
                chatMessage.getMessage() != null ? chatMessage.getMessage() : "",
                (chatMessage.getType().compareTo(ChatMessage.TYPE_GEOLOCATION) == 0 ? context.getString(R.string.geolocation_in_small_chat) : ""),
                (chatMessage.getType().compareTo(ChatMessage.TYPE_IMAGE) == 0 ? context.getString(R.string.img_in_small_chat) : ""))
        );

        if (convertView == null) {
            convertView = getInflatedLayoutForType();
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.textMessage.setText(message);
        Locale locale = new Locale("ru");
        Locale.setDefault(locale);
        viewHolder.timeMessage.setText(TimeUtils.timeShortFormat(TimeUtils.getChatTime(chatMessage.getDate())));
        return convertView;
    }


    private View getInflatedLayoutForType() {
        return LayoutInflater.from(context).inflate(R.layout.view_message_small_chat, null);
    }

    class ViewHolder {
        @InjectView(R.id.textMessage)
        TextView textMessage;

        @InjectView(R.id.timeMessage)
        TextView timeMessage;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
