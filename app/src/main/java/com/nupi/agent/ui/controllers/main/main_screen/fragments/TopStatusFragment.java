package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.application.service.GeolocationService;
import com.nupi.agent.enums.UpdateStatus;
import com.nupi.agent.events.GeolocationConnectedEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.helpers.TemperatureFormatter;
import com.nupi.agent.network.MeteorDataUpdater;
import com.nupi.agent.network.meteor.NupiMeteorApi;
import com.nupi.agent.network.weather.OpenWeatherMapWebService;
import com.nupi.agent.network.weather.models.CurrentWeather;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.utils.Connectivity;
import com.nupi.agent.utils.TelephonyIntents;
import com.nupi.agent.utils.TimeUtils;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class TopStatusFragment extends NupiFragment {

    public static final int SYNC_REFRESH_DELAY_MINUTES = 10;
    public static final int YELLOW_ALERT_DELAY = 1_800_000;
    public static final int RED_ALERT_DELAY = 2 * YELLOW_ALERT_DELAY;
    @Inject
    protected NupiMeteorApi nupiMeteorApi;
    @InjectView(R.id.connectionInfo)
    TextView textViewConnection;
    @InjectView(R.id.syncInfo)
    TextView textViewSync;
    @InjectView(R.id.syncInfoButton)
    ImageView imageViewSyncInfoButton;
    @InjectView(R.id.textViewWeather)
    TextView textViewWeather;
    @InjectView(R.id.status_internet)
    View viewStatusInternet;
    @InjectView(R.id.battery_percent)
    TextView textViewBatteryPercent;
    @InjectView(R.id.charging)
    ImageView imageViewCharging;
    @InjectView(R.id.battery)
    ImageView imageViewBattery;
    @Inject
    GeolocationService geolocationService;
    @Inject
    OpenWeatherMapWebService openWeatherMapWebService;
    private boolean isOnSync = false;
    private Subscription weatherSubscription;
    private Subscription timerSubscription;
    private BroadcastReceiver connectionChangeReceiver;
    private BroadcastReceiver batteryChangeReceiver;
    private int LOCATION_PERMISSIONS_CHECK = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNupiApp().getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_status, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setConnection();
        setDelay();
    }

    @Override
    public void onStart() {
        super.onStart();

        initConnectionChangeReceiver();
        initBatteryChangedReceiver();

        registerConnectionChangeReceiver();
        registerBatteryChangedReceiver();


        timerSubscription = Observable
                .interval(SYNC_REFRESH_DELAY_MINUTES, TimeUnit.MINUTES)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        checkAndUpdateWeather();
                        setDelay();
                    }
                });


        new MeteorDataUpdater(this).updateFromMeteor();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkAndUpdateWeather();
    }

    @Override
    public void onStop() {
        super.onStop();

        unregisterConnectionChangeReceiver();
        unregisterBatteryChangeReceiver();
        if (weatherSubscription != null) {
            weatherSubscription.unsubscribe();
        }
        if (timerSubscription != null) {
            timerSubscription.unsubscribe();
        }


    }


    private void registerBatteryChangedReceiver() {
        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        getActivity().registerReceiver(batteryChangeReceiver, batteryLevelFilter);

    }


    private void initBatteryChangedReceiver() {

        batteryChangeReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                fillBattery(intent);
            }
        };

    }

    private void fillBattery(Intent intent) {
        if (isVisible()) {
            int rawlevel = intent.getIntExtra("level", -1);
            int scale = intent.getIntExtra("scale", -1);
            int level = -1;
            if (rawlevel >= 0 && scale > 0) {
                level = (rawlevel * 100) / scale;
            }
            textViewBatteryPercent.setText(String.format("%d%%", level));

            if (level > 49) {
                imageViewBattery.setImageResource(R.drawable.nupi_battery_white);
                textViewBatteryPercent.setTextColor(getResources().getColor(R.color.text_button));
            } else if (level > 19) {
                imageViewBattery.setImageResource(R.drawable.nupi_battery_yellow);
                textViewBatteryPercent.setTextColor(getResources().getColor(R.color.text_yellow));
            } else {
                imageViewBattery.setImageResource(R.drawable.nupi_battery_red);
                textViewBatteryPercent.setTextColor(getResources().getColor(R.color.text_red_battery));
            }

            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
            imageViewCharging.setVisibility(isCharging ? View.VISIBLE : View.GONE);
        }
    }


    public void checkAndUpdateWeather() {
        if (geolocationService.isConnected()) {

            if (Build.VERSION.SDK_INT > 22 && (ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showToast(R.string.need_permission_location);
                } else {
                    FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSIONS_CHECK);
                }
            } else {
                updateWeather();
            }

        } else
            showToast(R.string.need_permission_location);

    }


    private void initConnectionChangeReceiver() {
        connectionChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setConnection();
            }
        };
    }

    public void registerConnectionChangeReceiver() {
        IntentFilter filter =
                new IntentFilter(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
        filter.addAction(TelephonyIntents.ACTION_DATA_CONNECTION_FAILED);
        filter.addAction(TelephonyIntents.ACTION_SERVICE_STATE_CHANGED);
        getActivity().registerReceiver(connectionChangeReceiver, filter);
    }

    public void unregisterConnectionChangeReceiver() {
        getActivity().unregisterReceiver(connectionChangeReceiver);
    }

    public void unregisterBatteryChangeReceiver() {
        getActivity().unregisterReceiver(batteryChangeReceiver);

    }

    public String getStatusWeather(CurrentWeather currentWeather) {
        Type typeOfHashMap = new TypeToken<Map<Integer, String>>() {
        }.getType();
        Map<Integer, String> map = new Gson().fromJson(getResources().getString(R.string.weather), typeOfHashMap);

        int code = currentWeather.getWeather().get(0).getId();
        if (map.containsKey(code))
            return map.get(code);
        else
            return currentWeather.getWeather().get(0).getDescription();
    }

    private void updateWeather() {


        Location lastLocation = geolocationService.getLastKnownLocation();
        if (lastLocation != null) {

            weatherSubscription =
                    openWeatherMapWebService
                            .fetchCurrentWeather(lastLocation.getLongitude(), lastLocation.getLatitude())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Action1<CurrentWeather>() {
                                @Override
                                public void call(CurrentWeather currentWeather) {
                                    textViewWeather.setText(
                                            String.format(getResources().getString(R.string.weather_status),
                                                    TemperatureFormatter.format(currentWeather.getMain().getTemp().floatValue()),
                                                    getStatusWeather(currentWeather),
                                                    currentWeather.getWind().getSpeed().intValue()));

                                    final String imageUrl = String.format("http://openweathermap.org/img/w/%s.png", currentWeather.getWeather().get(0).getIcon());
                                    Log.i("image", imageUrl);
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    textViewWeather.setText(getActivity().getString(R.string.weather_sync_error));
                                }
                            });

        }
    }


    private void setConnection() {
        textViewConnection.setText(String.format("%s", Connectivity.getConnectionType(getActivity())));
        viewStatusInternet.setBackgroundDrawable(Connectivity.isConnected(getActivity())
                ? Connectivity.isConnectedFast(getActivity())
                ? getResources().getDrawable(R.drawable.selector_status_circle_accepted)
                : getResources().getDrawable(R.drawable.selector_status_circle_send)
                : getResources().getDrawable(R.drawable.selector_status_circle_rejected));
    }

    private void setDelay() {
        if (isVisible()) {
            if (!preferences.isLoggedIn() || preferences.getLastSyncTime() == NupiPreferences.NO_SYNC) {
                imageViewSyncInfoButton.setBackground(getResources().getDrawable(R.drawable.selector_nupi_button_syn_grey));
                setDelayMessage(getString(R.string.no));
            } else {
                long delay = TimeUtils.currentTime() - preferences.getLastSyncTime();
                imageViewSyncInfoButton.setBackground(delay < RED_ALERT_DELAY ? delay < YELLOW_ALERT_DELAY
                        ? getResources().getDrawable(R.drawable.selector_nupi_button_syn_green)
                        : getResources().getDrawable(R.drawable.selector_nupi_button_syn_yellow)
                        : getResources().getDrawable(R.drawable.selector_nupi_button_syn_red));
                setDelayMessage(TimeUtils.dateTimeToString(preferences.getLastSyncTime()));
            }
        }
    }

    private void setDelayMessage(CharSequence message) {
        textViewSync.setText(String.format("%s:\n%s", getString(R.string.sync), message));
    }

    @OnClick(R.id.syncInfo)
    void onSyncClick() {
        if (!isOnSync) {
            isOnSync = true;
            setDelayMessage(getString(R.string.refresh));
            new MeteorDataUpdater(this).updateFromMeteor();
            sendButtonEvent("Sync", true);
        } else {
            sendButtonEvent("Sync", false);
            showToast(R.string.sync_in_process);
        }
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        isOnSync = false;
        if (event.updateStatus == UpdateStatus.UPDATE_FINISHED)
            setDelay();
    }

    @Subscribe
    public void onGeolocationServiceConnectedEvent(GeolocationConnectedEvent event) {
        checkAndUpdateWeather();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSIONS_CHECK) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                updateWeather();
            } else {
                // TODO: 28/02/16 ask permission if it is required!
                showToast(R.string.need_permission_location);
            }
        }
    }

}
