package com.nupi.agent.ui.controllers.main.nomenclature.fragments;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.events.CounterAgentSearchButtonPressedEvent;
import com.nupi.agent.events.DismissNumPadEvent;
import com.nupi.agent.events.HomeButtonEvent;
import com.nupi.agent.events.PricesSearchStartEvent;
import com.nupi.agent.events.ShowPriceNumPadEvent;
import com.nupi.agent.helpers.RecycleViewItemClickSupport;
import com.nupi.agent.network.meteor.OrderItem;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.main.nomenclature.adapters.NomenclatureTreeMeteorAdapter;
import com.nupi.agent.ui.dialogs.YesNoDialog;
import com.squareup.otto.Subscribe;

import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import icepick.State;
import io.realm.RealmResults;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */

public class PricesTreeFragment extends NupiFragment {

    @InjectView(R.id.goodsListView)
    RecyclerView goodsListView;

    @InjectView(R.id.buttonsRibbon)
    HorizontalScrollView buttonsRibbon;

    @InjectView(R.id.hierarchy)
    LinearLayout buttonsContainer;

    @State
    String parentId = "";

    private List<View> hierarchyButtonList;
    private NomenclatureTreeMeteorAdapter nomenclatureTreeMeteorAdapter;

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prices, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hierarchyButtonList = new LinkedList<>();

        List<String> path = preferences.getKeepBreadcrumbs() ? preferences.getBreadCrumbs() : null;
        if (path == null || path.size() == 0) {
            path = new LinkedList<>();
            path.add(CounterAgent.ROOT_GUID);
            preferences.setBreadCrumbs(path);
        } else {
            for (int i = 1; i < path.size(); i++) {
                Nomenclature folder = realmExchangeOperations.getRealmObjectByGuid(getMeteorRealm(), path.get(i), Nomenclature.class);
                if (folder == null || !folder.isFolder()) {
                    path = new LinkedList<>();
                    path.add(Nomenclature.ROOT_GUID);
                    preferences.setBreadCrumbs(path);
                    break;
                }
            }
        }

        String parentGuid = path.get(path.size() - 1);
        nomenclatureTreeMeteorAdapter = new NomenclatureTreeMeteorAdapter(getActivity(), getMeteorRealm(), realmExchangeOperations, orderApi, selectedItemsHolder, getNomenclatureChildren(parentGuid));
        for (int i = 0; i < path.size() - 1; i++) {
            addButton(path.get(i));
        }
        openChildNode(parentGuid);

        goodsListView.setAdapter(nomenclatureTreeMeteorAdapter);
        goodsListView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
        goodsListView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        goodsListView.setLayoutManager(linearLayoutManager);

        RecycleViewItemClickSupport
                .addTo(goodsListView)
                .setOnItemClickListener(new RecycleViewItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        if (position > -1)
                            onItemClick(position);
                    }
                });


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && (keyEvent.getAction() == KeyEvent.ACTION_UP)) {
                    onHomeButtonPress();
                    return true;
                }
                return false;
            }
        });

    }

    public RealmResults<Nomenclature> getNomenclatureChildren(String parentId) {
        return realmExchangeOperations.getNomenclatureChildren(getMeteorRealm(), parentId);
    }

    private void openChildNode(String parentId) {
        this.parentId = parentId;
        addButton(parentId);
        nomenclatureTreeMeteorAdapter.updateData(getNomenclatureChildren(parentId));
        bus.post(new DismissNumPadEvent());
    }

    private void saveFolder() {
        List<String> path = new LinkedList<>();
        for (View view : hierarchyButtonList) {
            path.add((String) view.getTag());
        }
        preferences.setBreadCrumbs(path);
    }

    private void addButton(final String priceId) {
        final int buttonCount = hierarchyButtonList.size();

        final View breadCrumbsView = initNewBreadCrumbsNode(priceId);

        breadCrumbsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Answers.getInstance().logCustom(new CustomEvent("Node Button, breadcrumbs"));
                nomenclatureTreeMeteorAdapter.updateData(getNomenclatureChildren(priceId));
                bus.post(new DismissNumPadEvent());
                for (int i = hierarchyButtonList.size() - 1; i > buttonCount; i--) {
                    buttonsContainer.removeView(hierarchyButtonList.remove(i));
                }
                recalculateColors();
                breadCrumbsView.setClickable(false);
                scrollToRight();
                saveFolder();
            }
        });


        hierarchyButtonList.add(breadCrumbsView);
        buttonsContainer.addView(breadCrumbsView);

        recalculateColors();

        scrollToRight();
        saveFolder();

    }

    private void recalculateColors() {
        for (int i = 0; i < hierarchyButtonList.size(); i++) {
            final ViewHolder viewHolder = new ViewHolder(hierarchyButtonList.get(i));
            switch (hierarchyButtonList.size() - i - 1) {
                case 0:
                    viewHolder.backgroundView.setClickable(false);
                    viewHolder.imageView.setVisibility(View.GONE);
                    viewHolder.backgroundView.setBackgroundColor(getResources().getColor(R.color.transparent));
                    break;
                case 1:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_1);
                    break;
                case 2:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_2);
                    break;
                case 3:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_3);
                    break;
                case 4:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_4);
                    break;
                default:
                    viewHolder.backgroundView.setClickable(true);
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    viewHolder.backgroundView.setBackgroundResource(R.drawable.selector_nupi_bread_crumbs_5);
                    break;
            }
        }
    }

    @NonNull
    private View initNewBreadCrumbsNode(String priceId) {
        final View breadCrumbsView = getActivity().getLayoutInflater().inflate(!Nomenclature.ROOT_GUID.equals(priceId) ? R.layout.view_bread_crumbs : R.layout.view_bread_crumbs_root, null);
        breadCrumbsView.setTag(priceId);
        final ViewHolder viewHolder = new ViewHolder(breadCrumbsView);

        breadCrumbsView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT));


        final Nomenclature nomenclature = realmExchangeOperations.getNomenclatureById(getMeteorRealm(), priceId);
        if (Nomenclature.ROOT_GUID.equals(priceId)) {
            viewHolder.textView.setText("");
        } else if (nomenclature != null)
            viewHolder.textView.setText(nomenclature.getName());
        else
            viewHolder.textView.setText("/");
        return breadCrumbsView;
    }

    private void scrollToRight() {
        ViewTreeObserver vto = buttonsRibbon.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (isVisible()) {
                    buttonsRibbon.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    buttonsRibbon.scrollTo(buttonsContainer.getWidth(), 0);
                }
            }
        });
    }


    public void onItemClick(int position) {
        Answers.getInstance().logCustom(new CustomEvent("Item click, price tree"));
        Nomenclature item = nomenclatureTreeMeteorAdapter.getItem(position);
        if (!item.isFolder()) {
            OrderItem orderItem = orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedCounterAgentGuid()).getGood(item.getGuid());

            Double price = realmExchangeOperations.getPrice(getMeteorRealm(), item.getStoreUnit());
            if (price == null || price <= 0f) {
                Toast.makeText(getActivity(), getString(R.string.no_price), Toast.LENGTH_SHORT).show();
                return;
            }


            selectedItemsHolder.setSelectedGood(orderItem != null ? orderItem : new OrderItem(item.getGuid()));
            boolean enable = nomenclatureTreeMeteorAdapter.setSelectedIndex(position);
            nomenclatureTreeMeteorAdapter.notifyDataSetChanged();
            if (enable)
                bus.post(new ShowPriceNumPadEvent());
            else
                bus.post(new DismissNumPadEvent());
        } else
            openChildNode(item.getGuid());
    }

    @OnClick(R.id.home_button)
    void onHomeButtonPress() {
        sendButtonEvent("Home", true);
        new YesNoDialog(getActivity(), getString(R.string.incomplete_order_warning)) {
            @Override
            public void handlePositiveButton() {
                bus.post(new HomeButtonEvent());
            }
        }.show();
    }


    @OnClick(R.id.linearLayoutSearch)
    void onSearchButtonClick() {
        sendButtonEvent("Search Price", true);
        bus.post(new PricesSearchStartEvent());
        bus.post(new CounterAgentSearchButtonPressedEvent());
    }

    @Subscribe
    public void onDismissNumPad(DismissNumPadEvent event) {
        // TODO: 23.12.15 add quantities
//        pricesAdapter.setOrderedItems(orderApi.getOrderRequestForClient(selectedItemsHolder.getSelectedClient()));
        nomenclatureTreeMeteorAdapter.notifyDataSetChanged();
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'view_bread_crumbs.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.textView)
        TextView textView;
        @InjectView(R.id.imageView)
        ImageView imageView;
        @InjectView(R.id.backgroundView)
        LinearLayout backgroundView;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
