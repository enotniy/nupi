package com.nupi.agent.ui.controllers.planner.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.nupi.agent.R;
import com.nupi.agent.database.models.planner.ClientInfo;
import com.nupi.agent.database.models.planner.PlannerRoute;
import com.nupi.agent.utils.TimeUtils;

import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 10.09.14 in IntelliJ Idea
 */


public class DraggablePointsAdapter extends RecyclerView.Adapter<DraggablePointsAdapter.ViewHolder> implements DraggableItemAdapter<DraggablePointsAdapter.ViewHolder> {

    private static final String TAG = "MyDraggableItemAdapter";
    protected Context context;
    protected List<ClientInfo> points;
    private PlannerRoute plannerRoute;
    private int selectedIndex = -1;

    public DraggablePointsAdapter(Context context, PlannerRoute plannerRoute) {
        points = new LinkedList<>();
        for (ClientInfo clientInfo : plannerRoute.getClientsList())
            points.add(clientInfo);
        this.context = context;
        this.plannerRoute = plannerRoute;
        setHasStableIds(true);
    }

    public static boolean hitTest(View v, int x, int y) {
        final int tx = (int) (ViewCompat.getTranslationX(v) + 0.5f);
        final int ty = (int) (ViewCompat.getTranslationY(v) + 0.5f);
        final int left = v.getLeft() + tx;
        final int right = v.getRight() + tx;
        final int top = v.getTop() + ty;
        final int bottom = v.getBottom() + ty;

        return (x >= left) && (x <= right) && (y >= top) && (y <= bottom);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_planner_editor_create, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ClientInfo item = points.get(position);

        holder.setClientInfo(item);
        holder.number.setText(String.valueOf(position + 1));
        holder.shop.setText(String.valueOf(item.getClientName()));
        if (item.getWorkingBeginTime() != null && item.getWorkingEndTime() != null)
            holder.workingTime.setText(String.format(context.getString(R.string.working_time), TimeUtils.timeFormat(item.getWorkingBeginTime()), TimeUtils.timeFormat(item.getWorkingEndTime())));
        else
            holder.workingTime.setText("");

        if (item.getLunchBeginTime() != null && item.getLunchEndTime() != null)
            holder.lunchTime.setText(String.format(context.getString(R.string.lunch_time), TimeUtils.timeFormat(item.getLunchBeginTime()), TimeUtils.timeFormat(item.getLunchEndTime())));
        else
            holder.lunchTime.setText("");

        holder.visitFrequency.setText(item.getVisitFrequency() != null ? String.valueOf(item.getVisitFrequency()) : "");
        holder.responsiblePerson.setText(item.getResponsiblePerson() != null ? String.valueOf(item.getResponsiblePerson()) : "");
        String phone = item.getContacts() != null ? String.valueOf(item.getContacts()) : "";
        String additional_phone = item.getContacts() != null ? String.valueOf(item.getAdditionalPhone()) : "";
        if (!phone.equals("") && !additional_phone.equals(""))
            phone += "\n";
        String email = item.getContacts() != null ? String.valueOf(item.getEmail()) : "";
        if ((!phone.equals("") || !additional_phone.equals("")) && !email.equals(""))
            additional_phone += "\n";
        holder.contacts.setText(String.format("%s%s%s", phone, additional_phone, email));

        // set background resource (target view ID: container)
        final int dragState = holder.getDragStateFlags();

        if (((dragState & DraggableItemConstants.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;

            if ((dragState & DraggableItemConstants.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.drawable.selector_list_item_selected;

                // need to clear drawable state here to get correct appearance of the dragging item.
                Drawable drawable = holder.container.getBackground();
                if (drawable != null)
                    drawable.setState(new int[]{});
            } else if ((dragState & DraggableItemConstants.STATE_FLAG_DRAGGING) != 0) {
                bgResId = R.drawable.selector_list_item_selected;
            } else {
                bgResId = R.drawable.selector_list_item;
            }

            holder.container.setBackgroundResource(bgResId);
        }

        if (position == selectedIndex)
            holder.container.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.selector_list_item_selected));
        else
            holder.container.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.selector_list_item));

    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex == this.selectedIndex ? -1 : selectedIndex;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return points.size();
    }

    @Override
    public long getItemId(int position) {
        return points.get(position).hashCode();
    }

    public String getPlannerRouteName() {
        return plannerRoute != null ? plannerRoute.getRouteName() : null;
    }


    public String getSelectedItemGuid() {
        if (selectedIndex > -1)
            return points.get(selectedIndex).getClientId();
        else
            return null;
    }

    public void removeSelectedPoint() {
        if (getSelectedIndex() > -1) {
            points.remove(getSelectedIndex());
            setSelectedIndex(-1);
            notifyDataSetChanged();
        }
    }

    public void upSelectedPoint() {
        int position = getSelectedIndex();
        if (position > 0) {
            final ClientInfo item = points.remove(position);
            points.add(position - 1, item);
            notifyItemMoved(position, position - 1);
            setSelectedIndex(position - 1);
            notifyDataSetChanged();
        }
    }

    public void downSelectedPoint() {
        int position = getSelectedIndex();
        if (position > -1 && position < points.size() - 1) {
            final ClientInfo item = points.remove(position);
            points.add(position + 1, item);
            notifyItemMoved(position, position + 1);
            setSelectedIndex(position + 1);
            notifyDataSetChanged();
        }
    }


    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        Log.d(TAG, "onMoveItem(fromPosition = " + fromPosition + ", toPosition = " + toPosition + ")");

        if (fromPosition == toPosition)
            return;

        final ClientInfo item = points.remove(fromPosition);
        points.add(toPosition, item);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public boolean onCheckCanStartDrag(ViewHolder holder, int position, int x, int y) {
        // x, y --- relative from the itemView's top-left
        final View containerView = holder.container;
        final View dragHandleView = holder.container;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

        return hitTest(dragHandleView, x - offsetX, y - offsetY);
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(ViewHolder holder, int position) {
        // no drag-sortable range specified
        return null;
    }


    public List<ClientInfo> getClients() {
        return points;
    }

    class ViewHolder extends AbstractDraggableItemViewHolder {

        @InjectView(R.id.number)
        TextView number;
        @InjectView(R.id.shop)
        TextView shop;
        @InjectView(R.id.working_time)
        TextView workingTime;
        @InjectView(R.id.lunch_time)
        TextView lunchTime;
        @InjectView(R.id.info)
        LinearLayout info;
        @InjectView(R.id.visit_frequency)
        TextView visitFrequency;
        @InjectView(R.id.responsible_person)
        TextView responsiblePerson;
        @InjectView(R.id.contacts)
        TextView contacts;
        @InjectView(R.id.container)
        FrameLayout container;
        @InjectView(R.id.clientRow)
        RelativeLayout clientRow;
        ClientInfo clientInfo;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public ClientInfo getClientInfo() {
            return clientInfo;
        }

        public void setClientInfo(ClientInfo clientInfo) {
            this.clientInfo = clientInfo;
        }

        @OnClick(R.id.container)
        void onShopClick() {
            setSelectedIndex(points.indexOf(clientInfo));
        }
    }
}

