package com.nupi.agent.ui.controllers.analytics.activities;

import android.os.Bundle;

import com.nupi.agent.R;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.controllers.analytics.fragments.PickerClientFragment;
import com.nupi.agent.ui.controllers.analytics.fragments.PickerPriceFragment;
import com.squareup.otto.Subscribe;


public class PickerActivity extends NupiActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);
        int type = getIntent().getIntExtra(Picker.TYPE_PICKER, Picker.TYPE_PICKER_GOODS);
        if (savedInstanceState == null)
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, type == Picker.TYPE_PICKER_GOODS ? new PickerPriceFragment() : new PickerClientFragment())
                    .commit();

    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        reactOnUpdateWorkflowEvent(event);
    }

}
