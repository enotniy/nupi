package com.nupi.agent.ui.controllers.main.cash.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.nupi.agent.R;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.models.CounterAgentGroup;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.events.ContractSelectedEvent;
import com.nupi.agent.events.RealisationSelectedEvent;
import com.nupi.agent.utils.StringUtils;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Pasenchuk Victor on 16.10.14
 */
public class ContractAdapter extends AbstractExpandableItemAdapter<ContractAdapter.ContractViewHolder, ContractAdapter.PaymentViewHolder> {

    @Inject
    RealmExchangeOperations meteorOperations;
    @Inject
    Bus bus;

    private List<CounterAgentGroup> counterAgentGroups;
    private Context context;
    private ArrayList<String> selectedDocs = new ArrayList<>();
    private String selectedContract = null;
    private String currentContract = null;

    public ContractAdapter(Activity activity, List<CounterAgentGroup> counterAgentGroups) {
        this.context = activity;
        this.counterAgentGroups = counterAgentGroups;
        ((NupiApp) activity.getApplication()).getAppComponent().inject(this);
        setHasStableIds(true);
    }

    @Override
    public void onBindGroupViewHolder(ContractViewHolder holder, int groupPosition, int viewType) {
        final Contract contract = counterAgentGroups.get(groupPosition).getContract();
        holder.delay.setText(String.valueOf(contract.isDebtDaysControl() ? contract.getDebtDaysMax() : 0));
        holder.docName.setText(contract.getName());
        holder.debt.setText(StringUtils.getMoneyFormat(counterAgentGroups.get(groupPosition).getPaymentSum()));
        holder.itemView.setClickable(true);
        holder.setContract(contract.getGuid());
        if (selectedContract != null && selectedContract.equals(contract.getGuid()))
            holder.clientRow.setBackground(context.getResources().getDrawable(R.color.background_list_selected_pressed));
        else
            holder.clientRow.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item));
    }

    @Override
    public void onBindChildViewHolder(PaymentViewHolder holder, int groupPosition, int childPosition, int viewType) {

        PaymentByOrder paymentByOrder = counterAgentGroups.get(groupPosition).getPaymentsByOrders().get(childPosition);
        holder.debt.setText(String.valueOf(paymentByOrder.getSum()));
        holder.docName.setText(paymentByOrder.getDocInfo());
        double sum = paymentByOrder.getSum();
        holder.debt.setText(StringUtils.getMoneyFormat(sum));
        holder.debt.setTextColor(context.getResources().getColor(sum < 0 ? R.color.red : R.color.text_list));
        holder.setPaymentByOrder(paymentByOrder);
        setDocDelay(groupPosition, childPosition, holder);
        if (selectedDocs.contains(paymentByOrder.getGuid()) && selectedContract == null)
            holder.clientRow.setBackground(context.getResources().getDrawable(R.color.background_list_selected_pressed));
        else
            holder.clientRow.setBackground(context.getResources().getDrawable(R.drawable.selector_list_item_selected));
    }

    @Override
    public int getGroupCount() {
        return counterAgentGroups.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return counterAgentGroups.get(groupPosition).getPaymentsByOrders().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return counterAgentGroups.get(groupPosition).hashCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public ContractViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_contract, parent, false);
        return new ContractViewHolder(v);
    }

    @Override
    public PaymentViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item_document, parent, false);
        return new PaymentViewHolder(v);
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(ContractViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return false;
    }

    private void setDocDelay(int groupPosition, int childPosition, PaymentViewHolder holder) {
        ArrayList<Long> delays = counterAgentGroups.get(groupPosition).getDelays();
        long delay = 0;
        if (delays.size() > childPosition)
            delay = delays.get(childPosition);
        long credit = counterAgentGroups.get(groupPosition).getDelay();

        final TextView textViewDelay = holder.delay;
        if (credit - delay < 0) {
            writeDelay(textViewDelay, delay - credit);
            textViewDelay.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            writeDelay(textViewDelay, 0);
            textViewDelay.setTextColor(context.getResources().getColor(R.color.text_list));
        }
    }

    private void writeDelay(TextView textView, long delayed) {
        textView.setText(String.format("%d", delayed));
    }

    public void addSelectedIndex(String docsGuid, String contract) {
        if (selectedContract != null)
            selectedContract = null;

        if (currentContract == null || !contract.equals(currentContract)) {
            currentContract = contract;
            selectedDocs.clear();
            selectedDocs.add(docsGuid);
        } else {
            if (selectedDocs.contains(docsGuid)) {
                selectedDocs.remove(docsGuid);
                if (selectedDocs.size() == 0)
                    selectedContract = currentContract;
            } else
                selectedDocs.add(docsGuid);
        }
    }

    public int getCountSelectedDocs() {
        return selectedDocs.size();
    }

    public void addSelectedContract(String docsGuid) {
        selectedDocs = new ArrayList<>();
        if (selectedContract != null && selectedContract.equals(docsGuid))
            selectedContract = null;
        else
            selectedContract = docsGuid;
    }

    public String getSelectedContract() {
        return selectedContract;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_counter_agents_group_view.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class ContractViewHolder extends AbstractExpandableItemViewHolder {
        @InjectView(R.id.docName)
        TextView docName;
        @InjectView(R.id.debt)
        TextView debt;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;
        @InjectView(R.id.delay)
        TextView delay;

        private String contract;

        ContractViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        public void setContract(String contract) {
            this.contract = contract;
        }

        @OnClick(R.id.clientRow)
        void onItemClick() {
            if (contract != null) {
                bus.post(new ContractSelectedEvent(contract));
            }
        }
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'list_item_counter_agents.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    class PaymentViewHolder extends AbstractExpandableItemViewHolder {
        @InjectView(R.id.docName)
        TextView docName;
        @InjectView(R.id.delay)
        TextView delay;
        @InjectView(R.id.debt)
        TextView debt;
        @InjectView(R.id.clientRow)
        LinearLayout clientRow;

        private PaymentByOrder paymentByOrder;

        PaymentViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        @OnClick(R.id.clientRow)
        void onItemClick() {
            if (paymentByOrder != null) {
                bus.post(new RealisationSelectedEvent(paymentByOrder.getGuid()));
            }
        }

        public void setPaymentByOrder(PaymentByOrder paymentByOrder) {
            this.paymentByOrder = paymentByOrder;
        }
    }
}