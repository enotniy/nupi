package com.nupi.agent.ui.controllers.analytics.fragments;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.ExpandedPayment;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import butterknife.InjectView;
import io.realm.RealmChangeListener;

/**
 * Created by User on 15.10.2015
 */

public abstract class BaseAnalyticsFragment extends NupiFragment implements RealmChangeListener {

    private static final int VIEW_WEEKS = 5;
    public int observedWeeks = 5;
    @InjectView(R.id.bar_chart)
    CombinedChart barChart;

    @InjectView(R.id.period)
    TextView textViewPeriod;

    private float[] points;
    private String[] legend;
    private String period;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_analytics, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getRegistryRealm().addChangeListener(this);
    }

    public void setLabel() {
        textViewPeriod.setText(period);
        barChart.setDescription("");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        getRegistryRealm().removeChangeListener(this);
        super.onPause();
    }

    protected void initChart() {
        points = new float[observedWeeks];
        legend = new String[observedWeeks];
        Calendar calendar = TimeUtils.getCalendarForAnalytic();
        getLastWeekAnalytics(calendar);
        for (int i = observedWeeks - 2; i >= 0; i--)
            getWeekAnalytics(calendar, i);
        period = legend[0] + " - " + legend[observedWeeks - 1];
        barChart.setDrawBarShadow(false);
        barChart.setMaxVisibleValueCount(Integer.MAX_VALUE);
        barChart.setDrawGridBackground(false);
        barChart.setGridBackgroundColor(R.color.transparent);
        barChart.setDrawBorders(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setDrawHighlightArrow(true);
        barChart.setDragEnabled(true);
        barChart.setVisibleXRange(VIEW_WEEKS, VIEW_WEEKS);
        barChart.moveViewToX(observedWeeks);
        barChart.setVisibleXRangeMinimum(VIEW_WEEKS);
        barChart.setTouchEnabled(true);
        barChart.setPinchZoom(true);
        barChart.setAutoScaleMinMaxEnabled(true);
        barChart.setScaleYEnabled(false);


        Legend legend = barChart.getLegend();
        legend.setEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGridColor(getResources().getColor(R.color.background_title_cell));
        xAxis.setDrawAxisLine(true);
        xAxis.setAxisLineColor(getResources().getColor(R.color.grey));
        xAxis.setTextColor(getResources().getColor(R.color.text_chart));
        xAxis.setTextSize(convertDpToPixel(getResources().getDimension(R.dimen.text_small)));

        YAxis yAxisRight = barChart.getAxisRight();
        barChart.getAxisRight().setEnabled(false);

        yAxisRight.setTextColor(getResources().getColor(R.color.background_title_cell));

        YAxis yAxisLeft = barChart.getAxisLeft();
        yAxisLeft.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxisLeft.setDrawTopYLabelEntry(true);
        yAxisLeft.setDrawAxisLine(false);
        yAxisLeft.setGridColor(getResources().getColor(R.color.background_title_cell));
        yAxisLeft.setTextColor(getResources().getColor(R.color.text_chart));
        yAxisLeft.setTextSize(convertDpToPixel(getResources().getDimension(R.dimen.text_small)));


        float max = 0;
        for (float i : points) {
            max = i > max ? i : max;
        }
        yAxisLeft.setAxisMaxValue(max * 1.3f);
        yAxisLeft.setSpaceTop(30);
    }

    private void getLastWeekAnalytics(Calendar calendar) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        final Date endDate = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, -dayOfWeek + 2);
        loadWeekAnalytics(calendar, observedWeeks - 1, endDate);
    }

    private void getWeekAnalytics(Calendar calendar, int position) {
        final Date endDate = calendar.getTime();
        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        loadWeekAnalytics(calendar, position, endDate);
    }

    private void loadWeekAnalytics(Calendar calendar, int position, Date endDate) {
        final Date startDate = calendar.getTime();
        double sum = realmExchangeOperations.getExpandedPaymentByPeriod(getMeteorRealm(), startDate, endDate, ExpandedPayment.TYPE_RECEIPT);
        points[position] = (float) sum;
        legend[position] = TimeUtils.dateFormatShort(startDate);
    }

    protected void setBarData() {
        CombinedData combinedData = new CombinedData(legend);
        combinedData.setData(generateBarData(observedWeeks));
        barChart.setData(combinedData);
    }

    protected void setLineData() {
        CombinedData combinedData = new CombinedData(legend);
        combinedData.setData(generateLineData(observedWeeks));
        barChart.setData(combinedData);
    }

    private BarData generateBarData(int count) {
        ArrayList<String> xVals = new ArrayList<>();
        xVals.addAll(Arrays.asList(legend).subList(0, count));

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            yVals1.add(new BarEntry(points[i], i));
        }

        BarDataSet set1 = new BarDataSet(yVals1, "DataSet");
        set1.getYVals();
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1.setDrawValues(true);
        set1.setValueTextColor(getResources().getColor(R.color.text_chart_value));
        set1.setHighlightEnabled(false);
        set1.setBarSpacePercent(30f);
        set1.setColor(getResources().getColor(R.color.text_other_not_active));

        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData barData = new BarData(xVals, dataSets);
        barData.setValueTextSize(convertDpToPixel(getResources().getDimension(R.dimen.text_bar_small)));

        barChart.setDrawMarkerViews(true);
        return barData;
    }

    public LineData generateLineData(int count) {

        LineData d = new LineData();

        ArrayList<Entry> entries = new ArrayList<>();

        for (int index = 0; index < count; index++)
            entries.add(new Entry(points[index], index));

        LineDataSet set = new LineDataSet(entries, "Line DataSet");
        set.setColor(getResources().getColor(R.color.text_other_not_active));
        set.setLineWidth(2.5f);
        set.setCircleColor(R.color.text_other_not_active);
        set.setCircleSize(5f);
        set.setFillColor(R.color.text_other_not_active);
        set.setDrawCubic(false);
        set.setDrawValues(true);
        set.setValueTextSize(getResources().getDimension(R.dimen.text_analytics));
        set.setHighlightEnabled(false);
        set.setValueTextColor(getResources().getColor(R.color.text_chart_value));

        set.setAxisDependency(YAxis.AxisDependency.LEFT);

        d.addDataSet(set);

        barChart.setDrawMarkerViews(true);
        return d;
    }

    public float convertDpToPixel(float dp) {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        final float px = dp / (metrics.densityDpi / 160f);
        return px;
    }

}
