package com.nupi.agent.ui.controllers.main.main_screen.fragments;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.nupi.agent.R;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.enums.SortType;
import com.nupi.agent.enums.UpdateStatus;
import com.nupi.agent.events.ClientSelectedEvent;
import com.nupi.agent.events.CounterAgentSearchButtonPressedEvent;
import com.nupi.agent.events.SearchStringEvent;
import com.nupi.agent.events.UpdateWorkflowEvent;
import com.nupi.agent.helpers.RecycleViewItemClickSupport;
import com.nupi.agent.network.meteor.OrderRequest;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.general.fragments.ButtonsFragment;
import com.nupi.agent.ui.controllers.main.main_screen.adapters.CounterAgentsMeteorAdapter;
import com.nupi.agent.ui.controllers.main.nomenclature.fragments.PriceReceiptFragment;
import com.nupi.agent.ui.dialogs.SelectClientModeDialog;
import com.squareup.otto.Subscribe;

import butterknife.InjectView;
import butterknife.OnClick;
import icepick.State;
import io.realm.Sort;

/**
 * Created by Pasenchuk Victor on 30.09.14
 */
public class CounterAgentsFragment extends NupiFragment {

    @InjectView(R.id.shopsListView)
    RecyclerView clientsListView;
    @InjectView(R.id.clientRow)
    LinearLayout linearLayoutNewPoint;
    @State
    String filterText = "";
    @InjectView(R.id.sortByName)
    ImageView sortByName;
    @InjectView(R.id.sortDebt)
    ImageView sortDebt;
    @InjectView(R.id.searchCounterAgent)
    RelativeLayout searchCounterAgent;
    @InjectView(R.id.homeButton)
    ImageView homeButton;
    @InjectView(R.id.clientSymbol)
    ImageView imageViewClientSymbol;


    // private ClientsAdapter clientsAdapter;
    private CounterAgentsMeteorAdapter counterAgentsMeteorAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_clients_left, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.two_buttons, new ButtonsFragment())
                .commit();


        RecycleViewItemClickSupport
                .addTo(clientsListView)
                .setOnItemClickListener(new RecycleViewItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        selectedItemsHolder.setSelectedCounterAgentGuid(counterAgentsMeteorAdapter.getItem(position).getGuid());
                        SelectClientModeDialog selectClientModeDialog = new SelectClientModeDialog(getActivity(), bus);
                        selectClientModeDialog.show();
                    }
                });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        clientsListView.setLayoutManager(linearLayoutManager);

        setNewPoint();
        initRecycleView();
        try {
            setClientsAdapter();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        setNewPoint();
        setSortButtonName(preferences.getSortType() == SortType.ABC_ORDER, preferences.getSortOrder());
        setSortButtonDebt(preferences.getSortType() == SortType.DEBT_ORDER, preferences.getSortOrder());
    }

    public void setNewPoint() {
        OrderRequest orderRequest = orderApi.getOrderRequestForClient(CounterAgent.NEW_POINT);
        if (orderRequest != null && orderRequest.getOrderItems().size() > 0) {
            imageViewClientSymbol.setVisibility(View.VISIBLE);
            linearLayoutNewPoint.setBackground(getResources().getDrawable(R.drawable.selector_list_item_selected));
        } else {
            imageViewClientSymbol.setVisibility(View.GONE);
            linearLayoutNewPoint.setBackground(getResources().getDrawable(R.drawable.selector_list_item));
        }
    }

    private void initRecycleView() {
        clientsListView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.divider_nupi_horizontal_list), true));
        clientsListView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getActivity(), R.drawable.material_shadow_z1)));
    }

    private void setClientsAdapter() {
        counterAgentsMeteorAdapter = new CounterAgentsMeteorAdapter(getMeteorRealm(), realmExchangeOperations, orderApi, preferences.getSortOrder(), preferences.getSortType());
        clientsListView.setAdapter(counterAgentsMeteorAdapter);
    }


    @OnClick(R.id.clientRow)
    public void onNewPointClick() {
        if (preferences.isLoggedIn()) {
            selectedItemsHolder.setSelectedCounterAgentGuid(PriceReceiptFragment.NEW_COUNTER_AGENT_GUID);
            bus.post(new ClientSelectedEvent());
        }
    }

    @OnClick(R.id.homeButton)
    void onHomeButtonClick() {
        if (preferences.getSearchVisible()) {
            searchCounterAgent.setVisibility(View.VISIBLE);
            if (preferences.isLoggedIn()) {
                bus.post(new CounterAgentSearchButtonPressedEvent());
            }
        }
    }

    @Subscribe
    public void onCounterAgentsSearchButtonPressedEvent(CounterAgentSearchButtonPressedEvent event) {
        if (preferences.getSearchVisible()) {
            searchCounterAgent.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.sortByName)
    void onSortByNameButtonClick() {
        if (counterAgentsMeteorAdapter != null) {
            sendButtonEvent("Sort ABC", true);
            if (counterAgentsMeteorAdapter.getSortType() == SortType.ABC_ORDER) {
                counterAgentsMeteorAdapter.changeOrder();
            }

            setSortButtonName(true, counterAgentsMeteorAdapter.getOrder());
            setSortButtonDebt(false, Sort.DESCENDING);

            counterAgentsMeteorAdapter.sort(SortType.ABC_ORDER);
            preferences.setSortOrder(counterAgentsMeteorAdapter.getOrder());
            preferences.setSortType(SortType.ABC_ORDER);
        } else {
            sendButtonEvent("Sort ABC", false);
        }
    }

    @OnClick(R.id.sortDebt)
    void onSortByDebtButtonClick() {
        if (counterAgentsMeteorAdapter != null) {
            sendButtonEvent("Sort Debt", true);
            if (counterAgentsMeteorAdapter.getSortType() == SortType.DEBT_ORDER)
                counterAgentsMeteorAdapter.changeOrder();

            setSortButtonDebt(true, counterAgentsMeteorAdapter.getOrder());
            setSortButtonName(false, counterAgentsMeteorAdapter.getOrder());

            counterAgentsMeteorAdapter.sort(SortType.DEBT_ORDER);
            preferences.setSortOrder(counterAgentsMeteorAdapter.getOrder());
            preferences.setSortType(SortType.DEBT_ORDER);
        } else {
            sendButtonEvent("Sort Debt", false);
        }
    }

    @OnClick(R.id.searchCounterAgent)
    void onSearchButtonClick() {
        sendButtonEvent("Search Client", true);
        searchCounterAgent.setVisibility(preferences.getSearchVisible() ? View.VISIBLE : View.GONE);
        bus.post(new CounterAgentSearchButtonPressedEvent());
    }

    @Subscribe
    public void filterByString(SearchStringEvent searchStringEvent) {
        String searchString = searchStringEvent.getSearchString().toString();
        if (!searchString.equals(filterText)) {
            filterText = searchString;
            counterAgentsMeteorAdapter.setStringFilter(searchString);
        }
    }

    @Subscribe
    public void onUpdateWorkflowEvent(UpdateWorkflowEvent event) {
        if (event.updateStatus == UpdateStatus.UPDATE_FINISHED) {
            if (counterAgentsMeteorAdapter == null)
                setClientsAdapter();
            else
                counterAgentsMeteorAdapter.notifyDataSetChanged();
        }
    }


    public void setSortButtonDebt(boolean active, Sort order) {
        if (active) {
            switch (order) {
                case ASCENDING:
                    sortDebt.setImageResource(R.drawable.nupi_sort_1_9);
                    break;
                case DESCENDING:
                    sortDebt.setImageResource(R.drawable.nupi_sort_9_1);
                    break;
            }
            sortDebt.setBackgroundColor(getResources().getColor(R.color.background_list));
        } else {
            sortDebt.setImageResource(R.drawable.nupi_sort_9_1_disable);
            sortDebt.setBackgroundResource(R.drawable.selector_nupi_blue_dark);
        }
    }

    public void setSortButtonName(boolean active, Sort order) {
        if (active) {
            switch (order) {
                case ASCENDING:
                    sortByName.setImageResource(R.drawable.nupi_sort_a_ya_active);
                    break;
                case DESCENDING:
                    sortByName.setImageResource(R.drawable.nupi_sort_ya_a_active);
                    break;
            }
            sortByName.setBackgroundColor(getResources().getColor(R.color.background_list));
        } else {
            sortByName.setImageResource(R.drawable.nupi_sort_a_ya_disable);
            sortByName.setBackgroundResource(R.drawable.selector_nupi_blue_dark);
        }
    }
}
