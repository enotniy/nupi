package com.nupi.agent.application.models;

import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.utils.TimeUtils;

import java.util.List;

public class ContractGroup {
    Contract contract;
    Payment payment;
    List<PaymentByOrder> paymentByOrders;

    public ContractGroup(Contract contract, Payment payment, List<PaymentByOrder> paymentByOrders) {
        this.contract = contract;
        this.payment = payment;
        this.paymentByOrders = paymentByOrders;
    }

    public Contract getContract() {
        return contract;
    }

    public Payment getPayment() {
        return payment;
    }

    public List<PaymentByOrder> getPaymentByOrders() {
        return paymentByOrders;
    }

    public long getSize() {
        return paymentByOrders.size();
    }

    public long getDelay(int ind) {
        long docTime = TimeUtils.calculateDate(paymentByOrders.get(ind)).getTime();
        long days = ((TimeUtils.currentTime() - docTime) / (1000 * 60 * 60 * 24));
        return days < 0 ? 0 : days;
    }
}
