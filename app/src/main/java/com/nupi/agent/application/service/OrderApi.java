package com.nupi.agent.application.service;

import com.google.gson.annotations.SerializedName;
import com.nupi.agent.network.meteor.OrderRequest;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pasenchuk Victor on 05.01.15
 */
public class OrderApi {

    @SerializedName("orders")
    private final Map<String, OrderRequest> orders = new HashMap<>();

    public void addToOrderForClient(String clientGuid, String nomenclatureGuid, String unitGuid, double price, double quantity) {
        OrderRequest orderRequest = orders.get(clientGuid);
        if (orderRequest != null) {
            orderRequest.addOrUpdateGood(nomenclatureGuid, unitGuid, price, quantity);
        } else {
            orderRequest = new OrderRequest(clientGuid);
            orderRequest.addOrUpdateGood(nomenclatureGuid, unitGuid, price, quantity);
            orders.put(clientGuid, orderRequest);
        }
    }

    public void removeOrderForClient(String clientGuid, String nomenclatureGuid) {
        OrderRequest orderRequest = orders.get(clientGuid);
        if (orderRequest != null) {
            orderRequest.removeGood(nomenclatureGuid);
        }
    }

    public OrderRequest getOrderRequestForClient(String clientGuid) {
        if (clientGuid != null) {
            if (orders.get(clientGuid) != null)
                return orders.get(clientGuid);
        } else {
            return new OrderRequest(clientGuid);
        }
        OrderRequest orderRequest = new OrderRequest(clientGuid);
        orders.put(clientGuid, orderRequest);
        return orderRequest;
    }

    public OrderRequest removeOrderRequestForClient(String clientGuid) {
        final OrderRequest orderRequest = orders.remove(clientGuid);
        return orderRequest != null ? orderRequest : new OrderRequest();
    }

    public void setRealisationForClient(String clientGuid, OrderRequest orderRequest) {
        orders.put(clientGuid, orderRequest);
    }

    public void changeClientForRealisation(String oldClientGuid, String newClientGuid) {
        OrderRequest orderRequest = removeOrderRequestForClient(oldClientGuid);
        orderRequest.setCounterAgent(newClientGuid);
        orders.put(newClientGuid, orderRequest);
    }

    public Collection<OrderRequest> getOrderRequests() {
        return orders.values();
    }

    public boolean hasOrders(String counterAgentGuid) {
        return getOrderRequestForClient(counterAgentGuid).hasOrders();
    }

}
