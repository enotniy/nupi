package com.nupi.agent.application.models;

import com.nupi.agent.database.models.meteor.Nomenclature;

/**
 * Created by User on 01.02.2016.
 */
public class NomenclatureSaleGroup {
    private Nomenclature nomenclature;
    private double count = 0f;
    private double sum = 0f;
    private int type = 0;

    public NomenclatureSaleGroup(Nomenclature nomenclature, double count, double sum) {
        this.nomenclature = nomenclature;
        this.count = count;
        this.sum = sum;
    }

    public Nomenclature getNomenclature() {
        return nomenclature;
    }

    public double getCount() {
        return count;
    }

    public double getSum() {
        return sum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
