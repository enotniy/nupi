package com.nupi.agent.application.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.network.meteor.OrderItem;

import javax.inject.Inject;

/**
 * Created by Pasenchuk Victor on 05.01.15
 */

public class SelectedItemsHolder {

    @Inject
    transient NupiPreferences preferences;


    @SerializedName("selectedCounterAgentGuid")
    @Expose
    private String selectedCounterAgentGuid;

    @SerializedName("selectedGood")
    @Expose
    private OrderItem selectedGood;

    @SerializedName("selectedRouteGuid")
    @Expose
    private String selectedRouteGuid;

    @SerializedName("chatUsersSelected")
    private boolean chatUsersSelected = true;

    @SerializedName("chatSelectedId")
    private String chatSelectedId;

    public SelectedItemsHolder() {
    }

    public SelectedItemsHolder(NupiApp app) {
        app.getAppComponent().inject(this);
        SelectedItemsHolder holder = preferences.getSelectedItemsHolder();
        selectedGood = holder.getSelectedGood();
        chatUsersSelected = holder.isChatUsersSelected();
        chatSelectedId = holder.getChatSelectedId();
    }

    public OrderItem getSelectedGood() {
        return selectedGood;
    }

    public void setSelectedGood(OrderItem selectedGood) {
        this.selectedGood = selectedGood;
        save();
    }

    public boolean isChatUsersSelected() {
        return chatUsersSelected;
    }

    public void setChatUsersSelected(boolean chatUsersSelected) {
        this.chatUsersSelected = chatUsersSelected;
        save();
    }

    public String getChatSelectedId() {
        return chatSelectedId;
    }

    public void setChatSelectedId(String chatSelectedId) {
        this.chatSelectedId = chatSelectedId;
        save();
    }

    public String getSelectedCounterAgentGuid() {
        return selectedCounterAgentGuid;
    }

    public void setSelectedCounterAgentGuid(String selectedCounterAgentGuid) {
        this.selectedCounterAgentGuid = selectedCounterAgentGuid;
        save();
    }

    private void save() {
        preferences.setSelectedItemsHolder(this);
    }


    public String getSelectedRouteGuid() {
        return selectedRouteGuid;
    }

    public void setSelectedRouteGuid(String selectedRouteGuid) {
        this.selectedRouteGuid = selectedRouteGuid;
        save();
    }
}
