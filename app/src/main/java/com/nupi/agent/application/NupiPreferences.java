package com.nupi.agent.application;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nupi.agent.application.models.Picker;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.application.service.SelectedItemsHolder;
import com.nupi.agent.di.AppModule;
import com.nupi.agent.enums.SortType;
import com.nupi.agent.helpers.RealmGsonHelper;
import com.nupi.agent.network.meteor.OrderRequest;
import com.nupi.agent.network.meteor.PaymentRequest;
import com.nupi.agent.network.meteor.StatusResponse;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import io.realm.Sort;

/**
 * Created by Pasenchuk Victor on 21.10.14
 */
public class NupiPreferences {

    public static final int NO_SYNC = -1;


    public static final int
            TAB_TODAY = 1,
            TAB_ANALYTICS = 2,
            TAB_OTHER = 3;

    public static final int
            STORE_UNIT = 1,
            REPORT_UNIT = 2;

    private static final String PREFERENCES_KEY = "NUPI_SHARED_PREFERENCES";


    private static final String SYNC_TIME_KEY = "SYNC_TIME_KEY";
    private static final String ORDER_REQUESTS_KEY = "ORDER_REQUESTS_KEY";
    private static final String PAYMENT_REQUESTS_KEY = "PAYMENT_REQUESTS_KEY";
    private static final String BREADCRUMBS_KEY = "BREADCRUMBS_KEY";
    private static final String KEEP_BREADCRUMBS_KEY = "KEEP_BREADCRUMBS_KEY";
    private static final String ORDERS_API_KEY = "ORDERS_API_KEY";
    private static final String SELECTED_ITEMS_HOLDER_KEY = "SELECTED_ITEMS_HOLDER_KEY";
    private static final String SORT_TYPE_KEY = "SORT_TYPE_KEY";
    private static final String SORT_ORDER_KEY = "SORT_ORDER_KEY";
    private static final String ROUTE_INFO_KEY = "ROUTE_INFO_KEY";
    private static final String NUPI_TOKEN_KEY = "NUPI_TOKEN_KEY";
    private static final String LAST_LOGIN_KEY = "LAST_LOGIN_KEY";
    private static final String WALLET_VISIBILITY_KEY = "WALLET_VISIBILITY_KEY";
    private static final String ALLOW_FLOAT_QUANTITIES_CLICK = "ALLOW_FLOAT_QUANTITIES_CLICK";
    private static final String GCM_REGISTRATION_ID = "GCM_REGISTRATION_ID";
    private static final String GCM_IS_NUPI_REGISTERED = "GCM_IS_NUPI_REGISTERED";
    private static final String MAIN_TAB_KEY = "MAIN_TAB_KEY";
    private static final String DEFAULT_UNIT_KEY = "DEFAULT_UNIT_KEY";
    private static final String BASE_URL_KEY = "BASE_URL_KEY";
    private static final String STATUS_RESPONSE_KEY = "STATUS_RESPONSE_KEY";
    private static final String IS_PLAY_SERVICES_AVAILABLE = "IS_PLAY_SERVICES_AVAILABLE";
    private static final String IS_LOGGED_IN = "IS_LOGGED_IN";
    private static final String PICKER_PRICE = "PICKER_PRICE";
    private static final String PICKER_CLIENTS = "PICKER_CLIENTS";
    private static final String CHAT_ID_KEY = "CHAT_ID_KEY";
    private static final String LAST_POSITION_LATITUDE = "LAST_POSITION_LATITUDE";
    private static final String LAST_POSITION_LONGITUDE = "LAST_POSITION_LONGITUDE";
    private final Gson gson;
    private SharedPreferences sharedPreferences;


    // TODO: 05.01.16 fix
    private boolean searchIsVisible = false;


    public NupiPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        gson = RealmGsonHelper.getRealmGson();
    }

    public static int getNoSync() {
        return NO_SYNC;
    }

    public float getLastPositionLatitude() {
        return sharedPreferences.getFloat(LAST_POSITION_LATITUDE, 55f);
    }

    public void setLastPositionLatitude(float coord) {
        sharedPreferences.edit().putFloat(LAST_POSITION_LATITUDE, coord).apply();
    }

    public float getLastPositionLongitude() {
        return sharedPreferences.getFloat(LAST_POSITION_LONGITUDE, 37f);
    }

    public void setLastPositionLongitude(float coord) {
        sharedPreferences.edit().putFloat(LAST_POSITION_LONGITUDE, coord).apply();
    }

    public long getLastSyncTime() {
        return sharedPreferences.getLong(SYNC_TIME_KEY, NO_SYNC);
    }

    public void setLastSyncTime(long time) {
        sharedPreferences.edit().putLong(SYNC_TIME_KEY, time).apply();
    }

    public String getChatId() {
        return sharedPreferences.getString(CHAT_ID_KEY, "");
    }

    public void setChatId(String chatId) {
        sharedPreferences.edit().putString(CHAT_ID_KEY, chatId).apply();
    }

    public SelectedItemsHolder getSelectedItemsHolder() {
        String holder = sharedPreferences.getString(SELECTED_ITEMS_HOLDER_KEY, null);
        return holder != null ? gson.fromJson(holder, SelectedItemsHolder.class) : new SelectedItemsHolder();
    }

    public void setSelectedItemsHolder(SelectedItemsHolder holder) {
        sharedPreferences.edit().putString(SELECTED_ITEMS_HOLDER_KEY, gson.toJson(holder)).apply();
    }

    public OrderApi getOrdersApi() {
        String orderApi = sharedPreferences.getString(ORDERS_API_KEY, null);
        return orderApi != null ? gson.fromJson(orderApi, OrderApi.class) : new OrderApi();
    }

    public void setOrdersApi(OrderApi ordersApi) {
        sharedPreferences.edit().putString(ORDERS_API_KEY, gson.toJson(ordersApi)).apply();
    }

    public List<OrderRequest> getOrderRequests() {
        final String emptyList = gson.toJson(new LinkedList<OrderRequest>());
        return new LinkedList<>(Arrays.asList(gson.fromJson(sharedPreferences.getString(ORDER_REQUESTS_KEY, emptyList), OrderRequest[].class)));
    }

    public void setOrderRequests(List<OrderRequest> orderRequests) {
        sharedPreferences.edit().putString(ORDER_REQUESTS_KEY, gson.toJson(orderRequests)).apply();
    }


    public List<PaymentRequest> getPaymentRequests() {
        final String emptyList = gson.toJson(new LinkedList<PaymentRequest>());
        return new LinkedList<>(Arrays.asList(gson.fromJson(sharedPreferences.getString(PAYMENT_REQUESTS_KEY, emptyList), PaymentRequest[].class)));
    }

    public void setPaymentRequests(List<PaymentRequest> paymentRequests) {
        sharedPreferences.edit().putString(PAYMENT_REQUESTS_KEY, gson.toJson(paymentRequests)).apply();
    }


    public List<String> getBreadCrumbs() {
        final String emptyList = gson.toJson(new LinkedList<String>());
        return gson.fromJson(sharedPreferences.getString(BREADCRUMBS_KEY, emptyList), new TypeToken<LinkedList<String>>() {
        }.getType());
    }

    public void setBreadCrumbs(List<String> strings) {
        sharedPreferences.edit().putString(BREADCRUMBS_KEY, gson.toJson(strings)).apply();
    }

    public boolean getKeepBreadcrumbs() {
        return sharedPreferences.getBoolean(KEEP_BREADCRUMBS_KEY, false);
    }

    public void setKeepBreadcrumbs(boolean keep) {
        sharedPreferences.edit().putBoolean(KEEP_BREADCRUMBS_KEY, keep).apply();
    }

    public SortType getSortType() {
        final String type = sharedPreferences.getString(SORT_TYPE_KEY, null);
        if (type != null)
            return gson.fromJson(type, SortType.class);
        return SortType.ABC_ORDER;
    }

    public void setSortType(SortType sortType) {
        sharedPreferences.edit().putString(SORT_TYPE_KEY, gson.toJson(sortType)).apply();
    }

    public Sort getSortOrder() {
        return sharedPreferences.getBoolean(SORT_ORDER_KEY, true) ? Sort.ASCENDING : Sort.DESCENDING;
    }

    public void setSortOrder(Sort order) {
        sharedPreferences.edit().putBoolean(SORT_ORDER_KEY, order.getValue()).apply();
    }

    public void deleteAccount() {
        sharedPreferences.edit().remove(ROUTE_INFO_KEY).apply();
        sharedPreferences.edit().remove(NUPI_TOKEN_KEY).apply();
        sharedPreferences.edit().remove(IS_LOGGED_IN).apply();
    }

    public String getNupiToken() {
        return sharedPreferences.getString(NUPI_TOKEN_KEY, null);
    }

    public void setNupiToken(String token) {
        sharedPreferences.edit().putString(NUPI_TOKEN_KEY, token).apply();
    }


    public String getLastLogin() {
        return sharedPreferences.getString(LAST_LOGIN_KEY, null);
    }

    public void setLastLogin(String lastLogin) {
        sharedPreferences.edit().putString(LAST_LOGIN_KEY, lastLogin).apply();
    }

    public int getMainTab() {
        return sharedPreferences.getInt(MAIN_TAB_KEY, TAB_OTHER);
    }

    public void setMainTab(int mainTab) {
        sharedPreferences.edit().putInt(MAIN_TAB_KEY, mainTab).apply();
    }

    public int getDefaultUnit() {
        return sharedPreferences.getInt(DEFAULT_UNIT_KEY, REPORT_UNIT);
    }

    public void setDefaultUnit(int reportUnit) {
        sharedPreferences.edit().putInt(DEFAULT_UNIT_KEY, reportUnit).apply();
    }

    public Picker getPickerPrice() {
        Gson gson = new Gson();
        String picker = sharedPreferences.getString(PICKER_PRICE, null);
        if (picker == null)
            return null;
        return gson.fromJson(picker, Picker.class);
    }

    public void setPickerPrice(Picker listSelectedFilterPrice) {
        Gson gson = new Gson();
        String json = gson.toJson(listSelectedFilterPrice);
        sharedPreferences.edit().putString(PICKER_PRICE, json).apply();
    }

    public StatusResponse getStatusResponse() {
        final Gson gson = new Gson();
        final String statusResponse = sharedPreferences.getString(STATUS_RESPONSE_KEY, null);
        if (statusResponse == null)
            return null;
        return gson.fromJson(statusResponse, StatusResponse.class);
    }

    public void setStatusResponse(StatusResponse statusResponse) {
        Gson gson = new Gson();
        String json = gson.toJson(statusResponse);
        sharedPreferences.edit().putString(STATUS_RESPONSE_KEY, json).apply();
    }

    public Picker getPickerClients() {
        Gson gson = new Gson();
        String picker = sharedPreferences.getString(PICKER_CLIENTS, null);
        if (picker == null)
            return null;
        return gson.fromJson(picker, Picker.class);
    }

    public void setPickerClients(Picker listSelectedFilterPrice) {
        Gson gson = new Gson();
        String json = gson.toJson(listSelectedFilterPrice);
        sharedPreferences.edit().putString(PICKER_CLIENTS, json).apply();
    }

    public String getBaseUrl() {
        return sharedPreferences.getString(BASE_URL_KEY, AppModule.HTTP_API_METEOR_SERVER);
    }

    public void setBaseUrl(String baseUrl) {
        sharedPreferences.edit().putString(BASE_URL_KEY, baseUrl).apply();
    }

    public String getGcmRegistrationId() {
        return sharedPreferences.getString(GCM_REGISTRATION_ID, null);
    }

    public void setGcmRegistrationId(String reg_id) {
        sharedPreferences.edit().putString(GCM_REGISTRATION_ID, reg_id).apply();
    }

    public boolean getGcmIsNupiRegistered() {
        return sharedPreferences.getBoolean(GCM_IS_NUPI_REGISTERED, false);
    }

    public void setGcmIsNupiRegistered(boolean is_registered) {
        sharedPreferences.edit().putBoolean(GCM_IS_NUPI_REGISTERED, is_registered).apply();
    }

    public boolean isWalletVisible() {
        return sharedPreferences.getBoolean(WALLET_VISIBILITY_KEY, true);
    }

    public void changeWalletVisible() {
        sharedPreferences.edit().putBoolean(WALLET_VISIBILITY_KEY, !isWalletVisible()).apply();
    }

    public boolean isFloatQuantityAllowed() {
        return sharedPreferences.getBoolean(ALLOW_FLOAT_QUANTITIES_CLICK, false);
    }

    public void setFloatQuantityAllowed(boolean allowed) {
        sharedPreferences.edit().putBoolean(ALLOW_FLOAT_QUANTITIES_CLICK, allowed).apply();
    }

    public void changeSearchVisible() {
        this.searchIsVisible = !this.searchIsVisible;
    }

    public boolean getSearchVisible() {
        return this.searchIsVisible;
    }

    public void setIsPlayServicesAvailable(Boolean isAvailable) {
        sharedPreferences.edit().putBoolean(IS_PLAY_SERVICES_AVAILABLE, isAvailable).apply();
    }

    public boolean isPlayServicesAvailable() {
        return sharedPreferences.getBoolean(IS_PLAY_SERVICES_AVAILABLE, false);
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGGED_IN, false);
    }

    public void setLoggedIn(boolean loggedIn) {
        sharedPreferences.edit().putBoolean(IS_LOGGED_IN, loggedIn).apply();
    }

}
