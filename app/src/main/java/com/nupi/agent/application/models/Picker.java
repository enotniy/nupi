package com.nupi.agent.application.models;

import com.google.gson.annotations.SerializedName;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Nomenclature;
import com.nupi.agent.enums.SelectionType;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 11.12.2015.
 */
public class Picker {

    public static final int TYPE_PICKER_GOODS = 0;
    public static final int TYPE_PICKER_CLIENTS = 1;
    public static final String TYPE_PICKER = "TYPE_PICKER";

    @SerializedName("selectionTypeHashMap")
    private final HashMap<String, SelectionType> selectionTypeHashMap = new HashMap<>();

    public Picker(Nomenclature[] prices, boolean allSelected) {
        for (Nomenclature price : prices)
            selectionTypeHashMap.put(price.getGuid(), allSelected ? SelectionType.ALL : SelectionType.NONE);
    }

    public Picker(CounterAgent[] clients, boolean allSelected) {
        for (CounterAgent client : clients)
            selectionTypeHashMap.put(client.getGuid(), allSelected ? SelectionType.ALL : SelectionType.NONE);
    }

    public void selectElement(Tree<String> idsTree, SelectionType selectionType) {
        goDown(idsTree, selectionType);
        goUp(idsTree);
    }

    private void goDown(Tree<String> idsTree, SelectionType selectionType) {
        selectionTypeHashMap.put(idsTree.getHead(), selectionType);
        for (Tree<String> tree : idsTree.getSubTrees())
            goDown(tree, selectionType);
    }

    private void goUp(Tree<String> idsTree) {
        if (idsTree.getParent() != null) {
            int countAll = 0;
            boolean hasPartial = false;
            final Collection<String> successors = idsTree.getSuccessors(idsTree.getParent().getHead());
            for (String elem : successors) {
                if (selectionTypeHashMap.keySet().contains(elem)) {
                    switch (selectionTypeHashMap.get(elem)) {
                        case ALL:
                            countAll++;
                            break;
                        case PARTIAL:
                            hasPartial = true;
                            break;
                    }
                    if (hasPartial)
                        break;
                }
            }

            if (countAll == successors.size())
                selectionTypeHashMap.put(idsTree.getParent().getHead(), SelectionType.ALL);
            else if (hasPartial || countAll > 0)
                selectionTypeHashMap.put(idsTree.getParent().getHead(), SelectionType.PARTIAL);
            else
                selectionTypeHashMap.put(idsTree.getParent().getHead(), SelectionType.NONE);

            goUp(idsTree.getParent());
        }
    }

    public SelectionType getTypeSelection(String elemID) {
        return selectionTypeHashMap.get(elemID);
    }

    public List<String> getSelectedIds() {
        List<String> ids = new LinkedList<>();
        for (Map.Entry<String, SelectionType> entry : selectionTypeHashMap.entrySet())
            if (entry.getValue() == SelectionType.ALL)
                ids.add(entry.getKey());
        return ids;
    }


}
