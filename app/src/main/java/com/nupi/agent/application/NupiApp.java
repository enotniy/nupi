package com.nupi.agent.application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.nupi.agent.di.AppComponent;
import com.nupi.agent.di.AppModule;
import com.nupi.agent.di.DaggerAppComponent;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Pasencuk Victor on 16.09.14
 */

public class NupiApp extends Application {


    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();


        setupPicasso();


        Fabric.with(this, new Crashlytics());
    }

    private void setupPicasso() {
        //TODO: decide about size
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
