package com.nupi.agent.application.models;

import com.nupi.agent.database.models.meteor.Contract;
import com.nupi.agent.database.models.meteor.CounterAgent;
import com.nupi.agent.database.models.meteor.Payment;
import com.nupi.agent.database.models.meteor.PaymentByOrder;
import com.nupi.agent.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasenchuk Victor on 02.11.14
 */
public class CounterAgentGroup {
    private CounterAgent client;
    private List<PaymentByOrder> paymentsByOrders;
    private ArrayList<Long> delays;
    private boolean hasDelays = false;
    private Contract contract;
    private Payment payment;

    private boolean pinned = false;

    public CounterAgentGroup(CounterAgent client, List<PaymentByOrder> paymentsByOrders, Contract contract) {
        this.client = client;
        this.paymentsByOrders = paymentsByOrders;
        this.contract = contract;
        delays = new ArrayList<>();
        long currentTime = TimeUtils.currentTime();
        initData(paymentsByOrders, currentTime);
    }

    public CounterAgentGroup(Payment payment, List<PaymentByOrder> paymentsByOrders, Contract contract) {
        this.payment = payment;
        this.paymentsByOrders = paymentsByOrders;
        this.contract = contract;
        delays = new ArrayList<>();
        long currentTime = TimeUtils.currentTime();
        initData(paymentsByOrders, currentTime);
    }

    private Payment getPayment() {
        return payment;
    }

    public double getPaymentSum() {
        return payment != null ? payment.getSum() : 0;
    }

    private void initData(List<PaymentByOrder> paymentsByOrders, long currentTime) {
        if (contract != null && contract.isDebtDaysControl())
            for (PaymentByOrder paymentByOrder : paymentsByOrders) {
                long docTime = TimeUtils.calculateDate(paymentByOrder).getTime();
                long days = ((currentTime - docTime) / (1000 * 60 * 60 * 24));
                if (days - contract.getDebtDaysMax() > 0) hasDelays = true;
                days = days < 0 ? 0 : days;
                delays.add(days);
            }
        else
            delays.add(0L);
    }

    public CounterAgent getClient() {
        return client;
    }

    public List<PaymentByOrder> getPaymentsByOrders() {
        return paymentsByOrders;
    }

    public ArrayList<Long> getDelays() {
        return delays;
    }

    public boolean hasDelays() {
        return hasDelays;
    }

    public boolean isPinned() {
        return pinned;
    }

    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }

    public boolean isHasDelays() {
        return hasDelays;
    }

    public Contract getContract() {
        return contract;
    }


    public long getDelay() {
        if (contract != null && contract.isDebtDaysControl())
            return contract.getDebtDaysMax();
        else
            return 0L;
    }
}
