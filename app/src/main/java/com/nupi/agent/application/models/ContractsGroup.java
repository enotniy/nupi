package com.nupi.agent.application.models;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pasenchuk Victor on 02.11.14
 */
public class ContractsGroup {

    public static final int CONTRACT_POSITION = -2;
    public static final int NO_INIT_POSITION = -1;
    private List<ContractGroup> contractGroups = new LinkedList<>();
    private int indexInGroup = NO_INIT_POSITION;
    private boolean pinned = false;

    public ContractsGroup(List<ContractGroup> contractGroups) {
        for (ContractGroup contractGroup : contractGroups) {
            this.contractGroups.add(contractGroup);
        }
    }

    public int getSize() {
        int size = contractGroups.size();
        for (ContractGroup contractGroup : contractGroups) {
            size += contractGroup.getSize();
        }
        return size;
    }


    public ContractGroup getGroup(int position) {
        indexInGroup = CONTRACT_POSITION;
        for (ContractGroup contractGroup : contractGroups) {
            int size = NO_INIT_POSITION;
            size++;
            size += contractGroup.getSize();
            if (position <= size) {
                indexInGroup = position;
                return contractGroup;
            }
            position -= (size + 1);
        }
        return null;
    }

    public boolean isContract(int ind) {
        for (ContractGroup contractGroup : contractGroups) {
            if (0 == ind)
                return true;
            int size = NO_INIT_POSITION;
            size += (contractGroup.getSize() + 1);
            if (ind <= size) {
                return false;
            }
            ind -= (size + 1);
        }
        return false;
    }

    public int getIndexInGroup() {
        if (indexInGroup == 0)
            return CONTRACT_POSITION;
        else
            return --indexInGroup;
    }


    public boolean isPinned() {
        return pinned;
    }

    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }
}
