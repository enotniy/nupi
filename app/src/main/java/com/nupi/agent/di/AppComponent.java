package com.nupi.agent.di;

import com.nupi.agent.application.service.GeolocationService;
import com.nupi.agent.application.service.SelectedItemsHolder;
import com.nupi.agent.network.MeteorDataUpdater;
import com.nupi.agent.network.gcm.RegistrationIntentService;
import com.nupi.agent.network.upload.UploadService;
import com.nupi.agent.ui.base.NupiActivity;
import com.nupi.agent.ui.base.NupiFragment;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportAnalyticsClientsAdapter;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportAnalyticsGoodsAdapter;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportAnalyticsResultAdapter;
import com.nupi.agent.ui.controllers.analytics.adapters.ReportClientAdapter;
import com.nupi.agent.ui.controllers.chat.adapters.ChatMessageAdapter;
import com.nupi.agent.ui.controllers.chat.fragments.BaseChatFragment;
import com.nupi.agent.ui.controllers.chat.fragments.ChatMessageFragment;
import com.nupi.agent.ui.controllers.debt.adapters.DebtsAdapter;
import com.nupi.agent.ui.controllers.debt.fragments.DebtsFragment;
import com.nupi.agent.ui.controllers.general.adapters.ChatSmallMessageAdapter;
import com.nupi.agent.ui.controllers.geolocation.adapters.GeolocationCounterAgentsMeteorAdapter;
import com.nupi.agent.ui.controllers.main.activities.MainActivity;
import com.nupi.agent.ui.controllers.main.cash.adapters.ContractAdapter;
import com.nupi.agent.ui.controllers.main.cash.fragments.CashAcceptorFragment;
import com.nupi.agent.ui.controllers.main.main_screen.fragments.TopStatusFragment;
import com.nupi.agent.ui.controllers.main.nomenclature.adapters.NomenclatureSearchMeteorAdapter;
import com.nupi.agent.ui.controllers.main.nomenclature.adapters.PricesOrderAdapter;
import com.nupi.agent.ui.controllers.main.nomenclature.fragments.PricesRightFragment;
import com.nupi.agent.ui.controllers.nupi_settings.activities.LoginActivity;
import com.nupi.agent.ui.controllers.nupi_settings.activities.RealmFileOperationsActivity;
import com.nupi.agent.ui.controllers.planner.adapters.PointsAdapter;
import com.nupi.agent.ui.controllers.planner.adapters.PointsEditorAdapter;
import com.nupi.agent.ui.controllers.planner.fragments.PlannerEditorFragment;
import com.nupi.agent.ui.controllers.receipt.adapters.ReceiptEntryAdapter;
import com.nupi.agent.ui.controllers.receipt.adapters.ReceiptPreviewEntryAdapter;
import com.nupi.agent.ui.dialogs.SelectClientDialog;
import com.nupi.agent.ui.dialogs.SelectCounterAgentDialog;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pasencukviktor on 25/02/16
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(NupiActivity activity);

    void inject(NupiFragment fragment);

    void inject(GeolocationService geolocationService);

    void inject(MeteorDataUpdater updater);

    void inject(RegistrationIntentService registrationIntentService);

    void inject(UploadService uploadService);

    void inject(ReportAnalyticsClientsAdapter adapter);

    void inject(ReportAnalyticsResultAdapter adapter);

    void inject(ReportAnalyticsGoodsAdapter adapter);

    void inject(ReportClientAdapter adapter);

    void inject(SelectedItemsHolder selectedItemsHolder);

    void inject(ChatMessageAdapter chatMessageAdapter);

    void inject(BaseChatFragment fragment);

    void inject(ChatMessageFragment fragment);

    void inject(DebtsAdapter adapter);

    void inject(DebtsFragment fragment);

    void inject(ChatSmallMessageAdapter adapter);

    void inject(GeolocationCounterAgentsMeteorAdapter agentsMeteorAdapter);

    void inject(MainActivity activity);

    void inject(ContractAdapter adapter);

    void inject(CashAcceptorFragment fragment);

    void inject(NomenclatureSearchMeteorAdapter adapter);

    void inject(PricesOrderAdapter adapter);

    void inject(PricesRightFragment fragment);

    void inject(TopStatusFragment fragment);

    void inject(LoginActivity activity);

    void inject(RealmFileOperationsActivity activity);

    void inject(PointsAdapter adapter);

    void inject(PointsEditorAdapter adapter);

    void inject(PlannerEditorFragment plannerEditorFragment);

    void inject(ReceiptPreviewEntryAdapter adapter);

    void inject(ReceiptEntryAdapter adapter);

    void inject(SelectClientDialog dialog);

    void inject(SelectCounterAgentDialog dialog);

}
