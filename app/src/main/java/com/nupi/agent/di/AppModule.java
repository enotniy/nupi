package com.nupi.agent.di;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nupi.agent.BuildConfig;
import com.nupi.agent.application.NupiApp;
import com.nupi.agent.application.NupiPreferences;
import com.nupi.agent.application.service.GeolocationService;
import com.nupi.agent.application.service.OrderApi;
import com.nupi.agent.application.service.SelectedItemsHolder;
import com.nupi.agent.database.RealmChatOperations;
import com.nupi.agent.database.RealmExchangeOperations;
import com.nupi.agent.database.RealmPlannerOperations;
import com.nupi.agent.database.RealmRegistryOperations;
import com.nupi.agent.helpers.RealmGsonHelper;
import com.nupi.agent.network.meteor.NupiMeteorApi;
import com.nupi.agent.network.nupi.NupiAuthApi;
import com.nupi.agent.network.nupi.NupiServerApi;
import com.nupi.agent.network.upload.UploadService;
import com.nupi.agent.network.weather.OpenWeatherMapWebService;
import com.nupi.agent.utils.TimeUtils;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.Endpoint;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Pasenchuk Victor on 16.09.14
 */

@Module
public class AppModule {

    public static final String MAPS_API_KEY = "AIzaSyA6n_YJELs9N18JWTir7OxhyV0lq90nkos";

    public static final String
            HTTP_API_OPENWEATHERMAP_ORG_DATA_2_5 = "http://api.openweathermap.org/data/2.5",
            HTTP_API_GOOGLE_MAPS = "https://maps.googleapis.com/maps/api",
            HTTP_API_BACKUP_SERVER = "http://77.221.204.102/",
            HTTP_API_METEOR_SERVER = "http://n7h.nupi.me/api/v1",
            HTTP_API_DEMO_SERVER = "http://demo.nupi.me/api/v1";
//            HTTP_API_METEOR_SERVER = "http://dev.nupi.me/api/v1";

    private NupiApp nupiApp;

    public AppModule(NupiApp app) {
        this.nupiApp = app;
    }

    @Provides
    @Singleton
    Bus provideBus() {
        return new Bus();
    }


    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(TimeUtils.DATE_FORMAT_SERVER).create();
    }

    @Provides
    @Singleton
    NupiPreferences provideSharedPreferences() {
        return new NupiPreferences(nupiApp);
    }

    @Provides
    @Singleton
    SelectedItemsHolder provideSelectionModelHolder() {
        return new SelectedItemsHolder(nupiApp);
    }

    @Provides
    @Singleton
    OrderApi provideOrderApi() {
        return provideSharedPreferences().getOrdersApi();
    }

    @Provides
    @Singleton
    GeolocationService provideGeolocationService() {
        return new GeolocationService(nupiApp);
    }

    @Provides
    @Singleton
    OpenWeatherMapWebService provideOpenWeatherMapWebService() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(HTTP_API_OPENWEATHERMAP_ORG_DATA_2_5)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter.create(OpenWeatherMapWebService.class);
    }

    @Provides
    @Singleton
    NupiServerApi provideNupiServerApi() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", "Token " + provideSharedPreferences().getNupiToken());
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient())
                .setEndpoint(getEndpoint())
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(RealmGsonHelper.getRealmGson()))
                .build();

        return restAdapter.create(NupiServerApi.class);
    }


    @Provides
    @Singleton
    NupiAuthApi provideNupiAuthApi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(getEndpoint())
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter.create(NupiAuthApi.class);
    }


    @Provides
    @Singleton
    NupiMeteorApi provideNupiMeteorAuthApi() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", "Token " + provideSharedPreferences().getNupiToken());
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient())
                .setEndpoint(getEndpoint())
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(RealmGsonHelper.getRealmGson()))
                .build();

        return restAdapter.create(NupiMeteorApi.class);
    }


    @NonNull
    private Endpoint getEndpoint() {
        return new Endpoint() {
            @Override
            public String getUrl() {
                if (BuildConfig.DEBUG)
                    return provideSharedPreferences().getBaseUrl();
                return
                        HTTP_API_DEMO_SERVER;
            }

            @Override
            public String getName() {
                return null;
            }
        };
    }


    @Provides
    @Singleton
    UploadService provideUploadService() {
        return new UploadService(nupiApp);
    }

    @Provides
    @Singleton
    RealmRegistryOperations provideRealmRegistryOperations() {
        return new RealmRegistryOperations(nupiApp);
    }

    @Provides
    @Singleton
    RealmChatOperations provideRealmChatOperations() {
        return new RealmChatOperations(nupiApp);
    }

    @Provides
    @Singleton
    RealmPlannerOperations provideRealmPlannerOperations() {
        return new RealmPlannerOperations(nupiApp);
    }

    @Provides
    @Singleton
    RealmExchangeOperations provideRealmMeteorOperations() {
        return new RealmExchangeOperations(nupiApp);
    }

}