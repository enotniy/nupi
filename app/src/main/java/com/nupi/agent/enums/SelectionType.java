package com.nupi.agent.enums;

/**
 * Created by Pasenchuk Victor on 26.12.15
 */
public enum SelectionType {
    ALL,
    PARTIAL,
    NONE
}
