package com.nupi.agent.enums;

/**
 * Created by Pasenchuk Victor on 28.12.15
 */
public enum TreeElementType {
    NODE(true), LEAF(false);

    private final boolean value;

    TreeElementType(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return this.value;
    }
}

