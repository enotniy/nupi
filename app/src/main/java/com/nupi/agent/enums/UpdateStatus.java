package com.nupi.agent.enums;

/**
 * Created by Pasenchuk Victor on 02.03.15
 */
public enum UpdateStatus {
    UPDATE_STARTED,
    FILE_DOWNLOADED,
    FILE_UNPACKED,
    FILE_PROCESSED,
    DATA_UPDATED,
    REGISTRY_UPDATED,
    UPDATE_FINISHED,
    UPDATE_FAILED
}
