package com.nupi.agent.utils;

import android.text.TextUtils;

import com.nupi.agent.database.models.chat.ChatGroup;
import com.nupi.agent.database.models.chat.ChatRoute;
import com.nupi.agent.database.models.chat.ChatUser;

/**
 * Created by Pasenchuk Victor on 30.10.15
 */
public class ChatMessageUtils {

    public static String getNickname(ChatUser chatUsers) {
        if (!TextUtils.isEmpty(chatUsers.getNickname()))
            return chatUsers.getNickname();

        final ChatRoute chatRoute = chatUsers.getChatRoute();
        if (chatRoute != null)
            if (!TextUtils.isEmpty(chatRoute.getName()))
                return chatRoute.getName();

        return "—";
    }

    public static String getFirstSymbols(String nickname) {
        String[] parts = nickname.trim().split(" ");
        if (parts.length > 1)
            return (parts[0].substring(0, 1) + parts[1].substring(0, 1)).toUpperCase();
        if (nickname.length() > 1)
            return nickname.substring(0, 2).toUpperCase();

        return nickname.substring(0, 1).toUpperCase();

    }

    public static String getHeadlineGroup(ChatGroup chatGroup) {
        if (!TextUtils.isEmpty(chatGroup.getHeadline()))
            return chatGroup.getHeadline();
        return "—";
    }
}
