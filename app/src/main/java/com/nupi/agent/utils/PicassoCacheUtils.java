package com.nupi.agent.utils;

import android.content.Context;
import android.widget.ImageView;

import com.nupi.agent.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by User on 28.10.2015
 */
public class PicassoCacheUtils {

    public static void loadCachedImage(final Context context, final String imageUrl, final ImageView imageView) {
        Picasso.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        loadNonCachedImage(context, imageUrl, imageView);
                    }
                });

    }

    public static void loadNonCachedImage(Context context, String imageUrl, ImageView imageView) {
        //TODO: icon for failed image downloading
        Picasso.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(imageView);
    }
}
