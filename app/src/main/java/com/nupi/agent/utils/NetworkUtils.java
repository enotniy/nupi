package com.nupi.agent.utils;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Pasenchuk Victor on 18.09.15
 */
public class NetworkUtils {
    public static final int CODE_ERROR = -2, NETWORK_ERROR = -1;

    public static int getNetworkError(Throwable throwable) {

        if (throwable instanceof RetrofitError) {
            Response response = ((RetrofitError) throwable).getResponse();
            if (response != null)
                return response.getStatus();
            return NETWORK_ERROR;
        }
        return CODE_ERROR;
    }
}
