package com.nupi.agent.utils;

import android.text.format.DateFormat;

import com.nupi.agent.database.models.meteor.PaymentByOrder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Pasenchuk Victor on 21.10.14
 * This class is responsible for detecting current time.
 */
public class TimeUtils {

    public static final String DATE_FORMAT_SERVER = "yyyy-MM-dd HH-mm-ss";
    public static final String DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
    public static final String TIME_FORMAT = "HH:mm";
    public static final String TIME_FORMAT_FULL = "HH:mm:ss";
    public static final String TIME_SHORT_FORMAT = "HH:mm";
    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String DATE_FORMAT_SHORT_WITH_YEAR = "dd.MM.yy";
    public static final String DATE_FORMAT_SHORT = "dd MMM";

    private static final SimpleDateFormat serverFormat = new SimpleDateFormat(DATE_FORMAT_SERVER, Locale.US);
    private static final SimpleDateFormat appFormat = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.US);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.US);
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT, Locale.US);
    private static final SimpleDateFormat timeShortFormat = new SimpleDateFormat(TIME_FORMAT, Locale.US);

    private static final long MILLISECOND_IN_DAY = 24*60*60*1000;

    public static long currentTime() {
        return new Date().getTime();
    }

    public static Date getFirstDay() {
        Calendar today = Calendar.getInstance();
        truncateDayTime(today);
        today.set(Calendar.DATE, 1);
        return today.getTime();
    }

    public static Date getPastDate(int pastDays) {
        return getDate(-pastDays + 1);
    }

    public static Date getEndOfDay() {
        return getDate(0);
    }

    private static Date getDate(int dayModifier) {
        Calendar today = Calendar.getInstance();
        truncateDayTime(today);
        today.add(Calendar.DATE, dayModifier);
        return today.getTime();
    }

    public static void truncateDayTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static String currentTimeInDbFormat() {
        return timeInDbFormat(currentTime());
    }

    public static String timeInDbFormat(long dateTime) {
        return serverFormat.format(new Date(dateTime));
    }

    public static String timeAppFormat(Date date) {
        return appFormat.format(date);
    }

    public static String dateFormat(Date date) {
        return dateFormat.format(date);
    }

    public static String timeFormat(Date date) {
        return date != null ? timeFormat.format(date) : "";
    }

    public static String timeShortFormat(Date date) {
        return timeFormat.format(date);
    }

    public static String dateFormatShort(Date date) {
        return new SimpleDateFormat(DATE_FORMAT_SHORT).format(date);
    }

    public static String dateFormatChat(Date date) {
        final Date now = new Date();
        if (date.getDay() == now.getDay() && ((now.getTime() - date.getTime()) < MILLISECOND_IN_DAY))
            return new SimpleDateFormat(TIME_FORMAT).format(date);
        else
            return new SimpleDateFormat(DATE_FORMAT_SHORT_WITH_YEAR).format(date);
    }

    public static CharSequence dateTimeToString(long dateTime) {
        Date date = new Date(dateTime);
        return DateFormat.format("k:mm (dd.MM.yy)", date);
    }

    public static Calendar getCalendarForAnalytic() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar;
    }

    public static CharSequence dateTimeToString(Date date) {
        return DateFormat.format("dd.MM.yy (k:mm:ss)", date);
    }

    public static Date getChatTime(Date date) {
        return new Date(date.getTime() + TimeZone.getDefault().getOffset(new Date().getTime()));
    }

    public static Date calculateDate(PaymentByOrder paymentByOrder) {
        try {
//      Документ расчетов с контрагентом (ручной учет) 00000001405 от 31.12.2015 0:00:00
            String string = paymentByOrder.getDocInfo().split("от ")[1];
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy K:mm:ss");
            return format.parse(string);
        } catch (Exception e) {
            return new Date();
        }
    }

}
