package com.nupi.agent.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Pasenchuk Victor on 26.05.15
 */
public class BitmapUtils {

    //TODO: check SD Card permission in Android 6+

    private static final String TAG = "BitmapUtils";

    public static Bitmap getBitmapThumbnail(File file) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

        bitmapOptions.inJustDecodeBounds = true; // obtain the size of the image, without loading it in memory
        BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);

        // find the best scaling factor for the desired dimensions
        int desiredWidth = 1024;
        int desiredHeight = 768;
        float widthScale = (float) bitmapOptions.outWidth / desiredWidth;
        float heightScale = (float) bitmapOptions.outHeight / desiredHeight;
        float scale = Math.min(widthScale, heightScale);

        int sampleSize = 1;
        while (sampleSize < scale) {
            sampleSize *= 2;
        }
        bitmapOptions.inSampleSize = sampleSize; // this value must be a power of 2,
        // this is why you can not have an image scaled as you would like to have
        bitmapOptions.inJustDecodeBounds = false; // now we want to load the image

        // Let's load just the part of the image necessary for creating the thumbnail, not the whole image
        return BitmapFactory.decodeFile(file.getAbsolutePath(), bitmapOptions);

    }

    public static File saveBitmapToCache(Bitmap bitmap, Context context) {

        FileOutputStream out = null;

        File cachedImage = new File(context.getExternalCacheDir(), "chat-image.jpg");
        try {
            out = new FileOutputStream(cachedImage);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
        } catch (IOException e) {
            Log.e(TAG, "Failed to convert image to JPEG", e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                Log.e(TAG, "Failed to close output stream", e);
            }
        }

        return cachedImage;

    }
}
