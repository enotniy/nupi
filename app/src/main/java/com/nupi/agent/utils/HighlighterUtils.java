package com.nupi.agent.utils;

import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.widget.TextView;

/**
 * Created by Pasenchuk Victor on 26.12.15
 */
public class HighlighterUtils {
    public static void highlightSearchString(String goodName, TextView textView, String searchQuery, int color) {

        final int startPos = goodName.toLowerCase().indexOf(searchQuery.toLowerCase());
        final int endPos = startPos + searchQuery.length();

        if (startPos != -1) // This should always be true, just a sanity check
        {
            Spannable spannable = new SpannableString(goodName);
            ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{color});
            TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.NORMAL, -1, blueColor, null);

            spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            textView.setText(spannable);
        } else
            textView.setText(goodName);
    }
}
