package com.nupi.agent.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;

import java.io.File;
import java.math.BigDecimal;

/**
 * Created by wildf on 19.01.2016.
 */
public class ExternalFilesUtils {

    public static final int SIZE_BYTES = 0;
    public static final int SIZE_KB = 1;
    public static final int SIZE_MB = 2;
    public static final int SIZE_GB = 3;
    private static File filesStorage;

    public ExternalFilesUtils(Context context, String dirName) {
        filesStorage = context.getExternalFilesDir(dirName);
    }

    private static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public File GetFilesDir() {
        return filesStorage;
    }

    public float GetFreeSpace(int SizeUnits) {
        StatFs stat = new StatFs(filesStorage.getPath());
        long bytesAvail;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvail = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        } else {
            bytesAvail = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        }
        final float KB = 1024L;
        float returnValue;
        switch (SizeUnits) {
            case SIZE_BYTES:
                return bytesAvail;
            case SIZE_KB:
                returnValue = bytesAvail / KB;
                break;
            case SIZE_MB:
                returnValue = bytesAvail / (KB * KB);
                break;
            case SIZE_GB:
                returnValue = bytesAvail / (KB * KB * KB);
                break;
            default:
                return bytesAvail;
        }

        return round(returnValue, 2);
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

}
