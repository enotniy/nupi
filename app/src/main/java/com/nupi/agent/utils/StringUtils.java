package com.nupi.agent.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by User on 26.01.2016.
 */
public class StringUtils {

    public static final double EPSILON = 0.0001f;
    private static final String MONEY_FORMAT = "###,##0.00";
    private static final String STOCK_FORMAT = "###,##0.000";

    public static double round(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_FLOOR);
        return bd.floatValue();
    }

    public static String getMoneyFormat(double value) {
        if (Math.abs(value) < EPSILON)
            value = 0;
        return new DecimalFormat(MONEY_FORMAT).format(value);
    }

    public static String getMoneyFormat(double value, Locale locale) {
        return new DecimalFormat(MONEY_FORMAT, DecimalFormatSymbols.getInstance(locale)).format(value);
    }

    public static String getStockFormat(double value) {
        if (Math.abs(value) < EPSILON)
            value = 0;
        return new DecimalFormat(STOCK_FORMAT).format(value);
    }

    public static String getStockFormat(double value, Locale locale) {
        return new DecimalFormat(STOCK_FORMAT, DecimalFormatSymbols.getInstance(locale)).format(value);
    }

}
